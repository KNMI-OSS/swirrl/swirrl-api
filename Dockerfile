FROM maven:3-amazoncorretto-17

RUN yum upgrade -y && yum update -y java-17-amazon-corretto
RUN yum install wget unzip -y && yum clean all

WORKDIR /usr/src/swirrl-api
ARG NOTEBOOK_SNAPSHOT_CREATEREPO_IMAGE_TAG

COPY pom.xml /usr/src/swirrl-api
RUN mvn --batch-mode dependency:resolve

COPY . /usr/src/swirrl-api
RUN wget https://github.com/swagger-api/swagger-ui/archive/master.zip
RUN unzip master.zip && mv swagger-ui-master swagger-ui
RUN mvn --batch-mode package -Dmaven.test.skip=true

FROM amazoncorretto:17
ARG NOTEBOOK_SNAPSHOT_CREATEREPO_IMAGE_TAG

RUN yum upgrade -y && yum install shadow-utils -y && yum update -y java-17-amazon-corretto && yum clean all

## Need to set the numerical uid explicitly for the K8s init container of swirrl-api
RUN useradd -d /home/swirrl -m -s /bin/bash -u 1000 swirrl
COPY --from=0 /usr/src/swirrl-api/target/swirrl-api-1.0.0.jar /usr/src/swirrl-api/
WORKDIR /usr/src/swirrl-api

USER swirrl
ENV HOME=/home/swirrl
ENV NOTEBOOK_SNAPSHOT_CREATEREPO_IMAGE_TAG $NOTEBOOK_SNAPSHOT_CREATEREPO_IMAGE_TAG
ENTRYPOINT ["java", "-jar", "swirrl-api-1.0.0.jar"]

HEALTHCHECK --interval=30s --timeout=30s --start-period=15s --retries=3 CMD [ "/bin/sh", "-c", "curl --fail http://localhost:8080/actuator/health || grep 'UP' || exit 1" ]
