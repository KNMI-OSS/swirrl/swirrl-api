# SWIRRL API
## Introduction

SWIRRL is a webservice API that allows Science Gateways to easily
integrate data analysis and visualisation tools in their websites and
re-purpose them to their users [8].

The API deals, on behalf of the clients, with the underlying
complexity of requesting and organising resources in a target cloud
platform hosting the Kubernetes [1] container-orchestration system. By
combining storage and tools, such as Jupyter Notebooks and the
Enlighten visualisation portal [7], which are implemented as
containerised services, the API creates dedicated working sessions
on-demand.

Thanks to the API’s workflow execution endpoint, which spawns job
executing CWL [6] workflows for data staging and batch processing,
SWIRRL sessions can be populated with raw-data of interest collected
from external data providers.  Staged data is considered immutable. In
the occurrence of updates, older versions are preserved.

The system is designed to offer customisation and
reproducibility. Notebooks can be further customised with additional
or updated libraries, and the provenance of such changes is
automatically captured. The recording of provenance is performed for
each method of the API’s affecting the session.

SWIRRL generates and stores provenance data thanks to two dedicated
components: the PROV-Template Catalog [3] and the Neo4j database
[5]. Both are integrated as microservices and exposed through the API.

The ability of collecting provenance will be complemented with the
possibility of creating snapshots of a running Notebook or Enlighten
[7] environment.  Snapshots of Jupyter [2] services will be also
stored to Git as Binder repositories. We show a comprehensive overview
of the system in the Figure below.

![](swirrl.png)

## References
   
 [1] [Kubernetes](https://kubernetes.io/ )
  
 [2]  [Jupyter Notebook](https://jupyter.org/) 

 [3]  [Provenance Template Catalog](https://github.com/EnvriPlus-PROV/ProvTemplateCatalog) 
  
 [4] [Neo4J semantic plugin, neosemantics](https://github.com/neo4j-labs/neosemantics) 
  
 [5] [Binder](https://binderhub.readthedocs.io) 
 
 [6] [CWL](https://www.commonwl.org/)
 
 [7] T. Langeland, O. D. Lampe, G. Fonnes, K. Atakan, J. Michalek, X. Wang,C. Rønnevik, T. Utheim, and K. Tellefsen. Epos-norway portal. In Geophysical Research Abstracts, volume 21, 2019.

 [8] Alessandro Spinuso, Mats Veldhuizen, Daniele Bailo, Valerio Vinciarelli, Tor Langeland; SWIRRL. Managing Provenance-aware and Reproducible Workspaces. Data Intelligence 2022; 4 (2): 243–258. doi: https://doi.org/10.1162/dint_a_00129


## Required Java version

The application requires Java >= 11.

## Development

### Integrated Development Environments

JetBrains IntelliJ is the IDE that has been used to develop this
application, but it should work with any capable IDE. The `pom.xml` is
usually enough to import this into the IDE as a project. It will be
helpful to install the Google Cloud Code plugin for development and
debugging.

It is necessary that the `notebook-images` configmap is deployed and
that it contains the notebook image tags of the notebook versions you
wish to deploy. This needs to be done from a clone of
[jupyterswirrlui](https://gitlab.com/KNMI-OSS/swirrl/jupyterswirrlui) 
by running `skaffold run` or `skaffold dev` in the jupyterswirrlui
working directory.

When the supporting services such as kafka, neo4j have been setup
as described in [swirrl-ansible](https://gitlab.com/KNMI-OSS/swirrl/swirrl-ansible)
you can build the docker images and deploy them to a local minikube
cluster using `skaffold dev` if you are going to do development or
`skaffold run` if you wish to just deploy and are not interested in
the logs or debugging. Using the Google Cloud Code plugin, this can
be done from within the IDE. This also enables the container to run
in the debugger so you can set break points, examine the content
of variables and step through the code as it executes.

### Swagger-UI

Using Swagger UI (available in the API at `/`) or if you have them
other REST API tools, it's easy to quickly test added functionality.

### Generator

The [Spring-Boot](https://spring.io/projects/spring-boot) stubs for
this API were generated from an [OpenAPI v3.0
spec](https://github.com/OAI/OpenAPI-Specification/blob/master/versions/3.0.1.md)
using [openapi-generator](https://openapi-generator.tech/).

The specs are available in `OpenApiSpecs/openapi-v<x>.<y>.yaml`.

#### Changing the specs

After making changes to the specs, the API `interface` can be updated
using `openapi-generator/generate.sh`. This will run the generator for
each version in `OpenApiSpecs`. It is probably required to make small
changes to the generated files as the return types required in this
project are more generic. Run a `git diff` to see the changes and
revert any change you didn't make yourself.

#### Adding API Versions

In order to add an API version, add a
`OpenApiSpecs/openapi-v<x>.<y>.yaml` and run the generator. Then add
the spec to the list of `urls` in
`src/main/resources/static/index.html`. This will make the
documentation available in swagger-ui.

#### Interfaces

The generator is configured, using
`openapi-generator/openapi-generator-options.json` and
`.openapi-generator-ignore`, to only generate the interface files,
e.g. `NotebookApi.java` and `WorkflowApi.java`. These do provide
default implementations (java 9 feature) that return a `501 - Not
Implemented` when called via HTTP.

#### Interface implementation

The interfaces are implemented in the `*Controller` classes in the
`org.openapitools.v1_0.api` package.

The workflow end points send messages via kafka to the
[swirrl-workflow](https://gitlab.com/KNMI-OSS/swirrl/swirrl-workflow/)
service.


## Deployment

### CI

The .gitlab-ci.yml automatically builds a Docker image for the API
using the Dockerfile which can be used to deploy the application.

### Deploying to kubernetes

If you have a working kubernetes cluster running and a configuration
setup on your work station that allows you to deploy to this cluster,
then deploying the swirrl-api service to kubernetes is as simple as
running the ansible playbook in
[swirrl-ansible](https://gitlab.com/KNMI-OSS/swirrl/swirrl-ansible)
which automates all the steps below provided you set the correct
secrets in the environment as described in [swirrl-ansible](https://gitlab.com/KNMI-OSS/swirrl/swirrl-ansible)

### Kubernetes access when running as docker container.

When running the Docker image outside a k8s cluster, a kubeconfig needs to be
available. This can be provided in a number of ways as documented in
kubernetes-client-java in `ClientBuilder.java` and reproduced here:

```
* Creates a builder which is pre-configured in the following way *
* <ul>
* <li>If $KUBECONFIG is defined, use that config file.
* <li>If $HOME/.kube/config can be found, use that.
* <li>If the in-cluster service account can be found, assume in cluster config.
* <li>Default to localhost:8080 as a last resort.
* </ul>
```

Certain end points will not work and a copy of swirrl-api is required
inside the cluster to provide the `/provenance` end points for storing
the provenance in neo4j.

#### Setting ClusterRoleBindings for swirrl-api

If the swirrl-api gives an exception immediately when creating a ConfigMap after
a `create notebook` API call, it is likely that the user the swirrl-api deployment
doesn't have the permissions to create ConfigMaps, PVCs, Pods, etc. Use the commands
below to create the necessary ClusterRoleBindings from the `clusterrolebinding.yaml`
template provided which also creates a service account:
```shell script
kubectl -n swirrl apply -f clusterrolebinding.yaml
```

#### Secrets for pulling docker images from gitlab

If the images for jobs or pods need to be pulled from a private (gitlab) repository,
then you need to create a deployment key for the registry/repository in gitlab first.
The steps to generate a deployment key are:
* In your gitlab repository's side bar select Settings->Repository
* Expand the 'Deploy Tokens' section
* Supply a meaningful name and check the `read_registry` checkbox.
* Copy paste the username and password in the command below:

Run the following kubectl command:
``` Shell
kubectl create -n swirrl secret docker-registry swirrl-api-deploy-notebook \
        --docker-server=registry.gitlab.com \
        --docker-username=gitlab+deploy-token-<some number> \
        --docker-password=<the password> \
        --docker-email='youremail@example.com'
```
Then the secret will be known inside the kubernetes cluster under the name 'swirrl-api-deploy-notebook',
for workflows the name is e.g. `swirrl-api-deploy-download-wf` for the download workflow.

In src/main/resources/k8s-specs/workflow/job.yaml the 'name' field of the 'imagePullSecrets' needs
to be the same as the name of the secret if this is a token to pull the job's docker image. In
src/main/resources/k8s-specs/notebook/deployment.yaml there is (now) also a secret name
specified. Change this according to the secret you created to pull the notebook docker images.

## Provenance
To trace provenance, SWIRRL uses a [PROV-Template expansion
service](https://gitlab.com/KNMI-OSS/swirrl/provtemplatecatalog/) To
generate the bindings which are used to expand the PROV-Templates,
binding beans are generated on the fly based on the template which is
being expanded.

The templates are stored in the prov-templates directory. Maven will
generate the binding beans during the generate-sources phase by
calling the ProvToolbox.
[ProvTemplateCatalog](https://gitlab.com/KNMI-OSS/swirrl/provtemplatecatalog/)
has an import function to import the templates from this
directory. This is generally run as a kubernetes job from the
import-templates docker image.

SWIRRL logo is kindly provided by [https://all-free-download.com](https://all-free-download.com)


# Resource limits and requests

The following table contains, what seems to be, workable resource limits. Requests could be set to around
half of the limits for more efficient resource usage, but for stability it could be required to set requests
the same as limits. If you set requests at around half the limits it means that any node will be able to host
twice as many pods, but if the total memory usage exceeds the available memory of the node some pods may be
evicted and restarted. CPU overcommitment doesn't seem to have any of these issues and this just means processes
will run slower overall once the node is out of CPU.

NOTE: The limits noted here might not be the ones currently assigned to the pods, as we have tried many different
configurations.

| pod                   | CPU   | MEM   | NOTES                                                                                                       |
|-----------------------|-------|-------|-------------------------------------------------------------------------------------------------------------|
| prov-template-mongodb | 200m  | 400M  | Seems to be tested quite well as minimal requirements, with higher workload could be too little             |
| prov-template-web     | 100m  | 400M  | Seems to be tested quite well as minimal requirements, with higher workload could be too little             |
| prov-template-api     | 100m  | 400M  | Seems to be tested quite well as minimal requirements, with higher workload could be too little             |
| swirrl-api            | 1000m | 1600M  | Seems to be tested quite well as minimal requirements, with higher workload could be too little             |
| swirrl-cron           | 100m  | 200M  | Seems to be tested quite well as minimal requirements, with higher workload could be too little             |
| nginx-custom-error    | 100m  | 200M  | Seems to be tested quite well as minimal requirements, with higher workload could be too little             |
| kafka                 | 400m  | 1200M | Seems to be tested quite well as minimal requirements, with higher workload could be too little             |
| zookeeper             | 350m  | 400M  | Seems to be tested quite well as minimal requirements, with higher workload could be too little             |
| neo4j                 | 1000m | 3000M | Seems to be tested quite well as minimal requirements, with higher workload could be too little             |
| swirrl-workflow       | 500m  | 500M  | Seems to be tested quite well as minimal requirements, with higher workload could be too little             |
| jupyter-notebook      | 1000m | 2700M | Anything above 2.5G memory seems to make everything work, not sure if notebook is actually usable like this |
| enlighten             | 1000m | 2700M | Currently has same limits as jupyter                                                                        |
| adaguc                | ?     | ?     | Seemed cpu heavy                                                                                            |
| create-snapshot       | 200m  | 400M  | Tested a little bit, could require more cpu and memory                                                      |
| delete-stage          | 100m  | 200M  | Tested a little bit, could require more cpu                                                                 |
| download-workflow     | 200m  | 400M  | Tested a little bit, could require more cpu                                                                 |
| rookwps-workflow      | 200m  | 400M  | Tested a little bit, could require more cpu                                                                 |
| opendap-workflow      | 200m  | 1000M | Tested a little bit, could require more cpu                                                                 |
| kdp-workflow          | 750m  | 2000M | Not really tested, could maybe use less cpu and memory                                                      |
| wow-workflow          | 500m  | 2000M | Tested a little bit, could maybe use less cpu                                                               |
