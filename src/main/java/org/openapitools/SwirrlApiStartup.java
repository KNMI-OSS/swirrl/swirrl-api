package org.openapitools;

import org.openapitools.v1_0.provenance.TemplateCatalog;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.util.TimeZone;

@Component
public class SwirrlApiStartup implements ApplicationListener<ContextRefreshedEvent> {

    private Logger logger = LoggerFactory.getLogger(SwirrlApiStartup.class);

    @Autowired
    static SwirrlApiConfiguration swirrlApiConfiguration;
    @Autowired
    static TemplateCatalog templateCatalog;

    public SwirrlApiStartup(SwirrlApiConfiguration swirrlApiConfiguration, TemplateCatalog templateCatalog) {
        this.swirrlApiConfiguration = swirrlApiConfiguration;
        this.templateCatalog = templateCatalog;
    }
    @Override
    public void onApplicationEvent(ContextRefreshedEvent applicationEvent) {
        logger.debug("Setting timezone to "+swirrlApiConfiguration.getTimezone());
        TimeZone.setDefault(TimeZone.getTimeZone(swirrlApiConfiguration.getTimezone()));
    }
}
