package org.openapitools.v1_0.api;

import com.google.common.io.ByteStreams;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import io.kubernetes.client.Exec;
import io.kubernetes.client.PodLogs;
import io.kubernetes.client.custom.V1Patch;
import io.kubernetes.client.openapi.*;
import io.kubernetes.client.openapi.apis.AppsV1Api;
import io.kubernetes.client.openapi.apis.BatchV1Api;
import io.kubernetes.client.openapi.apis.CoreV1Api;
import io.kubernetes.client.openapi.apis.NetworkingV1Api;
import io.kubernetes.client.openapi.models.*;
import io.kubernetes.client.util.Config;
import io.kubernetes.client.util.Watch;
import io.kubernetes.client.util.Yaml;
import org.json.JSONObject;
import org.openapitools.v1_0.model.Job;
import org.openapitools.v1_0.model.kubernetes.ServiceDeploymentFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.StreamUtils;

import javax.annotation.Nullable;
import javax.json.*;
import java.io.*;
import java.net.SocketTimeoutException;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.*;

import org.apache.commons.lang3.ArrayUtils;

import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

@Component
@Scope(value="prototype")
public class KubernetesService {

    private AppsV1Api appsV1Api;
    private CoreV1Api coreV1Api;
    private NetworkingV1Api networkingV1Api;
    private BatchV1Api batchV1Api;

    private Logger logger = LoggerFactory.getLogger(KubernetesService.class);

    // Constants.

    private final String SERVICE_INGRESS_NAME = "swirrl-services";
    private final String SERVICE_INGRESS_REWRITE_NAME = "swirrl-services-rewrite";
    private final String PRETTY = "";
    private final V1DeleteOptions EMPTY_V1_DELETE_OPTIONS = new V1DeleteOptions();
    private final Integer GRACE_PERIOD_SECONDS = 15;
    private final String PROPAGATION_POLICY = "Foreground";

    @Value("${notebook.service-ingress.filename}")
    private String serviceIngressDefaultFilename;

    @Value("${notebook.service-ingress-rewrite.filename}")
    private String serviceIngressRewriteFilename;

    @Value("${swirrl-api.namespace}")
    private String NAMESPACE;

    public KubernetesService() {

        ApiClient client = null;
        try {
            client = Config.defaultClient();
        } catch (IOException e) {
            throw new RuntimeException("Unable to initialize kubernetes client.");
        }
        // TODO: Determine a correct timeout here!
        client.setConnectTimeout(100);
        Configuration.setDefaultApiClient(client);

        this.coreV1Api = new CoreV1Api();
        this.appsV1Api = new AppsV1Api();
        this.networkingV1Api = new NetworkingV1Api();
        this.batchV1Api = new BatchV1Api();
    }

    public KubernetesService(
            @Nullable AppsV1Api appsV1Api,
            @Nullable CoreV1Api coreV1Api,
            @Nullable NetworkingV1Api networkingV1Api,
            @Nullable BatchV1Api batchV1Api) throws IOException {

        this();

        if (appsV1Api != null) {
            this.appsV1Api = appsV1Api;
        }
        if (coreV1Api != null) {
            this.coreV1Api = coreV1Api;
        }
        if (networkingV1Api != null) {
            this.networkingV1Api = networkingV1Api;
        }
        if (batchV1Api != null) {
            this.batchV1Api = batchV1Api;
        }
    }

    public ResponseEntity<?> createIngress(Map<String, List<V1HTTPIngressPath>> ingressMap) {
        if (null == ingressMap) {
            return EposUtils.handleError(
                    "Input ingress equals null.",
                    HttpStatus.INTERNAL_SERVER_ERROR.value(), null);
        }

        if (ingressMap.containsKey("default")) {
            String ingressName = SERVICE_INGRESS_NAME;
            for (V1HTTPIngressPath ingressPath : ingressMap.get("default")) {
                ResponseEntity<?> createIngressPathResponse = createIngressPath(ingressPath, ingressName);
                if (createIngressPathResponse.getStatusCode() != HttpStatus.OK) return createIngressPathResponse;
            }
        }
        if (ingressMap.containsKey("rewrite")) {
            String ingressName = SERVICE_INGRESS_REWRITE_NAME;
            for (V1HTTPIngressPath ingressPath : ingressMap.get("rewrite")) {
                ResponseEntity<?> createIngressPathResponse = createIngressPath(ingressPath, ingressName);
                if (createIngressPathResponse.getStatusCode() != HttpStatus.OK) return createIngressPathResponse;
            }
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    private ResponseEntity<?> createIngressPath(V1HTTPIngressPath ingressPath, String ingressName) {
        // Convert ingressPatch to JsonObject
        Gson gson = new Gson();
        String ingressPatchJson = gson.toJson(ingressPath);
        JsonReader jsonReader = Json.createReader(new StringReader(ingressPatchJson));
        JsonObject ingressPatchObject = jsonReader.readObject();
        jsonReader.close();

        // Create patch request object
        JsonPatchBuilder builder = Json.createPatchBuilder();
        JsonPatch patchBodyObject = builder.add("/spec/rules/0/http/paths/0", ingressPatchObject).build();
        V1Patch patchBody = new V1Patch(patchBodyObject.toJsonArray().toString());

        try {
            networkingV1Api.patchNamespacedIngress(ingressName, NAMESPACE, patchBody, PRETTY, null, null, null);
        } catch (final ApiException e) {
            HttpStatus httpStatus = HttpStatus.resolve(e.getCode());
            if (httpStatus == HttpStatus.NOT_FOUND) {
                logger.info("Need to create services-ingress-" + ingressName + " from configmap.");
                String serviceIngressFilename = serviceIngressDefaultFilename;
                if (ingressName.equals(SERVICE_INGRESS_REWRITE_NAME)) serviceIngressFilename = serviceIngressRewriteFilename;
                try {
                    V1Ingress serviceIngress = Yaml.loadAs(
                            new InputStreamReader(new FileInputStream(serviceIngressFilename)),
                            V1Ingress.class);
                    try {
                        networkingV1Api.createNamespacedIngress(NAMESPACE, serviceIngress, PRETTY, null, null);
                        networkingV1Api.patchNamespacedIngress(ingressName, NAMESPACE, patchBody, PRETTY, null, null, null);
                        return new ResponseEntity<>(HttpStatus.OK);
                    } catch (final ApiException createSvcIngressException) {
                        return EposUtils.handleError("Impossible to create ingress due to exception.",
                                createSvcIngressException.getCode(), createSvcIngressException);
                    }
                } catch (final IOException e1) {
                    e1.printStackTrace();
                }
            }
            return EposUtils.handleError("Impossible to create ingress due to exception.", e.getCode(), e);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    public ResponseEntity<?> createDeployment(V1Pod deployment) {
        if (null == deployment) {
            return EposUtils.handleError(
                    "Input deployment equals null.",
                    HttpStatus.INTERNAL_SERVER_ERROR.value(), null);
        }

        try {
            coreV1Api.createNamespacedPod(NAMESPACE, deployment, PRETTY, null,null);
        } catch (final ApiException e) {
            logger.debug(Yaml.dump(deployment));
            return EposUtils.handleError("Impossible to create deployment due to exception.", e.getCode(), e);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    // TODO: Maybe make this method something like waitOnRunningPodForLabel?
    // TODO: Currently the IOException of closing the socket before anything of use has been read is being eaten, probably not throw it in an async method!
    public V1Pod waitOnContainerReady(String label, Integer timeoutMinutes) throws IOException {
        if (null == timeoutMinutes) timeoutMinutes = 1;
        Integer minutesPassed = 0;
        Watch<V1Pod> watch =
                null;

        try {
            while (minutesPassed < timeoutMinutes) {
                try {
                    watch = Watch.createWatch(
                            Configuration.getDefaultApiClient().setReadTimeout(timeoutMinutes * 60000),
                            coreV1Api.listNamespacedPodCall(
                                    NAMESPACE,
                                    PRETTY,
                                    Boolean.TRUE,
                                    null,
                                    null,
                                    label,
                                    null,
                                    null,
                                    null,
                                    60,
                                    Boolean.TRUE,
                                    null),
                            new TypeToken<Watch.Response<V1Pod>>() {}.getType());
                } catch (ApiException e) {
                    // TODO: Correct error handling here.
                    e.printStackTrace();
                }
                for (Watch.Response<V1Pod> item : watch) {
                    List<V1ContainerStatus> containerStatuses = item.object.getStatus().getContainerStatuses();
                    if (
                            containerStatuses != null &&
                            containerStatuses.get(0).getReady() &&
                            containerStatuses.get(0).getImageID() != null &&
                            containerStatuses.get(0).getState() != null &&
                            containerStatuses.get(0).getState().getRunning() != null &&
                            containerStatuses.get(0).getState().getRunning().getStartedAt() != null
                    ) {
                        logger.debug("Containerstatus is ready");
                        watch.close();
                        return item.object;
                    }
                    if ("Running".equals(item.object.getStatus().getPhase())) {
                        logger.debug("Pod has status running now, waiting for readiness of Jupyter container ");
                    }
                }
                minutesPassed += 1;
            }
        } finally{
            watch.close();
        }
        return null;
        // TODO: Add error logging ...
    }

    public Boolean waitOnContainerTerminated(String label, int timeout) {
        while (timeout > 0 && retrievePodByLabel(label).getStatusCode() != HttpStatus.NOT_FOUND) {
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            timeout--;
        }
        if (timeout == 0) return false;
        return true;
    }

    public ResponseEntity<?> replaceDeployment(V1Deployment deployment) {
        if (null == deployment) {
            return EposUtils.handleError(
                    "Input  deployment equals null.",
                    HttpStatus.INTERNAL_SERVER_ERROR.value(), null);
        }

        final String appName = deployment.getMetadata().getName();

        try {
            appsV1Api.replaceNamespacedDeployment(appName, NAMESPACE, deployment, PRETTY, null, null);
        } catch (final ApiException e) {
            return EposUtils.handleError("Impossible to replace deployment due to exception.", e.getCode(), e);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    public ResponseEntity<?> createService(V1Service service) {
        if (null == service) {
            return EposUtils.handleError(
                    "Input service equals null.",
                    HttpStatus.INTERNAL_SERVER_ERROR.value(), null);
        }

        try {
            coreV1Api.createNamespacedService(NAMESPACE, service, PRETTY, null, null);
        } catch (final ApiException e) {
            return EposUtils.handleError("Impossible to create service due to exception.", e.getCode(), e);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    public ResponseEntity<?> createPersistentVolumeClaimWhenNotExists(V1PersistentVolumeClaim workingdirPvc) {
        if (null == workingdirPvc) {
            return EposUtils.handleError(
                    "Input PVC equals null.",
                    HttpStatus.INTERNAL_SERVER_ERROR.value(), null);
        }

        try {
            coreV1Api.createNamespacedPersistentVolumeClaim(NAMESPACE, workingdirPvc, PRETTY, null, null);
        } catch (final ApiException e) {

            // A conflict indicates that the PVC already exists, which is not an error situation for this method.
            if (HttpStatus.CONFLICT.value() == e.getCode()) {
                return new ResponseEntity<>(HttpStatus.OK);
            }

            return EposUtils.handleError("Impossible to create PVC due to exception.", e.getCode(), e);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    public ResponseEntity<?> createPersistentVolumeWhenNotExists(V1PersistentVolume workingdirPv) {
        if (null == workingdirPv) {
            return new ResponseEntity<>(HttpStatus.OK);
        }

        try {
            coreV1Api.createPersistentVolume(workingdirPv, PRETTY, null, null);
        } catch (final ApiException e) {

            // A conflict indicates that the PV already exists, which is not an error situation for this method.
            if (HttpStatus.CONFLICT.value() == e.getCode()) {
                return new ResponseEntity<>(HttpStatus.OK);
            }

            return EposUtils.handleError("Impossible to create PV due to exception.", e.getCode(), e);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    /**
     * creates a Job from a job spec.
     * @param job
     * @return the uuid of the created job. It should be possible to use this
     * UUID to obtain the job's status with the following code:
     * try { this.coreV1Api.listNamespacedPod(NAMESPACE, PRETTY, "continue", "controller-uid", true, NAMESPACE, 1, NAMESPACE, 60, false);	} catch (final ApiException e) {}
     */
    public ResponseEntity<?> createJob(V1Job job) {
        if (null == job) {
            return EposUtils.handleError(
                    "Input job equals null.",
                    HttpStatus.INTERNAL_SERVER_ERROR.value(), null);
        }

        Job apiResponse = new Job();
        try {
            V1Job result = batchV1Api.createNamespacedJob(NAMESPACE, job, PRETTY, null, null);
            apiResponse.setId(result.getMetadata().getUid());
            apiResponse.setLogs(new byte[0]);
            apiResponse.setStartTime(OffsetDateTime.now().toString());
            apiResponse.setStatus("");
        } catch (final ApiException e) {
            return EposUtils.handleError("Impossible to create job due to exception.", e.getCode(), e);
        }
        return new ResponseEntity<Job>(apiResponse, HttpStatus.OK);
    }

    /**
     * Retrieves the most recent pod associated with the job identified by runId.
     * @param jobId
     * @return A ResponseEntity which contains a V1Pod object when HttpStatus is OK, otherwise an error.
     */
    public ResponseEntity<?> retrieveLatestJobPod(String label, String jobId) {
        logger.debug("Retrieve status of job "+label+"="+jobId);
        ResponseEntity<?> result = this.retrievePodsByLabel(label+"="+jobId);
        if (HttpStatus.OK != result.getStatusCode()) {
            return EposUtils.handleError("Failed to retrieve status of job ["+jobId+"].",
                    result.getStatusCodeValue(), null);
        }
        V1PodList podlist = (V1PodList)result.getBody();
        V1Pod jobPod = null;
        OffsetDateTime mostRecentPodCreationTime = OffsetDateTime.of(1900, 1, 1, 0, 0, 0, 0, ZoneOffset.of("+00:00"));
        for (V1Pod pod : podlist.getItems()) {
            OffsetDateTime creationTimestamp = pod.getMetadata().getCreationTimestamp();
            if (creationTimestamp.compareTo(mostRecentPodCreationTime) > 0) {
                jobPod = pod;
                logger.debug("Pod: "+pod.getMetadata().toString());
                mostRecentPodCreationTime = creationTimestamp;
            }
        }
        if (null != jobPod) return new ResponseEntity<>(jobPod, HttpStatus.OK);

        /* We should never get here. Only if retrievePodsByLabel returned OK, but somehow
           returned an empty list anyway would this happen.
         */
        return EposUtils.handleError("Inconsistency detected.", HttpStatus.INTERNAL_SERVER_ERROR.value(),
                null);
    }

    public ResponseEntity<?> getJobStatus(String label, String jobId) {
        ResponseEntity<?> podResult = this.retrieveLatestJobPod(label, jobId);
        if (HttpStatus.OK != podResult.getStatusCode()) {
            return podResult;
        }

        V1Pod pod = (V1Pod) podResult.getBody();
        if (null == pod) return new ResponseEntity<>(new Job(), HttpStatus.NOT_FOUND);

        String status = pod.getStatus().getPhase();
        logger.debug("Job's pod UID: " + pod.getMetadata().getUid() + "\nStatus: " + pod.getStatus().getPhase());
        logger.debug("Job's container status: " + pod.getStatus().getContainerStatuses());
        byte[] logs = getPodLogs(pod);
        V1ContainerStateRunning runningState = null;
        if (pod.getStatus().getContainerStatuses() != null)
            runningState = pod.getStatus().getContainerStatuses().get(0).getState().getRunning();

        Job response = new Job();
        response.setId(jobId);
        response.setStartTime(pod.getMetadata().getCreationTimestamp().toString());
        response.setLogs(logs);
        response.setStatus(status);
        if (runningState != null) {
            response.setTimeLeftMinutes(String.valueOf(30 - Duration.between(runningState.getStartedAt(), OffsetDateTime.now(ZoneOffset.UTC)).toMinutes()));
        }

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    public byte[] getPodLogs(V1Pod pod) {
        PodLogs logs = new PodLogs();
        String status = pod.getStatus().getPhase();
        switch (status) {
            case "Succeeded" : case "Failed" :
                try {
                    // NOTE: This catches stdout as well as stderr into a single buffer. The k8s API has no support to
                    // discriminate between stdout and stderr as far as I can determine.
                    return logs.streamNamespacedPodLog(pod).readAllBytes();
                } catch (final ApiException e) {
                    logger.warn("Cannot read logs from pod " + pod.getMetadata().getName() + "\nException:\n" + e);
                } catch (final IOException e) {
                    logger.warn("Cannot read logs from pod " + pod.getMetadata().getName() + "\nException:\n" + e);
                }
                return "Error retrieving logs.".getBytes();
            case "Pending":
                return "No logs available while job is pending.".getBytes();
            case "Running":
                return "No logs available while job is running.".getBytes();
            default:
                return "No logs available.".getBytes();
        }
    }
    
    public ResponseEntity<?> retrieveDeployment(String deployName) {
        V1Deployment deployment;
        try {
            deployment = appsV1Api.readNamespacedDeployment(deployName, NAMESPACE, PRETTY);
        } catch (final ApiException e) {
            return EposUtils.handleError("Impossible to retrieve deployment due to exception.", e.getCode(), e);
        }
        return new ResponseEntity<>(deployment, HttpStatus.OK);
    }

    public ResponseEntity<?> createOrReplaceConfigMap(V1ConfigMap inputsConfigMap) {
        if (null == inputsConfigMap) {
            return EposUtils.handleError(
                    "Input configmap equals null.",
                    HttpStatus.INTERNAL_SERVER_ERROR.value(), null);
        }

        V1ConfigMap inputsConfigMapResult = null;
        try {
            inputsConfigMapResult = coreV1Api.replaceNamespacedConfigMap(
                    inputsConfigMap.getMetadata().getName(), NAMESPACE, inputsConfigMap, PRETTY, null, null);
        } catch (final ApiException e) {

            // Not found is an expected response when the config map does not exist yet.
            if (HttpStatus.NOT_FOUND.value() != e.getCode()) {
                return EposUtils.handleError("Impossible to replace configmap due to exception.", e.getCode(), e);
            }
        }
        if (inputsConfigMapResult == null) {
            try {
                inputsConfigMapResult = coreV1Api.createNamespacedConfigMap(NAMESPACE, inputsConfigMap, PRETTY, null, null);
            } catch (final ApiException e) {
                return EposUtils.handleError("Impossible to create configmap due to exception.", e.getCode(), e);
            }
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }


    public Map<String, String> getKConfigMap(String configMapName) {
        if (null == configMapName) {
            logger.info("Configmap should have a name");
            throw new RuntimeException();
        }
        try {
            logger.info(NAMESPACE);
            V1ConfigMap configMap = coreV1Api.readNamespacedConfigMap(configMapName, NAMESPACE, PRETTY);
            logger.info(configMap.toString());
            return configMap.getData();
        } catch (final ApiException e) {
            logger.error("Could not read configmap <"+configMapName+">. Reason:"+e.getCode()+" "+e.getResponseBody());
            return new HashMap<>();
        }
    }

    public ResponseEntity<?> getConfigMap(String configMapName) {
        if (null == configMapName) {
            return EposUtils.handleError("Invalid configmap given",
                    HttpStatus.BAD_REQUEST.value(), null);
        }
        try {
            V1ConfigMap configMap = coreV1Api.readNamespacedConfigMap(configMapName, NAMESPACE, PRETTY);
            return new ResponseEntity<>(configMap, HttpStatus.OK);
        } catch (final ApiException e) {
            return new ResponseEntity<>(null, HttpStatus.valueOf(e.getCode()));
        }
    }

    public ResponseEntity<?> createEmptyConfigmap(String configMapName) {
        if (null == configMapName) {
            return EposUtils.handleError("Invalid configmap given",
                    HttpStatus.BAD_REQUEST.value(), null);
        }
        try {
            V1ConfigMap emptyConfigMap = new V1ConfigMapBuilder()
                    .editOrNewMetadata().withName(configMapName).endMetadata().build();

            V1ConfigMap configMap = coreV1Api.createNamespacedConfigMap(NAMESPACE, emptyConfigMap, PRETTY, null, null);
            return new ResponseEntity<>(configMap, HttpStatus.OK);
        } catch (final ApiException e) {
            return EposUtils.handleError("Could not create configMap <"+configMapName+">", e.getCode(), e);
        }
    }

    /**
     * Retrieves a pod by the specified label.
     * Label should be written as:
     * <pre>
     *     label-name=label-value
     * </pre>
     *
     * For example:
     * <pre>
     *     notebook-id=1234567890
     * </pre>
     * @param label
     * @return A ResponseEntity which contains an V1Pod when HttpStatus is OK, otherwise an Error
     */
    public ResponseEntity<?> retrievePodByLabel(String label) {
        V1PodList v1PodList;

        try {
            v1PodList = coreV1Api.listNamespacedPod(NAMESPACE,
                    PRETTY,
                    Boolean.FALSE,
                    null,
                    null,
                    label,
                    null,
                    null,
                    null,
                    0,
                    null);
        } catch (ApiException e) {
            return EposUtils.handleError("Impossible to retrieve pod due to exception.", e.getCode(), e);
        }

        if (v1PodList.getItems().isEmpty()) {
            return EposUtils.handleError(
                    String.format("No pods were found for label %s", label),
                    HttpStatus.NOT_FOUND.value(),
                    null);
        }

        if (v1PodList.getItems().size() > 1) {
            return EposUtils.handleError(
                    String.format("%d pods found for label %s, expected 1.", v1PodList.getItems().size(), label),
                    HttpStatus.CONFLICT.value(),
                    null);
        }

        return new ResponseEntity<V1Pod>(v1PodList.getItems().get(0), HttpStatus.OK);
    }

    /**
     * Retrieves a list of pods having the specified label.
     * Label should be written as:
     * <pre>
     *     label-name=label-value
     * </pre>
     *
     * For example:
     * <pre>
     *     controller-uid=1234567890
     * </pre>
     * @param label
     * @return A ResponseEntity which contains an V1PodList when HttpStatus is OK and the list is not empty,
     * otherwise an Error
     */
    public ResponseEntity<?> retrievePodsByLabel(String label) {
        V1PodList v1PodList;

        try {
            v1PodList = coreV1Api.listNamespacedPod(NAMESPACE,
                    PRETTY,
                    Boolean.FALSE,
                    null,
                    null,
                    label,
                    null,
                    null,
                    null,
                    0,
                    null);
        } catch (ApiException e) {
            return EposUtils.handleError("Impossible to retrieve pod list due to exception.", e.getCode(), e);
        }

        if (v1PodList.getItems().isEmpty()) {
            return EposUtils.handleError(
                    String.format("No pods were found for label %s", label),
                    HttpStatus.NOT_FOUND.value(),
                    null);
        }

        return new ResponseEntity<V1PodList>(v1PodList, HttpStatus.OK);
    }

    public ResponseEntity<?> retrieveCronJobsByLabel(String label) {
        V1CronJobList cronJobList;
        try {
            cronJobList = batchV1Api.listNamespacedCronJob(NAMESPACE, PRETTY, Boolean.FALSE, null,
                    "metadata.name="+label,
                    null, null, null, null, 0, null);
        } catch (ApiException e) {
            return EposUtils.handleError("Impossible to list cron jobs due to exception.", e.getCode(), e);
        }
        if (cronJobList.getItems().isEmpty()) {
            return EposUtils.handleError(String.format("No pods were found for label %s", label),
                    HttpStatus.NOT_FOUND.value(), null);
        }
        return new ResponseEntity<V1CronJobList>(cronJobList, HttpStatus.OK);
    }

    public ResponseEntity<?> retrievePVCsByName(String name) {

        V1PersistentVolumeClaim pvc;
        try {
            pvc = coreV1Api.readNamespacedPersistentVolumeClaim(name, NAMESPACE, PRETTY);
        } catch (final ApiException e) {
            return EposUtils.handleError("Impossible to retrieve PVC due to exception.", e.getCode(), e);
        }
        return new ResponseEntity<>(pvc, HttpStatus.OK);
    }

    /**
     * Retrieves a list of PVCs having the specified label.
     * Label should be written as:
     * <pre>
     *     label-name=label-value
     * </pre>
     *
     * For example:
     * <pre>
     *     controller-uid=1234567890
     * </pre>
     * @param label
     * @return A ResponseEntity which contains an V1PersistentVolumeClaimList when HttpStatus is OK and the list is not empty,
     * otherwise an Error
     */
    public ResponseEntity<?> retrievePVCsByLabel(String label) {
        V1PersistentVolumeClaimList v1PVCList;
        try {
            v1PVCList = coreV1Api.listNamespacedPersistentVolumeClaim(NAMESPACE,
                    PRETTY,
                    Boolean.FALSE,
                    null,
                    null,
                    label,
                    null,
                    null,
                    null,
                    0,
                    null);
        } catch (ApiException e) {
            return EposUtils.handleError("Impossible to retrieve PVC list due to exception.", e.getCode(), e);
        }

        if (v1PVCList.getItems().isEmpty()) {
            return EposUtils.handleError(
                    String.format("No PVCs were found for label %s", label),
                    HttpStatus.NOT_FOUND.value(),
                    null);
        }

        return new ResponseEntity<V1PersistentVolumeClaimList>(v1PVCList, HttpStatus.OK);
    }


    /**
     * Method to execute short commands on a running pod.
     *
     * Currently we block the thread, which is not feasible if we are going to execute longer requests on K8s.
     * For now this implementation suffices, since we are only using this method to query things, not to start
     * long running processes.
     * @param pod
     * @param commands
     * @return A ResponseEntity which contains a String with the result when HttpStatus is OK, otherwise an Error
     *
     */
    public ResponseEntity<?> executeShellCommandOnPod(V1Pod pod, String[] commands) {

        String result = null;
        int exitvalue = -255;

        // The following code is based on
        // https://github.com/kubernetes-client/java/blob/master/examples/src/main/java/io/kubernetes/client/examples/ExecExample.java
        // Using coverV1API.connectPostNamespacedPodExec always resulted in a BAD REQUEST, upgrade request required, it seems
        // like that method is not supported yet by the kubernetes-java-client.
        Exec exec = new Exec();

        try {
            // We need to make a copy of the passed commands because exec is encoding the passed commands and thus
            // altering them.
            String[] redirection = new String[] {"2>&1"};
            String[] redirectedCommand = ArrayUtils.addAll(commands, redirection);
            String[] shellCommand = new String[] {"sh", "-c"};
            String[] localCommand = ArrayUtils.addAll(shellCommand, String.join(" ",  redirectedCommand ));
            logger.trace("executeShellCommandOnPod: localCommands: "+Arrays.toString(localCommand));

            final Process proc =
                    exec.exec(
                            pod,
                            localCommand,
                            false,
                            false
                    );

            Callable<String> readExecResult = () -> StreamUtils.copyToString(proc.getInputStream(), StandardCharsets.UTF_8);
            result = readExecResult.call();
            exitvalue = proc.exitValue();

        } catch (ApiException e) {
            return EposUtils.handleError(
                    String.format("Impossible to execute commands %s on pod %s", commands, pod.getMetadata().getName()),
                    e.getCode(), e);
        } catch (Exception e) {
            return EposUtils.handleError(
                    String.format("Impossible to execute commands %s on pod %s", commands, pod.getMetadata().getName()),
                    HttpStatus.INTERNAL_SERVER_ERROR.value(), e);
        }

        if (exitvalue == 0) {
            return new ResponseEntity<String>(result, HttpStatus.OK);
        } else {
            return new ResponseEntity<String>(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<?> writeStringToFileOnPod(V1Pod pod, String buffer, String filename) {
        return writeStringToFileOnContainer(pod, null, buffer, filename);
    }

    public ResponseEntity<?> writeStringToFileOnContainer(V1Pod pod, String container, String buffer, String filename) {
        Exec exec = new Exec();
        String [] command = new String [] {"sh", "-c", "cat > "+filename};

        try {
            final Process proc = exec.exec(pod, command, container, true, false);
            Thread in = new Thread(() -> {
                try {
                    int offset = 0;
                    int bufsize = 1024;
                    InputStream input = new ByteArrayInputStream(buffer.getBytes(), offset, bufsize);
                    while (offset < buffer.length()) {
                        ByteStreams.copy(input, proc.getOutputStream());
                        offset = offset + bufsize;
                        input = new ByteArrayInputStream(buffer.getBytes(), offset, bufsize);
                    }
                    logger.debug("Closing output stream");
                    proc.getOutputStream().close();
                    input.close();
                } catch (SocketTimeoutException ex) {
                    logger.debug("Timeout Exception in writer:"+ex);
                    ex.printStackTrace();
                } catch (IOException ex) {
                    logger.debug("Exception in writer:"+ex);
                    ex.printStackTrace();
                }
            });
            Thread.UncaughtExceptionHandler eh = new Thread.UncaughtExceptionHandler() {
                @Override
                public void uncaughtException(Thread t, Throwable e) {
                    logger.error("Uncaught exception in container writer thread.");
                }
            };
            in.start();
            boolean rc = proc.waitFor(2, TimeUnit.SECONDS);
            logger.debug("Return code for " + command.toString() + ": " + rc);
            in.join();
            proc.destroy();
            if (proc.exitValue() == 0)
                return new ResponseEntity<String>("Buffer written to "+filename+" on pod.", HttpStatus.OK);
        } catch (ApiException e) {
            return EposUtils.handleError(
                    String.format("Impossible to copy buffer [%s] into file %s to pod %s", buffer, filename, pod.getMetadata().getName()),
                    e.getCode(), e);
        } catch (Exception e) {
            return EposUtils.handleError(
                    String.format("Impossible to copy buffer [%s] into file %s to pod %s", buffer, filename, pod.getMetadata().getName()),
                    HttpStatus.INTERNAL_SERVER_ERROR.value(), e);
        }
        return new ResponseEntity<String>("Buffer NOT written to "+filename+" on pod.", HttpStatus.INTERNAL_SERVER_ERROR);
    }

    String getNotebookToken(V1Pod pod) {
        ResponseEntity<?> jpserverResponse = this.executeShellCommandOnPod(
                pod, new String [] {"cat", "/jupyterlab/.local/share/jupyter/runtime/jpserver-*.json"});
        try {
            String jpserverString = jpserverResponse.getBody().toString();
            JSONObject jpserverJson = new JSONObject(jpserverString);
            return jpserverJson.getString("token");
        } catch (Exception e) {
            return EposUtils.handleError(String.format("Could not obtain token from pod %s", pod.getMetadata().getName()),
                    HttpStatus.INTERNAL_SERVER_ERROR.value(), e).getBody().toString();
        }
    }

    public ResponseEntity<?> deleteConfigMapByName(String nameConfigmap) {

        try {
            coreV1Api.deleteNamespacedConfigMap(nameConfigmap, NAMESPACE, PRETTY, null, GRACE_PERIOD_SECONDS, null, PROPAGATION_POLICY, EMPTY_V1_DELETE_OPTIONS);
        } catch (final ApiException e) {
            return EposUtils.handleError("Impossible to delete configmap due to exception.", e.getCode(), e);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    public ResponseEntity<?> deleteServiceIngress(String serviceType, String serviceId) {
        String appName = ServiceDeploymentFactory.determineAppName(serviceId, serviceType);
        ResponseEntity<?> deleteResponse = deleteServiceIngressPath(appName, SERVICE_INGRESS_NAME);
        if (deleteResponse.getStatusCode() != HttpStatus.OK) return deleteResponse;
        deleteResponse = deleteServiceIngressPath(appName, SERVICE_INGRESS_REWRITE_NAME);
        if (deleteResponse.getStatusCode() != HttpStatus.OK) return deleteResponse;
        return new ResponseEntity<>(HttpStatus.OK);
    }


    private ResponseEntity<?> deleteServiceIngressPath(String appName, String ingressName) {
        try {
            V1Ingress serviceIngress = null;
            try {
                serviceIngress = networkingV1Api.readNamespacedIngress(ingressName, NAMESPACE, PRETTY);
            } catch (ApiException e) {
                if (HttpStatus.NOT_FOUND.value() == e.getCode()) {
                    logger.warn("Ingress of type " + ingressName + " not found");
                    return new ResponseEntity<>(HttpStatus.OK);
                } else {
                    logger.error(e.getResponseBody());
                    return EposUtils.handleError("Impossible to delete ingresses service path due to exception.", e.getCode(), e);
                }
            }
            List<Integer> deleteRules = new ArrayList<>();
            List<V1HTTPIngressPath> pathList = serviceIngress.getSpec().getRules().get(0).getHttp().getPaths();
            for (int i = 0; i < pathList.size(); i++) {
                V1HTTPIngressPath path = pathList.get(i);
                if (path.getBackend().getService().getName().equals(appName)) {
                    deleteRules.add(i);
                }
            }
            Collections.reverse(deleteRules);
            if (deleteRules.size() > 0) {
                for (Integer ruleIndex : deleteRules) {
                    logger.info("Deleting ingress path at location " + ruleIndex);
                    JsonPatchBuilder builder = Json.createPatchBuilder();
                    JsonPatch patchBodyObject = builder.remove("/spec/rules/0/http/paths/" + ruleIndex).build();
                    V1Patch patchBody = new V1Patch(patchBodyObject.toJsonArray().toString());
                    networkingV1Api.patchNamespacedIngress(ingressName, NAMESPACE, patchBody, PRETTY, null, null, null);
                }
            } else {
                logger.warn("No ingress path found");
            }
        } catch (ApiException e) {
            logger.error(e.getResponseBody());
            return EposUtils.handleError("Impossible to delete ingresses service path due to exception.", e.getCode(), e);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    public ResponseEntity<?> deletePvcsByLabel(String label) {

        try {
            coreV1Api.deleteCollectionNamespacedPersistentVolumeClaim(NAMESPACE, PRETTY, null, null, null, GRACE_PERIOD_SECONDS, label, 0, Boolean.TRUE, PROPAGATION_POLICY, null, null, null, EMPTY_V1_DELETE_OPTIONS);
        } catch (ApiException e) {
            return EposUtils.handleError("Impossible to delete persistentvolumeclaims by label due to exception.", e.getCode(), e);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    public ResponseEntity<?> deleteDeploymentsByLabel(String label) {

        try {
            coreV1Api.deleteCollectionNamespacedPod(NAMESPACE, PRETTY, null, null, null, GRACE_PERIOD_SECONDS, label, 0, Boolean.TRUE, PROPAGATION_POLICY, null, null, null, EMPTY_V1_DELETE_OPTIONS);
        } catch (final ApiException e) {
            return EposUtils.handleError("Impossible to delete deployments by label due to exception.", e.getCode(), e);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    public ResponseEntity<?> deleteServiceByName(String nameService) {
        try {
            coreV1Api.deleteNamespacedService(nameService, NAMESPACE, PRETTY, null, GRACE_PERIOD_SECONDS, null, PROPAGATION_POLICY, EMPTY_V1_DELETE_OPTIONS);
        } catch (ApiException e) {
            if (HttpStatus.NOT_FOUND.value() != e.getCode()) {
                return EposUtils.handleError("Impossible to delete service by name due to exception.", e.getCode(), e);
            }
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    public ResponseEntity<?> deleteConfigMapsByLabel(String label) {

        try {
            coreV1Api.deleteCollectionNamespacedConfigMap(NAMESPACE, PRETTY, null, null, null, GRACE_PERIOD_SECONDS, label, 0, Boolean.TRUE, PROPAGATION_POLICY, null, null, null, EMPTY_V1_DELETE_OPTIONS);
        } catch (final ApiException e) {
            return EposUtils.handleError("Impossible to delete configmaps by label due to exception.", e.getCode(), e);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    public ResponseEntity<?> deleteJobsByLabel(String label) {

        try {
            batchV1Api.deleteCollectionNamespacedJob(NAMESPACE, PRETTY, null, null, null, GRACE_PERIOD_SECONDS, label, 0, Boolean.TRUE, PROPAGATION_POLICY, null, null, null, EMPTY_V1_DELETE_OPTIONS);
            batchV1Api.deleteCollectionNamespacedCronJob(NAMESPACE, PRETTY, null, null, null, GRACE_PERIOD_SECONDS, label, 0, Boolean.TRUE, PROPAGATION_POLICY, null, null, null, EMPTY_V1_DELETE_OPTIONS);
        } catch (final ApiException e) {
            logger.warn("Impossible to delete jobs by label due to exception. "+e.getCode());
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    public ResponseEntity<?> deletePodsByLabel(String label) {

        try {
        	coreV1Api.deleteCollectionNamespacedPod(NAMESPACE, PRETTY, null, null, null, GRACE_PERIOD_SECONDS, label, 0, Boolean.TRUE, PROPAGATION_POLICY, null, null, null, EMPTY_V1_DELETE_OPTIONS);
        } catch (final ApiException e) {
            return EposUtils.handleError("Impossible to delete pods by label due to exception.", e.getCode(), e);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }


    /**
     * Method to retrieve jobs that are in phase Running, and have a specific label.
     *
     * Currently jobs are found by iterating over workflow pods that have a specific label.
     *
     * TODO: Instead workflow jobs should be iterated to find the active jobs.
     * A job is done computing in any of these cases:
     * * the job completed successfully.
     *       Field status:completionTime is available then
     * * the job failed and stopped retrying.
     *       Field status:conditions:reason = BackoffLimitExceeded    AND
     *       Field status:conditions:status = "True"                  AND
     *       Field status:conditions:type   =  Failed
     * In all other cases (for instance; during job initialisation), it is safe to assume a job is still active
     *
     *
     * @param label
     * @return A ResponseEntity which contains a ArrayList<String> with the jobnames when HttpStatus is OK, otherwise an Error
     */
    public ResponseEntity<?> retrieveActiveJobsByLabel(String label) {
        logger.debug("retrieveActiveJobsByLabel({})", label);
    	ArrayList<String> activeJobs = new ArrayList<String>();
        V1PodList v1PodList;

        try {
        	v1PodList = coreV1Api.listNamespacedPod(NAMESPACE, PRETTY, Boolean.TRUE, null, null, label, null, null, null,0, null);
        } catch (ApiException e) {
            return EposUtils.handleError("Impossible to retrieve pods due to exception.", e.getCode(), e);
        }

        List<V1Pod> v1Pods = v1PodList.getItems();
        for (V1Pod v1Pod : v1Pods)
        {
            String status = v1Pod.getStatus().getPhase();
            if(status.equals("Running") | status.equals("Pending"))
    		{
                List<V1OwnerReference> ownerReferenceList = v1Pod.getMetadata().getOwnerReferences();
                if (ownerReferenceList != null) {
                    for (V1OwnerReference ownerReference : ownerReferenceList) {
                        if (ownerReference.getKind().equals("Job")) {
                            activeJobs.add(ownerReference.getName());
                            logger.debug("Active job: {}", ownerReference.getName());
                        }
                    }
                }
            }
        }

        return new ResponseEntity<ArrayList<String>>(activeJobs, HttpStatus.OK);
    }

    public Boolean isPodSessionIdEqualTo(V1Pod pod, String sessionId) {
        return pod.getMetadata().getLabels().get(ServiceDeploymentFactory.SESSION_ID_LABEL).equals(sessionId);
    }

}
