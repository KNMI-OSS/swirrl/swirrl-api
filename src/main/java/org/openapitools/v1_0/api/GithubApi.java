/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech) (3.1.2).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
package org.openapitools.v1_0.api;

import org.openapitools.v1_0.model.Error;
import org.openapitools.v1_0.model.GithubAccessToken;
import org.openapitools.v1_0.model.GithubClientId;
import org.openapitools.v1_0.model.GithubRequestToken;
import io.swagger.annotations.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import javax.validation.constraints.*;
import java.util.List;
import java.util.Map;
import java.util.Optional;
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2023-09-15T12:23:18.056Z[GMT]")

@Validated
@Api(value = "github", description = "the github API")
public interface GithubApi {

    default Optional<NativeWebRequest> getRequest() {
        return Optional.empty();
    }

    @ApiOperation(value = "Retrieves an access token from github.", nickname = "getGithubAccessToken", notes = "After a user has authorized swirrl api in github this endpoint can be used to generate an access token.", response = GithubAccessToken.class, tags={ "github", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "Success", response = GithubAccessToken.class),
        @ApiResponse(code = 405, message = "Invalid input", response = Error.class),
        @ApiResponse(code = 500, message = "Internal server error", response = Error.class) })
    @RequestMapping(value = "/github/access_token",
        produces = { "application/json" }, 
        consumes = { "application/json", "application/xml" },
        method = RequestMethod.POST)
    default ResponseEntity<?> getGithubAccessToken(@ApiParam(value = "request github access token"  )  @Valid @RequestBody GithubRequestToken githubRequestToken) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "{  \"access_token\" : \"xxxxxxxxxxxxxxxxxxxx\",  \"scope\" : \"repo\",  \"token_type\" : \"bearer\"}");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }


    @ApiOperation(value = "Returns the client id of the swirrl github application", nickname = "getGithubClientId", notes = "Returns the client id of the swirrl github application", response = GithubClientId.class, tags={ "github", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "Success", response = GithubClientId.class),
        @ApiResponse(code = 500, message = "Internal server error", response = Error.class) })
    @RequestMapping(value = "/github/client_id",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    default ResponseEntity<?> getGithubClientId() {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    ApiUtil.setExampleResponse(request, "application/json", "{  \"clientId\" : \"xxxxxxxxxxxxxxxxxxxx\"}");
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }

}
