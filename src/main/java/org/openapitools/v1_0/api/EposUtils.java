package org.openapitools.v1_0.api;

import io.kubernetes.client.openapi.models.V1ConfigMapVolumeSource;
import io.kubernetes.client.openapi.models.V1Pod;
import io.kubernetes.client.openapi.models.V1Volume;
import org.openapitools.v1_0.model.Error;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;

import java.util.List;


public class EposUtils {

    private static Logger logger = LoggerFactory.getLogger(EposUtils.class);
    
    public static V1ConfigMapVolumeSource findConfigmapNameForVolume(V1Pod currentDeployment, String nameVolume) {
        List<V1Volume> currentVolumeList = currentDeployment.getSpec().getVolumes();
        for (int i = 0; i < currentVolumeList.size(); i++)
        {
            if(currentVolumeList.get(i).getName().equals(nameVolume) && (currentVolumeList.get(i).getConfigMap()!=null))
            {
                return currentVolumeList.get(i).getConfigMap();
            }
        }
        logger.error("Could not find configmap for volume with name {}", nameVolume);
        return null;
    }

    /**
     * Logs the message as an error and builds up a ResponseEntity containing the error.
     * If the httpCode is not recognized, a HttpStatus of 500 (Internal server error) is returned.
     * @param message
     * @param httpCode
     * @param exception Optional exception, used for logging the stacktrace.
     * @return
     */
    public static ResponseEntity<Error> handleError(String message, int httpCode, @Nullable Exception exception) {

        logger.error(message, exception);

        HttpStatus httpStatus = HttpStatus.resolve(httpCode);
        if (httpStatus == null) {
            httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        }

        Error error = new Error();
        error.setDescription(message);
        error.setCode(httpStatus.toString());

        return new ResponseEntity<>(error, httpStatus);
    }
}


