package org.openapitools.v1_0.api;

import io.kubernetes.client.openapi.models.*;
import org.openapitools.v1_0.model.*;
import org.openapitools.v1_0.model.Error;
import org.openapitools.v1_0.model.kubernetes.*;
import org.openapitools.v1_0.provenance.ProvenanceApi;
import org.json.JSONObject;
import org.json.JSONArray;
import org.openapitools.v1_0.provenance.ProvenanceService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.json.JSONException;
import org.yaml.snakeyaml.Yaml;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.Charset;
import java.security.SecureRandom;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.io.StringWriter;
import java.util.*;
import java.util.concurrent.TimeUnit;

@Service
public class NotebookService {

    @Autowired
    NotebookDeploymentFactory notebookDeploymentFactory;

    @Autowired
    ObjectFactory<KubernetesService> kubernetesServiceFactory;

    @Autowired
    CreateSnapshotJobFactory createSnapshotJobFactory;

    @Autowired
    private ProvenanceApi provenanceApi;

    @Autowired
    ProvenanceService provenanceService;

    @Value("${notebook.url.origin}")
    private String NOTEBOOK_HOST;

    @Value("${notebook.login.enabled}")
    private String NB_LOGIN_ENABLED;

    private static final String SERVICE_TYPE = "jupyter";

    private Logger logger = LoggerFactory.getLogger(NotebookService.class);

    public static final String[] COMMAND_GET_INSTALLED_LIBS = { "swirrl-conda", "list" };
    public static final String[] COMMAND_PIP_INSTALL = { "pip", "install" };
    public static final String[] COMMAND_PIP_UNINSTALL = {"pip", "uninstall", "--yes"};
    public static final String[] COMMAND_CONDA_INSTALL = { "swirrl-conda", "install", "--yes" };
    public static final String[] COMMAND_CONDA_UNINSTALL = { "swirrl-conda", "uninstall", "--yes" };
    public static final String[] COMMAND_CONDA_RESTORE_ENV = { "swirrl-conda", "env",  "update", "-n", "base", "--file", "/home/jovyan/work/.environment.yml", "--prune" };
    public static final String PREVIOUS_JUPYTER_DOC = "/home/jovyan/work/.jupyter-id-";


    @Value("${notebook.restore.enabled}")
    Boolean restoreEnabled;
    @Value("${notebook.docker.image.type}")
    String notebookImageType;
    @Value("${notebook.docker.image.tag}")
    private String dockerImageTag;
    @Value("${deployment.pv.dynamicprovisioning}")
    private boolean dynamicProvisioning;

    public ResponseEntity<?> createNotebook(Notebook notebook) {
        UserInfo userInfo = new UserInfo("mock_user_id", "swirrl-api", "mock_authmode_id", "mock_group_id");
        return this.createNotebook(notebook, userInfo);
    }

    public ResponseEntity<?> createNotebook(Notebook notebook, UserInfo userInfo) {
        String startTime = OffsetDateTime.now().toString();
        ResponseEntity<?> response =  this.createNotebook(notebook, determineNotebookDockerImage(), userInfo);
        provenanceService.traceProvenanceCreateSession(notebook, startTime, this, userInfo);
        return response;
    }

    private String determineNotebookDockerImage() {
        try {
            Properties properties = new Properties();
            properties.load(new FileInputStream("/home/swirrl/notebook-images.properties"));
            String property = String.format("notebook.docker.image.%s", notebookImageType);
            return properties.getProperty(property);
        } catch (FileNotFoundException e) {
            logger.error("/home/swirrl/notebook-images.properties not found.");
            return dockerImageTag;
        } catch (IOException e) {
            logger.error("exception trying to read notebook-images.properties.");
            e.printStackTrace();
            return dockerImageTag;
        }
    }

    public ResponseEntity<?> checkActiveJobsWithLabel(KubernetesService kubernetesService, String label) {
        ResponseEntity<?> activeResponse;
        activeResponse = kubernetesService.retrieveActiveJobsByLabel(label);
        if (HttpStatus.OK != activeResponse.getStatusCode()) {
            return activeResponse;
        }

        @SuppressWarnings("unchecked")
        ArrayList<String> activeJobs = (ArrayList<String>) activeResponse.getBody();
        if (activeJobs.size() > 0) {
            String error = String.format("Impossible to run operation on notebook due to active job(s): %s", activeJobs.toString());
            logger.error(error);

            return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    public ResponseEntity<?> storeSnapshot(String serviceId, Snapshot snapshot, String authHeader, UserInfo userInfo) {

        // Use for git commit:
        String commitMessage = snapshot.getUserMessage();

        if(commitMessage == null || commitMessage.equals("")) {
            Error error = new Error().code("400").description("Message must not be empty");
            return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
        }

        // Check that serviceId is valid:
        KubernetesService kubernetesService = kubernetesServiceFactory.getObject();

        final String label = NotebookDeploymentFactory.determineServiceLabel(serviceId);

        ResponseEntity<?> servicePodResult = kubernetesService.retrievePodByLabel(label);
        if (HttpStatus.OK != servicePodResult.getStatusCode()) {
            return servicePodResult;
        }

        // Check that there is no active job on service:
        // Currently, no jobs run on services but we decided to keep this change as that might change in the future.
        ResponseEntity<?> statusOnService = checkActiveJobsWithLabel(kubernetesService, label);
        if (HttpStatus.OK != statusOnService.getStatusCode()) {
            Error error = new Error().code("500").description(statusOnService.getBody().toString());

            logger.error(error.getDescription());
            return new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        // Check that there is no active job on session:
        // First, get the session id from the notebook in question:
        try{
            V1Pod pod = (V1Pod) servicePodResult.getBody();

            Map podLabels = pod.getMetadata().getLabels();

            if (! pod.getMetadata().getName().startsWith("jupyter")) {
                return EposUtils.handleError("Service with id "+serviceId+" is not a Jupyter notebook: snapshot end point not supported.",
                        HttpStatus.BAD_REQUEST.value(), null);
            }
            String imageTag = null;
            for (V1Container container : pod.getSpec().getContainers()) {
                if (container.getName().equals("jupyter")) {
                    imageTag = container.getImage();
                }
            }
            if (null == imageTag) {
                Error error = new Error().code("500").description("Could not determine base image for snapshot.");
                return new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
            }

            String sessionId = (String)podLabels.get(ServiceDeploymentFactory.SESSION_ID_LABEL);
            logger.debug("SessionId: "+sessionId);
            String poolId = (String)podLabels.get(ServiceDeploymentFactory.POOL_ID_LABEL);
            logger.debug("PoolId: "+poolId);

            // Second, check that there is no workflow job running with that id:
            String workflowPrefix = WorkflowDeploymentFactory.WORKFLOW_SESSION_ID_LABEL;
            logger.debug("workflow prefix: "+workflowPrefix);

            if(sessionId != null){
                ResponseEntity<?> statusOnSession = checkActiveJobsWithLabel(kubernetesService, String.format("%s=%s", workflowPrefix, sessionId));
                if (HttpStatus.OK != statusOnSession.getStatusCode()) {
                    Error error = new Error().code("500").description(statusOnSession.getBody().toString());

                    logger.error(error.getDescription());
                    return new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
                }
            }

            String accessToken = null;
            if (null != authHeader) {
                accessToken = authHeader.substring("Bearer ".length());
            }

            CreateSnapshotJob job = createSnapshotJobFactory.makeCreateSnapshotJob(sessionId, poolId, serviceId, snapshot.getUserMessage(), imageTag, accessToken);
            ResponseEntity<?> jobResponse = kubernetesService.createJob(job.getJob());
            if (HttpStatus.OK != jobResponse.getStatusCode()) {
                Error error = new Error().code("500").description("Could not start job to create snapshot.");
                logger.error(error.getDescription());
                return new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
            }
            Job jobResult = (Job)jobResponse.getBody();
            SnapshotCompletionHandler snapshotJobCompletionHandler = new SnapshotCompletionHandler(
                    jobResult, serviceId, poolId, sessionId, snapshot, userInfo);
            Thread t = new Thread(snapshotJobCompletionHandler);
            t.start();

            SnapshotURL url = new SnapshotURL();
            url.setSnapshotURL("Pending completion");
            return new ResponseEntity<>(url, HttpStatus.OK);
        } catch(Exception e){
            logger.error("Exception in storeSnapshot:", e);
        }
        Error error = new Error().code("500").description("Could not obtain URL for snapshot.");
        logger.error(error.getDescription());
        return new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    private class SnapshotCompletionHandler implements Runnable {

        Job job;
        String serviceId;
        String poolId;
        String sessionId;
        Snapshot snapshot;
        UserInfo userInfo;

        public SnapshotCompletionHandler(Job job, String serviceId, String poolId, String sessionId, Snapshot snapshot, UserInfo userInfo) {
            this.job = job;
            this.serviceId = serviceId;
            this.poolId = poolId;
            this.sessionId = sessionId;
            this.snapshot = snapshot;
            this.userInfo = userInfo;
        }

        public void run() {
            KubernetesService kubernetesService = kubernetesServiceFactory.getObject();
            try {
                int timeout = 300;
                logger.debug("Going to wait for job " + this.job.getId() + " to end.");
                while (timeout > 0) {
                    ResponseEntity<?> response = kubernetesService.getJobStatus("controller-uid", this.job.getId());
                    if (response.getStatusCode() != HttpStatus.OK) {
                        TimeUnit.SECONDS.sleep(1);
                        timeout--;
                        continue;
                    }
                    Job jobStatus = (Job) response.getBody();
                    String status = jobStatus.getStatus();
                    if (status.compareTo("Succeeded") == 0 || status.compareTo("Failed") == 0) {
                        jobStatus.setEndTime(OffsetDateTime.now().toString());
                        logger.debug("Job " + this.job.getId() + " ended.");
                        SnapshotURL url = new SnapshotURL();
                        url.setSnapshotURL(new String(jobStatus.getLogs(), Charset.forName("UTF-8")).trim());
                        logger.debug("Returning url: " + url);
                        if (status.compareTo("Succeeded") == 0) {
                            provenanceService.traceProvenanceSnapshot(this.serviceId, this.poolId, sessionId, url, snapshot, jobStatus, null, userInfo);
//                            return new ResponseEntity<>(url, HttpStatus.OK);
                        } else if (status.compareTo("Failed") == 0) {
                            provenanceService.traceProvenanceSnapshot(serviceId, poolId, sessionId, null, snapshot, jobStatus,
                                    url.getSnapshotURL(), userInfo);
//                            return new ResponseEntity<>(url, HttpStatus.INTERNAL_SERVER_ERROR);
                        }
                        break;
                    }
                    TimeUnit.SECONDS.sleep(1);
                    timeout--;
                }
            } catch (InterruptedException exception) {
                // TODO: Handle this exception neatly.
                logger.error("Exception: ", exception);
            }
        }
    }

    private ResponseEntity<?> restoreNotebook(Notebook notebook, String activityId, String prevJupyterId, String notebookDockerImageTag, UserInfo userInfo) {
        String startTime = OffsetDateTime.now().toString();
        ResponseEntity<?> response =  this.createNotebook(notebook, notebookDockerImageTag, userInfo);
        provenanceService.traceProvenanceRestoreSession(notebook, activityId, startTime, prevJupyterId, this, userInfo);
        return response;
    }

    private ResponseEntity<?> createNotebook(Notebook notebook, String notebookDockerImageTag, UserInfo userInfo) {
        KubernetesService kubernetesService = kubernetesServiceFactory.getObject();
        if (notebook.getId() == null) {
            notebook.setId(UUID.randomUUID().toString());
        }

        String token = "";
        if (!NB_LOGIN_ENABLED.equals("no")) { // Always set a token if not explicitly disabled.
            final SecureRandom random = new SecureRandom();
            final byte bytes[] = new byte[24];
            random.nextBytes(bytes);
            token = NotebookService.bytesToHex(bytes);
        }
        if (null == notebook.getServiceURL())
            notebook.setServiceURL(buildNotebookURL(notebook.getId(), token));
        else if (notebook.getServiceURL().contains("token="))
            token = notebook.getServiceURL().split("=")[1];

        if (null == notebook.getSessionId()) {
            if (null == notebook.getPoolId()) {
                notebook.setSessionId(UUID.randomUUID().toString());
                notebook.setPoolId(UUID.randomUUID().toString());
            } else {
                return EposUtils.handleError("Session ID is mandatory if a pool ID is given.",
                        HttpStatus.BAD_REQUEST.value(), null);
            }

        } else {
            ResponseEntity<?> sessionResponse = kubernetesService.retrievePVCsByLabel(
                    ServiceDeploymentFactory.determineSessionLabel(notebook.getSessionId()));
            if (HttpStatus.OK != sessionResponse.getStatusCode()) {
                return EposUtils.handleError("Session ID " + notebook.getSessionId() + " does not exist.",
                        sessionResponse.getStatusCodeValue(), null);
            }
            if (null == notebook.getPoolId()) {
                notebook.setPoolId(UUID.randomUUID().toString());
            } else {
                ResponseEntity<?> poolResponse = kubernetesService.retrievePVCsByLabel(
                        ServiceDeploymentFactory.determinePoolLabel(notebook.getPoolId()));
                if (HttpStatus.OK != poolResponse.getStatusCode()) {
                    return EposUtils.handleError("Pool ID " + notebook.getPoolId() + " does not exist.",
                            poolResponse.getStatusCodeValue(), null);
                }
                V1PersistentVolumeClaimList pvclist = (V1PersistentVolumeClaimList) poolResponse.getBody();
                String sessionId = pvclist.getItems().get(0).getMetadata().getLabels().get(ServiceDeploymentFactory.SESSION_ID_LABEL);
                if (!sessionId.equals(notebook.getSessionId())) {
                    return EposUtils.handleError("Pool ID " + notebook.getPoolId() + " does not match session ID " + notebook.getSessionId(),
                            HttpStatus.BAD_REQUEST.value(), null);
                }
            }
        }
        NotebookDeployment notebookDeployment = notebookDeploymentFactory.makeNotebookDeployment(notebook, notebookDockerImageTag, userInfo, token);

        ResponseEntity<?> createConfigmapResponse =
                kubernetesService.createOrReplaceConfigMap(notebookDeployment.getConfigmap());
        if (HttpStatus.OK != createConfigmapResponse.getStatusCode()) {
            return createConfigmapResponse;
        }

        if (!this.dynamicProvisioning) {
            ResponseEntity<?> workingdirPvResponse =
                    kubernetesService.createPersistentVolumeWhenNotExists(notebookDeployment.getWorkingdirPv());
            if (HttpStatus.OK != workingdirPvResponse.getStatusCode()) {
                return workingdirPvResponse;
            }

            ResponseEntity<?> datadirPvResponse =
                    kubernetesService.createPersistentVolumeWhenNotExists(notebookDeployment.getDatadirPv());
            if (HttpStatus.OK != datadirPvResponse.getStatusCode()) {
                return datadirPvResponse;
            }
        }

        ResponseEntity<?> createPipWrapperConfigMapResponse =
                kubernetesService.createOrReplaceConfigMap(notebookDeployment.getPipWrapperConfigMap());
        if (HttpStatus.OK != createPipWrapperConfigMapResponse.getStatusCode()) {
            return createPipWrapperConfigMapResponse;
        }

        ResponseEntity<?> createCondaWrapperConfigMapResponse =
                kubernetesService.createOrReplaceConfigMap(notebookDeployment.getCondaWrapperConfigMap());
        if (HttpStatus.OK != createCondaWrapperConfigMapResponse.getStatusCode()) {
            return createCondaWrapperConfigMapResponse;
        }

        ResponseEntity<?> createEnvVarConfigMapResponse =
                kubernetesService.createOrReplaceConfigMap(notebookDeployment.getEnvVarConfigMap());
        if (HttpStatus.OK != createEnvVarConfigMapResponse.getStatusCode()) {
            return createEnvVarConfigMapResponse;
        }

        ResponseEntity<?> workingdirPvcResponse =
                kubernetesService.createPersistentVolumeClaimWhenNotExists(notebookDeployment.getWorkingdirPvc());
        if (HttpStatus.OK != workingdirPvcResponse.getStatusCode()) {
            return workingdirPvcResponse;
        }

        ResponseEntity<?> pvcResponse =
                kubernetesService.createPersistentVolumeClaimWhenNotExists(notebookDeployment.getDatadirPvc());
        if (HttpStatus.OK != pvcResponse.getStatusCode()) {
            return pvcResponse;
        }

        ResponseEntity<?> createDeploymentResponse =
                kubernetesService.createDeployment(notebookDeployment.getDeployment());
        if (HttpStatus.OK != createDeploymentResponse.getStatusCode()) {
            return createDeploymentResponse;
        }

        ResponseEntity<?> createServiceResponse =
                kubernetesService.createService(notebookDeployment.getService());
        if (HttpStatus.OK != createServiceResponse.getStatusCode()) {
            if (HttpStatus.CONFLICT == createServiceResponse.getStatusCode()) {
                // If the service already exists, skip creating the ingress
                // as this means that this was a restore call.
                return new ResponseEntity<Notebook>(notebook, HttpStatus.OK);
            }
            return createServiceResponse;
        }
        ResponseEntity<?> createIngressResponse =
                kubernetesService.createIngress(notebookDeployment.getIngress());
        if (HttpStatus.OK != createIngressResponse.getStatusCode()) {
            return createIngressResponse;
        }

        return new ResponseEntity<Notebook>(notebook, HttpStatus.OK);
    }

    public String determineNotebookStatus(KubernetesService kubernetesService, String notebookId, String sessionId ){

        final String label = NotebookDeploymentFactory.determineServiceLabel(notebookId);

        ResponseEntity<?> servicePodResult = kubernetesService.retrievePodByLabel(label);
        if (HttpStatus.OK != servicePodResult.getStatusCode()) {
            return servicePodResult.toString();
        }

        V1Pod pod = (V1Pod) servicePodResult.getBody();

        try {
            String podStatus = pod.getStatus().getPhase().toString();

            String snapshotJobLabel = createSnapshotJobFactory.CREATE_SNAPSHOT_SERVICE_ID_LABEL;
            ResponseEntity<?> activeJobsResponse;
            activeJobsResponse = kubernetesService.retrieveActiveJobsByLabel(String.format("%s=%s", snapshotJobLabel, notebookId));
            ArrayList<String> snapshotJobs = (ArrayList<String>) activeJobsResponse.getBody();

            String workflowJobLabel = WorkflowDeploymentFactory.WORKFLOW_SESSION_ID_LABEL;
            ResponseEntity<?> activeWorkflowResponse;
            activeWorkflowResponse = kubernetesService.retrieveActiveJobsByLabel(String.format("%s=%s", workflowJobLabel, sessionId));
            ArrayList<String> workflowJobs = (ArrayList<String>) activeWorkflowResponse.getBody();

            // Notebook creating, pod not ready
            if(podStatus.equals("Pending")){
                return "Generating";
            }

            // Notebook pod available but workflow job running
            if(podStatus.equals("Running") && workflowJobs.size() > 0){
                return "Workflow in progress";
            }

            // Notebook pod available but snapshot job running
            if(podStatus.equals("Running") && snapshotJobs.size() > 0){
                return "Snapshot in progress";
            }

            // Notebook pod available, no job running
            if (podStatus.equals("Running") && snapshotJobs.size() == 0 && workflowJobs.size() == 0) {
                return "Active";
            }

            else {
                logger.error(podStatus);
                return "Failed";
            }
        }
        catch(Exception e){
            logger.info(e.toString());
        }
        return "";
    }

    public ResponseEntity<?> getNotebookById(final String notebookId) {

        KubernetesService kubernetesService = kubernetesServiceFactory.getObject();

        ExtendedNotebook notebook = new ExtendedNotebook();
        notebook.setId(notebookId);
        notebook.setServiceURL(buildNotebookURL(notebookId, null));
        notebook.setUserRequestedLibraries(new ArrayList<>());
        notebook.setInstalledLibraries(new ArrayList<>());


        final String serviceLabel = NotebookDeploymentFactory.determineServiceLabel(notebookId);
        // TODO: Refactor this into KubernetesService class or something like that.
        ResponseEntity<?> servicePodResult = kubernetesService.retrievePodByLabel(serviceLabel);
        if (HttpStatus.OK != servicePodResult.getStatusCode()) {
            return servicePodResult;
        }

        V1Pod pod = (V1Pod) servicePodResult.getBody();
        Map podLabels = pod.getMetadata().getLabels();

        String sessionId = (String) podLabels.get(ServiceDeploymentFactory.SESSION_ID_LABEL);
        notebook.setSessionId(sessionId);
        String poolId = (String) podLabels.get(ServiceDeploymentFactory.POOL_ID_LABEL);
        notebook.setPoolId(poolId);

        final String sessionLabel = NotebookDeploymentFactory.determineSessionLabel(sessionId);
        final String poolLabel = NotebookDeploymentFactory.determinePoolLabel(poolId);
        List<Volume> volumes = new ArrayList<>();

        ResponseEntity<?> pvcAllListResponse = kubernetesService.retrievePVCsByLabel(sessionLabel);
        if (pvcAllListResponse != null) {
            V1PersistentVolumeClaimList pvcAllList = (V1PersistentVolumeClaimList) pvcAllListResponse.getBody();

            for (V1PersistentVolumeClaim pvc : pvcAllList.getItems()) {
                if (pvc.getMetadata().getName().startsWith("data-directory")) {
                    Volume volume = new Volume();
                    volume.setName(pvc.getMetadata().getName());
                    volume.setSize(pvc.getSpec().getResources().getRequests().get("storage").getNumber());
                    volumes.add(volume);
                }
            }
        }
        ResponseEntity<?> pvcWorkingListResponse = kubernetesService.retrievePVCsByLabel(poolLabel);
        if (pvcWorkingListResponse != null) {
            V1PersistentVolumeClaimList pvcWorkingList = (V1PersistentVolumeClaimList) pvcWorkingListResponse.getBody();

            for (V1PersistentVolumeClaim pvc : pvcWorkingList.getItems()) {
                Volume volume = new Volume();
                volume.setName(pvc.getMetadata().getName());
                volume.setSize(pvc.getSpec().getResources().getRequests().get("storage").getNumber());
                volumes.add(volume);
            }
        }
        notebook.setVolumes(volumes);


        ResponseEntity<?> getInstalledLibsResult = getInstalledLibs(notebookId);
        if (HttpStatus.NOT_FOUND == getInstalledLibsResult.getStatusCode()){
            // Libraries not yet available (C4I polling):
            String notebookProgressStatus = determineNotebookStatus(kubernetesService, notebookId, sessionId);
            notebook.setNotebookStatus(notebookProgressStatus);
            return new ResponseEntity<>(notebook, HttpStatus.NOT_FOUND);
        }
        if (HttpStatus.OK != getInstalledLibsResult.getStatusCode()){
            // Any other error:
            return getInstalledLibsResult;
        }

        appendNotebookLibraries(notebook.getInstalledLibraries(), (String) getInstalledLibsResult.getBody());

        String notebookProgressStatus = determineNotebookStatus(kubernetesService, notebookId, sessionId);
        notebook.setNotebookStatus(notebookProgressStatus);

        return new ResponseEntity<>(notebook, HttpStatus.OK);
    }

    private ResponseEntity<?> getInstalledLibs(final String notebookId) {
        KubernetesService kubernetesService = kubernetesServiceFactory.getObject();
        // Step 1: Retrieve Pod
        ResponseEntity<?> podResult = kubernetesService.retrievePodByLabel(
                NotebookDeploymentFactory.determineServiceLabel(notebookId));
        if (HttpStatus.OK != podResult.getStatusCode()) {
            return podResult;
        }
        V1Pod pod = (V1Pod) podResult.getBody();

        /*
         We used to have an intermediate step for returning the libs that were installed explicitly on
         request by the user. However, since we no longer keep track in .user_requirements.txt, we
         cannot determine this anymore. Also, provenance doesn't explicitly record the difference, it
         just records installed libs and the pip and (at some point in the future) conda command lines
         so the environment can be reproduced, but we don't parse that any more. To keep the output
         of this end point consistent with the provenance we leave the userRequestedLibraries field
         empty.
         */

        // Step 2: Retrieve all installed libraries
        ResponseEntity<?> getInstalledLibsResult = kubernetesService.executeShellCommandOnPod(pod, COMMAND_GET_INSTALLED_LIBS);
        if (HttpStatus.OK != getInstalledLibsResult.getStatusCode()) {
            logger.warn(getInstalledLibsResult.toString());
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return getInstalledLibsResult;
    }

    /**
     * Adds the pip libraries listed in libraryListToAppend to the currentLibraryList.
     * The libraryListToAppend should be of the following format:
     * <pre>
     *      lib1==version1
     *      lib2==version2
     *      lib3
     *      lib4==version4
     * </pre>
     *
     * Note that it is possible to have no version specified (lib3).
     * @param currentLibraryList The list to which we want to append libraries
     * @param libraryListToAppend The library list which we want to append.
     */
    private void appendNotebookLibraries(List<NotebookLibrary> currentLibraryList, String libraryListToAppend) {
        for (String line : libraryListToAppend.split("\n")) {

            if (line.isEmpty() || line.startsWith("#")) {
                continue;
            }

            String[] splittedLine = line.split(" +");

            NotebookLibrary notebookLibrary = new NotebookLibrary();
            notebookLibrary.setLibname(splittedLine[0]);
            notebookLibrary.setLibversion("unknown"); // Override below if possible.
            notebookLibrary.setSource("unknown"); // Override below if possible.
            if (splittedLine.length > 3) {
                notebookLibrary.setLibversion(splittedLine[1]+"-"+splittedLine[2]);
                notebookLibrary.setSource(splittedLine[3]);
            } else if (splittedLine.length > 2) {
                notebookLibrary.setLibversion(splittedLine[1]+"-"+splittedLine[2]);
            } else if (splittedLine.length > 1) {
                notebookLibrary.setLibversion(splittedLine[1]);
            }

            currentLibraryList.add(notebookLibrary);
        }
    }

    public ResponseEntity<?> updateNotebook(String id, final Notebook notebook) {

        KubernetesService kubernetesService = kubernetesServiceFactory.getObject();

        notebook.setId(id);
        notebook.setServiceURL(buildNotebookURL(id, null));

        logger.trace("updateNotebook(): Fetching Pod with label " + NotebookDeploymentFactory.determineServiceLabel(id));

        ResponseEntity<?> podResult = kubernetesService.retrievePodByLabel(
                NotebookDeploymentFactory.determineServiceLabel(id));
        if (HttpStatus.OK != podResult.getStatusCode()) {
            return podResult;
        }
        V1Pod pod = (V1Pod) podResult.getBody();

        final String label = NotebookDeploymentFactory.determineServiceLabel(id);
        Map podLabels = pod.getMetadata().getLabels();
        String poolId = (String)podLabels.get(ServiceDeploymentFactory.POOL_ID_LABEL);
        notebook.setPoolId(poolId);
        String sessionId = (String)podLabels.get(ServiceDeploymentFactory.SESSION_ID_LABEL);
        notebook.setSessionId(sessionId);

        List<NotebookLibrary> libraries = notebook.getUserRequestedLibraries();

        ResponseEntity<?> pipInstallResult = installWithPip(libraries, pod);
        if (HttpStatus.OK != pipInstallResult.getStatusCode()) {
            ResponseEntity<?> condaInstallResult = installWithConda(libraries, pod);
            if (HttpStatus.OK != condaInstallResult.getStatusCode()) {
                return EposUtils.handleError(
                        (String) pipInstallResult.getBody() + (String) condaInstallResult.getBody(),
                        condaInstallResult.getStatusCodeValue(), null);
            }
        }

        return new ResponseEntity<>(notebook, HttpStatus.OK);
    }

    private ResponseEntity<?> installWithPip(List<NotebookLibrary> libraries, V1Pod pod) {
        KubernetesService kubernetesService = kubernetesServiceFactory.getObject();
        String[] commandWithLibs = buildInstallCommand(COMMAND_PIP_INSTALL, libraries);

        ResponseEntity<?> pipInstallResult = kubernetesService.executeShellCommandOnPod(pod, commandWithLibs);
        logger.info("installWithPip: " + commandWithLibs.toString() + " returned: "
                + pipInstallResult.getBody());
        return pipInstallResult;
    }

    private ResponseEntity<?> installWithConda(List<NotebookLibrary> libraries, V1Pod pod) {
        KubernetesService kubernetesService = kubernetesServiceFactory.getObject();
        String [] commandWithLibs = buildInstallCommand(COMMAND_CONDA_INSTALL, libraries);

        ResponseEntity<?> condaInstallResult = kubernetesService.executeShellCommandOnPod(pod, commandWithLibs);
        logger.info("installWithConda: " + commandWithLibs.toString() + " returned: "
                + condaInstallResult.getBody());
        return condaInstallResult;
    }

    public ResponseEntity<?> restoreNotebookLibs(String id, String activityId, UserInfo userInfo) {
        logger.debug("restoreNotebookLibs("+id+", "+activityId+")");

        if (null == userInfo) {
            userInfo = new UserInfo("mock_user_id", "swirrl-api", "mock_authmode_id", "mock_group_id");
        }

        // First make sure the service ID that was passed is valid.
        KubernetesService kubernetesService = kubernetesServiceFactory.getObject();
        ResponseEntity<?> podResult = kubernetesService.retrievePodByLabel(
                NotebookDeploymentFactory.determineServiceLabel(id));
        if (HttpStatus.OK != podResult.getStatusCode()) {
            return podResult;
        }
        V1Pod pod = (V1Pod) podResult.getBody();

        String token = kubernetesService.getNotebookToken(pod);

        Notebook notebook = new Notebook();
        notebook.setId(id);
        notebook.setServiceURL(buildNotebookURL(id, token));
        if (! pod.getMetadata().getName().startsWith("jupyter")) {
            return EposUtils.handleError("Service with id "+id+" is not a Jupyter notebook: restorelibs end point not supported.",
                    HttpStatus.BAD_REQUEST.value(), null);
        }

        String activityDoc = null;
        String prevJupyterId = null;
        try {
            ResponseEntity<?> activityResponse = provenanceApi.getActivityById(activityId);
            if (activityResponse.getStatusCode() != HttpStatus.OK) {
                return activityResponse;
            }
            activityDoc = activityResponse.getBody().toString();
            JSONObject activityObj = new JSONObject(activityDoc);
            if (! activityObj.getString("swirrl:serviceId").equals(id)) {
                return EposUtils.handleError("Activity "+activityId+" does not belong to service with id "+id,
                        HttpStatus.BAD_REQUEST.value(), null);
            }
            String sessionId = activityObj.getString("swirrl:sessionId");
            String poolId = activityObj.getString("swirrl:poolId");
            prevJupyterId = activityObj.getJSONObject("prov:generated").getString("@id").split(":")[2];
            if (! kubernetesService.isPodSessionIdEqualTo(pod, sessionId)) {
                return EposUtils.handleError("Activity "+activityId+" does not belong to service with id "+id,
                        HttpStatus.BAD_REQUEST.value(), null);
            }
            notebook.setSessionId(sessionId);
            notebook.setPoolId(poolId);
        } catch (JSONException e) {
            logger.error("Exception in restoreNotebookLibs:", e);
        }

        String notebookDockerImageTag = getDockerImageTagByActivity(activityDoc, notebook.getId());
        if (null == notebookDockerImageTag) {
            return EposUtils.handleError("Could not extract docker image tag from activity.",
                    HttpStatus.INTERNAL_SERVER_ERROR.value(), null);
        }

        List<NotebookLibrary> libList = getLibListByActivity(activityDoc);
        if (null == libList) {
            return EposUtils.handleError("Could not extract libraries from activity.",
                    HttpStatus.INTERNAL_SERVER_ERROR.value(), null);
        }
        logger.trace("libListAsString: "+libList.toString());
        createEnvironmentYaml(kubernetesService, pod, libList, "/home/jovyan/work/.environment-"+notebook.getId()+".yml");

        /*
         * We can call the createNotebook method while passing a Notebook object we create setting the
         * id to the current id and filling in the required libraries. Of course, we need to delete the old one first.
         * environment.yml is already stored on the persistent volume, so we use that in start-notebook.sh to
         * initialise the installed libraries also.
         *
         * It will require that the user saves his/her work in the notebook, Jupyter does autosave, so that should
         * be ok. A running workflow is fine. A workflow doesn't use the notebook pod, but runs in its own pod with the
         * volumes from the session mounted.
         */
        // We create a placeholder configmap so that we know that a restore is being executed, this is used in the cron job
        String placeholderConfigmapName = pod.getMetadata().getName() + "-restore-placeholder";
        if (this.restoreEnabled) {
            logger.info("Creating a placeholder configmap");
            logger.debug(String.valueOf(Collections.singletonMap("session-id", notebook.getSessionId())));
            V1ConfigMap configmap = new V1ConfigMap()
                    .kind("ConfigMap")
                    .metadata(new V1ObjectMeta()
                            .name(placeholderConfigmapName)
                            .labels(Collections.singletonMap("session-id", notebook.getSessionId())));
            logger.debug(String.valueOf(configmap));
            kubernetesService.createOrReplaceConfigMap(configmap);
            logger.info("Deleting notebook with id "+notebook.getId());
            this.deleteNotebook(notebook.getId());
            logger.debug("Waiting for pod " + notebook.getId() + " to terminate.");
            if (!kubernetesService.waitOnContainerTerminated(
                    NotebookDeploymentFactory.determineServiceLabel(notebook.getId()), 100)) {
                return EposUtils.handleError("Container "+notebook.getId()+" did not terminate.",
                        HttpStatus.INTERNAL_SERVER_ERROR.value(), null);
            }
        }
        logger.info("Re-create notebook with id "+notebook.getId()+" and image "+notebookDockerImageTag);
        this.restoreNotebook(notebook, activityId, prevJupyterId, notebookDockerImageTag, userInfo);
        // Add the entire liblist in the return value. Do this after calling restoreNotebook, or they will end up
        // in requirements.txt also.
        notebook.setUserRequestedLibraries(libList);
        logger.info("Deleting the placeholder configmap");
        kubernetesService.deleteConfigMapByName(placeholderConfigmapName);
        // Clear the token from the URL before we return it for security.
        notebook.setServiceURL(this.buildNotebookURL(notebook.getId(), null));
        return new ResponseEntity<>(notebook, HttpStatus.OK);
    }

    private ResponseEntity<?> createEnvironmentYaml(KubernetesService kubernetesService, V1Pod pod, List<NotebookLibrary> libList,
                                                    String filename) {
        List<Object> condaLibList = new ArrayList<>();
        List<String> pipLibList = new ArrayList<>();
        Map<String, List<String>> pipLibMap = new HashMap<>();

        for (NotebookLibrary library : libList) {
            if (library.getLibname().equals("swirrlui")) continue;
            if (library.getSource().equals("pypi")) {
                // Strip build nr, it is always "pypi_0"
                pipLibList.add(library.getLibname()+"=="+library.getLibversion().split("-")[0]);
            } else {
                condaLibList.add(library.getLibname()+"="+library.getLibversion().replace("-", "="));
            }
        }
        pipLibMap.put("pip", pipLibList);
        condaLibList.add(pipLibMap);

        Map<String, Object> environmentObject = new HashMap<>();
        environmentObject.put("name", "base");
        environmentObject.put("channels", new String [] {"conda-forge", "defaults"});
        environmentObject.put("dependencies", condaLibList);
        environmentObject.put("prefix", "/opt/conda");
        Yaml yaml = new Yaml();
        StringWriter writer = new StringWriter();
        yaml.dump(environmentObject, writer);

        return kubernetesService.writeStringToFileOnPod(pod, writer.toString(), filename);
    }

    private String getDockerImageTagByActivity(String activityDoc, String notebookId) {
        try {
            String createActivityId = null;

            JSONObject activityJSON = new JSONObject(activityDoc);
            String sessionId = activityJSON.getString("swirrl:sessionId");
            ResponseEntity<?> sessionActivity = provenanceApi.getSessionActivity(sessionId, null, null, null, null, Boolean.TRUE, Boolean.FALSE);
            JSONObject sessionActivityJSON = new JSONObject(sessionActivity.getBody().toString());
            JSONArray activityArray = sessionActivityJSON.getJSONArray("@graph");
            for (int i = 0; i < activityArray.length(); i++) {
                JSONArray activityType = activityArray.getJSONObject(i).getJSONArray("@type");
                for (int j = 0; j < activityType.length(); j++) {
                    if (activityType.getString(j).equals("swirrl:CreateNotebook") && activityArray.getJSONObject(i).has("swirrl:serviceId")) {
                        String serviceId = activityArray.getJSONObject(i).getString("swirrl:serviceId");
                        if (serviceId.equals(notebookId)) {
                            createActivityId = activityArray.getJSONObject(i).getString("@id");
                        }
                    }
                }
            }

            ResponseEntity<?> activityResponse = provenanceApi.getActivityById(createActivityId);
            if (activityResponse.getStatusCode() != HttpStatus.OK) {
                return null;
            }
            JSONObject createActivityDoc = new JSONObject(activityResponse.getBody().toString());
            JSONArray associations = createActivityDoc.getJSONArray("prov:wasAssociatedWith");
            String systemImageRef = null;
            for (int i = 0; i < associations.length(); i++) {
                JSONArray activityType = associations.getJSONObject(i).getJSONObject("prov:plan").getJSONArray("@type");
                for (int j = 0; j < activityType.length(); j++) {
                    if (activityType.getString(j).equals("swirrl:SystemImage")) {
                        systemImageRef = associations.getJSONObject(i).getJSONObject("prov:plan").getString("prov:atLocation");
                    }
                }
            }
            return systemImageRef.replaceAll("(docker|docker-pullable)://","");
        } catch (JSONException e) {
            logger.error("Exception in getDockerImageTagByActivity:", e);
            return null;
        }
    }

    private List<NotebookLibrary> getLibListByActivity(String activityDoc) {

        try {
            JSONObject document = new JSONObject(activityDoc);
            JSONArray hadMember = document.getJSONObject("prov:generated").getJSONArray("prov:hadMember");

            List<NotebookLibrary> libList = new ArrayList<NotebookLibrary>();

            for (int i = 0; i < hadMember.length(); i++) {
                if (!hadMember.getJSONObject(i).has("prov:hadMember")) continue;
                JSONArray members = hadMember.getJSONObject(i).getJSONArray("prov:hadMember");
                for (int j = 0; j < members.length(); j++) {
                    NotebookLibrary nl = new NotebookLibrary();
                    nl.libname(members.getJSONObject(j).getString("swirrl:name"));
                    nl.libversion(members.getJSONObject(j).getString("swirrl:version"));
                    nl.setSource(members.getJSONObject(j).getString("swirrl:installationMode"));
                    libList.add(nl);
                }
            }
            return libList;
        } catch(JSONException e) {
            logger.error("Exception in getLibListByActivity:", e);
            return null;
        }
    }

    private String[] buildInstallCommand(String[] commandStr, List<NotebookLibrary> libraries) {
        List<String> command = new ArrayList<String>();
        for (String cmdel : commandStr) {
            command.add(cmdel);
        }
        for (NotebookLibrary library : libraries) {
            String libversion = library.getLibversion();

            // Only add version in command line if we are installing and the version is known and present.
            if (commandStr[1].equals("install") && null != libversion && !libversion.isEmpty() && !libversion.equals("unknown")) {
                String[] splittedVersionString = libversion.split("-");
                if (splittedVersionString.length > 1)
                    command.add(library.getLibname() + "==" + splittedVersionString[0] + "=" + splittedVersionString[1]);
                else
                    command.add(library.getLibname() + "==" + splittedVersionString[0]);
            } else
                command.add(library.getLibname());
        }
        logger.trace("Converting command "+command.toString()+" to string array");
        String[] commandWithLibs = command.toArray(new String[0]);
        return commandWithLibs;
    }

    /**
     * Composes the URL under which the notebook will be running.
     * @param notebookId
     * @return
     */
    public String buildNotebookURL(String notebookId, String token) {
        if (null != token && !token.equals(""))
            return NOTEBOOK_HOST +
                ServiceDeploymentFactory.buildServicePath(
                        notebookDeploymentFactory.getServiceType(),
                        notebookId) + "?token="+token;
        else
            return NOTEBOOK_HOST +
                    ServiceDeploymentFactory.buildServicePath(
                            notebookDeploymentFactory.getServiceType(),
                            notebookId);
    }

    public ResponseEntity<?> deleteNotebook(String notebookId) {

        KubernetesService kubernetesService = kubernetesServiceFactory.getObject();

        ResponseEntity<?> activeResponse, deleteResponse;
        final String notebookAppName = ServiceDeploymentFactory.determineAppName(
                notebookId, notebookDeploymentFactory.getServiceType());
        final String notebookLabel = NotebookDeploymentFactory.determineServiceLabel(notebookId);

        checkActiveJobsWithLabel(kubernetesService, notebookLabel);

        deleteResponse = kubernetesService.deleteJobsByLabel(notebookLabel);
        if (HttpStatus.OK != deleteResponse.getStatusCode()) {
            return deleteResponse;
        }

        deleteResponse = kubernetesService.deleteConfigMapsByLabel(notebookLabel);
        if (HttpStatus.OK != deleteResponse.getStatusCode()) {
            return deleteResponse;
        }

        deleteResponse = kubernetesService.deleteServiceIngress(SERVICE_TYPE, notebookId);
        if (HttpStatus.OK != deleteResponse.getStatusCode()) {
            return deleteResponse;
        }

        // deleting service by appName instead of by label;
        // because the required function coreV1Api.deleteCollectionNamespacedService does not exist
        deleteResponse = kubernetesService.deleteServiceByName(notebookAppName);
        if (HttpStatus.OK != deleteResponse.getStatusCode()) {
            return deleteResponse;
        }

        deleteResponse = kubernetesService.deletePodsByLabel(notebookLabel);
        if (HttpStatus.OK != deleteResponse.getStatusCode()) {
            return deleteResponse;
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    private static String bytesToHex(byte[] hash) {
        StringBuilder hexString = new StringBuilder(2 * hash.length);
        for (byte h : hash) {
            String hex = Integer.toHexString(0xff & h);
            if (hex.length() == 1)
                hexString.append('0');
            hexString.append(hex);
        }
        return hexString.toString();
    }
}
