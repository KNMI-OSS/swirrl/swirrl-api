package org.openapitools.v1_0.api;

import io.swagger.annotations.ApiParam;
import org.openapitools.v1_0.model.UserInfo;
import org.openapitools.v1_0.model.Workflow;
import org.openapitools.v1_0.model.WorkflowList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.NativeWebRequest;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Iterator;
import java.util.Optional;
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2018-07-25T15:13:08.866Z[GMT]")

@CrossOrigin
@Controller
@RequestMapping("${openapi.ePOSNotebook.base-path:/v1.0}")
public class WorkflowApiController implements WorkflowApi {

    private Logger logger = LoggerFactory.getLogger(WorkflowApiController.class);

    private final NativeWebRequest request;

    @Autowired
    WorkflowService workflowService;

    @org.springframework.beans.factory.annotation.Autowired
    public WorkflowApiController(NativeWebRequest request) {
        this.request = request;
    }

    @Override
    public Optional<NativeWebRequest> getRequest() {
        return Optional.ofNullable(request);
    }


    /**
     * NOTE: if the interface was regenerated by swagger and you get a compiler error
     * about incompatible return type then you need to change ResponseEntity<SomeClass>
     * to ResponseEntity<?> in the interface (WorkflowApi.java) and not in this class.
     */
    @Override
    public ResponseEntity<?> runWorkflow(@PathVariable("workflowName") String workflowName,
                                         @Valid @RequestBody Workflow workflow) {
        UserInfo userInfo = this.getUserInfoFromHeaders();

        String sessionHeader = request.getHeader("sessionID");
        logger.debug("sessionID header: "+sessionHeader);
        String agentName = request.getHeader("agent");
        logger.debug("Agent header: "+agentName);
		return workflowService.runWorkflow(workflowName, workflow, sessionHeader, userInfo,
                agentName != null ? agentName : "SWIRRL-API");
	}

	@Override
    public ResponseEntity<?> getWorkflowStatus(
            @ApiParam(value = "The workflow name",required=true) @PathVariable("workflowName") String workflowName,
            @ApiParam(value = "The run ID",required=true) @PathVariable("jobId") String jobId) {
        return workflowService.getWorkflowStatus(jobId);
    }

    @Override
    public ResponseEntity<?> getWorkflows() {
        return workflowService.getWorkflows();
    }

	@Override
    public ResponseEntity<?> deleteLatestStage(@ApiParam(value = "Id of the session.",required=true) @PathVariable("id") String id) {
        String sessionHeader = request.getHeader("sessionID");
        logger.debug("sessionID header: "+sessionHeader);
        String agentName = request.getHeader("agent");
        UserInfo userInfo = this.getUserInfoFromHeaders();
        return workflowService.deleteLatestStage(id, sessionHeader, userInfo, agentName);
    }

    @Override
    public ResponseEntity<?> deleteCronWorkflow(@ApiParam(value = "The workflow name. This has to be a cron workflow.",required=true) @PathVariable("workflowName") String workflowName,@ApiParam(value = "The session ID.",required=true) @PathVariable("id") String id) {
        String sessionHeader = request.getHeader("sessionID");
        logger.debug("sessionID header: "+sessionHeader);
        String agentName = request.getHeader("agent");
        UserInfo userInfo = this.getUserInfoFromHeaders();
        return workflowService.deleteCronWorkflow(workflowName, id, sessionHeader, userInfo, agentName);
    }

    private UserInfo getUserInfoFromHeaders() {
        UserInfo userInfo = null;
        logger.debug("Received headers:");
        for (Iterator<String> it = request.getHeaderNames(); it.hasNext(); ) {
            String header = it.next();
            logger.debug("Header: "+header+" Value: "+request.getHeader(header).toString());
            if (header.toLowerCase().equals("userinfo")) {
                userInfo = new UserInfo(request.getHeader(header).toString());
            }
        }
        return userInfo;
    }

    @Override
    public ResponseEntity<?> registerWorkflow(@NotNull @ApiParam(value = "The workflow name.", required = true) @Valid @RequestParam(value = "workflowName", required = true) String workflowName,
                                                 @ApiParam(value = "") @Valid @RequestParam(value = "imageTag", required = false) String imageTag) {
        return workflowService.registerWorkflow(workflowName, imageTag);
    }
}
