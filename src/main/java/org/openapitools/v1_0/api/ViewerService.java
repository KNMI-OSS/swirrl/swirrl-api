package org.openapitools.v1_0.api;

import io.kubernetes.client.openapi.models.V1Pod;
import io.kubernetes.client.openapi.models.V1PersistentVolumeClaimList;
import org.openapitools.v1_0.model.Viewer;
import org.openapitools.v1_0.model.UserInfo;
import org.openapitools.v1_0.model.kubernetes.*;
import org.openapitools.v1_0.provenance.ProvenanceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.OffsetDateTime;
import java.util.UUID;

@Service
public class ViewerService {

    @Autowired
    ObjectFactory<KubernetesService> kubernetesServiceFactory;

    @Autowired
    ProvenanceService provenanceService;

    @Autowired // necessary to initialize it's @Value annotated members.
    ViewerDeploymentFactoryProvider factoryProvider;
    @Value("${deployment.pv.dynamicprovisioning}")
    private boolean dynamicProvisioning;
    @Value("${notebook.url.origin}")
    private String VIEWER_HOST;
    @Value("${allowed.viewer.types}")
    public String allowedViewerTypes;

    private Logger logger = LoggerFactory.getLogger(ViewerService.class);

    public ResponseEntity<?> createViewer(Viewer viewer) {
        return createViewer(viewer, null);
    }

    public ResponseEntity<?> createViewer(Viewer viewer, UserInfo userInfo) {
        String startTime = OffsetDateTime.now().toString();

        KubernetesService kubernetesService = kubernetesServiceFactory.getObject();

        viewer.setId(UUID.randomUUID().toString());
        viewer.setServiceURL(buildViewerUrl(viewer.getServiceType(), viewer.getId()));
        if (null == viewer.getSessionId()) {
            if (null == viewer.getPoolId()) {
                viewer.setSessionId(UUID.randomUUID().toString());
                viewer.setPoolId(UUID.randomUUID().toString());
            } else {
                return EposUtils.handleError("Session ID is mandatory if a pool ID is given.",
                        HttpStatus.BAD_REQUEST.value(), null);
            }

        } else {
            ResponseEntity<?> sessionResponse = kubernetesService.retrievePVCsByLabel(
                    ServiceDeploymentFactory.determineSessionLabel(viewer.getSessionId()));
            if (HttpStatus.OK != sessionResponse.getStatusCode()) {
                return EposUtils.handleError("Session ID " + viewer.getSessionId() + " does not exist.",
                        sessionResponse.getStatusCodeValue(), null);
            }
            if (null == viewer.getPoolId()) {
                viewer.setPoolId(UUID.randomUUID().toString());
            } else {
                ResponseEntity<?> poolResponse = kubernetesService.retrievePVCsByLabel(
                        ServiceDeploymentFactory.determinePoolLabel(viewer.getPoolId()));
                if (HttpStatus.OK != poolResponse.getStatusCode()) {
                    return EposUtils.handleError("Pool ID " + viewer.getPoolId() + " does not exist.",
                            poolResponse.getStatusCodeValue(), null);
                }
                V1PersistentVolumeClaimList pvclist = (V1PersistentVolumeClaimList) poolResponse.getBody();
                String sessionId = pvclist.getItems().get(0).getMetadata().getLabels().get(ServiceDeploymentFactory.SESSION_ID_LABEL);
                if (!sessionId.equals(viewer.getSessionId())) {
                    return EposUtils.handleError("Pool ID " + viewer.getPoolId() + " does not match session ID " + viewer.getSessionId(),
                            HttpStatus.BAD_REQUEST.value(), null);
                }
            }
        }
        ServiceDeployment serviceDeployment = null;
        ViewerDeploymentFactory viewerFactory = factoryProvider.getFactory(viewer);
        serviceDeployment = viewerFactory.makeViewerDeployment(viewer);


        if (!this.dynamicProvisioning) {
            ResponseEntity<?> workingdirPvResponse =
                    kubernetesService.createPersistentVolumeWhenNotExists(serviceDeployment.getWorkingdirPv());
            if (HttpStatus.OK != workingdirPvResponse.getStatusCode()) {
                return workingdirPvResponse;
            }

            ResponseEntity<?> datadirPvResponse =
                    kubernetesService.createPersistentVolumeWhenNotExists(serviceDeployment.getDatadirPv());
            if (HttpStatus.OK != datadirPvResponse.getStatusCode()) {
                return datadirPvResponse;
            }
        }

        ResponseEntity<?> workingdirPvcResponse =
                kubernetesService.createPersistentVolumeClaimWhenNotExists(serviceDeployment.getWorkingdirPvc());
        if (HttpStatus.OK != workingdirPvcResponse.getStatusCode()) {
            return workingdirPvcResponse;
        }

        ResponseEntity<?> pvcResponse =
                kubernetesService.createPersistentVolumeClaimWhenNotExists(serviceDeployment.getDatadirPvc());
        if (HttpStatus.OK != pvcResponse.getStatusCode()) {
            return pvcResponse;
        }

        ResponseEntity<?> createDeploymentResponse =
                kubernetesService.createDeployment(serviceDeployment.getDeployment());
        if (HttpStatus.OK != createDeploymentResponse.getStatusCode()) {
            return createDeploymentResponse;
        }

        ResponseEntity<?> createServiceResponse =
                kubernetesService.createService(serviceDeployment.getService());
        if (HttpStatus.OK != createServiceResponse.getStatusCode()) {
            return createServiceResponse;
        }

        ResponseEntity<?> createIngressResponse =
                kubernetesService.createIngress(serviceDeployment.getIngress());
        if (HttpStatus.OK != createIngressResponse.getStatusCode()) {
            return createIngressResponse;
        }

        provenanceService.traceProvenanceCreateViewer(viewer, startTime, userInfo);

        return new ResponseEntity<>(viewer, HttpStatus.OK);
    }

    public ResponseEntity<?> getViewerById(String serviceType, final String viewerId) {

        KubernetesService kubernetesService = kubernetesServiceFactory.getObject();

        Viewer viewer = new Viewer();
        viewer.setId(viewerId);
        viewer.setServiceURL(buildViewerUrl(serviceType, viewerId));

        // Retrieve the pod to determine the session id and pool id.
        final String viewerLabel = ServiceDeploymentFactory.determineServiceLabel(viewerId);
        ResponseEntity<?> podResult = kubernetesService.retrievePodByLabel(viewerLabel);
        if (HttpStatus.OK != podResult.getStatusCode()) {
            return podResult;
        }
        V1Pod pod = (V1Pod) podResult.getBody();
        viewer.setSessionId(pod.getMetadata().getLabels().get(ServiceDeploymentFactory.SESSION_ID_LABEL));
        viewer.setPoolId(pod.getMetadata().getLabels().get(ServiceDeploymentFactory.POOL_ID_LABEL));
        viewer.setServiceType(serviceType);

        return new ResponseEntity<>(viewer, HttpStatus.OK);
    }

    /**
     * Composes the URL under which the notebook will be running.
     * @param serviceType
     * @param serviceId
     * @return
     */
    public String buildViewerUrl(String serviceType, String serviceId) {
        return VIEWER_HOST +
                ServiceDeploymentFactory.buildServicePath(
                        serviceType,
                        serviceId) +
                "/";
    }

    public ResponseEntity<?> deleteViewer(String serviceType, String viewerId) {

        KubernetesService kubernetesService = kubernetesServiceFactory.getObject();

        ResponseEntity<?> activeResponse, deleteResponse;
        final String viewerAppName = ServiceDeploymentFactory.determineAppName(
                viewerId,
                serviceType
                );
        final String viewerLabel = ServiceDeploymentFactory.determineServiceLabel(viewerId);

        deleteResponse = kubernetesService.deleteServiceIngress(serviceType, viewerId);
        if (HttpStatus.OK != deleteResponse.getStatusCode()) {
            return deleteResponse;
        }

        // deleting service by appName instead of by label;
        // because the required function coreV1Api.deleteCollectionNamespacedService does not exist
        deleteResponse = kubernetesService.deleteServiceByName(viewerAppName);
        if (HttpStatus.OK != deleteResponse.getStatusCode()) {
            return deleteResponse;
        }

        deleteResponse = kubernetesService.deletePodsByLabel(viewerLabel);
        if (HttpStatus.OK != deleteResponse.getStatusCode()) {
            return deleteResponse;
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
