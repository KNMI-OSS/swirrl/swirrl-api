package org.openapitools.v1_0.api;

import org.openapitools.v1_0.model.GithubClientId;
import org.openapitools.v1_0.model.GithubRequestToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.request.NativeWebRequest;

import javax.validation.Valid;
import java.util.*;

import static java.lang.System.getenv;

@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2018-07-25T15:13:08" +
        ".866Z[GMT]")

@Controller
@RequestMapping("${openapi.ePOSNotebook.base-path:/v1.0}")
public class GithubApiController implements GithubApi {

    @Value("${github.client_id}")
    private String CLIENT_ID;

    private final NativeWebRequest request;

    @Autowired
    NotebookService notebookService;

    private Logger logger = LoggerFactory.getLogger(GithubApiController.class);
    @Autowired
    public GithubApiController(final NativeWebRequest request) {
        this.request = request;
    }

    @Value("${endpoint.notebook.restore.enabled}")
    private boolean endPointNotebookRestoreEnabled;

    /**
     * NOTE: if the interface was regenerated by swagger and you get a compiler
     * error about incompatible return type then you need to change
     * ResponseEntity<SomeClass> to ResponseEntity<?> in the interface
     * (GithubApi.java) and not in this class.
     */
    @Override
    public ResponseEntity<?> getGithubAccessToken(@Valid @RequestBody GithubRequestToken githubRequestToken) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

        Map<String, String> body = new HashMap<String, String>();
        body.put("client_id", CLIENT_ID);
        body.put("client_secret", getenv("GITHUB_CLIENT_SECRET"));
        body.put("code", githubRequestToken.getCode());
        body.put("state", githubRequestToken.getState());

        RestTemplate restTemplate = new RestTemplate();

        ResponseEntity<String> result = restTemplate.exchange(
                "https://github.com/login/oauth/access_token",
                HttpMethod.POST,
                new HttpEntity<Map<String, String>>(body, headers),
                String.class);

        return result;
    }

    /**
     * NOTE: if the interface was regenerated by swagger and you get a compiler
     * error about incompatible return type then you need to change
     * ResponseEntity<SomeClass> to ResponseEntity<?> in the interface
     * (GithubApi.java) and not in this class.
     */
    @Override
    public ResponseEntity<?> getGithubClientId() {
        return new ResponseEntity<>(new GithubClientId().clientId(CLIENT_ID), HttpStatus.OK);
    }
}
