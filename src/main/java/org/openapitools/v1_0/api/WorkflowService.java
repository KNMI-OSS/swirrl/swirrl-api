package org.openapitools.v1_0.api;

import io.kubernetes.client.openapi.models.V1ConfigMap;
import org.apache.kafka.clients.admin.NewTopic;
import org.openapitools.v1_0.model.*;
import org.openapitools.v1_0.model.kubernetes.*;
import org.openapitools.v1_0.provenance.ProvenanceService;
import org.openapitools.v1_0.provenance.ProvenanceApi;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.config.TopicBuilder;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.*;

@Service
public class WorkflowService {

    @Autowired
    WorkflowDeploymentFactory workflowDeploymentFactory;

    @Autowired
    ObjectFactory<KubernetesService> kubernetesServiceFactory;

    @Autowired
    KubernetesService kubernetesService;

    @Autowired
    private ProvenanceService provenanceService;

    @Autowired
    private ProvenanceApi provenanceApi;

    @Autowired
    private SessionFactory sessionFactory;

    @Autowired
    private KafkaTemplate<String, ActiveWorkflow> kafkaTemplate;

    /**
     * Create  the "workflow" topic if it doesn't exist yet. Spring will ignore this
     * bean if the topic exists. Should not be necessary, because we should have
     * AUTO_CREATE_TOPICS_ENABLE=true in /swirrl-workflow/deploy/kubernetes.yaml. We
     * relay on the automatic create for the session topics.
     * @return
     */
    @Bean
    public NewTopic workflowTopic() {
        return TopicBuilder.name("workflow").build();
    }

    private static final String CUSTOM_WORKFLOW_CONFIGMAP_NAME = "custom-workflows";
    private static final String WORKFLOW_CONFIGMAP_NAME = "swirrl-workflow-properties";

    private static Logger logger = LoggerFactory.getLogger(WorkflowService.class);

    public ResponseEntity<?> runWorkflow(String workflowName, Workflow workflow, String sessionHeader, UserInfo userInfo, String runAgent) {
        logger.debug("Run workflow");
        var sessionResult = kubernetesService.retrievePVCsByName(
                sessionFactory.determineDatadirPersistentVolumeClaimName(workflow.getSessionId()));
        if(sessionResult.getStatusCode().value() != 200){
            return sessionResult;
        }

        if (workflowName.contains("cron") &&
                HttpStatus.OK == kubernetesService.retrieveCronJobsByLabel(
                        workflowName+"-"+workflow.getSessionId()).getStatusCode()) {
            Job response = new Job();
            response.setStatus("There is already a cron job of this type for this session. "+
                    "Please delete the existing cron job before submitting a new one.");
            return new ResponseEntity<Job>(response, HttpStatus.CONFLICT);
        }

        String runId = UUID.randomUUID().toString();
        if (null == userInfo) {
            userInfo = new UserInfo("mock_user_id", "swirrl-api", "mock_authmode_id", "mock_group_id");
        }

        ActiveWorkflow activeWorkflow = new ActiveWorkflow(workflow.getSessionId(),
                workflow.getInputs(), runId, workflowName, ActiveWorkflow.RUN_ACTIVITY_COMMAND, sessionHeader,
                userInfo.getName(), userInfo.getId(), userInfo.getAuthMode(), userInfo.getGroup(), runAgent);
        kafkaTemplate.send("workflow", activeWorkflow);

        Job apiResponse = new Job();
        apiResponse.setId(runId);
        apiResponse.setLogs(new byte[0]);
        apiResponse.setStatus("");
        return new ResponseEntity<Job>(apiResponse, HttpStatus.OK);
    }

    public ResponseEntity<?> deleteLatestStage(String id, String sessionHeader, UserInfo userInfo, String agentName) {
        KubernetesService kubernetesService = kubernetesServiceFactory.getObject();

        final String label = WorkflowDeploymentFactory.determineWorkflowSessionLabel(id);
        logger.trace("Find if jobs with label "+label+" are running.");
        ResponseEntity<?> activeResponse = kubernetesService.retrieveActiveJobsByLabel(label);
        if (HttpStatus.OK != activeResponse.getStatusCode()) {
            return activeResponse;
        } else {
            ArrayList<String> activeJobs = (ArrayList<String>) activeResponse.getBody();
            if (activeJobs.size() > 0) {
                // Send proper JSON response here:
                String error = String.format("Impossible to delete stage due to active job(s): %s", activeJobs.toString());
                logger.error(error);

                return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
            } else {
                logger.trace("Number of active jobs: "+activeJobs.size()+" we can delete latest stage.");
            }
        }

        ResponseEntity<?> response = kubernetesService.retrievePVCsByLabel(
        ServiceDeploymentFactory.SESSION_ID_LABEL + "=" + id);

        if (HttpStatus.OK != response.getStatusCode()) {
            return EposUtils.handleError("Session ID \'" + id + "\' does not exist.",
                    response.getStatusCodeValue(), null);
        }

        String workflowName = "DELETE_LATEST";
        if (null == userInfo) {
            userInfo = new UserInfo("mock_user_id", "swirrl-api", "mock_authmode_id", "mock_group_id");
        }

        ActiveWorkflow activeWorkflow = new ActiveWorkflow(id,
                null, "", workflowName, ActiveWorkflow.DELETE_LATEST_STAGE_ACTIVITY_COMMAND, sessionHeader,
                userInfo.getName(), userInfo.getId(), userInfo.getAuthMode(), userInfo.getGroup(), agentName);
        kafkaTemplate.send("workflow", activeWorkflow);

        logger.info("Rollback job send to kafka");
        return new ResponseEntity<>(HttpStatus.OK);

    }

    public ResponseEntity<?> deleteCronWorkflow(String workflowName, String sessionId, String sessionHeader, UserInfo userInfo, String agentName) {
        if (null == userInfo) {
            userInfo = new UserInfo("mock_user_id", "swirrl-api", "mock_authmode_id", "mock_group_id");
        }
        ActiveWorkflow activeWorkflow = new ActiveWorkflow(sessionId, null, "", workflowName,
                ActiveWorkflow.DELETE_CRON_WORKFLOW_COMMAND, sessionHeader,
                userInfo.getName(), userInfo.getId(), userInfo.getAuthMode(), userInfo.getGroup(), agentName);
        kafkaTemplate.send("workflow", activeWorkflow);
        logger.info("Delete cron workflow sent to kafka");
        return new ResponseEntity<>(HttpStatus.OK);
    }

    public ResponseEntity<?> getWorkflowStatus(String jobId) {
        return kubernetesService.getJobStatus("run-id", jobId);
    }
    public ResponseEntity<?> getWorkflows() {
        logger.info("do getWorkflows");
        Map<String, String> workflowConfigMap = kubernetesService.getKConfigMap(WORKFLOW_CONFIGMAP_NAME);
        List<WorkflowItem> workflowList = ParseUtils.parseWorkflowMap(workflowConfigMap);

        Map<String, String> customWorkflowConfigMap = kubernetesService.getKConfigMap(CUSTOM_WORKFLOW_CONFIGMAP_NAME);
        logger.info("workflow custom workflow configmap: {}", customWorkflowConfigMap.toString());
        List<WorkflowItem> customWorkflowList = ParseUtils.parseCustomWorkflowMap(customWorkflowConfigMap);


        var list = new WorkflowList().integratedWorkflows(workflowList).customWorkflows(customWorkflowList);

        return new ResponseEntity<>(list, HttpStatus.OK);
    }


    public void sendDeleteSessionWorkflow(String sessionId) {
        logger.debug("delete session workflow: "+sessionId);
        ActiveWorkflow activeWorkflow = new ActiveWorkflow(
                sessionId, null, null, null, ActiveWorkflow.DELETE_SESSION_ACTIVITY_COMMAND, null,
                null, null, null, null, null);
        kafkaTemplate.send("workflow", activeWorkflow);
    }

    public ResponseEntity<?> registerWorkflow(String workflowName, String imageTag) {
        logger.debug("register workflow <"+workflowName+"> with image tag <"+imageTag+">");

        V1ConfigMap configMap = null;
        // check if the configmap with the custom workflows exists, if not, create it.
        ResponseEntity<?> k8sResponse = kubernetesService.getConfigMap(CUSTOM_WORKFLOW_CONFIGMAP_NAME);
        if (k8sResponse != null && k8sResponse.getStatusCode().value() != HttpStatus.OK.value()) {
            configMap = (V1ConfigMap)kubernetesService.createEmptyConfigmap(CUSTOM_WORKFLOW_CONFIGMAP_NAME).getBody();
        } else {
            configMap = (V1ConfigMap)k8sResponse.getBody();
        }

        InlineResponse200 response = new InlineResponse200();
        try {
            Map<String, String> workflows = configMap.getData();
            if (workflows == null)
                workflows = new HashMap<>();
            // Add custom prefix to prevent name clash with integrated workflows
            Set<String> workflowNames = workflows.keySet();
            int version = 1;
            for (String name : workflowNames) {
                if (name.startsWith("custom-"+workflowName)) {
                    int i = name.lastIndexOf('.')+2; // +2 to skip the "_v" part and get the number X of name_vX
                    if (Integer.parseInt(name.substring(i)) == version) version++;
                }
            }
            workflows.put("custom-"+workflowName+".v"+version, imageTag);
            configMap.data(workflows);
            kubernetesService.createOrReplaceConfigMap(configMap);
            response.workflowName("custom-"+workflowName+".v"+version);
            response.imageTag(imageTag);
        } catch (NullPointerException e) {
            return EposUtils.handleError("Could not create custom workflow configmap", HttpStatus.INTERNAL_SERVER_ERROR.value(), e);
        }

        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
