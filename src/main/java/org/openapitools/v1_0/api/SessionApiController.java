package org.openapitools.v1_0.api;

import io.swagger.annotations.ApiParam;
import org.openapitools.v1_0.model.kubernetes.*;
import org.openapitools.v1_0.provenance.ProvenanceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.NativeWebRequest;

import java.util.Optional;

@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2018-07-25T15:13:08" +
        ".866Z[GMT]")

@Controller
@RequestMapping("${openapi.ePOSNotebook.base-path:/v1.0}")
public class SessionApiController implements SessionApi {
    private static Logger logger = LoggerFactory.getLogger(SessionApiController.class);

    private final NativeWebRequest request;

    @Autowired
    ObjectFactory<KubernetesService> kubernetesServiceFactory;

    @Autowired
    public SessionApiController(final NativeWebRequest request) {
        this.request = request;
    }

    @Autowired
    private WorkflowService workflowService;

    @Autowired
    private ProvenanceService provenanceService;

    @Override
    public ResponseEntity<?> deleteSession(@ApiParam(value = "Id of the session.",required=true) @PathVariable("id") String id) {
        KubernetesService kubernetesService = kubernetesServiceFactory.getObject();

        ResponseEntity<?> activeResponse, deleteResponse, stillInUseResponse;

        stillInUseResponse = kubernetesService.retrievePodByLabel(ServiceDeploymentFactory.determineSessionLabel(id));
        if (HttpStatus.OK == stillInUseResponse.getStatusCode()) {
            return new EposUtils().handleError("Session " + id + " still in use.", HttpStatus.CONFLICT.value(), null);
        }

        workflowService.sendDeleteSessionWorkflow(id);

        deleteResponse = kubernetesService.deleteJobsByLabel(WorkflowDeploymentFactory.determineWorkflowSessionLabel(id));
         /*
          Jobs and job pods have a different session id label. This is to make sure end points that look for
          notebooks or jobs by session id only find notebooks or jobs depending on what they are interested in.
         */
        if (HttpStatus.OK != deleteResponse.getStatusCode()) {
            return deleteResponse;
        }

        deleteResponse = kubernetesService.deletePodsByLabel(WorkflowDeploymentFactory.determineWorkflowSessionLabel(id));
        if (HttpStatus.OK != deleteResponse.getStatusCode()) {
            return deleteResponse;
        }

        deleteResponse = kubernetesService.deleteJobsByLabel(CreateSnapshotJobFactory.determineSnapshotSessionLabel(id));
        if (HttpStatus.OK != deleteResponse.getStatusCode()) {
            return deleteResponse;
        }

        deleteResponse = kubernetesService.deletePodsByLabel(CreateSnapshotJobFactory.determineSnapshotSessionLabel(id));
        if (HttpStatus.OK != deleteResponse.getStatusCode()) {
            return deleteResponse;
        }

        deleteResponse = kubernetesService.deletePvcsByLabel(ServiceDeploymentFactory.determineSessionLabel(id));
        if (HttpStatus.OK != deleteResponse.getStatusCode()) {
            return deleteResponse;
        }
        return new ResponseEntity<>(HttpStatus.OK);

    }

    @Override
    public Optional<NativeWebRequest> getRequest() {
        return Optional.ofNullable(request);
    }
}
