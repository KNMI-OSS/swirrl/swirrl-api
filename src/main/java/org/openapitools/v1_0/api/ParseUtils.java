package org.openapitools.v1_0.api;

import org.openapitools.v1_0.model.WorkflowItem;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.*;

public class ParseUtils {

    private static final Set<String> notShowableImagesIntegratedWorkflow = Set.of("workflow-base",
            "workflow-pre", "workflow-post","workflow-dockercmds", "workflow-dinddaemon");
    public static List<WorkflowItem> parseWorkflowMap(Map<String, String> workflowConfigMap) {
        String configs = workflowConfigMap.get("swirrl-workflow.properties");
        Properties properties = new Properties();
        InputStream stream = new ByteArrayInputStream(configs.getBytes(StandardCharsets.UTF_8));
        try {
            properties.load(stream);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        List<WorkflowItem> list = new ArrayList<>();

        for (Map.Entry<Object, Object> e : properties.entrySet()) {
            var item = new WorkflowItem();
            String[] nameArr = e.getValue().toString().split("/");
            String n = nameArr[nameArr.length - 1];
            var j = n.split(":");
            if (j[0].startsWith("workflow") && !notShowableImagesIntegratedWorkflow.contains(j[0])) {
                String name = j[0].substring(9);
                item.setName(name);
                item.setVersion("latest");
                item.setImageTag(n);
                list.add(item);
            }
        }
        return list;
    }

    public static List<WorkflowItem> parseCustomWorkflowMap(Map<String, String> workflowConfigMap) {
        List<WorkflowItem> list = new ArrayList<>();

        for (var e : workflowConfigMap.entrySet()) {
            var item = new WorkflowItem();
            String[] nameVersion = e.getKey().split("\\.");
            item.setName(e.getKey());
            item.setVersion(nameVersion[1]);
            item.setImageTag(e.getValue());
            list.add(item);
        }
        return list;
    }
}
