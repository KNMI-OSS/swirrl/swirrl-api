package org.openapitools.v1_0.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * GithubRequestToken
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2023-09-15T12:23:18.056Z[GMT]")

public class GithubRequestToken   {
  @JsonProperty("code")
  private String code = null;

  @JsonProperty("state")
  private String state = null;

  public GithubRequestToken code(String code) {
    this.code = code;
    return this;
  }

  /**
   * The temporary code obtained from github after logging in
   * @return code
  **/
  @ApiModelProperty(example = "xxxxxxxxxxxxxxxxxxxx", value = "The temporary code obtained from github after logging in")


  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public GithubRequestToken state(String state) {
    this.state = state;
    return this;
  }

  /**
   * The unguessable random string provided to github
   * @return state
  **/
  @ApiModelProperty(example = "xxxxxxxxxxxxxxxxxxxx", value = "The unguessable random string provided to github")


  public String getState() {
    return state;
  }

  public void setState(String state) {
    this.state = state;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    GithubRequestToken githubRequestToken = (GithubRequestToken) o;
    return Objects.equals(this.code, githubRequestToken.code) &&
        Objects.equals(this.state, githubRequestToken.state);
  }

  @Override
  public int hashCode() {
    return Objects.hash(code, state);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class GithubRequestToken {\n");
    
    sb.append("    code: ").append(toIndentedString(code)).append("\n");
    sb.append("    state: ").append(toIndentedString(state)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

