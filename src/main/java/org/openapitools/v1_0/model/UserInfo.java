package org.openapitools.v1_0.model;

import java.security.Key;

import com.auth0.jwt.JWT;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import java.util.Base64;

public class UserInfo {
    private String provenanceUser;
    private String subject;
    private String authMode;
    private String group;

    private Logger logger = LoggerFactory.getLogger(UserInfo.class);

    public UserInfo(String userInfoHeader) {
        Pattern JWTpattern = Pattern.compile("^[A-Za-z0-9-_=]+\\.[A-Za-z0-9-_=]+\\.?[A-Za-z0-9-_.+/=]*$");
        Matcher JWTmatcher = JWTpattern.matcher(userInfoHeader);
        if (JWTmatcher.find()) {
            logger.debug("Decoding JWT token");
            try {
                DecodedJWT jwt = JWT.decode(userInfoHeader);
                String issuer = jwt.getIssuer();
                subject = jwt.getSubject();
                authMode = jwt.getType();
                String payload = new String(Base64.getDecoder().decode(jwt.getPayload()));
                logger.debug("Payload: "+payload);
                try {
                    JSONObject payloadObject = new JSONObject(payload);
                    group = payloadObject.getJSONArray("groups").getString(0);
                } catch (JSONException e) {
                    logger.debug("Exception parsing JWT payload: "+ e.getMessage());
                    group = "No groups";
                }
                provenanceUser = new String( subject + "@" + issuer);
                logger.debug("Provenance user: " + provenanceUser);
            } catch (JWTDecodeException exception) {
                logger.error("Could not decode JWT: "+ exception);
            }
        } else {
            logger.debug("Base64 decoding userinfo header");
            try {
                String payload = new String(Base64.getDecoder().decode(userInfoHeader));
                logger.debug("Payload: "+payload);
                try {
                    JSONObject payloadObject = new JSONObject(payload);
                    provenanceUser = payloadObject.getString("provenanceUser");
                    subject = payloadObject.getString("userName");
                    authMode = payloadObject.getString("authMode");
                    group = payloadObject.getString("group");
                } catch (JSONException e) {
                    logger.error("Exception parsing userinfo payload: "+ e.getMessage());
                }
            } catch (IllegalArgumentException exception) {
                logger.error("Could not base64 decode userinfo header: " + exception);
            }
        }
    }

    public UserInfo(String subject, String issuer, String authMode, String group) {
        this.subject = subject;
        this.provenanceUser = subject+"@"+issuer;
        this.authMode = authMode;
        this.group = group;
    }

    public String getName() {
        return this.provenanceUser;
    }
    public String getId() {
        return subject;
    }
    public String getAuthMode() {
        return authMode;
    }
    public String getGroup() {
        return group;
    }
}
