package org.openapitools.v1_0.model.kubernetes;

import io.kubernetes.client.openapi.models.V1ConfigMap;

import java.util.Map;

public interface WorkflowConfigMapBuilder {
    public V1ConfigMap build(String sessionId, Map<String, String> appLabels);
}
