package org.openapitools.v1_0.model.kubernetes;

import io.kubernetes.client.openapi.models.V1PersistentVolume;
import io.kubernetes.client.openapi.models.V1PersistentVolumeBuilder;
import io.kubernetes.client.openapi.models.V1PersistentVolumeClaim;
import io.kubernetes.client.openapi.models.V1PersistentVolumeClaimBuilder;
import io.kubernetes.client.openapi.models.V1ResourceRequirements;
import org.springframework.beans.factory.annotation.Value;
import io.kubernetes.client.custom.Quantity;
import io.kubernetes.client.util.Yaml;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

@Service
public class SessionFactory {

    @Value("${swirrl-api.namespace}")
    private String namespace;

    @Value("${efs.work.volume.id}")
    private String workingVolumeId;

    @Value("${efs.data.volume.id}")
    private String dataVolumeId;

    private static final String WORKINGDIRECTORY_PVC_NAME_PREFIX = "working-directory-";
    private static final String DATADIRECTORY_PVC_NAME_PREFIX = "data-directory-";

    private static Logger logger = LoggerFactory.getLogger(SessionFactory.class);

    public V1PersistentVolume makeDatadirPersistentVolume(String sessionId, String size) {
        if (dataVolumeId == null) {
            return null;
        }
        final String datadirPvName = DATADIRECTORY_PVC_NAME_PREFIX + sessionId + "-pv";
        final String datadirPvcName = DATADIRECTORY_PVC_NAME_PREFIX + sessionId;
        final Map<String, String> appLabels = Map.of("app", datadirPvcName,
                ServiceDeploymentFactory.SESSION_ID_LABEL, sessionId);
        final String resource = "/k8s-specs/volume/pv.yaml";

        return makePersistentVolume(datadirPvName, resource, size, appLabels, datadirPvcName, dataVolumeId);
    }

    public V1PersistentVolume makeWorkingdirPersistentVolume(String sessionId, String poolId, String size) {
        if (workingVolumeId == null) {
            return null;
        }
        final String workingdirPvName = WORKINGDIRECTORY_PVC_NAME_PREFIX + poolId + "-pv";
        final String workingdirPvcName = WORKINGDIRECTORY_PVC_NAME_PREFIX + poolId;
        final Map<String, String> appLabels = Map.of("app", workingdirPvcName,
                ServiceDeploymentFactory.POOL_ID_LABEL, poolId,
                ServiceDeploymentFactory.SESSION_ID_LABEL, sessionId);
        final String resource = "/k8s-specs/volume/pv.yaml";

        return makePersistentVolume(workingdirPvName, resource, size, appLabels, workingdirPvcName, workingVolumeId);
    }

    public V1PersistentVolumeClaim makeDatadirPersistentVolumeClaim(String sessionId, String size) {
        final String datadirPvcName = DATADIRECTORY_PVC_NAME_PREFIX + sessionId;
        final Map<String, String> appLabels = Map.of("app", datadirPvcName,
                ServiceDeploymentFactory.SESSION_ID_LABEL, sessionId);
        final String resource = "/k8s-specs/volume/pvc.yaml";

        return makePersistentVolumeClaim(datadirPvcName, resource, size, appLabels);
    }

    public V1PersistentVolumeClaim makeWorkingdirPersistentVolumeClaim(String sessionId, String poolId, String size) {
        final String workingdirPvcName = WORKINGDIRECTORY_PVC_NAME_PREFIX + poolId;
        final Map<String, String> appLabels = Map.of("app", workingdirPvcName,
                ServiceDeploymentFactory.POOL_ID_LABEL, poolId,
                ServiceDeploymentFactory.SESSION_ID_LABEL, sessionId);
        final String resource = "/k8s-specs/volume/pvc.yaml";

        return makePersistentVolumeClaim(workingdirPvcName, resource, size, appLabels);
    }

    public String determineDatadirPersistentVolumeClaimName(String sessionId) {
        return DATADIRECTORY_PVC_NAME_PREFIX + sessionId;
    }

    public String determineWorkingdirPersistentVolumeClaimName(String poolId) {
        return WORKINGDIRECTORY_PVC_NAME_PREFIX + poolId;
    }

    private V1PersistentVolumeClaim makePersistentVolumeClaim(String pvcName, String resource,
                                               String size, Map<String, String> appLabels) {
        // Load pcv yaml from src/main/resources
        V1PersistentVolumeClaim pvc = null;
        try {
            pvc = Yaml.loadAs(
                    new InputStreamReader(new ClassPathResource(resource).getInputStream()),
                    V1PersistentVolumeClaim.class);
        } catch (final IOException e) {
            logger.error("Exception occurred whilst reading yaml file.",e);
            return null;
        }

        // Set instance specific data using a Builder initialized with the loaded yaml
        pvc = new V1PersistentVolumeClaimBuilder(pvc)
                .editMetadata()
                .withName(pvcName)
                .withLabels(appLabels)
                .endMetadata()
                .editSpec()
                .withResources(new V1ResourceRequirements().putRequestsItem("storage", new Quantity(size)))
                .endSpec()
                .build();

        return pvc;
    }

    private V1PersistentVolume makePersistentVolume(String pvName, String resource, String size,
                                                 Map<String, String> appLabels, String pvcName, String volumeHandle) {
        // Load pcv yaml from src/main/resources
        V1PersistentVolume pv = null;
        try {
            pv = Yaml.loadAs(
                    new InputStreamReader(new ClassPathResource(resource).getInputStream()),
                    V1PersistentVolume.class);
        } catch (final IOException e) {
            logger.error("Exception occurred whilst reading yaml file.",e);
            return null;
        }

        // Set instance specific data using a Builder initialized with the loaded yaml
        pv = new V1PersistentVolumeBuilder(pv)
                .editMetadata()
                .withName(pvName)
                .withLabels(appLabels)
                .endMetadata()
                .editSpec()
                .withCapacity(Map.of("storage", new Quantity(size)))
                .editCsi()
                .withVolumeHandle(volumeHandle)
                .endCsi()
                .editClaimRef()
                .withName(pvcName)
                .withNamespace(namespace)
                .endClaimRef()
                .endSpec()
                .build();

        return pv;
    }
}
