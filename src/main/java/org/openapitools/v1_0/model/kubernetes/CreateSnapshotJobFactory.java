package org.openapitools.v1_0.model.kubernetes;

import io.kubernetes.client.openapi.models.V1Job;
import io.kubernetes.client.openapi.models.V1JobBuilder;
import io.kubernetes.client.openapi.models.V1LocalObjectReference;
import io.kubernetes.client.util.Yaml;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class CreateSnapshotJobFactory {
    private static Logger logger = LoggerFactory.getLogger(CreateSnapshotJobFactory.class);

    @Autowired
    SessionFactory sessionFactory;

    @Value("${notebook.snapshot.createrepo.image.tag}")
    private String dockerImageTag;
    @Value("${notebook.snapshot.createrepo.image.pull.secret}")
    private String dockerImagePullSecret;
    @Value("${swirrl.internal.origin}")
    private String apiHostPort;
    @Value("${server.servlet.context-path}")
    private String apiContextPath;
    @Value("${swirrl-api.version}")
    private String apiVersion;

    public final static String CREATE_SNAPSHOT_JOB_PREFIX = "css";
    public final static String CREATE_SNAPSHOT_SESSION_ID_LABEL = CREATE_SNAPSHOT_JOB_PREFIX+ServiceDeploymentFactory.SESSION_ID_LABEL;
    public final static String CREATE_SNAPSHOT_POOL_ID_LABEL = CREATE_SNAPSHOT_JOB_PREFIX+ServiceDeploymentFactory.POOL_ID_LABEL;
    public final static String CREATE_SNAPSHOT_SERVICE_ID_LABEL = CREATE_SNAPSHOT_JOB_PREFIX+ServiceDeploymentFactory.SERVICE_ID_LABEL;

    public static String determineSnapshotSessionLabel(String sessionId) {
        return CREATE_SNAPSHOT_SESSION_ID_LABEL + "=" + sessionId;
    }

    public CreateSnapshotJob makeCreateSnapshotJob(String sessionId, String poolId, String serviceId, String message, String notebookImageTag, String accessToken) {
        V1Job job = this.buildCreateSnapshotJob(sessionId, poolId, serviceId, message, notebookImageTag, accessToken);
        if (null == job) return null;
        return (new CreateSnapshotJob(sessionId, poolId, serviceId, job, notebookImageTag));
    }

    private V1Job buildCreateSnapshotJob(String sessionId, String poolId, String serviceId, String message, String notebookImageTag, String accessToken) {
        V1Job job = null;
        try {
            job = Yaml.loadAs(new InputStreamReader(
                    new ClassPathResource("/k8s-specs/jobs/create-snapshot.yaml").getInputStream()),V1Job.class);
        } catch (final IOException e) {
            logger.error("Exception occurred whilst reading yaml file.",e);
            return null;
        }
        final String label = NotebookDeploymentFactory.determineServiceLabel(serviceId);
        final String appName = determineAppName(serviceId);
        final Map<String, String> appLabels = Map.of(
                "type", "snapshot",
                "app", appName,
                CREATE_SNAPSHOT_SESSION_ID_LABEL, sessionId,
                CREATE_SNAPSHOT_SERVICE_ID_LABEL, serviceId,
                CREATE_SNAPSHOT_POOL_ID_LABEL, poolId
        );
        final String jobTimestamp = new SimpleDateFormat("yyyyMMddHHmmSS").format(new Date());

        V1LocalObjectReference dockerImagePullSecretObj = new V1LocalObjectReference();
        dockerImagePullSecretObj.setName(dockerImagePullSecret);

        List<String> arguments = new ArrayList<String>();
        arguments.add("--repo="+this.slugRepoName(message)+'-'+UUID.randomUUID().toString().split("-")[0]); /* Only use first part of UUID */
        arguments.add("--description='"+message+"'");
        arguments.add("--serviceid="+serviceId);
        arguments.add("--api-url="+apiHostPort+apiContextPath+"/"+apiVersion+"/notebook");
        arguments.add("--notebook-image="+notebookImageTag);
        if (null != accessToken) arguments.add("--token="+accessToken);

        job = new V1JobBuilder(job)
                .editMetadata().withName(appName+"-"+jobTimestamp).withLabels(appLabels).endMetadata()
                .editSpec().editTemplate().editMetadata().withName(appName).withLabels(appLabels).endMetadata()
                .editSpec()
                .withImagePullSecrets(dockerImagePullSecretObj)
                .editMatchingContainer(item -> "job-create-snapshot".equals(item.getName()))
                .withImage(dockerImageTag)
                .withArgs(arguments)
                .editMatchingVolumeMount(item -> item.getName().equals("data"))
                .withSubPath(sessionId)
                .endVolumeMount()
                .editMatchingVolumeMount(item -> item.getName().equals("working-directory"))
                .withSubPath(poolId)
                .endVolumeMount()
                .endContainer()
                .editMatchingVolume(item -> "working-directory".equals(item.getName()))
                .editPersistentVolumeClaim()
                .withClaimName(sessionFactory.determineWorkingdirPersistentVolumeClaimName(poolId))
                .endPersistentVolumeClaim()
                .endVolume()
                .editMatchingVolume(item -> "data".equals(item.getName()))
                .editPersistentVolumeClaim()
                .withClaimName(sessionFactory.determineDatadirPersistentVolumeClaimName(sessionId))
                .endPersistentVolumeClaim()
                .endVolume()
                .endSpec().endTemplate().endSpec()
                .build();
        return job;
    }

    /**
     * Slugs first three words of a string.
     * @param message
     * @return first 3 words of message slugged, unless there are less than 3 words
     */
    private String slugRepoName(String message) {
        List<String> msgList = new ArrayList<String>(Arrays.asList(message.split(" ")));
        List<String> repoNameList = new ArrayList<String>();
        for (String word : msgList) {
            Pattern p = Pattern.compile("[^A-Z0-9a-z\\-]");
            Matcher m = p.matcher(word);
            word = m.replaceAll("-");
            repoNameList.add(word.toLowerCase());
        }
        if (repoNameList.size() > 2) return String.join("-", repoNameList.subList(0,3));
        else return String.join("-", repoNameList.subList(0,repoNameList.size()));
    }

    public static String determineAppName(String serviceId) {
        return CREATE_SNAPSHOT_JOB_PREFIX + "-" + serviceId;
    }
}
