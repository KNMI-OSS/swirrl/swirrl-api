package org.openapitools.v1_0.model.kubernetes;

import io.kubernetes.client.openapi.models.*;
import org.openapitools.v1_0.model.Viewer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class EnlightenDeploymentFactory extends ViewerDeploymentFactory {

    private static Logger logger = LoggerFactory.getLogger(EnlightenDeploymentFactory.class);

    @Value("${enlighten.viewer.docker.image.tag}")
    private String dockerImageTag;

    private final String ENLIGHTEN_DATA_PATH = "/data/data";
    private final String ENLIGHTEN_VIEW_PATH = "/data";

    public EnlightenDeploymentFactory(@Value("#{${viewer.docker.image.tagmap}}")Map<String, String> viewerImageTagMap) {
        this.viewerImageTagMap = viewerImageTagMap;
    }
    @Override
    public String getServiceType() {
        return "enlighten";
    }

    public ServiceDeployment makeViewerDeployment(Viewer viewer) {
        return makeEnlightenDeployment(viewer);
    }

    private ServiceDeployment makeEnlightenDeployment(Viewer enlighten) {

        ServiceDeployment serviceDeployment = this.makeServiceDeployment(enlighten);

        if (serviceDeployment == null) {
            logger.error("Impossible to prepare the deployment of the service.");
            return null;
        }

        String appName = serviceDeployment.getAppName();
        Map<String, String> appLabels = serviceDeployment.getAppLabels();

        V1Pod deployment = buildEnlightenDeployment(
                serviceDeployment.getDeployment(),
                enlighten.getId());


        V1Service service = buildEnlightenService(
                serviceDeployment.getService());

        serviceDeployment.setDeployment(deployment);
        serviceDeployment.setService(service);

        return serviceDeployment;
    }

    private V1Pod buildEnlightenDeployment(V1Pod deployment,
                                                 String serviceId) {
        V1EnvVar datasetPath = new V1EnvVar();
        datasetPath.name("DATASET_PATH");
        datasetPath.value(ENLIGHTEN_DATA_PATH+"/latest");
        V1EnvVar viewPath = new V1EnvVar();
        viewPath.name("VIEW_PATH");
        viewPath.value(ENLIGHTEN_VIEW_PATH);

        V1EnvVar urlPrefix = new V1EnvVar();
        urlPrefix.name("URL_PREFIX");
        urlPrefix.value(ServiceDeploymentFactory.buildServicePath(this.getServiceType(), serviceId));

        List<V1EnvVar> environment = new ArrayList<V1EnvVar>();
        environment.add(datasetPath);
        environment.add(viewPath);
        environment.add(urlPrefix);

        V1LocalObjectReference secret = new V1LocalObjectReference();
        secret.setName("enlighten-deploy");
        logger.debug("Setting enlighten pull secret");

        deployment = new V1PodBuilder(deployment)
                .editSpec()
                .withImagePullSecrets(secret)
                .removeMatchingFromInitContainers(item -> item.getName().equals("service-init-container"))
                .editMatchingContainer(item -> getServiceType().equals(item.getName()))
                .withImage(this.determineImageTag(this.getServiceType()))
                .editReadinessProbe()
                .editTcpSocket()
                .withNewPort(80)
                .endTcpSocket()
                .endReadinessProbe()
                .withPorts(new V1ContainerPortBuilder().withContainerPort(80).build())
                .addAllToEnv(environment)
                .editMatchingVolumeMount(item -> "working-directory".equals(item.getName()))
                .withMountPath(ENLIGHTEN_VIEW_PATH)
                .endVolumeMount()
                .editMatchingVolumeMount(item -> "data".equals(item.getName()))
                .withMountPath(ENLIGHTEN_DATA_PATH) // Give the nested folder the same name as in the jupyter case to prevent an empty dir in a different session.
                .endVolumeMount()
                .endContainer()
                .endSpec()
                .build();

        return deployment;
    }

    private V1Service buildEnlightenService(V1Service service) {

        service = new V1ServiceBuilder(service)
                .editSpec()
                .editFirstPort()
                .withNewTargetPort(80) // Enlighten runs under port 80 by default, which can only be changed by overriding the entrypoint of the container.
                .endPort()
                .endSpec()
                .build();

        return service;
    }
}
