package org.openapitools.v1_0.model.kubernetes;

import io.kubernetes.client.custom.Quantity;
import io.kubernetes.client.openapi.models.*;
import io.kubernetes.client.proto.V1beta1Extensions;
import io.kubernetes.client.util.Yaml;
import org.openapitools.v1_0.api.KubernetesService;
import org.openapitools.v1_0.model.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@org.springframework.stereotype.Service
public abstract class ServiceDeploymentFactory {

    @Autowired
    SessionFactory sessionFactory;

    private static Logger logger = LoggerFactory.getLogger(ServiceDeploymentFactory.class);

    private final String DEFAULT_CONTAINER_NAME = "container-name";
    public final static String SERVICE_ID_LABEL = "service-id";
    public final static String POOL_ID_LABEL = "pool-id";
    public final static String SESSION_ID_LABEL = "session-id";

    @Value("${service.docker.image.pull.secret}")
    private String dockerImagePullSecret;

    @Value("${volume.workingdirectory.size}")
    private String workingdirSize;

    @Value("${volume.datadirectory.size}")
    private String datadirSize;

    public abstract String getServiceType();

    public ServiceDeployment makeServiceDeployment(Service service) {

        if (service == null
                || service.getId() == null
                || service.getSessionId() == null) {
            logger.error("No serviceId or sessionId are provided, these are required.");
            return null;
        }

        String serviceType = getServiceType();
        String serviceId = service.getId();
        String poolId = service.getPoolId();
        String sessionId = service.getSessionId();

        final String appName = determineAppName(serviceId, serviceType);
        final Map<String, String> appLabels = Map.of(
                "type", serviceType,
                "app", appName,
                SERVICE_ID_LABEL, serviceId,
                POOL_ID_LABEL, poolId,
                SESSION_ID_LABEL, sessionId);

        V1PersistentVolumeClaim workingdirPvc = sessionFactory.makeWorkingdirPersistentVolumeClaim(sessionId, poolId, workingdirSize);
        V1PersistentVolumeClaim datadirPvc = sessionFactory.makeDatadirPersistentVolumeClaim(sessionId, datadirSize);

        V1PersistentVolume workingdirPv = sessionFactory.makeWorkingdirPersistentVolume(sessionId, poolId, workingdirSize);
        V1PersistentVolume datadirPv = sessionFactory.makeDatadirPersistentVolume(sessionId, datadirSize);

        V1Pod deployment = buildDeployment(
                appName,
                appLabels,
                workingdirPvc.getMetadata().getName(),
                datadirPvc.getMetadata().getName(),
                serviceType,
                sessionId,
                poolId
        );

        V1Service returnService = buildService(appName, appLabels);
        V1HTTPIngressPath ingress = buildIngress(appName, serviceType, serviceId);
        List<V1HTTPIngressPath> ingressList = new ArrayList<>();
        ingressList.add(ingress);
        Map<String, List<V1HTTPIngressPath>> ingressMap = new HashMap<>();
        ingressMap.put("default", ingressList);

        return new ServiceDeployment(workingdirPvc, datadirPvc, workingdirPv, datadirPv, deployment, returnService, ingressMap, appName, appLabels);
    }

    public static String determineAppName(String serviceId, String serviceType) {
        return serviceType + "-" + serviceId;
    }

    public static String determineServiceLabel(String notebookId) {
        return SERVICE_ID_LABEL + "=" + notebookId;
    }

    public static String determinePoolLabel(String sessionId) {
        return POOL_ID_LABEL + "=" + sessionId;
    }

    public static String determineSessionLabel(String sessionId) {
        return SESSION_ID_LABEL + "=" + sessionId;
    }

    private V1Pod buildDeployment(String appName,
                                         Map<String, String> appLabels,
                                         String workingdirPvcName,
                                         String datadirPvcName,
                                         String serviceType,
                                         String sessionId,
                                         String poolId) {
        V1Pod deployment = null;
        try {
            deployment = Yaml.loadAs(
                    new InputStreamReader(
                            new ClassPathResource("/k8s-specs/service/deployment.yaml").getInputStream()),
                    V1Pod.class);
        } catch (final IOException e) {
            logger.error("Exception occured whilst reading yaml file.",e);
            return null;
        }

        V1LocalObjectReference dockerImagePullSecretObj = new V1LocalObjectReference();
        dockerImagePullSecretObj.setName(dockerImagePullSecret);
        List<V1EnvVar> environment = new ArrayList<V1EnvVar>();

        // Set instance specific values using Builder initialised with yaml file.

        deployment = new V1PodBuilder(deployment)
                .editMetadata()
                .withName(appName)
                .withLabels(appLabels)
                .endMetadata()
                .editSpec()
                .withImagePullSecrets(dockerImagePullSecretObj)
                .editMatchingInitContainer(item -> item.getName().equals("service-init-container"))
                .editMatchingVolumeMount(item -> item.getName().equals("working-directory"))
                .withSubPath(poolId)
                .endVolumeMount()
                .endInitContainer()
                .editMatchingContainer(item -> DEFAULT_CONTAINER_NAME.equals(item.getName()))
                .withName(serviceType)
                .withEnv(environment)
                .editMatchingVolumeMount(item -> item.getName().equals("data"))
                .withSubPath(sessionId)
                .endVolumeMount()
                .editMatchingVolumeMount(item -> item.getName().equals("working-directory"))
                .withSubPath(poolId)
                .endVolumeMount()
                .endContainer()
                .editMatchingVolume(item -> "working-directory".equals(item.getName()))
                .editPersistentVolumeClaim()
                .withClaimName(workingdirPvcName)
                .endPersistentVolumeClaim()
                .endVolume()
                .editMatchingVolume(item -> "data".equals(item.getName()))
                .editPersistentVolumeClaim()
                .withClaimName(datadirPvcName)
                .endPersistentVolumeClaim()
                .endVolume()
                .endSpec()
                .build();

        return deployment;
    }

    private V1Service buildService(String appName, Map<String, String> appLabels) {
        V1Service service = null;
        try {
            service = Yaml.loadAs(
                    new InputStreamReader(new ClassPathResource("/k8s-specs/service/service.yaml").getInputStream()),
                    V1Service.class);
        } catch (final IOException e) {
            logger.error("Exception occured whilst reading yaml file.",e);
            return null;
        }

        service = new V1ServiceBuilder(service)
                .editMetadata()
                .withName(appName)
                .withLabels(appLabels)
                .endMetadata()
                .editSpec()
                .withSelector(appLabels)
                .endSpec()
                .build();

        return service;
    }

    public static V1HTTPIngressPath buildIngress(String appName, String serviceType, String serviceId) {
        V1HTTPIngressPath ingressPatch = null;
        try {
            ingressPatch = Yaml.loadAs(
                    new InputStreamReader(new ClassPathResource("/k8s-specs/service/ingress-patch.yaml").getInputStream()),
                    V1HTTPIngressPath.class);
        } catch (final IOException e) {
            logger.error("Exception occured whilst reading yaml file.",e);
            return null;
        }

        ingressPatch = new V1HTTPIngressPathBuilder(ingressPatch)
                .withPath(buildServicePath(serviceType, serviceId))
                .editBackend()
                .editService()
                .withName(appName)
                .endService()
                .endBackend()
                .build();

        return ingressPatch;
    }

    /**
     * Composes the path under which the service will be running.
     * @param serviceType
     * @param serviceId
     * @return
     */
    public static String buildServicePath(String serviceType, String serviceId) {
        return "/swirrl/" + serviceType + "/" + serviceId;
    }
}
