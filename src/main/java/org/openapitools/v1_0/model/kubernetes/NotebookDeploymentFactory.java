package org.openapitools.v1_0.model.kubernetes;

import io.kubernetes.client.custom.Quantity;
import io.kubernetes.client.openapi.models.*;
import io.kubernetes.client.util.Yaml;
import org.apache.commons.io.IOUtils;
import org.openapitools.v1_0.api.KubernetesService;
import org.openapitools.v1_0.model.Notebook;
import org.openapitools.v1_0.model.NotebookLibrary;
import org.openapitools.v1_0.model.UserInfo;
import org.openapitools.v1_0.provenance.TemplateCatalog;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class NotebookDeploymentFactory extends ServiceDeploymentFactory {

    private static Logger logger = LoggerFactory.getLogger(NotebookDeploymentFactory.class);

    public final static String STARTUP_DIRECTORY = "startup-directory";
    public final static String PIP_WRAPPER_VOLUME = "pip-wrapper";
    public final static String CONDA_WRAPPER_VOLUME = "conda-wrapper";
    public final static String ENVVAR_VOLUME = "env-var";

    @Value("${notebook.url.origin}")
    private String NOTEBOOK_URL;
    @Value("${swirrl.internal.origin}")
    private String SWIRRL_INTERNAL;
    @Value("${api.url.origin}")
    private String SWIRRL_URL;
    @Value("${notebook.login.enabled}")
    private String NB_LOGIN_ENABLED;
    @Value("${provenance.template.expansion-url}")
    private String EXPANSION_URL;
    @Value("${notebook.ingress.hostname}")
    private String NOTEBOOK_INGRESS_HOSTNAME;
    @Value("${notebook.resource.cpu.limit}")
    private String NOTEBOOK_RESOURCE_CPU_LIMIT;
    @Value("${notebook.resource.cpu.request}")
    private String NOTEBOOK_RESOURCE_CPU_REQUEST;
    @Value("${notebook.resource.memory.limit}")
    private String NOTEBOOK_RESOURCE_MEMORY_LIMIT;
    @Value("${notebook.resource.memory.request}")
    private String NOTEBOOK_RESOURCE_MEMORY_REQUEST;
    @Value("${notebook.secure_cookies}")
    private String secureCookies;

    @Autowired
    private TemplateCatalog templateCatalog;

    @Override
    public String getServiceType() {
        return "jupyter";
    }

    public NotebookDeployment makeNotebookDeployment(Notebook notebook, String dockerImageTag, UserInfo userInfo, String token) {

        ServiceDeployment serviceDeployment = this.makeServiceDeployment(notebook);

        if (serviceDeployment == null) {
            logger.error("Impossible to prepare the deployment of the service.");
            return null;
        }

        String appName = serviceDeployment.getAppName();
        String sessionId = notebook.getSessionId();
        String poolId = notebook.getPoolId();
        String serviceId = notebook.getId();
        Map<String, String> appLabels = serviceDeployment.getAppLabels();

        // Create configmaps.
        final String deployVersion = new SimpleDateFormat("yyyyMMddHHmmSS").format(new Date());
        final String configmapName = appName + "-" + STARTUP_DIRECTORY + deployVersion;
        final String pipWrapperConfigmapName = appName + "-" + PIP_WRAPPER_VOLUME;
        final String condaWrapperConfigmapName = appName + "-" + CONDA_WRAPPER_VOLUME;
        final String envVarConfigmapName = appName + "-" + ENVVAR_VOLUME;

        V1ConfigMap configmap = buildConfigmap(configmapName, appLabels, notebook.getUserRequestedLibraries());
        V1ConfigMap configmapPipWrapper = buildPipWrapperConfigmap(pipWrapperConfigmapName, appLabels);
        V1ConfigMap configMapCondaWrapper = buildCondaWrapperConfigmap(condaWrapperConfigmapName, appLabels);
        V1ConfigMap configMapEnvVar = buildEnvVarConfigmap(envVarConfigmapName, appLabels, sessionId, poolId, serviceId, userInfo);

        // Update deployment with links to configmaps and correct start commands.
        V1Pod deployment = buildNotebookDeployment(
                serviceDeployment.getDeployment(),
                dockerImageTag,
                configmapName,
                pipWrapperConfigmapName,
                condaWrapperConfigmapName,
                envVarConfigmapName,
                serviceId,
                token);

        return new NotebookDeployment(configmap,
                configmapPipWrapper,
                configMapCondaWrapper,
                configMapEnvVar,
                serviceDeployment.getWorkingdirPvc(),
                serviceDeployment.getDatadirPvc(),
                serviceDeployment.getWorkingdirPv(),
                serviceDeployment.getDatadirPv(),
                deployment,
                serviceDeployment.getService(),
                serviceDeployment.getIngress(),
                serviceDeployment.getAppName(),
                serviceDeployment.getAppLabels());
    }

    private V1ConfigMap buildConfigmap(String configmapName, Map<String, String> appLabels, List<NotebookLibrary> libraries) {

        V1ConfigMap configmap = null;
        try {
            configmap = Yaml.loadAs(
                    new InputStreamReader(new ClassPathResource("/k8s-specs/notebook/configmap.yaml").getInputStream()),
                    V1ConfigMap.class);
        } catch (final IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        // libraries
        StringBuilder requirements = new StringBuilder();
        if(libraries != null)
        {
            for ( NotebookLibrary library : libraries) {
                requirements.append(library.getLibname());
                String libversion=library.getLibversion();

                if(libversion != null && !libversion.isEmpty()) {
                    requirements.append("==");
                    requirements.append(libversion);
                }
                requirements.append("\n");
            }
        }
        String wrapperCommonScript = null;
        try {
            wrapperCommonScript = IOUtils.toString(new InputStreamReader(
                    new ClassPathResource("wrapper_common.py").getInputStream()));
        } catch (final IOException e1) {
            e1.printStackTrace();
        }

        configmap = new V1ConfigMapBuilder(configmap)
                .editMetadata()
                .withName(configmapName)
                .withLabels(appLabels)
                .endMetadata()
                .addToData("requirements.txt", requirements.toString())
                .addToData("wrapper_common.py", wrapperCommonScript.toString())
                .build();

        return configmap;
    }

    private V1ConfigMap buildPipWrapperConfigmap(String configmapName, Map<String, String> appLabels) {

        V1ConfigMap configmap = new V1ConfigMap();
        String pipWrapperScript = null;
        try {
            pipWrapperScript = IOUtils.toString(new InputStreamReader(
                    new ClassPathResource("pip-wrapper.py").getInputStream()));
        } catch (final IOException e1) {
            e1.printStackTrace();
        }

        configmap = new V1ConfigMapBuilder(configmap)
                .editOrNewMetadata()
                .withName(configmapName)
                .withLabels(appLabels)
                .endMetadata()
                .addToData("pip", pipWrapperScript)
                .build();

        return configmap;
    }

    private V1ConfigMap buildCondaWrapperConfigmap(String configmapName, Map<String, String> appLabels) {
        V1ConfigMap configmap = new V1ConfigMap();
        String condaWrapperScript = null;
        try {
            condaWrapperScript = IOUtils.toString(new InputStreamReader(
                    new ClassPathResource("conda-wrapper.py").getInputStream()));
        } catch (final IOException e1) {
            e1.printStackTrace();
        }

        configmap = new V1ConfigMapBuilder(configmap)
                .editOrNewMetadata()
                .withName(configmapName)
                .withLabels(appLabels)
                .endMetadata()
                .addToData("swirrl-conda", condaWrapperScript)
                .build();

        return configmap;
    }

    private V1ConfigMap buildEnvVarConfigmap(String configmapName, Map<String, String> appLabels, String sessionId, String poolId, String serviceId, UserInfo userInfo) {
        V1ConfigMap configmap = new V1ConfigMap();

        configmap = new V1ConfigMapBuilder(configmap)
                .editOrNewMetadata()
                .withName(configmapName)
                .withLabels(appLabels)
                .endMetadata()
                .addToData("sessionId", sessionId)
                .addToData("poolId", poolId)
                .addToData("serviceId", serviceId)
                .addToData("notebookUrlOrigin", NOTEBOOK_URL)
                .addToData("swirrlInternalOrigin", SWIRRL_INTERNAL)
                .addToData("templateExpansionUrl", EXPANSION_URL)
                .addToData("notebookIngressHostname", NOTEBOOK_INGRESS_HOSTNAME)
                .addToData("swirrlUrl", SWIRRL_URL)
                .addToData("loginEnabled", NB_LOGIN_ENABLED)
                .addToData("provenanceUser", userInfo.getName())
                .addToData("userName", userInfo.getId())
                .addToData("authMode", userInfo.getAuthMode())
                .addToData("group", userInfo.getGroup())
                .build();

        return configmap;
    }

    private V1Pod buildNotebookDeployment(V1Pod deployment,
                                                 String dockerImageTag,
                                                 String configmapName,
                                                 String pipWrapperConfigmapName,
                                                 String condaWrapperConfigmapName,
                                                 String envVarConfigmapName,
                                                 String serviceId,
                                                 String token) {


        List<V1KeyToPath> keyToPathList = new ArrayList<>();

        V1KeyToPath startNotebookPip = new V1KeyToPath();
        startNotebookPip.setKey("start-notebook-pip.sh");
        startNotebookPip.setPath("start-notebook-pip.sh");
        startNotebookPip.setMode(0754);
        keyToPathList.add(startNotebookPip);

        V1KeyToPath requirements = new V1KeyToPath();
        requirements.setKey("requirements.txt");
        requirements.setPath("requirements.txt");
        keyToPathList.add(requirements);

        V1KeyToPath wrapperCommon = new V1KeyToPath();
        wrapperCommon.setKey("wrapper_common.py");
        wrapperCommon.setPath("wrapper_common.py");
        keyToPathList.add(wrapperCommon);

        V1EnvVar labEnv = new V1EnvVar();
        labEnv.name("JUPYTER_ENABLE_LAB");
        labEnv.value("yes");

        V1ResourceRequirements resourceRequirements = new V1ResourceRequirements();
        Map<String, Quantity> limits = new HashMap<>();
        limits.put("memory", Quantity.fromString(NOTEBOOK_RESOURCE_MEMORY_LIMIT));
        limits.put("cpu", Quantity.fromString(NOTEBOOK_RESOURCE_CPU_LIMIT));
        resourceRequirements.setLimits(limits);
        resourceRequirements.setRequests(limits); // Set equal to limits for now.

        deployment = new V1PodBuilder(deployment)
                .editSpec()
                .editMatchingContainer(item -> getServiceType().equals(item.getName()))
                .withImage(dockerImageTag)
                .withCommand("/data/scripts/start-notebook-pip.sh")
                .withArgs(List.of(
                        "--ServerApp.root_dir=/home/jovyan/work",
                        "--ServerApp.allow_origin=*",
                        "--ServerApp.default_url=/lab",
                        "--IdentityProvider.token="+token,
                        "--ServerApp.shutdown_no_activity_timeout=1210000", // 2 weeks.
                        "--TerminalManager.cull_inactive_timeout=1214000",
                        "--TerminalManager.cull_interval=3600",
                        "--MappingKernelManager.cull_connected=True",
                        "--MappingKernelManager.cull_idle_timeout=1210000",
                        "--MappingKernelManager.cull_interval=3600",
                        "--no-browser",
                        "--ServerApp.base_url=" + ServiceDeploymentFactory.buildServicePath(getServiceType(), serviceId),
                        secureCookies.equals("true")
                                ? "--IdentityProvider.cookie_options={\"SameSite\":\"Lax\",\"Secure\":True}"
                                : "--IdentityProvider.cookie_options={\"SameSite\":\"Lax\",\"Secure\":False}"))
                .addToEnv(labEnv)
                .addNewVolumeMount()
                .withMountPath("/data/scripts")
                .withName(STARTUP_DIRECTORY)
                .endVolumeMount()
                .addNewVolumeMount()
                .withMountPath("/opt/conda/bin/pip")
                .withName(PIP_WRAPPER_VOLUME)
                .withSubPath("pip")
                .endVolumeMount()
                .addNewVolumeMount()
                .withMountPath("/opt/conda/bin/swirrl-conda")
                .withName(CONDA_WRAPPER_VOLUME)
                .withSubPath("swirrl-conda")
                .endVolumeMount()
                .addNewVolumeMount()
                .withMountPath("/data/env_vars")
                .withName(ENVVAR_VOLUME)
                .endVolumeMount()
                .withResources(resourceRequirements)
                .endContainer()
                .addNewVolume()
                .withName(STARTUP_DIRECTORY)
                .withNewConfigMap()
                .withName(configmapName)
                .withDefaultMode(0644)
                .withItems(keyToPathList)
                .endConfigMap()
                .endVolume()
                .addNewVolume()
                .withName(PIP_WRAPPER_VOLUME)
                .withNewConfigMap()
                .withName(pipWrapperConfigmapName)
                .withDefaultMode(0555)
                .endConfigMap()
                .endVolume()
                .addNewVolume()
                .withName(CONDA_WRAPPER_VOLUME)
                .withNewConfigMap()
                .withName(condaWrapperConfigmapName)
                .withDefaultMode(0555)
                .endConfigMap()
                .endVolume()
                .addNewVolume()
                .withName(ENVVAR_VOLUME)
                .withNewConfigMap()
                .withName(envVarConfigmapName)
                .withDefaultMode(0555)
                .endConfigMap()
                .endVolume()
                .endSpec()
                .build();

        return deployment;
    }
}
