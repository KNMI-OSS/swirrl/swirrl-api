package org.openapitools.v1_0.model.kubernetes;

import java.util.List;
import java.util.Map;
import java.util.Objects;

import io.kubernetes.client.openapi.models.*;

public class NotebookDeployment extends ServiceDeployment {

    private V1ConfigMap configmap;
    private V1ConfigMap pipWrapperConfigMap;
    private V1ConfigMap condaWrapperConfigMap;
    private V1ConfigMap envVarConfigMap;
    protected NotebookDeployment(V1ConfigMap configmap, V1ConfigMap pipWrapperConfigMap, V1ConfigMap condaWrapperConfigMap,
                                 V1ConfigMap envVarConfigMap, V1PersistentVolumeClaim workingdirPvc,
                                 V1PersistentVolumeClaim datadirPvc, V1PersistentVolume workingdirPv,
                                 V1PersistentVolume datadirPv, V1Pod deployment, V1Service service,
                                 Map<String, List<V1HTTPIngressPath>> ingress, String appName, Map<String, String> appLabels) {
        super(workingdirPvc, datadirPvc, workingdirPv, datadirPv, deployment, service, ingress, appName, appLabels);
        this.configmap = configmap;
        this.pipWrapperConfigMap = pipWrapperConfigMap;
        this.condaWrapperConfigMap = condaWrapperConfigMap;
        this.envVarConfigMap = envVarConfigMap;
    }

    @Override
    public String toString() {
        return "NotebookDeployment [configmap=" + configmap +  ", pipWrapperConfigMap=" + pipWrapperConfigMap
                + ", workingdirPvc=" + this.getWorkingdirPvc() + ", datadirPvc=" + this.getDatadirPvc() + ", deployment="
                + this.getDeployment() + ", service=" + this.getService() + ", ingress=" + this.getIngress() + "]";
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;

        NotebookDeployment other = (NotebookDeployment) obj;
        return    super.equals(obj) && Objects.equals(configmap, other.configmap) &&
                Objects.equals(pipWrapperConfigMap, other.pipWrapperConfigMap) &&
                Objects.equals(this.getWorkingdirPvc(), other.getWorkingdirPvc()) &&
                Objects.equals(this.getDatadirPvc(), other.getDatadirPvc()) &&
                Objects.equals(this.getDeployment(), other.getDeployment()) &&
                Objects.equals(this.getService(), other.getService()) &&
                Objects.equals(this.getIngress(), other.getIngress());
    }

    public V1ConfigMap getConfigmap() {
        return configmap;
    }
    public void setConfigmap(V1ConfigMap configmap) {
        this.configmap = configmap;
    }
    public V1ConfigMap getPipWrapperConfigMap() {
        return pipWrapperConfigMap;
    }
    public void setPipWrapperConfigMap(V1ConfigMap pipWrapperConfigMap) {
        this.pipWrapperConfigMap = pipWrapperConfigMap;
    }
    public V1ConfigMap getCondaWrapperConfigMap() {
        return condaWrapperConfigMap;
    }

    public void setCondaWrapperConfigMap(V1ConfigMap condaWrapperConfigMap) {
        this.condaWrapperConfigMap = condaWrapperConfigMap;
    }

    public V1ConfigMap getEnvVarConfigMap() {
        return envVarConfigMap;
    }

    public void setEnvVarConfigMap(V1ConfigMap envVarConfigMap) {
        this.envVarConfigMap = envVarConfigMap;
    }
}
