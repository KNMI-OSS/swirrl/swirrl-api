package org.openapitools.v1_0.model.kubernetes;

import org.openapitools.v1_0.model.Viewer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Map;
import java.util.Properties;

public abstract class ViewerDeploymentFactory extends ServiceDeploymentFactory {

    private static Logger logger = LoggerFactory.getLogger(ViewerDeploymentFactory.class);

    protected Map<String, String> viewerImageTagMap;

    public ViewerDeploymentFactory() {
        super();
    }
    public abstract ServiceDeployment makeViewerDeployment(Viewer viewer);

    protected String determineImageTag(String viewerName) {
        try {
            Properties properties = new Properties();
            properties.load(new FileInputStream("/home/swirrl/viewer/viewer-images.properties"));
            String property = String.format("%s.viewer.docker.image.tag", viewerName);
            logger.debug("Getting viewer image {} from tagmap in viewer-images properties: {}", viewerName, properties.getProperty(property));
            return properties.getProperty(property);
        } catch (FileNotFoundException e) {
            logger.debug("Getting viewer image {} from tagmap in application properties.", viewerName);
            return viewerImageTagMap.get(viewerName);
        } catch (IOException e) {
            e.printStackTrace();
            return viewerImageTagMap.get(viewerName);
        }
    }
}
