package org.openapitools.v1_0.model.kubernetes;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class WorkflowDeploymentFactory {

	private static Logger logger = LoggerFactory.getLogger(WorkflowDeploymentFactory.class);

    public final static String WORKFLOW_PREFIX = "cwl";
    public final static String WORKFLOW_SESSION_ID_LABEL = ServiceDeploymentFactory.SESSION_ID_LABEL+"-workflow";

    private Map<String, WorkflowConfigMapBuilder> wfConfigMaps = Map.of(
            "download", new DownloadWorkflowConfigMapBuilder(),
            "opendap", new OpendapWorkflowConfigMapBuilder());

    public static String determineAppName(String sessionId, String workflowName) {
        return WORKFLOW_PREFIX + "-" + sessionId;
    }

    public static String determineWorkflowSessionLabel(String sessionId) {
        return WORKFLOW_SESSION_ID_LABEL + "=" + sessionId;
    }
	
}
