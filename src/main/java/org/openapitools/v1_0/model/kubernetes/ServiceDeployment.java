package org.openapitools.v1_0.model.kubernetes;

import io.kubernetes.client.openapi.models.*;

import java.util.List;
import java.util.Map;
import java.util.Objects;

public class ServiceDeployment {

    private V1PersistentVolumeClaim workingdirPvc;
    private V1PersistentVolumeClaim datadirPvc;
    private V1PersistentVolume workingdirPv;
    private V1PersistentVolume datadirPv;
    private V1Pod deployment;
    private V1Service service;
    private Map<String, List<V1HTTPIngressPath>> ingress;
    private String appName;
    private Map<String, String> appLabels;

    protected ServiceDeployment(V1PersistentVolumeClaim workingdirPvc, V1PersistentVolumeClaim datadirPvc,
                                V1PersistentVolume workingdirPv, V1PersistentVolume datadirPv,
                                V1Pod deployment, V1Service service, Map<String, List<V1HTTPIngressPath>> ingress,
                                String appName, Map<String, String> appLabels) {
        this.workingdirPvc = workingdirPvc;
        this.datadirPvc = datadirPvc;
        this.workingdirPv = workingdirPv;
        this.datadirPv = datadirPv;
        this.deployment = deployment;
        this.service = service;
        this.ingress = ingress;
        this.appName = appName;
        this.appLabels = appLabels;
    }

    @Override
    public String toString() {
        return "ServiceDeployment [workingdirPvc=" + workingdirPvc + ", datadirPvc=" + datadirPvc + ", workingdirPv=" + workingdirPv + ", datadirPv=" + datadirPv + ", deployment="
                + deployment + ", service=" + service + ", ingress=" + ingress + ", appName=" + appName +
                ", appLabels=" + appLabels + "]";
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;

        ServiceDeployment other = (ServiceDeployment) obj;
        return
                Objects.equals(workingdirPvc, other.workingdirPvc) &&
                Objects.equals(datadirPvc, other.datadirPvc) &&
                Objects.equals(deployment, other.deployment) &&
                Objects.equals(service, other.service) &&
                Objects.equals(ingress, other.ingress) &&
                Objects.equals(appName, other.appName) &&
                Objects.equals(appLabels, other.appLabels);
    }
    public V1PersistentVolume getWorkingdirPv() {
        return workingdirPv;
    }
    public void setWorkingdirPv(V1PersistentVolume workingdirPv) {
        this.workingdirPv = workingdirPv;
    }
    public V1PersistentVolume getDatadirPv() {
        return datadirPv;
    }
    public void setDatadirPv(V1PersistentVolume datadirPv) {
        this.datadirPv = datadirPv;
    }
    public V1PersistentVolumeClaim getWorkingdirPvc() {
        return workingdirPvc;
    }
    public void setWorkingdirPvc(V1PersistentVolumeClaim workingdirPvc) {
        this.workingdirPvc = workingdirPvc;
    }
    public V1PersistentVolumeClaim getDatadirPvc() {
        return datadirPvc;
    }
    public void setDatadirPvc(V1PersistentVolumeClaim datadirPvc) {
        this.datadirPvc = datadirPvc;
    }
    public V1Pod getDeployment() {
        return deployment;
    }
    public void setDeployment(V1Pod deployment) {
        this.deployment = deployment;
    }
    public V1Service getService() {
        return service;
    }
    public void setService(V1Service service) {
        this.service = service;
    }
    public Map<String, List<V1HTTPIngressPath>> getIngress() {
        return ingress;
    }
    public void setIngress(Map<String, List<V1HTTPIngressPath>> ingress) {
        this.ingress = ingress;
    }
    public String getAppName() {
        return appName;
    }
    public void setAppName(String appName) {
        this.appName = appName;
    }
    public Map<String, String> getAppLabels() {
        return appLabels;
    }
    public void setAppLabels(Map<String, String> appLabels) {
        this.appLabels = appLabels;
    }
}
