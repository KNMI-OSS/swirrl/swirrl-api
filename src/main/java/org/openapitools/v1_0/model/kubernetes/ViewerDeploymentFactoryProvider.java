package org.openapitools.v1_0.model.kubernetes;

import org.openapitools.v1_0.model.Viewer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ViewerDeploymentFactoryProvider {
    @Autowired
    private AdagucDeploymentFactory adagucDeploymentFactory;
    @Autowired // necessary to initialize the @Value annotated members preventing PVC size being null for instance.
    private EnlightenDeploymentFactory enlightenDeploymentFactory;
    public ViewerDeploymentFactory getFactory(Viewer viewer) {
        if ("adaguc".equalsIgnoreCase(viewer.getServiceType()))
            return adagucDeploymentFactory;
        if ("enlighten".equalsIgnoreCase(viewer.getServiceType()))
            return enlightenDeploymentFactory;
        return null;
    }
}
