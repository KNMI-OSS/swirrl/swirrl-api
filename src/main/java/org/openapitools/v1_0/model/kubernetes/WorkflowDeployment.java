package org.openapitools.v1_0.model.kubernetes;

import io.kubernetes.client.openapi.models.V1ConfigMap;
import io.kubernetes.client.openapi.models.V1Job;
import io.kubernetes.client.openapi.models.V1PersistentVolumeClaim;

import java.util.Objects;

public class WorkflowDeployment {

    private V1ConfigMap inputsConfigMap;
    private V1ConfigMap workflowConfigMap;
    private V1PersistentVolumeClaim datadirPvc;
    private V1Job job;

    protected WorkflowDeployment(V1ConfigMap inputsConfigMap, V1ConfigMap workflowConfigMap,
                                 V1PersistentVolumeClaim datadirPvc, V1Job job) {
        super();
        this.inputsConfigMap = inputsConfigMap;
        this.workflowConfigMap = workflowConfigMap;
        this.datadirPvc = datadirPvc;
        this.job = job;
    }

    @Override
    public String toString() {
        return "WorkflowDeployment [inputsConfigMap=" + inputsConfigMap + ", workflowConfigMap=" + workflowConfigMap
                + ", datadirPvc=" + datadirPvc + ", job=" + job + "]";
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;

        WorkflowDeployment other = (WorkflowDeployment) obj;
        return Objects.equals(inputsConfigMap, other.inputsConfigMap) &&
                Objects.equals(workflowConfigMap, other.workflowConfigMap) &&
                Objects.equals(datadirPvc, other.datadirPvc) &&
                Objects.equals(job, other.job);
    }

    public V1ConfigMap getInputsConfigMap() {
        return inputsConfigMap;
    }
    public void setInputsConfigMap(V1ConfigMap inputsConfigMap) {
        this.inputsConfigMap = inputsConfigMap;
    }
    public V1ConfigMap getWorkflowConfigMap() {
        return workflowConfigMap;
    }
    public void setWorkflowConfigMap(V1ConfigMap workflowConfigMap) {
        this.workflowConfigMap = workflowConfigMap;
    }
    public V1PersistentVolumeClaim getDatadirPvc() {
        return datadirPvc;
    }
    public void setDatadirPvc(V1PersistentVolumeClaim datadirPvc) {
        this.datadirPvc = datadirPvc;
    }
    public V1Job getJob() {
        return job;
    }
    public void setJob(V1Job job) {
        this.job = job;
    }



}


