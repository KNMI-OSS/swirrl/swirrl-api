package org.openapitools.v1_0.model.kubernetes;

import io.kubernetes.client.custom.IntOrString;
import io.kubernetes.client.custom.Quantity;
import io.kubernetes.client.openapi.models.*;
import org.openapitools.v1_0.model.Viewer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

@Service
public class AdagucDeploymentFactory extends ViewerDeploymentFactory {

    private static Logger logger = LoggerFactory.getLogger(AdagucDeploymentFactory.class);

    private final String ADAGUC_DATA_PATH = "/data/adaguc-autowms";

    public AdagucDeploymentFactory(@Value("#{${viewer.docker.image.tagmap}}")Map<String, String> viewerImageTagMap) {
        this.viewerImageTagMap = viewerImageTagMap;
    }

    @Override
    public String getServiceType() {
        return "adaguc";
    }

    @Override
    public ServiceDeployment makeViewerDeployment(Viewer viewer) {
        viewer.setServiceURL(viewer.getServiceURL() + "adaguc-viewer/");
        return this.makeAdagucDeployment(viewer);
    }

    private ServiceDeployment makeAdagucDeployment(Viewer adaguc) {

        ServiceDeployment serviceDeployment = this.makeServiceDeployment(adaguc);

        if (serviceDeployment == null) {
            logger.error("Impossible to prepare the deployment of the service.");
            return null;
        }

        V1Pod deployment = buildAdagucDeployment(
                serviceDeployment.getDeployment(),
                adaguc.getServiceURL());


        V1Service service = buildAdagucService(
                serviceDeployment.getService());

        Map<String, List<V1HTTPIngressPath>> ingressMap = buildIngressMap(serviceDeployment.getIngress());

        serviceDeployment.setIngress(ingressMap);
        serviceDeployment.setDeployment(deployment);
        serviceDeployment.setService(service);

        return serviceDeployment;
    }

    private Map<String, List<V1HTTPIngressPath>> buildIngressMap(Map<String, List<V1HTTPIngressPath>> ingressMap) {
        V1HTTPIngressPath ingressPath = ingressMap.get("default").get(0);
        V1HTTPIngressPath viewerIngress = new V1HTTPIngressPathBuilder(ingressPath)
                .withNewPath(ingressPath.getPath() + "(/|$)(adaguc-viewer/.*)")
                .build();
        V1HTTPIngressPath serverIngress = new V1HTTPIngressPathBuilder(ingressPath)
                .withNewPath(ingressPath.getPath() + "/adaguc-viewer/adaguc-server(/|$)(.*)")
                .editBackend()
                .editService()
                .editPort()
                .withNumber(8080)
                .endPort()
                .endService()
                .endBackend()
                .build();

        ingressMap.remove("default");
        ingressMap.put("rewrite", new ArrayList<>());
        ingressMap.get("rewrite").add(viewerIngress);
        ingressMap.get("rewrite").add(serverIngress);

        return ingressMap;
    }

    private V1Pod buildAdagucDeployment(V1Pod deployment,
                                                 String serviceUrl) {
        deployment = new V1PodBuilder(deployment)
                .editSpec()
                .removeMatchingFromInitContainers(item -> item.getName().equals("service-init-container"))
                .removeMatchingFromContainers(item -> getServiceType().equals(item.getName()))
                .addNewContainer()
                .withName("adaguc-viewer")
                .withImage(this.determineImageTag("adagucviewer"))
                .addNewPort()
                .withContainerPort(80)
                .endPort()
                .addNewEnv()
                .withName("LOCAL_ADAGUCSERVER_ADDR")
                .withValue(serviceUrl + "adaguc-server")
                .endEnv()
                .addNewEnv()
                .withName("REMOTE_ADAGUCSERVER_ADDR")
                .withValue("http://localhost:8080")
                .endEnv()
                .addNewEnv()
                .withName("ADAGUCSERVICES_AUTOWMS")
                .withValue(serviceUrl + "adaguc-server/autowms?")
                .endEnv()
                .withNewReadinessProbe()
                .withNewTcpSocket()
                .withPort(new IntOrString(80))
                .endTcpSocket()
                .withInitialDelaySeconds(1)
                .withPeriodSeconds(1)
                .endReadinessProbe()
                .withNewResources()
                .withRequests(Map.of("memory", Quantity.fromString("4000M"), "cpu",  Quantity.fromString("1000m")))
                .withLimits(Map.of("memory", Quantity.fromString("4000M"), "cpu",  Quantity.fromString("1000m")))
                .endResources()
                .endContainer()
                .addNewContainer()
                .withName("my-adaguc-server")
                .withImage(this.determineImageTag("adagucserver"))
                .addNewPort()
                .withContainerPort(8080)
                .endPort()
                .addNewVolumeMount()
                .withName("data")
                .withMountPath("/data/adaguc-autowms/data")
                .endVolumeMount()
                .addNewVolumeMount()
                .withName("working-directory")
                .withMountPath("/data/adaguc-autowms/work")
                .endVolumeMount()
                .addNewEnv()
                .withName("EXTERNALADDRESS")
                .withValue(serviceUrl + "adaguc-server")
                .endEnv()
                .addNewEnv()
                .withName("ADAGUC_DB")
                .withValue("host=localhost port=5432 user=adaguc password=adaguc dbname=adaguc")
                .endEnv()
                .addNewEnv()
                .withName("ADAGUC_ENABLELOGBUFFER")
                .withValue("TRUE")
                .endEnv()
                .addNewEnv()
                .withName("ADAGUC_AUTOWMS_DIR")
                .withValue("/data/adaguc-autowms")
                .endEnv()
                .addNewEnv()
                .withName("ADAGUC_DATA_DIR")
                .withValue("/data/adaguc-data")
                .endEnv()
                .addNewEnv()
                .withName("ADAGUC_DATASET_DIR")
                .withValue("/data/adaguc-datasets")
                .endEnv()
                .withNewReadinessProbe()
                .withNewTcpSocket()
                .withPort(new IntOrString(8080))
                .endTcpSocket()
                .withInitialDelaySeconds(1)
                .withPeriodSeconds(1)
                .endReadinessProbe()
                .withNewResources()
                .withRequests(Map.of("memory", Quantity.fromString("4000M"), "cpu",  Quantity.fromString("1000m")))
                .withLimits(Map.of("memory", Quantity.fromString("4000M"), "cpu",  Quantity.fromString("1000m")))
                .endResources()
                .endContainer()
                .addNewContainer()
                .withName("adaguc-db")
                .withImage(this.determineImageTag("adagucdatabase"))
                .addNewPort()
                .withContainerPort(5432)
                .endPort()
                .addNewEnv()
                .withName("POSTGRES_USER")
                .withValue("adaguc")
                .endEnv()
                .addNewEnv()
                .withName("POSTGRES_PASSWORD")
                .withValue("adaguc")
                .endEnv()
                .addNewEnv()
                .withName("POSTGRES_DB")
                .withValue("adaguc")
                .endEnv()
                .withNewReadinessProbe()
                .withNewTcpSocket()
                .withPort(new IntOrString(5432))
                .endTcpSocket()
                .withInitialDelaySeconds(1)
                .withPeriodSeconds(1)
                .endReadinessProbe()
                .withNewResources()
                .withRequests(Map.of("memory", Quantity.fromString("4000M"), "cpu",  Quantity.fromString("1000m")))
                .withLimits(Map.of("memory", Quantity.fromString("4000M"), "cpu",  Quantity.fromString("1000m")))
                .endResources()
                .endContainer()
                .endSpec()
                .build();

        return deployment;
    }

    private V1Service buildAdagucService(V1Service service) {

        service = new V1ServiceBuilder(service)
                .editSpec()
                .editFirstPort()
                .withNewTargetPort(80)
                .withName("viewer-port")
                .endPort()
                .addNewPort()
                .withProtocol("TCP")
                .withPort(8080)
                .withTargetPort(new IntOrString(8080))
                .withName("server-port")
                .endPort()
                .endSpec()
                .build();

        return service;
    }
}
