package org.openapitools.v1_0.model.kubernetes;

import io.kubernetes.client.openapi.models.V1Job;

public class CreateSnapshotJob {
    private V1Job job;
    private String serviceId;
    private String poolId;
    private String sessionId;
    private String notebookImageTag;

    public CreateSnapshotJob(String sessionId, String poolId, String serviceId, V1Job job, String notebookImageTag) {
        this.sessionId = sessionId;
        this.poolId = poolId;
        this.serviceId = serviceId;
        this.job = job;
        this.notebookImageTag = notebookImageTag;
    }

    public V1Job getJob() {
        return job;
    }

    public void setJob(V1Job job) {
        this.job = job;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getPoolId() {
        return poolId;
    }

    public void setPoolId(String poolId) {
        this.poolId = poolId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }
}
