package org.openapitools.v1_0.model.kubernetes;

import io.kubernetes.client.openapi.models.V1ConfigMap;
import io.kubernetes.client.openapi.models.V1ConfigMapBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.util.StreamUtils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Map;

public class DownloadWorkflowConfigMapBuilder implements WorkflowConfigMapBuilder {
    private static Logger logger = LoggerFactory.getLogger(DownloadWorkflowConfigMapBuilder.class);
    private static String workflowName = "download";

    @Override
    public V1ConfigMap build(String sessionId, Map<String, String> appLabels) {
        String workflowConfigMapName = "cwl-configmap-" + workflowName + "workflow-" + sessionId;
        V1ConfigMap workflowConfigMap = null;
        try {
            String scatterWorkflow = StreamUtils.copyToString(
                    new ClassPathResource("/k8s-specs/workflow/download/accept_url_array_scatter.cwl").getInputStream(),
                    StandardCharsets.UTF_8);
            String singleWorkflow = StreamUtils.copyToString(
                    new ClassPathResource("/k8s-specs/workflow/download/accept_single_url_curl.cwl").getInputStream(),
                    StandardCharsets.UTF_8);
            workflowConfigMap = new V1ConfigMapBuilder()
                    .withNewMetadata().withName(workflowConfigMapName).withLabels(appLabels).endMetadata()
                    .withData(Map.of("accept_url_array_scatter.cwl", scatterWorkflow))
                    .addToData(Map.of("accept_single_url_curl.cwl", singleWorkflow))
                    .build();
        } catch (IOException e) {
            logger.error("Exception occurred whilst reading yaml file.",e);
            return null;
        }
        return workflowConfigMap;
    }
}
