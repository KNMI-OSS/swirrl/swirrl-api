package org.openapitools.v1_0.model.kubernetes;

import io.kubernetes.client.openapi.models.V1ConfigMap;
import io.kubernetes.client.openapi.models.V1ConfigMapBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.util.StreamUtils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Map;

public class OpendapWorkflowConfigMapBuilder implements WorkflowConfigMapBuilder {
    private static Logger logger = LoggerFactory.getLogger(OpendapWorkflowConfigMapBuilder.class);
    private static String workflowName = "opendap";

    @Override
    public V1ConfigMap build(String sessionId, Map<String, String> appLabels) {
        String workflowConfigMapName = "cwl-configmap-" + workflowName + "workflow-" + sessionId;
        V1ConfigMap workflowConfigMap = null;
        try {
            String mainCwl = StreamUtils.copyToString(
                    new ClassPathResource("/k8s-specs/workflow/opendap/job.cwl").getInputStream(),
                    StandardCharsets.UTF_8);
            String opendapCwl = StreamUtils.copyToString(
                    new ClassPathResource("/k8s-specs/workflow/opendap/subset_and_download.cwl").getInputStream(),
                    StandardCharsets.UTF_8);
            String filelistCwl = StreamUtils.copyToString(
                    new ClassPathResource("/k8s-specs/workflow/opendap/urllist_to_file.cwl").getInputStream(),
                    StandardCharsets.UTF_8);
            workflowConfigMap = new V1ConfigMapBuilder()
                    .withNewMetadata().withName(workflowConfigMapName).withLabels(appLabels).endMetadata()
                    .withData(Map.of("job.cwl", mainCwl))
                    .addToData(Map.of("subset_and_download.cwl", opendapCwl))
                    .addToData(Map.of("urllist_to_file.cwl", filelistCwl))
                    .build();
        } catch (IOException e) {
            logger.error("Exception occurred whilst reading yaml file.",e);
            return null;
        }
        return workflowConfigMap;
    }
}
