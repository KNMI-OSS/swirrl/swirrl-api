package org.openapitools.v1_0.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * GithubAccessToken
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2023-09-15T12:23:18.056Z[GMT]")

public class GithubAccessToken   {
  @JsonProperty("access_token")
  private String accessToken = null;

  @JsonProperty("scope")
  private String scope = null;

  @JsonProperty("token_type")
  private String tokenType = null;

  public GithubAccessToken accessToken(String accessToken) {
    this.accessToken = accessToken;
    return this;
  }

  /**
   * The access token from github
   * @return accessToken
  **/
  @ApiModelProperty(example = "xxxxxxxxxxxxxxxxxxxx", value = "The access token from github")


  public String getAccessToken() {
    return accessToken;
  }

  public void setAccessToken(String accessToken) {
    this.accessToken = accessToken;
  }

  public GithubAccessToken scope(String scope) {
    this.scope = scope;
    return this;
  }

  /**
   * The scope of the token from github
   * @return scope
  **/
  @ApiModelProperty(example = "repo", value = "The scope of the token from github")


  public String getScope() {
    return scope;
  }

  public void setScope(String scope) {
    this.scope = scope;
  }

  public GithubAccessToken tokenType(String tokenType) {
    this.tokenType = tokenType;
    return this;
  }

  /**
   * The type of token from github
   * @return tokenType
  **/
  @ApiModelProperty(example = "bearer", value = "The type of token from github")


  public String getTokenType() {
    return tokenType;
  }

  public void setTokenType(String tokenType) {
    this.tokenType = tokenType;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    GithubAccessToken githubAccessToken = (GithubAccessToken) o;
    return Objects.equals(this.accessToken, githubAccessToken.accessToken) &&
        Objects.equals(this.scope, githubAccessToken.scope) &&
        Objects.equals(this.tokenType, githubAccessToken.tokenType);
  }

  @Override
  public int hashCode() {
    return Objects.hash(accessToken, scope, tokenType);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class GithubAccessToken {\n");
    
    sb.append("    accessToken: ").append(toIndentedString(accessToken)).append("\n");
    sb.append("    scope: ").append(toIndentedString(scope)).append("\n");
    sb.append("    tokenType: ").append(toIndentedString(tokenType)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

