package org.openapitools.v1_0.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * SnapshotURL
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2023-09-15T12:23:18.056Z[GMT]")

public class SnapshotURL   {
  @JsonProperty("snapshotURL")
  private String snapshotURL = null;

  public SnapshotURL snapshotURL(String snapshotURL) {
    this.snapshotURL = snapshotURL;
    return this;
  }

  /**
   * Url that points to snapshot git repo
   * @return snapshotURL
  **/
  @ApiModelProperty(example = "www.gitlab.com", value = "Url that points to snapshot git repo")


  public String getSnapshotURL() {
    return snapshotURL;
  }

  public void setSnapshotURL(String snapshotURL) {
    this.snapshotURL = snapshotURL;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    SnapshotURL snapshotURL = (SnapshotURL) o;
    return Objects.equals(this.snapshotURL, snapshotURL.snapshotURL);
  }

  @Override
  public int hashCode() {
    return Objects.hash(snapshotURL);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class SnapshotURL {\n");
    
    sb.append("    snapshotURL: ").append(toIndentedString(snapshotURL)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

