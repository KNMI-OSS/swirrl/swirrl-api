package org.openapitools.v1_0.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.openapitools.v1_0.model.Notebook;
import org.openapitools.v1_0.model.NotebookLibrary;
import org.openapitools.v1_0.model.Volume;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * ExtendedNotebook
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2023-09-15T12:23:18.056Z[GMT]")

public class ExtendedNotebook extends Notebook  {
  @JsonProperty("notebookStatus")
  private String notebookStatus = null;

  @JsonProperty("volumes")
  @Valid
  private List<Volume> volumes = null;

  @JsonProperty("installedLibraries")
  @Valid
  private List<NotebookLibrary> installedLibraries = null;

  public ExtendedNotebook notebookStatus(String notebookStatus) {
    this.notebookStatus = notebookStatus;
    return this;
  }

  /**
   * Current status of the notebook.
   * @return notebookStatus
  **/
  @ApiModelProperty(value = "Current status of the notebook.")


  public String getNotebookStatus() {
    return notebookStatus;
  }

  public void setNotebookStatus(String notebookStatus) {
    this.notebookStatus = notebookStatus;
  }

  public ExtendedNotebook volumes(List<Volume> volumes) {
    this.volumes = volumes;
    return this;
  }

  public ExtendedNotebook addVolumesItem(Volume volumesItem) {
    if (this.volumes == null) {
      this.volumes = new ArrayList<>();
    }
    this.volumes.add(volumesItem);
    return this;
  }

  /**
   * Get volumes
   * @return volumes
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<Volume> getVolumes() {
    return volumes;
  }

  public void setVolumes(List<Volume> volumes) {
    this.volumes = volumes;
  }

  public ExtendedNotebook installedLibraries(List<NotebookLibrary> installedLibraries) {
    this.installedLibraries = installedLibraries;
    return this;
  }

  public ExtendedNotebook addInstalledLibrariesItem(NotebookLibrary installedLibrariesItem) {
    if (this.installedLibraries == null) {
      this.installedLibraries = new ArrayList<>();
    }
    this.installedLibraries.add(installedLibrariesItem);
    return this;
  }

  /**
   * All the pip installed python libraries which are installed on the notebook.
   * @return installedLibraries
  **/
  @ApiModelProperty(readOnly = true, value = "All the pip installed python libraries which are installed on the notebook.")

  @Valid

  public List<NotebookLibrary> getInstalledLibraries() {
    return installedLibraries;
  }

  public void setInstalledLibraries(List<NotebookLibrary> installedLibraries) {
    this.installedLibraries = installedLibraries;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ExtendedNotebook extendedNotebook = (ExtendedNotebook) o;
    return Objects.equals(this.notebookStatus, extendedNotebook.notebookStatus) &&
        Objects.equals(this.volumes, extendedNotebook.volumes) &&
        Objects.equals(this.installedLibraries, extendedNotebook.installedLibraries) &&
        super.equals(o);
  }

  @Override
  public int hashCode() {
    return Objects.hash(notebookStatus, volumes, installedLibraries, super.hashCode());
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ExtendedNotebook {\n");
    sb.append("    ").append(toIndentedString(super.toString())).append("\n");
    sb.append("    notebookStatus: ").append(toIndentedString(notebookStatus)).append("\n");
    sb.append("    volumes: ").append(toIndentedString(volumes)).append("\n");
    sb.append("    installedLibraries: ").append(toIndentedString(installedLibraries)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

