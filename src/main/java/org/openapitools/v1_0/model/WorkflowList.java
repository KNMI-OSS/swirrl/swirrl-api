package org.openapitools.v1_0.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.openapitools.v1_0.model.WorkflowItem;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * A list of all the workflows sorted by type 
 */
@ApiModel(description = "A list of all the workflows sorted by type ")
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2023-12-28T15:20:09.036Z[GMT]")

public class WorkflowList   {
  @JsonProperty("integratedWorkflows")
  @Valid
  private List<WorkflowItem> integratedWorkflows = null;

  @JsonProperty("customWorkflows")
  @Valid
  private List<WorkflowItem> customWorkflows = null;

  public WorkflowList integratedWorkflows(List<WorkflowItem> integratedWorkflows) {
    this.integratedWorkflows = integratedWorkflows;
    return this;
  }

  public WorkflowList addIntegratedWorkflowsItem(WorkflowItem integratedWorkflowsItem) {
    if (this.integratedWorkflows == null) {
      this.integratedWorkflows = new ArrayList<>();
    }
    this.integratedWorkflows.add(integratedWorkflowsItem);
    return this;
  }

  /**
   * Get integratedWorkflows
   * @return integratedWorkflows
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<WorkflowItem> getIntegratedWorkflows() {
    return integratedWorkflows;
  }

  public void setIntegratedWorkflows(List<WorkflowItem> integratedWorkflows) {
    this.integratedWorkflows = integratedWorkflows;
  }

  public WorkflowList customWorkflows(List<WorkflowItem> customWorkflows) {
    this.customWorkflows = customWorkflows;
    return this;
  }

  public WorkflowList addCustomWorkflowsItem(WorkflowItem customWorkflowsItem) {
    if (this.customWorkflows == null) {
      this.customWorkflows = new ArrayList<>();
    }
    this.customWorkflows.add(customWorkflowsItem);
    return this;
  }

  /**
   * Get customWorkflows
   * @return customWorkflows
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<WorkflowItem> getCustomWorkflows() {
    return customWorkflows;
  }

  public void setCustomWorkflows(List<WorkflowItem> customWorkflows) {
    this.customWorkflows = customWorkflows;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    WorkflowList workflowList = (WorkflowList) o;
    return Objects.equals(this.integratedWorkflows, workflowList.integratedWorkflows) &&
        Objects.equals(this.customWorkflows, workflowList.customWorkflows);
  }

  @Override
  public int hashCode() {
    return Objects.hash(integratedWorkflows, customWorkflows);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class WorkflowList {\n");
    
    sb.append("    integratedWorkflows: ").append(toIndentedString(integratedWorkflows)).append("\n");
    sb.append("    customWorkflows: ").append(toIndentedString(customWorkflows)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

