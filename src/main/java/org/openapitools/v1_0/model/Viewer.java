package org.openapitools.v1_0.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.openapitools.v1_0.model.Service;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Viewer
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2023-09-15T12:23:18.056Z[GMT]")

public class Viewer extends Service  {
  @JsonProperty("serviceType")
  private String serviceType = null;

  public Viewer serviceType(String serviceType) {
    this.serviceType = serviceType;
    return this;
  }

  /**
   * The type of viewer
   * @return serviceType
  **/
  @ApiModelProperty(value = "The type of viewer")


  public String getServiceType() {
    return serviceType;
  }

  public void setServiceType(String serviceType) {
    this.serviceType = serviceType;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Viewer viewer = (Viewer) o;
    return Objects.equals(this.serviceType, viewer.serviceType) &&
        super.equals(o);
  }

  @Override
  public int hashCode() {
    return Objects.hash(serviceType, super.hashCode());
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Viewer {\n");
    sb.append("    ").append(toIndentedString(super.toString())).append("\n");
    sb.append("    serviceType: ").append(toIndentedString(serviceType)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

