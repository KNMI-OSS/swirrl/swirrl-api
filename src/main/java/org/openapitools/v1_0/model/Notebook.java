package org.openapitools.v1_0.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.openapitools.v1_0.model.NotebookLibrary;
import org.openapitools.v1_0.model.Service;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Notebook
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2023-09-15T12:23:18.056Z[GMT]")

public class Notebook extends Service  {
  @JsonProperty("userRequestedLibraries")
  @Valid
  private List<NotebookLibrary> userRequestedLibraries = null;

  public Notebook userRequestedLibraries(List<NotebookLibrary> userRequestedLibraries) {
    this.userRequestedLibraries = userRequestedLibraries;
    return this;
  }

  public Notebook addUserRequestedLibrariesItem(NotebookLibrary userRequestedLibrariesItem) {
    if (this.userRequestedLibraries == null) {
      this.userRequestedLibraries = new ArrayList<>();
    }
    this.userRequestedLibraries.add(userRequestedLibrariesItem);
    return this;
  }

  /**
   * The python libraries which are required by the user
   * @return userRequestedLibraries
  **/
  @ApiModelProperty(value = "The python libraries which are required by the user")

  @Valid

  public List<NotebookLibrary> getUserRequestedLibraries() {
    return userRequestedLibraries;
  }

  public void setUserRequestedLibraries(List<NotebookLibrary> userRequestedLibraries) {
    this.userRequestedLibraries = userRequestedLibraries;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Notebook notebook = (Notebook) o;
    return Objects.equals(this.userRequestedLibraries, notebook.userRequestedLibraries) &&
        super.equals(o);
  }

  @Override
  public int hashCode() {
    return Objects.hash(userRequestedLibraries, super.hashCode());
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Notebook {\n");
    sb.append("    ").append(toIndentedString(super.toString())).append("\n");
    sb.append("    userRequestedLibraries: ").append(toIndentedString(userRequestedLibraries)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

