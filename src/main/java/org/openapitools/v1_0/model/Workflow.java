package org.openapitools.v1_0.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Currently download, wow, knmidataplatform, opendap and rookwps workflow supported. The inputs parameter needs to be passed as a base64 encoded YAML string. 
 */
@ApiModel(description = "Currently download, wow, knmidataplatform, opendap and rookwps workflow supported. The inputs parameter needs to be passed as a base64 encoded YAML string. ")
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2023-09-15T12:23:18.056Z[GMT]")

public class Workflow   {
  @JsonProperty("sessionId")
  private String sessionId = null;

  @JsonProperty("inputs")
  private byte[] inputs = null;

  public Workflow sessionId(String sessionId) {
    this.sessionId = sessionId;
    return this;
  }

  /**
   * Get sessionId
   * @return sessionId
  **/
  @ApiModelProperty(example = "mysession", value = "")


  public String getSessionId() {
    return sessionId;
  }

  public void setSessionId(String sessionId) {
    this.sessionId = sessionId;
  }

  public Workflow inputs(byte[] inputs) {
    this.inputs = inputs;
    return this;
  }

  /**
   * This is a base64 encoded yaml file. The example encodes the input shown below, which is input for the \"download\" workflow. For an example of the opendap workflow see https://gitlab.com/KNMI-OSS/swirrl/swirrl-workflow/-/blob/master/workflows/opendap/example/inputs-time.yml, for wow see https://gitlab.com/KNMI-OSS/swirrl/swirrl-workflow/-/blob/master/workflows/wow/example/inputs.yml, for knmidataplatform see https://gitlab.com/KNMI-OSS/swirrl/swirrl-workflow/-/blob/master/workflows/kdp/example/inputs.yml and for rookwps see https://gitlab.com/KNMI-OSS/swirrl/swirrl-workflow/-/blob/master/workflows/rookwps/example/inputs.yml  name: 'Test download workflow' message: 'Download files from highly available cdnjs website'  links:   - url: 'https://cdnjs.cloudflare.com/ajax/libs/swagger-ui/4.1.2/swagger-ui.js'     filename: filename1.js   - url: 'https://cdnjs.cloudflare.com/ajax/libs/mocha/9.1.3/mocha.min.js'     filename: filename2.js   - url: 'https://cdnjs.cloudflare.com/ajax/libs/react/17.0.2/umd/react.production.min.js'     filename: filename3.js  fileout: 'test' 
   * @return inputs
  **/
  @ApiModelProperty(value = "This is a base64 encoded yaml file. The example encodes the input shown below, which is input for the \"download\" workflow. For an example of the opendap workflow see https://gitlab.com/KNMI-OSS/swirrl/swirrl-workflow/-/blob/master/workflows/opendap/example/inputs-time.yml, for wow see https://gitlab.com/KNMI-OSS/swirrl/swirrl-workflow/-/blob/master/workflows/wow/example/inputs.yml, for knmidataplatform see https://gitlab.com/KNMI-OSS/swirrl/swirrl-workflow/-/blob/master/workflows/kdp/example/inputs.yml and for rookwps see https://gitlab.com/KNMI-OSS/swirrl/swirrl-workflow/-/blob/master/workflows/rookwps/example/inputs.yml  name: 'Test download workflow' message: 'Download files from highly available cdnjs website'  links:   - url: 'https://cdnjs.cloudflare.com/ajax/libs/swagger-ui/4.1.2/swagger-ui.js'     filename: filename1.js   - url: 'https://cdnjs.cloudflare.com/ajax/libs/mocha/9.1.3/mocha.min.js'     filename: filename2.js   - url: 'https://cdnjs.cloudflare.com/ajax/libs/react/17.0.2/umd/react.production.min.js'     filename: filename3.js  fileout: 'test' ")


  public byte[] getInputs() {
    return inputs;
  }

  public void setInputs(byte[] inputs) {
    this.inputs = inputs;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Workflow workflow = (Workflow) o;
    return Objects.equals(this.sessionId, workflow.sessionId) &&
        Objects.equals(this.inputs, workflow.inputs);
  }

  @Override
  public int hashCode() {
    return Objects.hash(sessionId, inputs);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Workflow {\n");
    
    sb.append("    sessionId: ").append(toIndentedString(sessionId)).append("\n");
    sb.append("    inputs: ").append(toIndentedString(inputs)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

