package org.openapitools.v1_0.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * NotebookLibrary
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2023-09-15T12:23:18.056Z[GMT]")

public class NotebookLibrary   {
  @JsonProperty("libname")
  private String libname = null;

  @JsonProperty("libversion")
  private String libversion = null;

  @JsonProperty("source")
  private String source = null;

  public NotebookLibrary libname(String libname) {
    this.libname = libname;
    return this;
  }

  /**
   * Get libname
   * @return libname
  **/
  @ApiModelProperty(value = "")


  public String getLibname() {
    return libname;
  }

  public void setLibname(String libname) {
    this.libname = libname;
  }

  public NotebookLibrary libversion(String libversion) {
    this.libversion = libversion;
    return this;
  }

  /**
   * Get libversion
   * @return libversion
  **/
  @ApiModelProperty(value = "")


  public String getLibversion() {
    return libversion;
  }

  public void setLibversion(String libversion) {
    this.libversion = libversion;
  }

  public NotebookLibrary source(String source) {
    this.source = source;
    return this;
  }

  /**
   * Get source
   * @return source
  **/
  @ApiModelProperty(value = "")


  public String getSource() {
    return source;
  }

  public void setSource(String source) {
    this.source = source;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    NotebookLibrary notebookLibrary = (NotebookLibrary) o;
    return Objects.equals(this.libname, notebookLibrary.libname) &&
        Objects.equals(this.libversion, notebookLibrary.libversion) &&
        Objects.equals(this.source, notebookLibrary.source);
  }

  @Override
  public int hashCode() {
    return Objects.hash(libname, libversion, source);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class NotebookLibrary {\n");
    
    sb.append("    libname: ").append(toIndentedString(libname)).append("\n");
    sb.append("    libversion: ").append(toIndentedString(libversion)).append("\n");
    sb.append("    source: ").append(toIndentedString(source)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

