package org.openapitools.v1_0.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Service
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2023-09-15T12:23:18.056Z[GMT]")

public class Service   {
  @JsonProperty("sessionId")
  private String sessionId = null;

  @JsonProperty("poolId")
  private String poolId = null;

  @JsonProperty("id")
  private String id = null;

  @JsonProperty("serviceURL")
  private String serviceURL = null;

  public Service sessionId(String sessionId) {
    this.sessionId = sessionId;
    return this;
  }

  /**
   * Optional ID of an existing session. Data volume of this session will be attached to the service.
   * @return sessionId
  **/
  @ApiModelProperty(example = "string", value = "Optional ID of an existing session. Data volume of this session will be attached to the service.")


  public String getSessionId() {
    return sessionId;
  }

  public void setSessionId(String sessionId) {
    this.sessionId = sessionId;
  }

  public Service poolId(String poolId) {
    this.poolId = poolId;
    return this;
  }

  /**
   * Optional ID of an existing service pool. Work volume of this service pool will be attached to the service.
   * @return poolId
  **/
  @ApiModelProperty(example = "string", value = "Optional ID of an existing service pool. Work volume of this service pool will be attached to the service.")


  public String getPoolId() {
    return poolId;
  }

  public void setPoolId(String poolId) {
    this.poolId = poolId;
  }

  public Service id(String id) {
    this.id = id;
    return this;
  }

  /**
   * ID of a service
   * @return id
  **/
  @ApiModelProperty(readOnly = true, value = "ID of a service")


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public Service serviceURL(String serviceURL) {
    this.serviceURL = serviceURL;
    return this;
  }

  /**
   * The URL where the service will be running
   * @return serviceURL
  **/
  @ApiModelProperty(readOnly = true, value = "The URL where the service will be running")


  public String getServiceURL() {
    return serviceURL;
  }

  public void setServiceURL(String serviceURL) {
    this.serviceURL = serviceURL;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Service service = (Service) o;
    return Objects.equals(this.sessionId, service.sessionId) &&
        Objects.equals(this.poolId, service.poolId) &&
        Objects.equals(this.id, service.id) &&
        Objects.equals(this.serviceURL, service.serviceURL);
  }

  @Override
  public int hashCode() {
    return Objects.hash(sessionId, poolId, id, serviceURL);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Service {\n");
    
    sb.append("    sessionId: ").append(toIndentedString(sessionId)).append("\n");
    sb.append("    poolId: ").append(toIndentedString(poolId)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    serviceURL: ").append(toIndentedString(serviceURL)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

