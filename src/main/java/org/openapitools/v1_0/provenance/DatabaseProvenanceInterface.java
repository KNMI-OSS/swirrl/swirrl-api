package org.openapitools.v1_0.provenance;

import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;

import java.io.IOException;


public interface DatabaseProvenanceInterface {
    public ResponseEntity<?> createProvenance(Resource body) throws IOException;
    public ResponseEntity<?>getSessionActivity(String swirrlColonSessionId, String person, String poolId, String serviceId, String type, Boolean includeRuns, Boolean includeAdditionalInfo) throws JSONException;
    public ResponseEntity<?> getServiceSessionIds(String serviceId) throws JSONException;
    public ResponseEntity<?> getActivityById(String activityId) throws JSONException;


}
