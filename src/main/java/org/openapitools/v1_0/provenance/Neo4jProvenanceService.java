package org.openapitools.v1_0.provenance;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import io.kubernetes.client.openapi.models.V1Container;
import io.kubernetes.client.openapi.models.V1Pod;
import org.apache.commons.io.output.ByteArrayOutputStream;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.neo4j.driver.*;
import org.openapitools.v1_0.api.EposUtils;
import org.openapitools.v1_0.api.KubernetesService;

import org.openprovenance.prov.interop.InteropFramework;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

import static java.lang.System.*;
import static org.neo4j.driver.Values.parameters;
import static org.openapitools.v1_0.model.kubernetes.ServiceDeploymentFactory.SERVICE_ID_LABEL;

@Service
@Conditional(ConditionNeo4j.class)
public class Neo4jProvenanceService implements DatabaseProvenanceInterface {

    @Autowired
    KubernetesService kubernetesService;
    private Driver driver;
    @Value("${neo4j.uri}")
    private String NEO4J_URI;

    private static Logger logger = LoggerFactory.getLogger(Neo4jProvenanceService.class);

    private synchronized Driver connect() {
        if (null == driver) {
            driver = GraphDatabase.driver(NEO4J_URI, AuthTokens.basic(getenv("NEO4J_USER"), getenv("NEO4J_PASSWORD")));
        }
        return driver;
    }

    public ResponseEntity<?> createProvenance(Resource body) throws IOException {
        byte[] bdata = FileCopyUtils.copyToByteArray(body.getInputStream());
        InputStream inputStream = new ByteArrayInputStream(bdata);
        OutputStream outputStream = new ByteArrayOutputStream();
        InteropFramework intF = new InteropFramework();
        org.openprovenance.prov.model.Document provDoc = intF.readDocument(inputStream,
                org.openprovenance.prov.interop.Formats.ProvFormat.PROVN, null);
        intF.writeDocument(outputStream, org.openprovenance.prov.interop.Formats.ProvFormat.RDFXML, provDoc);
        String newOutput = outputStream.toString();

        String rdfFile = writeRdf(newOutput);
        if (null == rdfFile) {
            return EposUtils.handleError("Cannot create RDF file.", HttpStatus.INTERNAL_SERVER_ERROR.value(), null);
        }

        /*
         * NOTE: There is chosen for sending a RDF through rdf/xml, but Turtle can be an
         * option.
         */
        connect();
        logger.debug("Creating provenance record.");
        try (Session session = driver.session()) {
            org.neo4j.driver.Record record;
            Result result = session.run("SHOW CONSTRAINTS");
            boolean hasBeenInitialized = false;
            while (result.hasNext()) {
                record = result.next();
                String name = record.get("name").asString();
                if (name.equals("n10s_unique_uri")) {
                    hasBeenInitialized = true;
                    break;
                }
            }
            if (!hasBeenInitialized) {
                session.run("CREATE CONSTRAINT n10s_unique_uri ON (r:Resource) ASSERT r.uri IS UNIQUE");
                session.run("CALL n10s.graphconfig.init({ shortenUrls: false, typesToLabels: true, commitSize: 90000 })");
                /* NOTE: the prefix is handeld with '__' instead of ':' in the Neo4j database */
                /* Initialize the database with the namespace mappings for Neo4J: */
            }
            result = session.run("CALL n10s.nsprefixes.list()");
            Map<String, String> resultMap = new HashMap<>();
            while (result.hasNext()) {
                record = result.next();
                resultMap.put(record.asMap().get("prefix").toString(), record.asMap().get("namespace").toString());
            }
            Map<String, String> namespaceMap = provDoc.getNamespace().getNamespaces();
            for (Map.Entry<String, String> entry : namespaceMap.entrySet()) {
                if (!resultMap.containsKey(entry.getValue())) {
                    logger.debug("Adding namespace " + entry.getValue() + " " + entry.getKey());
                    session.run("CALL n10s.nsprefixes.add(\"" + entry.getValue() + "\", \"" + entry.getKey() + "\")");
                }
            }
            String importStatement = "CALL n10s.rdf.import.fetch(\"file://" + rdfFile + "\",\"RDF/XML\")";
            logger.debug(importStatement);
            session.run(importStatement);
        }
        return new ResponseEntity<Resource>(body, HttpStatus.OK);
    }

    private String writeRdf(String newOutput) throws IOException {
        if (NEO4J_URI.contains("neo4j")) {
            return writeLocalRdf(newOutput);
        } else {
            return writeK8sRdf(newOutput);
        }
    }

    private String writeLocalRdf(String newOutput) throws IOException {
        String tempFile = "/Rdf/" + UUID.randomUUID().toString() + ".xml";
        String localTempFile = getenv("HOME") + tempFile;
        logger.debug("Write RDF to local directory: " + localTempFile);

        /*
         * Solution to write a temporary file to a local path. Needed because the plugin
         * Semantics needs a file.
         */
        File file = new File(localTempFile);
        FileWriter writer = new FileWriter(file);
        writer.write(newOutput);
        writer.close();

        // TODO: check if file is inserted correctly into Neo4j and then delete
        // so that the Rdf directory doesn't fill up if the notebook-api runs
        // for a long time.
        file.deleteOnExit();

        return tempFile;
    }

    private String writeK8sRdf(String newOutput) {
        String tempFile = "/Rdf/" + UUID.randomUUID().toString() + ".xml";
        logger.debug("Write RDF to pod: " + tempFile);

        ResponseEntity<?> podResult = kubernetesService.retrievePodByLabel("app=neo4j");
        if (HttpStatus.OK != podResult.getStatusCode()) {
            return null;
        }
        V1Pod pod = (V1Pod) podResult.getBody();
        kubernetesService.writeStringToFileOnContainer(pod, "neo4jdb", newOutput, tempFile);
        return tempFile;
    }

    public ResponseEntity<?> getSessionActivity(String swirrlColonSessionId, String person, String poolId, String serviceId, String type, Boolean includeRuns, Boolean includeAdditionalInfo) throws JSONException
    {
        Map<String, String> resultMap = new HashMap<>();

        org.neo4j.driver.Record record;
        String stringJsonTotal;

        JSONObject objectJsonTotal = new JSONObject();

        JSONArray arrayJsonTotal = new JSONArray();

        Result result;
        connect();
        logger.debug("getSessionActivity called: "+ swirrlColonSessionId);
        try (Session session = driver.session()) {
            /* (0.1) Context */
            result = session.run("CALL n10s.nsprefixes.list()");
            if (!result.hasNext()) {
                logger.error("Query number 0.1 failed and has no result.");
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
            while (result.hasNext()) {
                record = result.next();
                resultMap.put(record.asMap().get("prefix").toString(), record.asMap().get("namespace").toString());
            }
            /* (0.2) activities */
            result = session.run(
                    "MATCH (activity:prov__Activity{swirrl__sessionId:$swirrlColonSessionId})-->(:prov__Association)-->(user:prov__Person)" +
                            " RETURN labels(activity) AS `@type`, activity.uri AS `@id`, user.vcard__uid AS person," +
                            " activity.swirrl__sessionId AS `swirrl:sessionId`, activity.swirrl__poolId AS `swirrl:poolId`," +
                            " activity.swirrl__serviceId AS `swirrl:serviceId`, activity.prov__startedAtTime AS `prov:startedAtTime`," +
                            " activity.prov__endedAtTime AS `prov:endedAtTime`" +
                            " ORDER BY `prov:startedAtTime`",
                    parameters("swirrlColonSessionId", swirrlColonSessionId));

            if (!result.hasNext()) {
                logger.error("Query number 0.2 failed and has no result.");
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }

            while (result.hasNext()) {
                record = result.next();
                Map recordMap = record.asMap();
                JSONObject recordJson = new JSONObject(recordMap);
                if (matchesFilter(recordJson, poolId, serviceId, type, includeRuns, person)) {
                    if (includeAdditionalInfo) {
                        recordJson = addAdditionalInfo(recordJson, session);
                    }
                    logger.info("Adding activity to response.");
                    arrayJsonTotal.put(recordJson);
                }
            }
        }
        objectJsonTotal.put("@context", new JSONObject(resultMap));
        objectJsonTotal.put("@graph", arrayJsonTotal);
        stringJsonTotal = objectJsonTotal.toString().replace("__", ":");
        return new ResponseEntity<>(stringJsonTotal, HttpStatus.OK);
    }

    private Boolean hasType(JSONObject record, String type) {
        boolean hasType = false;
        try {
            JSONArray typeArray = record.getJSONArray("@type");
            for (int i = 0; i < typeArray.length(); i++) {
                if (typeArray.getString(i).equals(type)) {
                    hasType = true;
                    break;
                }
            }
        } catch (JSONException e) {
            hasType = false;
        }
        return hasType;
    }

    private JSONObject addAdditionalInfo(JSONObject record, Session session) {
        logger.info("Adding additional info to activity.");
        try {
            Result result;
            JSONObject nextResult;
            String activityId = record.getString("@id");
            if (hasType(record, "swirrl__CreateNotebook")) {
                /* (0.3) Used */
                result = session.run("MATCH (plan:prov__Plan)<-[:prov__used]-(activity:`prov__Activity`)\n"
                                + "WHERE activity.uri = $activityId AND plan.prov__value IS NOT NULL\n"
                                + "RETURN plan.prov__value AS `prov:value`",
                        parameters("activityId", activityId));
                if (!result.hasNext()) {
                    logger.error("Query number 0.3 failed and has no result.");
                }
                while (result.hasNext()) {
                    nextResult = new JSONObject(result.next().asMap());
                    record.put("prov:plan", nextResult.getString("prov:value"));
                }
            }
            if (hasType(record, "swirrl__UpdateLibs")) {
                /* (0.4) WasAssociatedWith */
                result = session.run("MATCH(activity:`prov__Activity`)-->(as:prov__Association)-[:prov__hadPlan]->(plan:prov__Entity)\n"
                            + "WHERE activity.uri = $activityId AND plan.prov__value IS NOT NULL\n"
                            + "RETURN plan.prov__value AS `prov:value`",
                    parameters("activityId", activityId));
                if (!result.hasNext()) {
                    logger.error("Query number 0.4 failed and has no result.");
                }
                while (result.hasNext()) {
                    nextResult = new JSONObject(result.next().asMap());
                    logger.debug(String.valueOf(nextResult));
                    record.put("prov:plan", nextResult.getString("prov:value"));
                }
            }
            if (hasType(record, "swirrl__CreateSnapshot")) {
                /* (0.5) Snapshot */
                result = session.run("MATCH(snapshot)-->(activity:`prov__Activity`)\n"
                            + "WHERE activity.uri = $activityId AND snapshot.prov__atLocation IS NOT NULL\n"
                            + "RETURN snapshot.prov__atLocation AS `prov:atLocation`",
                    parameters("activityId", activityId));
                if (!result.hasNext()) {
                    logger.error("Query number 0.5 failed and has no result.");
                }
                while (result.hasNext()) {
                    nextResult = new JSONObject(result.next().asMap());
                    logger.debug(String.valueOf(nextResult));
                    record.put("prov:atLocation", nextResult.getString("prov:atLocation"));
                }
            }
            if (hasType(record, "swirrl__RunWorkflow")) {
                /* (0.6) Workflow plan */
                result = session.run("MATCH(activity:`prov__Activity`)-->(as:prov__Association)-[:prov__hadPlan]->(plan:prov__Entity)\n"
                            + "WHERE activity.uri = $activityId AND plan.rdfs__label IS NOT NULL\n"
                            + "RETURN activity.swirrl__message as `swirrl:message`, plan.rdfs__label AS `rdfs:label`",
                    parameters("activityId", activityId));
                if (!result.hasNext()) {
                    logger.error("Query number 0.6 failed and has no result.");
                }
                while (result.hasNext()) {
                    nextResult = new JSONObject(result.next().asMap());
                    logger.debug(String.valueOf(nextResult));
                    record.put("prov:plan", nextResult.getString("rdfs:label"));
                    record.put("swirrl:message", nextResult.getString("swirrl:message"));
                }
            }
        } catch (JSONException e) {
            logger.error("Something went wrong while parsing json.");
        }
        return record;
    }

    private Boolean matchesFilter(JSONObject record, String poolId, String serviceId, String type, Boolean includeRuns, String person) {
        boolean matchesServiceId = true;
        boolean matchesPoolId = true;
        boolean matchesType = true;
        boolean matchesPerson = true;
        boolean isIncludedRun = false;

        if (includeRuns) {
            isIncludedRun = hasType(record, "swirrl__RunWorkflow") || hasType(record, "swirrl__RollbackWorkflow");
        }

        if (null != serviceId && !isIncludedRun) {
            try {
                logger.debug(record.getString("swirrl:serviceId"));
                logger.debug(serviceId);
                if (!record.getString("swirrl:serviceId").equals(serviceId)) {
                    matchesServiceId = false;
                }
            } catch (JSONException e) {
                matchesServiceId = false;
            }
        }
        if (null != poolId && !isIncludedRun) {
            try {
                if (!record.getString("swirrl:poolId").equals(poolId)) {
                    matchesPoolId = false;
                }
            } catch (JSONException e) {
                matchesPoolId = false;
            }
        }
        if (null != type) {
            matchesType = hasType(record, type.replace(".", "_").replace(":", "__"));
        }
        if (null != person) {
            try {
                if (!record.getString("person").equals(person)) {
                    matchesPerson = false;
                }
            } catch (JSONException e) {
                matchesPerson = false;
            }
        }

        return matchesServiceId && matchesPoolId && matchesType && matchesPerson;
    }

    public ResponseEntity<?> getServiceSessionIds(String serviceId) throws JSONException {
        Result result;
        org.neo4j.driver.Record record;

        String jsonString;
        JSONObject jsonAction = new JSONObject();

        connect();
        try (Session session = driver.session()) {

            /* Determine activity type  and call respective method to build the query*/
            result = session.run("MATCH (activity:`swirrl__CreateNotebook`)\n" + "WHERE activity.swirrl__serviceId = $serviceId\n"
                            + "RETURN activity.swirrl__serviceId as `swirrl:serviceId`, activity.swirrl__poolId as `swirrl:poolId`, activity.swirrl__sessionId as `swirrl:sessionId`",
                    parameters("serviceId", serviceId));
            if (!result.hasNext()) {
                logger.error("No service found by that id, cannot resolve");
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
            while (result.hasNext()) {
                record = result.next();
                jsonAction = new JSONObject(record.asMap());
            }
        }
        jsonString = jsonAction.toString().replace("__", ":");

        return new ResponseEntity<>(jsonString, HttpStatus.OK);
    }

    public ResponseEntity<?> getActivityById(String activityId) throws JSONException {
        Result result;
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        org.neo4j.driver.Record record;

        String jsonString;
        List<String> activityType = null;
        String[] keyValue;
        String json = null;

        JSONObject jsonAction = new JSONObject();

        connect();
        try (Session session = driver.session()) {

            /* Determine activity type  and call respective method to build the query*/
            result = session.run("MATCH (activity:`prov__Activity`)\n" + "WHERE activity.uri = $activityId\n"
                            + "RETURN labels(activity) AS `@type`",
                    parameters("activityId", activityId));
            if (!result.hasNext()) {
                logger.error("No activity found by that id, cannot resolve");
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
            while (result.hasNext()) {
                record = result.next();
                activityType = record.get("@type").asList().stream().map(object -> Objects.toString(object, null))
                        .collect(Collectors.toList());
                logger.info("Activity type: " + activityType.get(2));
            }

            if (activityType != null &&
                    (activityType.contains("swirrl__CreateEnlighten")
                    || activityType.contains("swirrl__CreateNotebook")
                    || activityType.contains("swirrl__UpdateLibs")
                    || activityType.contains("swirrl__RestoreLibs"))) {
                return handleNotebookActivities(activityId);
            }
            if (activityType != null &&
                    (activityType.contains("swirrl__RollbackWorkflow"))) {
                return handleRollbackActivities(activityId);
            }
            if (activityType != null && activityType.contains("swirrl__RunWorkflow")) {
                return handleActivityWorkflow(activityId);
            }
            if (activityType != null && activityType.contains("swirrl__CreateSnapshot")) {
                return handleSnapshotActivities(activityId);
            }
        }
        logger.error("Activity type unknown");
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    public ResponseEntity<?> handleSnapshotActivities(String activityId) throws JSONException {
        Result result;
        org.neo4j.driver.Record record;

        String jsonString;
        List<String> activityType = null;

        JSONObject jsonAction = new JSONObject();
        JSONObject jsonGenerated = new JSONObject();
        JSONArray jsonAssociations = new JSONArray();
        JSONArray jsonArrayUsed = new JSONArray();
        Map<String, String> resultMap = new HashMap<>();

        connect();
        try (Session session = driver.session()) {
            /* (1.1) Activity */
            result = session.run("MATCH (activity:`prov__Activity`)\n" + "WHERE activity.uri = $activityId\n"
                            + "RETURN activity.prov__atLocation AS `prov:atLocation`, activity.uri AS `@id`, activity.swirrl__sessionId AS `swirrl:sessionId`, activity.swirrl__poolId AS `swirrl:poolId`, activity.swirrl__serviceId AS `swirrl:serviceId`,\n"
                            + "activity.swirrl__message AS `swirrl:message`, activity.prov__startedAtTime AS `prov:startedAtTime`, activity.prov__endedAtTime AS `prov:endedAtTime`, labels(activity) AS `@type`",
                    parameters("activityId", activityId));
            if (!result.hasNext()) {
                logger.error("Query number 1.1 failed and has no result.");
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
            while (result.hasNext()) {
                record = result.next();
                // Convert List<Object> returned from asList into List<String>
                activityType = record.get("@type").asList().stream().map(object -> Objects.toString(object, null))
                        .collect(Collectors.toList());
                logger.info("Activity type: " + activityType.get(2));
                jsonAction = new JSONObject(record.asMap());
            }


            /* (1.2) Context */
            result = session.run("CALL n10s.nsprefixes.list()");
            if (!result.hasNext()) {
                logger.error("Query number 0.1 failed and has no result.");
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
            while (result.hasNext()) {
                record = result.next();
                resultMap.put(record.asMap().get("prefix").toString(), record.asMap().get("namespace").toString());
            }

            /* (1.3) Snapshot */
            result = session.run("MATCH(s)-->(activity:`prov__Activity`)\n" +
                            "WHERE activity.uri = $activityId \n"
                            + "RETURN labels(s) AS `@type`, s.prov__generatedAt AS `prov:generatedAt`, s.uri AS `@id`, s.prov__atLocation AS `prov:atLocation`, s.dcterms__description AS `dcterms:description`",
                    parameters("activityId", activityId));
            if (!result.hasNext()) {
                logger.error("Query number 1.3 failed and has no result.");
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
            while (result.hasNext()) {
                record = result.next();
                jsonGenerated = new JSONObject(record.asMap());
            }
            /* (1.4) Association, Agent */
            result = session.run("MATCH(activity:`prov__Activity`)-->(as:prov__Association)-->(agent:prov__Agent)" +
                            " WHERE activity.uri = $activityId" +
                            " RETURN agent.rdfs__label AS `rdfs:label`, agent.vcard__uid AS `vcard:uid`, agent.swirrl__group AS `swirrl:group`," +
                            " agent.swirrl__authMode AS `swirrl:authMode`, agent.uri AS `@id`, labels(agent) AS `@type`, as.uri AS `associationid`",
                    parameters("activityId", activityId));
            if (!result.hasNext()) {
                logger.error("Query number 1.4 failed and has no result.");
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
            while (result.hasNext()) {
                record = result.next();
                JSONObject recordJson = new JSONObject(record.asMap());
                JSONObject jsonAssociation = new JSONObject();
                jsonAssociation.put("@id", recordJson.getString("associationid"));
                recordJson.remove("associationid");
                jsonAssociation.put("prov:agent", recordJson);
                jsonAssociations.put(jsonAssociation);
            }

            /* (1.4.1) Association, plan */
            result = session.run("MATCH(activity:`prov__Activity`)-->(as:prov__Association)-[:prov__hadPlan]->(plan:prov__Entity)\n"
                            + "WHERE activity.uri = $activityId\n"
                            + "RETURN plan.prov__value AS `prov:value`, plan.swirrl__version AS `swirrl:version`, as.uri AS `associationid`, plan.swirrl__systemImageTag as `swirrl:systemImageTag`,\n"
                            + "plan.prov__atLocation AS `prov:atLocation`, plan.rdfs__label AS `rdfs:label`, plan.uri AS `@id`, labels(plan) AS `@type`",
                    parameters("activityId", activityId));
            if (!result.hasNext()) {
                logger.error("Query number 1.4.1 failed and has no result.");
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
            while (result.hasNext()) {
                record = result.next();
                JSONObject recordJson = new JSONObject(record.asMap());
                String associationid = recordJson.getString("associationid");
                recordJson.remove("associationid");
                boolean found = false;
                for (int i = 0; i < jsonAssociations.length(); i++) {
                    if (jsonAssociations.getJSONObject(i).getString("@id").equals(associationid)) {
                        found = true;
                        jsonAssociations.getJSONObject(i).put("prov:plan", recordJson);
                    }
                }
                if (!found) {
                    JSONObject jsonAssociation = new JSONObject();
                    jsonAssociation.put("prov:plan", recordJson);
                    jsonAssociations.put(jsonAssociation);
                }
            }
            /* (1.5) Notebook, entity (wasDerivedFrom) */
            result = session.run(
                    "MATCH(entity:`swirrl__Snapshot`)-[:prov__wasDerivedFrom]->(n:swirrl__Notebook)<-[:prov__used]-(activity:`prov__Activity`)\n" +
                            "WHERE activity.uri = $activityId\n" +
                            "RETURN n.uri AS `@id`, labels(n) AS `@type`",
                    parameters("activityId", activityId));
            if (!result.hasNext()) {
                logger.error("Query number 1.5 failed and has no result.");
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
            while (result.hasNext()) {
                record = result.next();
                jsonGenerated.put("prov:wasDerivedFrom", new JSONObject(record.asMap()));
            }

            /* 1.6 Entity (used) */
            result = session.run("MATCH (e:prov__Entity)<-[:prov__used]-(activity:`prov__Activity`) "
                            + "WHERE activity.uri = $activityId "
                            + "RETURN labels(e) as `@type`, e.uri AS `@id`, properties(e).prov__atLocation AS `prov:atLocation`, e.swirrl__volumeId AS `swirrl:volumeId`",
                    parameters("activityId", activityId));

            if (!result.hasNext()) {
                logger.error("Query number 1.6 failed and has no result.");
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
            while (result.hasNext()) {
                record = result.next();
                jsonArrayUsed.put(new JSONObject(record.asMap()));
            }
        }
        jsonAction.put("@context", new JSONObject(resultMap));
        jsonAction.put("prov:generated", jsonGenerated);
        jsonAction.put("prov:used", jsonArrayUsed);
        jsonAction.put("prov:wasAssociatedWith", jsonAssociations);
        jsonString = jsonAction.toString().replace("__", ":");

        return new ResponseEntity<>(jsonString, HttpStatus.OK);
    }

    public ResponseEntity<?> handleRollbackActivities(String activityId) throws JSONException {
        Result result;
        org.neo4j.driver.Record record;

        String jsonString;
        List<String> activityType = null;

        JSONObject jsonAction = new JSONObject();
        JSONObject jsonInvalidated = new JSONObject();
        JSONArray jsonAssociations = new JSONArray();


        Map<String, String> resultMap = new HashMap<>();

        connect();
        try (Session session = driver.session()) {
            /* (1.1) Activity */
            result = session.run("MATCH (activity:`prov__Activity`)\n" + "WHERE activity.uri = $activityId\n"
                            + "RETURN activity.swirrl__message AS `swirrl:message`, activity.prov__atLocation AS `prov:atLocation`, activity.uri AS `@id`, activity.swirrl__sessionId AS `swirrl:sessionId`, activity.swirrl__poolId AS `swirrl:poolId`, activity.swirrl__serviceId AS `swirrl:serviceId`,\n"
                            + "activity.prov__startedAtTime AS `prov:startedAtTime`, activity.prov__endedAtTime AS `prov:endedAtTime`, labels(activity) AS `@type`",
                    parameters("activityId", activityId));
            if (!result.hasNext()) {
                logger.error("Query number 1.1 failed and has no result.");
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
            while (result.hasNext()) {
                record = result.next();
                // Convert List<Object> returned from asList into List<String>
                activityType = record.get("@type").asList().stream().map(object -> Objects.toString(object, null))
                        .collect(Collectors.toList());
                logger.info("Activity type: " + activityType.get(2));
                jsonAction = new JSONObject(record.asMap());
            }

            /* (1.2) Context */
            result = session.run("CALL n10s.nsprefixes.list()");
            if (!result.hasNext()) {
                logger.error("Query number 0.1 failed and has no result.");
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
            while (result.hasNext()) {
                record = result.next();
                resultMap.put(record.asMap().get("prefix").toString(), record.asMap().get("namespace").toString());
            }


            /* (1.3) Invalidated */
            result = session.run("MATCH(n)-[:prov__wasInvalidatedBy]->(activity:`prov__Activity`)\n" +
                            "WHERE activity.uri = $activityId \n"
                            + "RETURN labels(n) AS `@type`, n.prov__generatedAt AS `prov:generatedAt`, n.uri AS `@id`, \n"
                            + "n.swirrl__volumeId AS `swirrl:volumeId`, n.swirrl__sessionId AS `swirrl:sessionId`",
                    parameters("activityId", activityId));
            if (!result.hasNext()) {
                logger.error("Query number 1.3 failed and has no result.");
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
            while (result.hasNext()) {
                record = result.next();
                jsonInvalidated = new JSONObject(record.asMap());
            }
            /* (1.6) Association, Agent */
            result = session.run("MATCH(activity:`prov__Activity`)-->(as:prov__Association)-->(agent:prov__Agent)" +
                            " WHERE activity.uri = $activityId" +
                            " RETURN agent.rdfs__label AS `rdfs:label`, agent.vcard__uid AS `vcard:uid`, agent.swirrl__group AS `swirrl:group`," +
                            " agent.swirrl__authMode AS `swirrl:authMode`, agent.uri AS `@id`, labels(agent) AS `@type`, as.uri AS `associationid`",
                    parameters("activityId", activityId));
            if (!result.hasNext()) {
                logger.error("Query number 1.6 failed and has no result.");
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
            while (result.hasNext()) {
                record = result.next();
                JSONObject recordJson = new JSONObject(record.asMap());
                JSONObject jsonAssociation = new JSONObject();
                jsonAssociation.put("@id", recordJson.getString("associationid"));
                recordJson.remove("associationid");
                jsonAssociation.put("prov:agent", recordJson);
                jsonAssociations.put(jsonAssociation);
            }

            /* (1.7) Association, plan */
            result = session.run("MATCH(activity:`prov__Activity`)-->(as:prov__Association)-[:prov__hadPlan]->(plan:prov__Entity)\n"
                            + "WHERE activity.uri = $activityId\n"
                            + "RETURN plan.prov__value AS `prov:value`, plan.swirrl__version AS `swirrl:version`, as.uri AS `associationid`,\n"
                            + "plan.prov__atLocation AS `prov:atLocation`, plan.rdfs__label AS `rdfs:label`, plan.uri AS `@id`, labels(plan) AS `@type`",
                    parameters("activityId", activityId));
            if (!result.hasNext()) {
                logger.error("Query number 1.7 failed and has no result.");
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
            while (result.hasNext()) {
                record = result.next();
                JSONObject recordJson = new JSONObject(record.asMap());
                String associationid = recordJson.getString("associationid");
                recordJson.remove("associationid");
                boolean found = false;
                for (int i = 0; i < jsonAssociations.length(); i++) {
                    if (jsonAssociations.getJSONObject(i).getString("@id").equals(associationid)) {
                        found = true;
                        jsonAssociations.getJSONObject(i).put("prov:plan", recordJson);
                    }
                }
                if (!found) {
                    JSONObject jsonAssociation = new JSONObject();
                    jsonAssociation.put("prov:plan", recordJson);
                    jsonAssociations.put(jsonAssociation);
                }
            }
        }

        jsonAction.put("@context", new JSONObject(resultMap));
        jsonAction.put("prov:wasAssociatedWith", jsonAssociations);
        jsonAction.put("prov:invalidated", jsonInvalidated);
        jsonString = jsonAction.toString().replace("__", ":");

        return new ResponseEntity<>(jsonString, HttpStatus.OK);
    }

    public ResponseEntity<?> handleNotebookActivities(String activityId) throws JSONException {
        Result result;
        org.neo4j.driver.Record record;

        String jsonString;
        List<String> activityType = null;

        JSONObject jsonAction = new JSONObject();
        JSONObject jsonGenerated = new JSONObject();

        JSONArray jsonAssociations = new JSONArray();
        JSONArray jsonLibraries;
        JSONArray jsonArrayMember;
        JSONArray jsonArrayUsed = new JSONArray();

        Map<String, String> resultMap = new HashMap<>();
        List<Object> hadMembers = new ArrayList<>();
        List<Object> libraries = new ArrayList();

        connect();
        try (Session session = driver.session()) {
            /* (1.1) Activity */
            result = session.run("MATCH (activity:`prov__Activity`)\n" + "WHERE activity.uri = $activityId\n"
                            + "RETURN activity.swirrl__message AS `swirrl:message`, activity.prov__atLocation AS `prov:atLocation`, activity.uri AS `@id`, activity.swirrl__sessionId AS `swirrl:sessionId`, activity.swirrl__poolId AS `swirrl:poolId`, activity.swirrl__serviceId AS `swirrl:serviceId`,\n"
                            + "activity.prov__startedAtTime AS `prov:startedAtTime`, activity.prov__endedAtTime AS `prov:endedAtTime`, labels(activity) AS `@type`",
                    parameters("activityId", activityId));
            if (!result.hasNext()) {
                logger.error("Query number 1.1 failed and has no result.");
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
            while (result.hasNext()) {
                record = result.next();
                // Convert List<Object> returned from asList into List<String>
                activityType = record.get("@type").asList().stream().map(object -> Objects.toString(object, null))
                        .collect(Collectors.toList());
                logger.info("Activity type: " + activityType.get(2));
                jsonAction = new JSONObject(record.asMap());
            }


            /* (1.2) Context */
            result = session.run("CALL n10s.nsprefixes.list()");
            if (!result.hasNext()) {
                logger.error("Query number 0.1 failed and has no result.");
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
            while (result.hasNext()) {
                record = result.next();
                resultMap.put(record.asMap().get("prefix").toString(), record.asMap().get("namespace").toString());
            }

            /* (1.3) Notebook */
            result = session.run("MATCH(n)-[:prov__wasGeneratedBy]->(activity:`prov__Activity`)\n" +
                            "WHERE activity.uri = $activityId \n"
                            + "RETURN labels(n) AS `@type`, n.prov__generatedAt AS `prov:generatedAt`, n.uri AS `@id`, n.prov__atLocation AS `prov:atLocation`",
                    parameters("activityId", activityId));
            if (!result.hasNext()) {
                logger.error("Query number 1.3 failed and has no result.");
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
            while (result.hasNext()) {
                record = result.next();
                jsonGenerated = new JSONObject(record.asMap());
            }


            /* (1.4) Notebook, :hadMember */
            result = session.run(
                    "MATCH (entity:`prov__Entity`)<-[:prov__hadMember]-(n)-[:prov__wasGeneratedBy]->(activity:`prov__Activity`)\n"
                            + "WHERE activity.uri = $activityId\n"
                            + "RETURN entity.uri AS `@id`, labels(entity) AS `@type`",
                    parameters("activityId", activityId));
            if (!result.hasNext()) {
                logger.error("Query number 1.4 failed and has no result.");
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
            while (result.hasNext()) {
                record = result.next();
                hadMembers.add(record.asMap());
            }
            jsonArrayMember = new JSONArray(hadMembers);


            if (activityType != null && !activityType.contains("swirrl__CreateEnlighten")) {
                /* (1.5) Notebook, :hadMember, :hadMember */
                result = session.run(
                        "MATCH(libEntity:`prov__Entity`)<-[:prov__hadMember]-(entity:`prov__Entity`)<-[:prov__hadMember]-(n)-[:prov__wasGeneratedBy]->(activity:`prov__Activity`)\n"
                                + "WHERE activity.uri = $activityId\n"
                                + "RETURN libEntity.swirrl__installationMode AS `swirrl:installationMode`, libEntity.swirrl__name AS `swirrl:name`, libEntity.swirrl__version AS `swirrl:version`, libEntity.uri AS `@id`, labels(libEntity) AS `@type`",
                        parameters("activityId", activityId));
                if (!result.hasNext()) {
                    logger.error("Query number 1.5 failed and has no result.");
                    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
                }
                while (result.hasNext()) {
                    record = result.next();
                    libraries.add(record.asMap());
                }
                jsonLibraries = new JSONArray(libraries);
                for (int i = 0; i < jsonArrayMember.length(); i++) {
                    JSONObject jsonObject = jsonArrayMember.getJSONObject(i);
                    JSONArray jsonArray = jsonObject.getJSONArray("@type");
                    String check = jsonArray.toString(i);
                    if (check.contains("swirrl__LibCollection")) {
                        jsonObject.put("prov:hadMember", jsonLibraries);
                    }
                }
            }


            /* (1.6) Association, Agent */
            result = session.run("MATCH(activity:`prov__Activity`)-->(as:prov__Association)-->(agent:prov__Agent)" +
                            " WHERE activity.uri = $activityId" +
                            " RETURN agent.rdfs__label AS `rdfs:label`, agent.vcard__uid AS `vcard:uid`, agent.swirrl__group AS `swirrl:group`," +
                            " agent.swirrl__authMode AS `swirrl:authMode`, agent.uri AS `@id`, labels(agent) AS `@type`, as.uri AS `associationid`",
                    parameters("activityId", activityId));
            if (!result.hasNext()) {
                logger.error("Query number 1.6 failed and has no result.");
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
            while (result.hasNext()) {
                record = result.next();
                JSONObject recordJson = new JSONObject(record.asMap());
                JSONObject jsonAssociation = new JSONObject();
                jsonAssociation.put("@id", recordJson.getString("associationid"));
                recordJson.remove("associationid");
                jsonAssociation.put("prov:agent", recordJson);
                jsonAssociations.put(jsonAssociation);
            }

            /* (1.7) Association, plan */
            result = session.run("MATCH(activity:`prov__Activity`)-->(as:prov__Association)-[:prov__hadPlan]->(plan:prov__Entity)\n"
                            + "WHERE activity.uri = $activityId\n"
                            + "RETURN plan.prov__value AS `prov:value`, plan.swirrl__version AS `swirrl:version`, as.uri AS `associationid`,\n"
                            + "plan.prov__atLocation AS `prov:atLocation`, plan.rdfs__label AS `rdfs:label`, plan.uri AS `@id`, labels(plan) AS `@type`",
                    parameters("activityId", activityId));
            if (!result.hasNext()) {
                logger.error("Query number 1.7 failed and has no result.");
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
            while (result.hasNext()) {
                record = result.next();
                JSONObject recordJson = new JSONObject(record.asMap());
                String associationid = recordJson.getString("associationid");
                recordJson.remove("associationid");
                boolean found = false;
                for (int i = 0; i < jsonAssociations.length(); i++) {
                    if (jsonAssociations.getJSONObject(i).getString("@id").equals(associationid)) {
                        found = true;
                        jsonAssociations.getJSONObject(i).put("prov:plan", recordJson);
                    }
                }
                if (!found) {
                    JSONObject jsonAssociation = new JSONObject();
                    jsonAssociation.put("prov:plan", recordJson);
                    jsonAssociations.put(jsonAssociation);
                }
            }


            if (activityType != null && activityType.contains("swirrl__UpdateLibs")) {
                /* (1.8) Notebook, entity (wasDerivedFrom) */
                result = session.run(
                        "MATCH(entity:`prov__Entity`)<-[:prov__hadMember]-(n:swirrl__Notebook)-[:prov__wasGeneratedBy]->(activity:`prov__Activity`)\n"
                                + "WHERE activity.uri = $activityId \n"
                                + "RETURN entity.uri AS `@id`, labels(entity) AS `@type`",
                        parameters("activityId", activityId));
                if (!result.hasNext()) {
                    logger.error("Query number 1.8 failed and has no result.");
                    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
                }
                while (result.hasNext()) {
                    record = result.next();
                    jsonGenerated.put("prov:wasDerivedFrom", new JSONObject(record.asMap()));
                }


                /* 1.8.1 activity (used) */
                result = session.run("MATCH (notebook:swirrl__Notebook)<-[:prov__used]-(activity:`prov__Activity`) "
                                + "WHERE activity.uri = $activityId "
                                + "RETURN labels(notebook) as `@type`, notebook.uri AS `@id`, properties(notebook).prov__atLocation AS `prov:atLocation`, notebook.swirrl__sessionId AS `swirrl:sessionId`, notebook.swirrl__poolId AS `swirrl:poolId`, notebook.swirrl__serviceId AS `swirrl:serviceId`",
                        parameters("activityId", activityId));

                if (!result.hasNext()) {
                    logger.error("Query number 1.8.1 failed and has no result.");
                    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
                }
                while (result.hasNext()) {
                    record = result.next();
                    jsonArrayUsed.put(new JSONObject(record.asMap()));
                }
            }


            if (activityType != null && activityType.contains("swirrl__CreateNotebook")) {
                /* (1.9) Used */
                result = session.run("MATCH (plan:prov__Plan)<-[:prov__used]-(activity:`prov__Activity`)\n"
                                + "WHERE activity.uri = $activityId\n"
                                + "RETURN plan.prov__value AS `prov:value`, plan.swirrl__version AS `swirrl:version`, plan.uri AS `@id`, labels(plan) AS `@type`",
                        parameters("activityId", activityId));
                if (!result.hasNext()) {
                    logger.error("Query number 1.9 failed and has no result.");
                    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
                }
                while (result.hasNext()) {
                    record = result.next();
                    jsonArrayUsed.put(new JSONObject(record.asMap()));
                }
            }
        }
        jsonGenerated.put("prov:hadMember", jsonArrayMember);
        if (jsonArrayUsed.length() != 0) {
            jsonAction.put("prov:used", jsonArrayUsed);
        }
        jsonAction.put("@context", new JSONObject(resultMap));
        jsonAction.put("prov:wasAssociatedWith", jsonAssociations);
        jsonAction.put("prov:generated", jsonGenerated);
        jsonString = jsonAction.toString().replace("__", ":");

        return new ResponseEntity<>(jsonString, HttpStatus.OK);
    }
    private void preParseJson(JSONObject json) throws JSONException {
        // Replace the "_type" key with "@type" and "uri" with "@id" to make the json consistent with JSON-LD
        json.put("@type", new JSONArray(json.getString("_type").split(":")));
        json.remove("_type");
        json.put("@id", json.getString("uri"));
        json.remove("uri");
    }

    private JSONArray getRookwpsProvenance(String activityId, Session session) throws JSONException {
        // This query (and the following parsing of the results) is based on the current provenance template used by ROOKWPS.
        // If ROOKWPS changes their provenance template this query needs to be updated to include the new relationships into the response.
        Result result = session.run("MATCH (p:provone__Execution {uri:$activityId})\n" +
                        "CALL apoc.path.expandConfig(p, {relationshipFilter: \"<provone__wasPartOf,<prov__hadActivity|prov__qualifiedAssociation>," +
                        "<prov__qualifiedDerivation|prov__entity>|prov__agent>|prov__hadPlan>,owl__sameAs>|prov__wasAttributedTo>\",\n" +
                        "    minLevel: 0, maxLevel: 4}) YIELD path\n" +
                        "WITH collect(path) AS fullpath CALL apoc.convert.toTree(fullpath, false) YIELD value\n" +
                        "WITH apoc.map.removeKeys(value, [\"_id\"], { recursive:true }) AS output RETURN output",
                parameters("activityId", activityId));
        if (result.hasNext()) {
            org.neo4j.driver.Record record = result.next();
            JSONObject rookwpsJson = new JSONObject(record.asMap()).getJSONObject("output");
            // Convert the output of the apoc.convert.toTree function to a list JSON-LD documents by renaming keys
            JSONArray executionsJsonArray = rookwpsJson.getJSONArray("provone__wasPartOf");
            for (int i = 0; i < executionsJsonArray.length(); i++) {
                JSONObject executionJson = executionsJsonArray.getJSONObject(i);
                preParseJson(executionJson);
                if (executionJson.has("prov__qualifiedAssociation")) {
                    executionJson.put("prov__wasAssociatedWith", executionJson.getJSONArray("prov__qualifiedAssociation"));
                    executionJson.remove("prov__qualifiedAssociation");
                    for (int j = 0; j < executionJson.getJSONArray("prov__wasAssociatedWith").length(); j++) {
                        JSONObject associationJson = executionJson.getJSONArray("prov__wasAssociatedWith").getJSONObject(j);
                        preParseJson(associationJson);
                        if (associationJson.has("prov__hadPlan")) {
                            for (int k = 0; k < associationJson.getJSONArray("prov__hadPlan").length(); k++) {
                                JSONObject hadPlanJson = associationJson.getJSONArray("prov__hadPlan").getJSONObject(k);
                                preParseJson(hadPlanJson);
                            }
                        }
                        if (associationJson.has("prov__agent")) {
                            for (int k = 0; k < associationJson.getJSONArray("prov__agent").length(); k++) {
                                JSONObject agentJson = associationJson.getJSONArray("prov__agent").getJSONObject(k);
                                preParseJson(agentJson);
                                if (agentJson.has("prov__wasAttributedTo")) {
                                    for (int l = 0; l < agentJson.getJSONArray("prov__wasAttributedTo").length(); l++) {
                                        JSONObject attributedJson = agentJson.getJSONArray("prov__wasAttributedTo").getJSONObject(l);
                                        preParseJson(attributedJson);
                                    }
                                }
                            }
                        }
                    }
                }
                if (executionJson.has("prov__hadActivity")) {
                    executionJson.put("prov:wasActivityOfInfluence", executionJson.getJSONArray("prov__hadActivity"));
                    executionJson.remove("prov__hadActivity");
                    for (int j = 0; j < executionJson.getJSONArray("prov:wasActivityOfInfluence").length(); j++) {
                        JSONObject activityJson = executionJson.getJSONArray("prov:wasActivityOfInfluence").getJSONObject(j);
                        preParseJson(activityJson);
                        if (activityJson.has("prov__qualifiedDerivation")) {
                            activityJson.put("prov__hadDerivation", activityJson.getJSONArray("prov__qualifiedDerivation"));
                            activityJson.remove("prov__qualifiedDerivation");
                            for (int k = 0; k < activityJson.getJSONArray("prov__hadDerivation").length(); k++) {
                                JSONObject hadDerivationJson = activityJson.getJSONArray("prov__hadDerivation").getJSONObject(k);
                                preParseJson(hadDerivationJson);
                                if (hadDerivationJson.has("owl__sameAs")) {
                                    for (int l = 0; l < hadDerivationJson.getJSONArray("owl__sameAs").length(); l++) {
                                        JSONObject sameAsJson = hadDerivationJson.getJSONArray("owl__sameAs").getJSONObject(l);
                                        preParseJson(sameAsJson);
                                    }
                                }
                            }
                        }
                        if (activityJson.has("prov__entity")) {
                            for (int k = 0; k < activityJson.getJSONArray("prov__entity").length(); k++) {
                                JSONObject entityJson = activityJson.getJSONArray("prov__entity").getJSONObject(k);
                                preParseJson(entityJson);
                            }
                        }
                    }
                }
            }
            return executionsJsonArray;
        }
        return null;
    }

    public ResponseEntity<?> handleActivityWorkflow(String activityId) throws JSONException {
        Result result;
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        org.neo4j.driver.Record record;

        String jsonString;
        List<String> activityType = null;

        JSONObject jsonAction = new JSONObject();
        JSONObject jsonGenerated = new JSONObject();
        JSONObject jsonStorage = new JSONObject();

        JSONArray jsonArrayUsed = new JSONArray();
        JSONArray jsonAssociations = new JSONArray();
        JSONArray jsonArrayHadMember;

        Map<String, String> resultMap = new HashMap<>();
        List<Object> hadMembers = new ArrayList<>();
        List<Object> agentAssociations = new ArrayList<>();
        List<Object> planAssociations = new ArrayList<>();

        connect();
        try (Session session = driver.session()) {
            /* (1.1) Activity */
            result = session.run("MATCH (activity:`prov__Activity`)\n" + "WHERE activity.uri = $activityId\n"
                            + "RETURN activity.prov__atLocation AS `prov:atLocation`, activity.uri AS `@id`, activity.swirrl__sessionId AS `swirrl:sessionId`, activity.swirrl__jobId AS `swirrl:jobId`,\n"
                            + "activity.prov__startedAtTime AS `prov:startedAtTime`, activity.prov__endedAtTime AS `prov:endedAtTime`, labels(activity) AS `@type`, activity.swirrl__message AS `swirrl:message`",
                    parameters("activityId", activityId));
            if (!result.hasNext()) {
                logger.error("Query number 1.1 failed and has no result.");
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
            while (result.hasNext()) {
                record = result.next();
                // Convert List<Object> returned from asList into List<String>
                activityType = record.get("@type").asList().stream().map(object -> Objects.toString(object, null))
                        .collect(Collectors.toList());
                logger.info("Activity type: " + activityType.get(2));
                jsonAction = new JSONObject(record.asMap());
            }

            /* (1.2) Context */
            result = session.run("CALL n10s.nsprefixes.list()");
            if (!result.hasNext()) {
                logger.error("Query number 0.1 failed and has no result.");
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
            while (result.hasNext()) {
                record = result.next();
                resultMap.put(record.asMap().get("prefix").toString(), record.asMap().get("namespace").toString());
            }

            /* (1.2.1) Collection */
            result = session.run("MATCH(n:`prov__Collection`)-->(activity:`prov__Activity`)\n" +
                            "WHERE activity.uri = $activityId \n"
                            + "RETURN labels(n) AS `@type`, n.uri AS `@id`, \n"
                            + "n.prov__atLocation AS `prov:atLocation`",
                    parameters("activityId", activityId));
            // Doesn't matter if this query has a result. If the workflow fails,
            // there will be no association with an output collection. So add
            // the result if we have it, otherwise move on.
            while (result.hasNext()) {
                record = result.next();
                jsonGenerated = new JSONObject(record.asMap());
            }

            /* (1.3) Storage */
            result = session.run("MATCH(n:`swirrl__Storage`)-->(:`prov__Collection`)-->(activity:`prov__Activity`)\n" +
                            "WHERE activity.uri = $activityId \n"
                            + "RETURN labels(n) AS `@type`, n.prov__generatedAt AS `prov:generatedAt`, n.uri AS `@id`, \n"
                            + "n.swirrl__volumeId AS `swirrl:volumeId`, n.swirrl__sessionId AS `swirrl:sessionId`",
                    parameters("activityId", activityId));
            // Doesn't matter if this query has a result. If the workflow fails,
            // there will be no association with an output collection. So add
            // the result if we have it, otherwise move on.
            while (result.hasNext()) {
                record = result.next();
                jsonStorage = new JSONObject(record.asMap());
            }

            /* (1.3.1) Storage (workflow), generated (wasDerivedFrom) */
            result = session.run(
                    "MATCH (activity:`prov__Activity`)<--(:`prov__Collection`)<--(s:swirrl__Storage)-[:prov__wasDerivedFrom]->(entity:swirrl__Storage)\n"
                            + "WHERE activity.uri = $activityId \n"
                            + "RETURN entity.uri AS `@id`, labels(entity) AS `@type`",
                    parameters("activityId", activityId));
            // Doesn't matter if this query has a result. If the workflow fails,
            // there will be no association with an output collection. So add
            // the result if we have it, otherwise move on.
            while (result.hasNext()) {
                record = result.next();
                jsonStorage.put("prov:wasDerivedFrom", new JSONObject(record.asMap()));
            }
            /* (1.3.1) Storage (workflow), invalidated (wasInvalidatedBy) */
            result = session.run(
                    "MATCH (activity:`prov__Activity`)<--(:`prov__Collection`)<--(s:swirrl__Storage)-[:prov__wasInvalidatedBy]->(entity:swirrl__RollbackWorkflow)\n"
                            + "WHERE activity.uri = $activityId \n"
                            + "RETURN entity.uri AS `@id`, labels(entity) AS `@type`",
                    parameters("activityId", activityId));
            while (result.hasNext()) {
                record = result.next();
                jsonStorage.put("prov:wasInvalidatedBy", new JSONObject(record.asMap()));
            }
            hadMembers.add(jsonStorage);

            /* (1.4.1) :hadMember, :hadMember */
            result = session.run(
                    "MATCH(childEntity:`provone__Data`)<-[:prov__hadMember]-(entity:`prov__Entity`)-[:prov__wasGeneratedBy]->(activity:`prov__Activity`)\n"
                            + "WHERE activity.uri = $activityId\n"
                            + "RETURN labels(childEntity) AS `@type`, childEntity.uri AS `@id`, childEntity.dcterms__source AS `dcterms:source`, \n"
                            + "childEntity.prov__value AS `prov:value`, childEntity.prov__atLocation AS `prov:atLocation` ",
                    parameters("activityId", activityId));
            // Doesn't matter if this query has a result. If the workflow fails,
            // there will be no association with an output collection. So add
            // the result if we have it, otherwise move on.
            while (result.hasNext()) {
                record = result.next();
                hadMembers.add(record.asMap());
            }
            jsonArrayHadMember = new JSONArray(hadMembers);
            /* (1.6) Association, Agent */
            result = session.run("MATCH(activity:`prov__Activity`)-->(as:prov__Association)-->(agent:prov__Agent)" +
                            " WHERE activity.uri = $activityId" +
                            " RETURN agent.rdfs__label AS `rdfs:label`, agent.vcard__uid AS `vcard:uid`, agent.swirrl__group AS `swirrl:group`," +
                            " agent.swirrl__authMode AS `swirrl:authMode`, agent.uri AS `@id`, labels(agent) AS `@type`, as.uri AS `associationid`",
                    parameters("activityId", activityId));
            if (!result.hasNext()) {
                logger.error("Query number 1.6 failed and has no result.");
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
            while (result.hasNext()) {
                record = result.next();
                JSONObject recordJson = new JSONObject(record.asMap());
                JSONObject jsonAssociation = new JSONObject();
                jsonAssociation.put("@id", recordJson.getString("associationid"));
                recordJson.remove("associationid");
                jsonAssociation.put("prov:agent", recordJson);
                jsonAssociations.put(jsonAssociation);
            }

            /* (1.7) Association, plan */
            result = session.run("MATCH(activity:`prov__Activity`)-->(as:prov__Association)-[:prov__hadPlan]->(plan:prov__Entity)\n"
                            + "WHERE activity.uri = $activityId\n"
                            + "RETURN plan.prov__value AS `prov:value`, plan.swirrl__version AS `swirrl:version`, as.uri AS `associationid`,\n"
                            + "plan.prov__atLocation AS `prov:atLocation`, plan.rdfs__label AS `rdfs:label`, plan.uri AS `@id`, labels(plan) AS `@type`",
                    parameters("activityId", activityId));
            if (!result.hasNext()) {
                logger.error("Query number 1.7 failed and has no result.");
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
            while (result.hasNext()) {
                record = result.next();
                JSONObject recordJson = new JSONObject(record.asMap());
                String associationid = recordJson.getString("associationid");
                recordJson.remove("associationid");
                boolean found = false;
                for (int i = 0; i < jsonAssociations.length(); i++) {
                    if (jsonAssociations.getJSONObject(i).getString("@id").equals(associationid)) {
                        found = true;
                        jsonAssociations.getJSONObject(i).put("prov:plan", recordJson);
                    }
                }
                if (!found) {
                    JSONObject jsonAssociation = new JSONObject();
                    jsonAssociation.put("prov:plan", recordJson);
                    jsonAssociations.put(jsonAssociation);
                }
            }

            /* (1.9.1) Used */
            result = session.run("MATCH (input:`provone__Data`)<-[:prov__used]-(activity:`prov__Activity`)\n"
                            + "WHERE activity.uri = $activityId\n"
                            + "RETURN input.prov__value AS `prov:value`, input.rdfs__label AS `rdfs:label`, input.uri AS `@id`, labels(input) AS `@type`",
                    parameters("activityId", activityId));
            if (!result.hasNext()) {
                logger.error("Query number 1.9.1 failed and has no result.");
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
            while (result.hasNext()) {
                record = result.next();
                jsonArrayUsed.put(new JSONObject(record.asMap()));
            }
            /* (2.0.0) ROOKWPS */
            result = session.run("MATCH (part:`provone__Execution`)-[:provone__wasPartOf]->(activity:`prov__Activity`)\n"
                            + "WHERE activity.uri = $activityId\n"
                            + "RETURN part.uri AS `@id`",
                    parameters("activityId", activityId));
            boolean hasRookProvenance = false;
            while (result.hasNext()) {
                record = result.next();
                JSONObject partJson = new JSONObject(record.asMap());
                String partId = partJson.getString("@id");
                if (partId.startsWith("urn:roocs:")) {
                    hasRookProvenance = true;
                }
            }
            if (hasRookProvenance) {
                JSONArray rookwpsJsonArray = getRookwpsProvenance(activityId, session);
                if (null == rookwpsJsonArray) {
                    logger.error("Query number 2.0.0 failed and has no result.");
                    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
                }
                jsonAction.put("provone:hadPart", rookwpsJsonArray);
            }

        }
        jsonGenerated.put("prov:hadMember", jsonArrayHadMember);
        jsonAction.put("prov:used", jsonArrayUsed);
        jsonAction.put("@context", new JSONObject(resultMap));
        jsonAction.put("prov:wasAssociatedWith", jsonAssociations);
        jsonAction.put("prov:generated", jsonGenerated);
        jsonString = jsonAction.toString().replace("__", ":");

        return new ResponseEntity<>(jsonString, HttpStatus.OK);
    }
}