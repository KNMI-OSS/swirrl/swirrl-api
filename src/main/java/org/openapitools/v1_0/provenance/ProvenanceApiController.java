package org.openapitools.v1_0.provenance;

import io.swagger.annotations.ApiParam;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.NativeWebRequest;

import javax.validation.Valid;
import java.io.IOException;
import java.util.Optional;
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2019-10-16T12:18:56.775Z[GMT]")

@Controller
@RequestMapping("${openapi.ePOSNotebook.base-path:/v1.0}")
public class ProvenanceApiController implements ProvenanceApi {

    private final NativeWebRequest request;

    @Autowired
    DatabaseProvenanceInterface databaseProvenanceService;

    @org.springframework.beans.factory.annotation.Autowired
    public ProvenanceApiController(NativeWebRequest request) {
        this.request = request;
    }

    @Override
    public Optional<NativeWebRequest> getRequest() {
        return Optional.ofNullable(request);
    }

    @Override
    public ResponseEntity<?> createProvenance(@ApiParam(value = ""  )  @Valid @RequestBody Resource body) throws IOException {
        return databaseProvenanceService.createProvenance(body);
    }

    public ResponseEntity<?> getSessionActivity(@PathVariable("sessionId") String swirrlColonSessionId,
                                                @Valid @RequestParam(value = "person", required = false) String person,
                                                @Valid @RequestParam(value = "poolId", required = false) String poolId,
                                                @Valid @RequestParam(value = "serviceId", required = false) String serviceId,
                                                @Valid @RequestParam(value = "type", required = false) String type,
                                                @Valid @RequestParam(value = "includeRuns", required = false) Boolean includeRuns,
                                                @Valid @RequestParam(value = "includeAdditionalInfo", required = false) Boolean includeAdditionalInfo) throws JSONException {
        if (null == includeRuns) includeRuns = Boolean.TRUE;
        if (null == includeAdditionalInfo) includeAdditionalInfo = Boolean.FALSE;
        return databaseProvenanceService.getSessionActivity(swirrlColonSessionId, person, poolId, serviceId, type, includeRuns, includeAdditionalInfo);
    }
    public ResponseEntity<?>getServiceSessionIds(@PathVariable("serviceId") String serviceId) throws JSONException {
        return databaseProvenanceService.getServiceSessionIds(serviceId);
    }
    public ResponseEntity<?>getActivityById(@PathVariable("activityId") String activityId) throws JSONException {
        return databaseProvenanceService.getActivityById(activityId);
    }
}
