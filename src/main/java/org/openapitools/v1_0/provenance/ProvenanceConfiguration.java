package org.openapitools.v1_0.provenance;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix="provenance")
public class ProvenanceConfiguration {
    private final Template template = new Template();

    public Template getTemplate() {
        return template;
    }

    public NotebookApi getNotebookApi() {
        return notebookApi;
    }

    private final NotebookApi notebookApi = new NotebookApi();

    public class Template {
        private String expansionUrl;
        private String createSessionId;
        private String updateSessionId;
        private String restoreSessionId;
        private String createSnapshotId;
        private String runWorkflowId;

        public String getCreateSessionId() {
            return createSessionId;
        }

        public void setCreateSessionId(String createSessionId) {
            this.createSessionId = createSessionId;
        }

        public String getUpdateSessionId() {
            return updateSessionId;
        }

        public void setUpdateSessionId(String updateSessionId) {
            this.updateSessionId = updateSessionId;
        }

        public String getRestoreSessionId() {
            return restoreSessionId;
        }

        public void setRestoreSessionId(String restoreSessionId) {
            this.restoreSessionId = restoreSessionId;
        }

        public String getCreateSnapshotId() {
            return createSnapshotId;
        }

        public void setCreateSnapshotId(String createSnapshotId) {
            this.createSnapshotId = createSnapshotId;
        }

        public String getExpansionUrl() {
            return expansionUrl;
        }

        public void setExpansionUrl(String expansionUrl) {
            this.expansionUrl = expansionUrl;
        }

        public String getRunWorkflowId() {
            return runWorkflowId;
        }

        public void setRunWorkflowId(String runWorkflowId) {
            this.runWorkflowId = runWorkflowId;
        }
    }

    public class NotebookApi {
        private String Id;
        private String k8sRecipeId;

        public String getId() {
            return Id;
        }

        public void setId(String id) {
            Id = id;
        }

        public String getK8sRecipeId() {
            return k8sRecipeId;
        }

        public void setK8sRecipeId(String k8sRecipeId) {
            this.k8sRecipeId = k8sRecipeId;
        }
    }
}
