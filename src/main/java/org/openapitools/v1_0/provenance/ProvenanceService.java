package org.openapitools.v1_0.provenance;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.kubernetes.client.openapi.models.V1Pod;
import io.kubernetes.client.openapi.models.V1PodCondition;
import io.kubernetes.client.openapi.models.V1PersistentVolumeClaim;
import io.kubernetes.client.openapi.models.V1PersistentVolumeClaimList;
import nl.knmi.*;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.hc.client5.http.impl.classic.HttpClients;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.apache.http.ssl.SSLContextBuilder;
import org.openapitools.v1_0.api.*;
import org.openapitools.v1_0.model.*;
import org.openapitools.v1_0.model.ExtendedNotebook;
import org.openapitools.v1_0.model.Notebook;
import org.openapitools.v1_0.model.NotebookLibrary;
import org.openapitools.v1_0.model.Job;
import org.openapitools.v1_0.model.kubernetes.*;
import org.openprovenance.prov.interop.InteropFramework;
import org.openprovenance.prov.model.ProvFactory;
import org.openprovenance.prov.model.QualifiedName;
import org.openprovenance.prov.template.expander.Bindings;
import org.openprovenance.prov.template.expander.BindingsJson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.net.ssl.SSLContext;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


@Service
public class ProvenanceService {

    @Autowired
    SessionFactory sessionFactory;

    @Autowired
    ObjectFactory<KubernetesService> kubernetesServiceFactory;
    @Autowired
    TemplateCatalog templateCatalog;

    @Value("${provenance.template.expansion-url}")
    private String TEMPLATE_EXPANSION_SERVICE_HOST;
    @Value("${provenance.notebookapi.id}")
    private String NOTEBOOK_API_ID;
    @Value("${server.port}")
    String serverPort;
    @Value("${server.servlet.context-path}")
    String contextPath;
    @Value("${swirrl-api.version}")
    private String apiVersion;


    private Logger logger = LoggerFactory.getLogger(ProvenanceService.class);

    private final String UUID_NAMESPACE = "urn:uuid:";
    private final String UUID_PREFIX = "uuid";

    /**
     * Expands a template using the template expansion service.
     * @param resultBindings The bindings used for the expansion. This should be v3 json bindings.
     * @param templateId The id with which the template to expand is registered in the template expansion service.
     * @return A response indicating if the expansion was successful. If it was successful, the body contains the expanded PROV document.
     */
    public ResponseEntity<?> expandProvTemplate(String resultBindings, String templateId) {
        // TODO: Temporary solution for selfsigned certificate and issue with K8S certificates. Otherwise just use RestTemplate restTemplate = new RestTemplate();
        try {
            SSLContext sslContext = SSLContextBuilder
                    .create()
                    .loadTrustMaterial(new TrustSelfSignedStrategy())   // Make sure that we trust selfsigned certificates
                    .build();

            CloseableHttpClient httpClient = HttpClients.custom().build();
            HttpComponentsClientHttpRequestFactory requestFactory =
                    new HttpComponentsClientHttpRequestFactory();

            requestFactory.setHttpClient(httpClient);

            RestTemplate restTemplate = new RestTemplate(requestFactory);

            logger.debug("Expanding bindings at " + TEMPLATE_EXPANSION_SERVICE_HOST + "/templates/" + templateId + "/expand");
            ResponseEntity<String> result = restTemplate.exchange(
                    TEMPLATE_EXPANSION_SERVICE_HOST + "/templates/" + templateId + "/expand?fmt=provn&writeprov=false&bindver=v3",
                    HttpMethod.POST,
                    new HttpEntity<String>(resultBindings),
                    String.class);

            return result;

        } catch (Exception e) {
            return EposUtils.handleError(
                    "Impossible to trace provenance due to exception, but notebook has been created successfully", HttpStatus.INTERNAL_SERVER_ERROR.value(), e);
        }
    }

    private void addLibrary2BindingsBean(ProvFactory provFactory, NotebookLibrary notebookLibrary, CreateNotebookBindingsBean bindingsBean) {

        bindingsBean.addLib(provFactory.newQualifiedName(UUID_NAMESPACE, notebookLibrary.getLibname() + "-" + notebookLibrary.getLibversion().replace(".", "_"), UUID_PREFIX));
        bindingsBean.addInstallationmode(notebookLibrary.getSource());
        bindingsBean.addLibname(notebookLibrary.getLibname());
        if (notebookLibrary.getLibversion() != null) {
            bindingsBean.addLibversion(notebookLibrary.getLibversion());
        } else {
            bindingsBean.addLibversion("-1"); //  Temporarily adding a dummy version to ensure that bindings are expanded correctly.
        }
    }
    private void addLibrary2BindingsBean(ProvFactory provFactory, NotebookLibrary notebookLibrary, RestoreLibsBindingsBean bindingsBean) {
        bindingsBean.addLib(provFactory.newQualifiedName(UUID_NAMESPACE, notebookLibrary.getLibname() + "-" + notebookLibrary.getLibversion().replace(".", "_"), UUID_PREFIX));
        logger.debug("Adding " + notebookLibrary.getLibname() + "-" + notebookLibrary.getLibversion().replace(".", "_"));
        bindingsBean.addInstallationmode(notebookLibrary.getSource());
        bindingsBean.addLibname(notebookLibrary.getLibname());
        if (notebookLibrary.getLibversion() != null) {
            bindingsBean.addLibversion(notebookLibrary.getLibversion());
        } else {
            bindingsBean.addLibversion("-1"); //  Temporarily adding a dummy version to ensure that bindings are expanded correctly.
        }
    }
    // Since we are waiting for the pod to be ready now, we can use the start time of the notebook for createSessionEndTime.
    private String getPodReadyTime (V1Pod pod) {
        String podReadyTime = pod.getStatus().getContainerStatuses().get(0).getState().getRunning().getStartedAt().toString();
        return podReadyTime;
    }

    @Async
    public void traceProvenanceCreateSession(Notebook notebook, String activityStartTime, NotebookService notebookService,
                                                          UserInfo userInfo) {
        this.traceProvenanceCreateSessionNonAsync(notebook, activityStartTime, notebookService, userInfo);
    }

    protected ResponseEntity<?> traceProvenanceCreateSessionNonAsync(Notebook notebook, String activityStartTime,
                                                                     NotebookService notebookService, UserInfo userInfo) {
        KubernetesService kubernetesService = kubernetesServiceFactory.getObject();

        V1Pod runningPod = null;
        try {
            runningPod = kubernetesService.waitOnContainerReady(NotebookDeploymentFactory.determineServiceLabel(notebook.getId()), 10);
            logger.debug("Pod ready at " + getPodReadyTime(runningPod));
            logger.debug("ImageID: " + runningPod.getStatus().getContainerStatuses().get(0).getImageID());
            logger.debug("Pod id: " + runningPod.getMetadata().getName());
        } catch (IOException exception) {
            // TODO: Handle this exception neatly.
            logger.error("Exception: ", exception);
        }
        String podStartTime = getPodReadyTime(runningPod);
        String podID = runningPod.getMetadata().getName();
        String imageID = runningPod.getStatus().getContainerStatuses().get(0).getImageID();

        ProvFactory provFactory = org.openprovenance.prov.xml.ProvFactory.getFactory();
        CreateNotebookBindingsBean bindingsBean = new CreateNotebookBindingsBean(provFactory);

        // Initialize User
        if (userInfo != null) {
            bindingsBean.addName(userInfo.getName());
            bindingsBean.addUser(provFactory.newQualifiedName(UUID_NAMESPACE, userInfo.getId(), UUID_PREFIX));
            bindingsBean.addAuthmode(userInfo.getAuthMode());
            bindingsBean.addGroup(userInfo.getGroup());
        }

        // Initialize NotebookAPI
        bindingsBean.addNotebookapi(provFactory.newQualifiedName(UUID_NAMESPACE, NOTEBOOK_API_ID, UUID_PREFIX));
        bindingsBean.addNameapi("SWIRRL-API");
        bindingsBean.addMethodpath("POST /notebook/");

        // Initialize Libs
        bindingsBean.addLiblist(provFactory.newQualifiedName(UUID_NAMESPACE, UUID.randomUUID().toString(), UUID_PREFIX));

        ResponseEntity<?> response = notebookService.getNotebookById(notebook.getId());
        logger.debug(response.toString());
        if (response.getStatusCode() == HttpStatus.OK) {
            logger.info("Determine installed libraries.");
            ExtendedNotebook extendedNotebook = (ExtendedNotebook) response.getBody();

            // Initialize User requested libs list
            List<String> userRequestedLibraryNames = new ArrayList<String>();
            if (notebook.getUserRequestedLibraries() != null) {
                for (NotebookLibrary notebookLibrary : notebook.getUserRequestedLibraries()) {
                    userRequestedLibraryNames.add(notebookLibrary.getLibname());
                }
            }
            String libListValue = "";
            for (String lib : userRequestedLibraryNames) libListValue += lib + "\n";
            bindingsBean.addLiblistvalue(libListValue);

            // Initialize Installed, user- and system-, Libs
            if (extendedNotebook.getInstalledLibraries() != null) {
                for (NotebookLibrary notebookLibrary : extendedNotebook.getInstalledLibraries()) {
                    addLibrary2BindingsBean(provFactory, notebookLibrary, bindingsBean);
                }
            }
        } else {
            logger.warn("Could not determine installed libraries. Providing empty list for provenance.");
            bindingsBean.addLib(provFactory.newQualifiedName(UUID_NAMESPACE, "", UUID_PREFIX));
        }


        logger.debug("Pod started at " + getPodReadyTime(runningPod));
        logger.debug("ImageID: " + runningPod.getStatus().getContainerStatuses().get(0).getImageID());
        logger.debug("Pod id: " + runningPod.getMetadata().getName());
        logger.debug("\n=====  Running Pod  =====\n" + runningPod.toString());

        bindingsBean.addImagelabel("Notebook");

        // Initialize SystemImage
        bindingsBean.addSystemimage(provFactory.newQualifiedName(UUID_NAMESPACE, imageID.replaceAll("[^a-zA-Z0-9]+", "-"), UUID_PREFIX));
        bindingsBean.addSystemimagelocation(imageID);

        // Initialize CreateSession // TODO: Do we want to use a vargen here?
        bindingsBean.addSessionid(notebook.getSessionId());
        bindingsBean.addPoolid(notebook.getPoolId());
        bindingsBean.addStarttime(activityStartTime);
        bindingsBean.addEndtime(podStartTime);
        bindingsBean.addNotebookid(notebook.getId());

        String workingdirVolumeName = sessionFactory.determineWorkingdirPersistentVolumeClaimName(notebook.getPoolId());
        bindingsBean.addVolume(provFactory.newQualifiedName(UUID_NAMESPACE, workingdirVolumeName, UUID_PREFIX));
        bindingsBean.addVolumeid(workingdirVolumeName);

        String datadirVolumeName = sessionFactory.determineDatadirPersistentVolumeClaimName(notebook.getSessionId());
        bindingsBean.addVolume(provFactory.newQualifiedName(UUID_NAMESPACE, datadirVolumeName, UUID_PREFIX));
        bindingsBean.addVolumeid(datadirVolumeName);

        // Initialize Jupyter
        bindingsBean.addJupyter(provFactory.newQualifiedName(UUID_NAMESPACE, notebook.getId(), UUID_PREFIX));
        if (notebook.getServiceURL().contains("token="))
            bindingsBean.addAccessurl(notebook.getServiceURL().split("\\?")[0]);
        else
            bindingsBean.addAccessurl(notebook.getServiceURL());
        bindingsBean.addGeneratedat(podStartTime);
        bindingsBean.addCreatejupyter(provFactory.newQualifiedName(UUID_NAMESPACE, UUID.randomUUID().toString(), UUID_PREFIX));

        // Make sure that the Jupyter id gets written to the running notebook.
        String command = "echo \"## CurrentJupyterUpdateID: " + notebook.getId() + "\" > " + notebookService.PREVIOUS_JUPYTER_DOC + notebook.getId();
        kubernetesService.executeShellCommandOnPod(runningPod, new String[] {command});

        // Export as json
        Bindings bindings = bindingsBean.getBindings();
        bindings.addVariableBindingsAsAttributeBindings();
        BindingsJson.BindingsBean bean = BindingsJson.toBean(bindings);

        ObjectMapper mapper = new ObjectMapper();
        String resultBindings;

        try {
            resultBindings = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(bean);
            logger.info(resultBindings);
        } catch (IOException e) {
            return EposUtils.handleError("Impossible to trace provenance due to exception, but notebook has been created successfully", HttpStatus.INTERNAL_SERVER_ERROR.value(), e);
        }

        ResponseEntity<?> expandProvenanceResponse = expandProvTemplate(resultBindings, templateCatalog.getIdByTitle("create_notebook"));
        if (HttpStatus.OK != expandProvenanceResponse.getStatusCode()) {
            return expandProvenanceResponse;
        }
        logger.info((String) expandProvenanceResponse.getBody());

        Resource resource = new ByteArrayResource(((String) expandProvenanceResponse.getBody()).getBytes());
        saveProvenanceSession(resource);
        return expandProvenanceResponse;
    }

    @Async
    public void traceProvenanceCreateViewer(Viewer viewer, String activityStartTime, UserInfo userInfo) {
        this.traceProvenanceCreateViewerNonAsync(viewer, activityStartTime, userInfo);
    }

    protected ResponseEntity<?> traceProvenanceCreateViewerNonAsync(Viewer viewer, String activityStartTime, UserInfo userInfo) {
        KubernetesService kubernetesService = kubernetesServiceFactory.getObject();

        V1Pod runningPod = null;
        try {
            runningPod = kubernetesService.waitOnContainerReady(ServiceDeploymentFactory.determineServiceLabel(viewer.getId()), 10);
            logger.debug("Pod ready at " + getPodReadyTime(runningPod));
            logger.debug("ImageID: " + runningPod.getStatus().getContainerStatuses().get(0).getImageID());
            logger.debug("Pod id: " + runningPod.getMetadata().getName());
        } catch (IOException exception) {
            // TODO: Handle this exception neatly.
            logger.error("Exception: ", exception);
        }
        String podStartTime = getPodReadyTime(runningPod);
        String imageID = runningPod.getStatus().getContainerStatuses().get(0).getImageID();
        logger.debug("ImageID: " + imageID);

        ProvFactory provFactory = org.openprovenance.prov.xml.ProvFactory.getFactory();
        CreateViewerBindingsBean bindingsBean = new CreateViewerBindingsBean(provFactory);

        // Initialize User
        if (userInfo != null) {
            bindingsBean.addUser(provFactory.newQualifiedName(UUID_NAMESPACE, userInfo.getId(), UUID_PREFIX));
            bindingsBean.addName(userInfo.getName());
            bindingsBean.addAuthmode(userInfo.getAuthMode());
            bindingsBean.addGroup(userInfo.getGroup());
        } else {
            bindingsBean.addUser(provFactory.newQualifiedName(UUID_NAMESPACE, "mock_user_id", UUID_PREFIX));
            bindingsBean.addAuthmode("auth_mode_id");
            bindingsBean.addGroup("mock_group_id");
            bindingsBean.addName("Mock User");
        }

        // Initialize NotebookAPI
        bindingsBean.addNotebookapi(provFactory.newQualifiedName(UUID_NAMESPACE, NOTEBOOK_API_ID, UUID_PREFIX));
        bindingsBean.addNameapi("SWIRRL-API");
        bindingsBean.addMethodpath("POST /viewer/" + viewer.getServiceType() + "/");

        logger.debug("Pod started at " + getPodReadyTime(runningPod));
        logger.debug("\n=====  Running Pod  =====\n" + runningPod.toString());

        bindingsBean.addImagelabel(viewer.getServiceType());

        // Initialize SystemImage
        bindingsBean.addSystemimage(provFactory.newQualifiedName(UUID_NAMESPACE, imageID.replaceAll("[^a-zA-Z0-9]+", "-"), UUID_PREFIX));
        bindingsBean.addSystemimagelocation(imageID);

        // Initialize CreateSession // TODO: Do we want to use a vargen here?
        bindingsBean.addSessionid(viewer.getSessionId());
        bindingsBean.addPoolid(viewer.getPoolId());
        bindingsBean.addStarttime(activityStartTime);
        bindingsBean.addEndtime(podStartTime);
        bindingsBean.addViewerid(viewer.getId());

        String workingdirVolumeName = sessionFactory.determineWorkingdirPersistentVolumeClaimName(viewer.getPoolId());
        bindingsBean.addVolume(provFactory.newQualifiedName(UUID_NAMESPACE, workingdirVolumeName, UUID_PREFIX));
        bindingsBean.addVolumeid(workingdirVolumeName);

        String datadirVolumeName = sessionFactory.determineDatadirPersistentVolumeClaimName(viewer.getSessionId());
        bindingsBean.addVolume(provFactory.newQualifiedName(UUID_NAMESPACE, datadirVolumeName, UUID_PREFIX));
        bindingsBean.addVolumeid(datadirVolumeName);

        // Initialize Jupyter
        bindingsBean.addViewer(provFactory.newQualifiedName(UUID_NAMESPACE, viewer.getId(), UUID_PREFIX));
        bindingsBean.addAccessurl(viewer.getServiceURL().split("\\?")[0]);
        bindingsBean.addGeneratedat(podStartTime);
        bindingsBean.addCreateviewer(provFactory.newQualifiedName(UUID_NAMESPACE, UUID.randomUUID().toString(), UUID_PREFIX));
        // Export as json
        Bindings bindings = bindingsBean.getBindings();
        bindings.addVariableBindingsAsAttributeBindings();
        BindingsJson.BindingsBean bean = BindingsJson.toBean(bindings);

        ObjectMapper mapper = new ObjectMapper();
        String resultBindings;

        try {
            resultBindings = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(bean);
            logger.info(resultBindings);
        } catch (IOException e) {
            return EposUtils.handleError("Impossible to trace provenance due to exception, but enlighten has been created successfully", HttpStatus.INTERNAL_SERVER_ERROR.value(), e);
        }

        ResponseEntity<?> expandProvenanceResponse = expandProvTemplate(resultBindings, templateCatalog.getIdByTitle("create_viewer"));
        if (HttpStatus.OK != expandProvenanceResponse.getStatusCode()) {
            return expandProvenanceResponse;
        }
        logger.info((String) expandProvenanceResponse.getBody());

        Resource resource = new ByteArrayResource(((String) expandProvenanceResponse.getBody()).getBytes());
        saveProvenanceSession(resource);
        return expandProvenanceResponse;
    }

//    @Async
    public ResponseEntity<?> traceProvenanceSnapshot(String serviceId, String poolId, String sessionId, SnapshotURL url, Snapshot snapshot, Job job, String errorMessage, UserInfo userInfo) {
        return this.traceProvenanceSnapshotNonAsync(serviceId, poolId, sessionId, url, snapshot, job, errorMessage, userInfo);
    }

    protected ResponseEntity<?> traceProvenanceSnapshotNonAsync(String serviceId, String poolId, String sessionId, SnapshotURL url, Snapshot snapshot, Job job, String errorMessage, UserInfo userInfo) {
        KubernetesService kubernetesService = kubernetesServiceFactory.getObject();
        V1Pod runningPod = null;
        try {
            runningPod = kubernetesService.waitOnContainerReady(NotebookDeploymentFactory.determineServiceLabel(serviceId), 3);
            logger.debug("Pod started at " + runningPod.getStatus().getContainerStatuses().get(0).getState().getRunning().getStartedAt().toString());
            logger.debug("Pod ready at " + getPodReadyTime(runningPod));
            logger.debug("ImageID: " + runningPod.getStatus().getContainerStatuses().get(0).getImageID());
            logger.debug("Pod id: " + runningPod.getMetadata().getName());
        } catch (IOException exception) {
            // TODO: Handle this exception neatly.
            logger.error("Exception: ", exception);
        }
        String podStartTime = getPodReadyTime(runningPod);
        String podID = runningPod.getMetadata().getName();
        String imageID = runningPod.getStatus().getContainerStatuses().get(0).getImageID();

        ProvFactory provFactory = org.openprovenance.prov.xml.ProvFactory.getFactory();
        CreateSnapshotBindingsBean bindingsBean = new CreateSnapshotBindingsBean(provFactory);

        // Initialize User
        if (userInfo != null) {
            bindingsBean.addUser(provFactory.newQualifiedName(UUID_NAMESPACE, userInfo.getId(), UUID_PREFIX));
            bindingsBean.addName(userInfo.getName());
            bindingsBean.addAuthmode(userInfo.getAuthMode());
            bindingsBean.addGroup(userInfo.getGroup());
        } else {
            bindingsBean.addUser(provFactory.newQualifiedName(UUID_NAMESPACE, "mock_user_id", UUID_PREFIX));
            bindingsBean.addAuthmode("auth_mode_id");
            bindingsBean.addGroup("mock_group_id");
            bindingsBean.addName("Mock User");
        }
        // Initialize NotebookAPI
        bindingsBean.addNameapi("SWIRRL-API");
        bindingsBean.addVersionapi(apiVersion);
        bindingsBean.addMethodpath("POST /notebook/"+serviceId+"/snapshot");
        bindingsBean.addSnapagent(provFactory.newQualifiedName(UUID_NAMESPACE, UUID.randomUUID().toString(), UUID_PREFIX));

        // Initialize CreateSession // TODO: Do we want to use a vargen here?
        bindingsBean.addSessionid(sessionId);
        bindingsBean.addPoolid(poolId);
        bindingsBean.addServiceid(serviceId);
        bindingsBean.addStarttime(job.getStartTime());
        bindingsBean.addEndtime(job.getEndTime());

        String workingdirVolumeName = sessionFactory.determineWorkingdirPersistentVolumeClaimName(poolId);
        bindingsBean.addWorkvolume(provFactory.newQualifiedName(UUID_NAMESPACE, workingdirVolumeName, UUID_PREFIX));
        bindingsBean.addWorkvolumeid(workingdirVolumeName);

        String datadirVolumeName = sessionFactory.determineDatadirPersistentVolumeClaimName(sessionId);
        bindingsBean.addVolume(provFactory.newQualifiedName(UUID_NAMESPACE, datadirVolumeName, UUID_PREFIX));

        // Initialize Jupyter
        bindingsBean.addJupyter(provFactory.newQualifiedName(UUID_NAMESPACE, serviceId, UUID_PREFIX));
        bindingsBean.addGeneratedat(podStartTime);

       // Snapshot specific
        bindingsBean.addSnapshot(provFactory.newQualifiedName(UUID_NAMESPACE, UUID.randomUUID().toString(), UUID_PREFIX));
        if (null != url)
            bindingsBean.addRepourl(url.getSnapshotURL());
        if (null != errorMessage)
            bindingsBean.addMessage(errorMessage);
        bindingsBean.addDescription(snapshot.getUserMessage());

        // Export as json
        Bindings bindings = bindingsBean.getBindings();
        bindings.addVariableBindingsAsAttributeBindings();
        BindingsJson.BindingsBean bean = BindingsJson.toBean(bindings);

        ObjectMapper mapper = new ObjectMapper();
        String resultBindings;

        try {
            resultBindings = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(bean);
            logger.info(resultBindings);
        } catch (IOException e) {
            return EposUtils.handleError("Impossible to trace provenance due to exception, but notebook has been created successfully", HttpStatus.INTERNAL_SERVER_ERROR.value(), e);
        }

        ResponseEntity<?> expandProvenanceResponse = expandProvTemplate(resultBindings, templateCatalog.getIdByTitle("create_snapshot"));

        if (HttpStatus.OK != expandProvenanceResponse.getStatusCode()) {
            return expandProvenanceResponse;
        }
        logger.info((String) expandProvenanceResponse.getBody());

        Resource resource = new ByteArrayResource(((String) expandProvenanceResponse.getBody()).getBytes());
        saveProvenanceSession(resource);
        return expandProvenanceResponse;
    }

    @Async
    public void traceProvenanceRestoreSession(Notebook notebook, String activityId, String activityStartTime, String prevJupyterId,
                                                           NotebookService notebookService, UserInfo userInfo) {
        this.traceProvenanceRestoreSessionNonAsync(notebook, activityId, activityStartTime, prevJupyterId, notebookService, userInfo);
    }
    protected ResponseEntity<?> traceProvenanceRestoreSessionNonAsync(Notebook notebook, String activityId, String activityStartTime,
                                                                      String prevJupyterId, NotebookService notebookService, UserInfo userInfo) {
        KubernetesService kubernetesService = kubernetesServiceFactory.getObject();

        V1Pod runningPod = null;
        try {
            runningPod = kubernetesService.waitOnContainerReady(NotebookDeploymentFactory.determineServiceLabel(notebook.getId()), 3);
            logger.debug("Pod ready at " + getPodReadyTime(runningPod));
            logger.debug("ImageID: " + runningPod.getStatus().getContainerStatuses().get(0).getImageID());
            logger.debug("Pod id: " + runningPod.getMetadata().getName());
        } catch (IOException exception) {
            // TODO: Handle this exception neatly.
            logger.error("Exception: ", exception);
        }
        String podStartTime = getPodReadyTime(runningPod);
        String podID = runningPod.getMetadata().getName();
        String imageID = runningPod.getStatus().getContainerStatuses().get(0).getImageID();

        ProvFactory provFactory = org.openprovenance.prov.xml.ProvFactory.getFactory();
        RestoreLibsBindingsBean bindingsBean = new RestoreLibsBindingsBean(provFactory);

        // Initialize User
        if (userInfo != null) {
            bindingsBean.addUser(provFactory.newQualifiedName(UUID_NAMESPACE, userInfo.getId(), UUID_PREFIX));
            bindingsBean.addName(userInfo.getName());
            bindingsBean.addAuthmode(userInfo.getAuthMode());
            bindingsBean.addGroup(userInfo.getGroup());
        } else {
            bindingsBean.addUser(provFactory.newQualifiedName(UUID_NAMESPACE, "mock_user_id", UUID_PREFIX));
            bindingsBean.addAuthmode("auth_mode_id");
            bindingsBean.addGroup("mock_group_id");
            bindingsBean.addName("Mock User");
        }
        // Initialize NotebookAPI
        bindingsBean.addNotebookapi(provFactory.newQualifiedName(UUID_NAMESPACE, NOTEBOOK_API_ID, UUID_PREFIX));
        bindingsBean.addNameapi("SWIRRL-API");
        bindingsBean.addMethodpath("PUT /notebook/"+notebook.getId()+"/restorelibs/"+activityId);

        ResponseEntity<?> response = notebookService.getNotebookById(notebook.getId());
        logger.debug(response.toString());

        if (response.getStatusCode() == HttpStatus.OK) {
            ExtendedNotebook extendedNotebook = (ExtendedNotebook) response.getBody();
            logger.info("Determine installed libraries.");
            // Initialize Installed, user- and system-, Libs
            // FIXME: make sure this is called after "conda env update" is called in the startup configmap.
            if (extendedNotebook.getInstalledLibraries() != null) {
                for (NotebookLibrary notebookLibrary : extendedNotebook.getInstalledLibraries()) {
                    addLibrary2BindingsBean(provFactory, notebookLibrary, bindingsBean);
                }
            }
        } else {
            logger.warn("Could not determine installed libraries. Providing empty list for provenance.");
            bindingsBean.addLib(provFactory.newQualifiedName(UUID_NAMESPACE, "", UUID_PREFIX));
        }


        logger.debug("Pod started at " + getPodReadyTime(runningPod));
        logger.debug("ImageID: " + runningPod.getStatus().getContainerStatuses().get(0).getImageID());
        logger.debug("Pod id: " + runningPod.getMetadata().getName());
        logger.debug("\n=====  Running Pod  =====\n" + runningPod.toString());

        bindingsBean.addImagelabel("Notebook");

        // Initialize SystemImage
        bindingsBean.addSystemimage(provFactory.newQualifiedName(UUID_NAMESPACE, imageID.replaceAll("[^a-zA-Z0-9]+", "-"), UUID_PREFIX));
        bindingsBean.addSystemimagelocation(imageID);

        // Initialize CreateSession // TODO: Do we want to use a vargen here?
        bindingsBean.addSessionid(notebook.getSessionId());
        bindingsBean.addPoolid(notebook.getPoolId());
        bindingsBean.addStarttime(activityStartTime);
        bindingsBean.addEndtime(podStartTime);
        bindingsBean.addNotebookid(notebook.getId());

        String workingdirVolumeName = sessionFactory.determineWorkingdirPersistentVolumeClaimName(notebook.getPoolId());
        bindingsBean.addVolume(provFactory.newQualifiedName(UUID_NAMESPACE, workingdirVolumeName, UUID_PREFIX));
        bindingsBean.addVolumeid(workingdirVolumeName);

        String datadirVolumeName = sessionFactory.determineDatadirPersistentVolumeClaimName(notebook.getSessionId());
        bindingsBean.addVolumeid(datadirVolumeName);

        V1PersistentVolumeClaimList pvcs = (V1PersistentVolumeClaimList)kubernetesService.retrievePVCsByLabel(
                ServiceDeploymentFactory.SESSION_ID_LABEL + "=" + notebook.getSessionId()).getBody();
        for (V1PersistentVolumeClaim pvc : pvcs.getItems()) {
            if (pvc.getMetadata().getName().equals(datadirVolumeName)) {
                Map<String, String> labels = pvc.getMetadata().getLabels();
                if (labels.containsKey("volumeid")) {
                    bindingsBean.addVolume(provFactory.newQualifiedName(UUID_NAMESPACE, labels.get("volumeid"), UUID_PREFIX));
                } else {
                    bindingsBean.addVolume(provFactory.newQualifiedName(UUID_NAMESPACE, datadirVolumeName, UUID_PREFIX));
                }
            }
        }

        // Initialize Jupyter
        String entityId = UUID.randomUUID().toString();
        bindingsBean.addJupyterprev(provFactory.newQualifiedName(UUID_NAMESPACE, prevJupyterId, UUID_PREFIX));
        bindingsBean.addJupyter(provFactory.newQualifiedName(UUID_NAMESPACE, entityId, UUID_PREFIX));
        if (notebook.getServiceURL().contains("token="))
            bindingsBean.addAccessurl(notebook.getServiceURL().split("\\?")[0]);
        else
            bindingsBean.addAccessurl(notebook.getServiceURL());
        bindingsBean.addGeneratedat(podStartTime);
        bindingsBean.addRestorejupyter(provFactory.newQualifiedName(UUID_NAMESPACE, UUID.randomUUID().toString(), UUID_PREFIX));

        // Make sure that the Jupyter id gets written to the running notebook.
        String command = "echo \"## CurrentJupyterUpdateID: " + entityId + "\" > " + notebookService.PREVIOUS_JUPYTER_DOC + notebook.getId();
        kubernetesService.executeShellCommandOnPod(runningPod, new String[] {command});

        // Export as json
        Bindings bindings = bindingsBean.getBindings();
        bindings.addVariableBindingsAsAttributeBindings();
        BindingsJson.BindingsBean bean = BindingsJson.toBean(bindings);

        ObjectMapper mapper = new ObjectMapper();
        String resultBindings;

        try {
            resultBindings = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(bean);
            logger.info(resultBindings);
        } catch (IOException e) {
            return EposUtils.handleError("Impossible to trace provenance due to exception, but notebook has been created successfully", HttpStatus.INTERNAL_SERVER_ERROR.value(), e);
        }

        ResponseEntity<?> expandProvenanceResponse = expandProvTemplate(resultBindings, templateCatalog.getIdByTitle("restore_libraries"));
        if (HttpStatus.OK != expandProvenanceResponse.getStatusCode()) {
            return expandProvenanceResponse;
        }
        logger.info((String) expandProvenanceResponse.getBody());

        Resource resource = new ByteArrayResource(((String) expandProvenanceResponse.getBody()).getBytes());
        saveProvenanceSession(resource);
        return expandProvenanceResponse;
    }

    public ResponseEntity<?> saveProvenanceSession(Resource resource){
        final String uri = "http://localhost:" + serverPort + contextPath + "/" + apiVersion + "/provenance";
        try {
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.postForLocation(uri, resource);
            restTemplate.getErrorHandler();
            return new ResponseEntity<>(HttpStatus.OK);
        }catch (Exception e){
            return EposUtils.handleError("Saving provenance session to "+uri+" has failed", HttpStatus.INTERNAL_SERVER_ERROR.value(), e);
        }
    }

}
