package org.openapitools.v1_0.provenance;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.ConfigurationCondition;

import org.springframework.boot.autoconfigure.condition.AnyNestedCondition;

public class ConditionMongodb extends AnyNestedCondition{
    public ConditionMongodb() {
        super(ConfigurationCondition.ConfigurationPhase.REGISTER_BEAN);
    }

    // Same condition on property
    @ConditionalOnProperty(name = "provenance.database.type", havingValue = "mongodb")
    static class ConditionalOnDatabaseProperty {
    }

    @ConditionalOnProperty(name = "provenance.database.type", havingValue = "all")
    static class ConditionalOnDatabasePropertyAll {
    }
}