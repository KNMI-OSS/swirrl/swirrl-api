package org.openapitools.v1_0.provenance;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.ConfigurationCondition;

import org.springframework.boot.autoconfigure.condition.AnyNestedCondition;

public class ConditionNeo4j extends AnyNestedCondition{
    public ConditionNeo4j() {
        super(ConfigurationCondition.ConfigurationPhase.REGISTER_BEAN);
    }

    // Same condition on property
    @ConditionalOnProperty(name = "provenance.database.type", havingValue = "neo4j")
    static class ConditionalOnDatabaseProperty {
    }

    @ConditionalOnProperty(name = "provenance.database.type", havingValue = "all")
    static class ConditionalOnDatabasePropertyAll {
    }
}