package org.openapitools.v1_0.provenance;

import org.apache.hc.client5.http.impl.classic.HttpClients;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

@Service
public class TemplateCatalog {
    private Map<String,String> provTemplates;

    @Autowired
    ProvenanceConfiguration provenanceConfiguration;
    private Logger logger = LoggerFactory.getLogger(TemplateCatalog.class);

    public TemplateCatalog(ProvenanceConfiguration provenanceConfiguration) {
        this.provenanceConfiguration = provenanceConfiguration;
        this.provTemplates = new HashMap<String,String>();
    }

    public String getIdByTitle(String title) {
        JSONArray templateList = this.getTemplateList();
        if (null != templateList) {
            try {
                for (int i = 0; i < templateList.length(); i++) {
                    String key_title = templateList.getJSONObject(i).getString("title");
                    String value_id = templateList.getJSONObject(i).getString("id");
                    logger.debug("Template " + i + " : " + key_title + " - " + value_id);
                    if (null != value_id) {
                        this.provTemplates.put(key_title, value_id);
                        if (key_title.equals(title))
                            return value_id;
                    }
                }
            } catch (Exception ex) {
                logger.error("Exception parsing template list: " + ex.getMessage());
            }
        }
        if(this.provTemplates.containsKey(title))
            return this.provTemplates.get(title);
        return null;
    }

    private JSONArray getTemplateList() {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        /* Call the template catalog and determine the template IDs. */
        HttpComponentsClientHttpRequestFactory requestFactory =
                new HttpComponentsClientHttpRequestFactory();

        requestFactory.setHttpClient(httpClient);

        RestTemplate restTemplate = new RestTemplate(requestFactory);
        try {
            RequestEntity request = RequestEntity.post(
                            new URI(provenanceConfiguration.getTemplate().getExpansionUrl() + "/getTemplateList"))
                    .accept(MediaType.APPLICATION_JSON).build();

            ResponseEntity<String> result = restTemplate.exchange(request, String.class);
            return new JSONArray(result.getBody());
        } catch (Exception ex) {
            logger.error("Could not obtain template list from provenance catalog at "+
                    provenanceConfiguration.getTemplate().getExpansionUrl() + "/getTemplateList");
        }
        return null;
    }
}
