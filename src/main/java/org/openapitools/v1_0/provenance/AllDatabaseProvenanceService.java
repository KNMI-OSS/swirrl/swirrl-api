package org.openapitools.v1_0.provenance;

import org.json.JSONException;
import org.openapitools.v1_0.api.EposUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service
@Primary
@ConditionalOnProperty(name = "provenance.database.type", havingValue = "all")
public class AllDatabaseProvenanceService implements DatabaseProvenanceInterface {

    @Autowired
    Neo4jProvenanceService neo4jProvenanceService;

    private static Logger logger = LoggerFactory.getLogger(AllDatabaseProvenanceService.class);

    public ResponseEntity<?> createProvenance(Resource body) throws IOException {
        ResponseEntity responseNeo4j = neo4jProvenanceService.createProvenance(body);
        if(HttpStatus.OK != responseNeo4j.getStatusCode()) {
            return EposUtils.handleError("Saving provenance to Neo4j has failed", HttpStatus.INTERNAL_SERVER_ERROR.value(),null);
        }
        return new ResponseEntity<>(body, HttpStatus.OK);
    }
    public ResponseEntity<?>getSessionActivity(String swirrlColonSessionId, String person, String poolId, String serviceId, String type, Boolean includeRuns, Boolean includeAdditionalInfo) throws JSONException {
        ResponseEntity response = neo4jProvenanceService.getSessionActivity(swirrlColonSessionId, person, poolId, serviceId, type, includeRuns, includeAdditionalInfo);
        if(HttpStatus.OK != response.getStatusCode()){
            return EposUtils.handleError("getting the activities from session has failed", HttpStatus.INTERNAL_SERVER_ERROR.value(),null);
        }
        return new ResponseEntity<List<?>>(HttpStatus.OK);
    }
    public ResponseEntity<?>getServiceSessionIds(String serviceId) throws JSONException {
        neo4jProvenanceService.getServiceSessionIds(serviceId);
        return new ResponseEntity<>(HttpStatus.OK);
    }
    public ResponseEntity<?>getActivityById(String activityId) throws JSONException {
        neo4jProvenanceService.getActivityById(activityId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
