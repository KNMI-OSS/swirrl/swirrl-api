#!/opt/conda/bin/python

# -*- coding: utf-8 -*-

import io
from contextlib import redirect_stderr
import sys

# From /opt/conda/bin/conda:
# Before any more imports, leave cwd out of sys.path for internal 'conda shell.*' commands.
# see https://github.com/conda/conda/issues/6549
if len(sys.argv) > 1 and sys.argv[1].startswith('shell.') and sys.path and sys.path[0] == '':
    # The standard first entry in sys.path is an empty string,
    # and os.path.abspath('') expands to os.getcwd().
    del sys.path[0]
from conda.cli import main

sys.path.append('/opt/conda/lib/swirrl')
# TODO: explicit reference to wrapper_functions with each use?
from wrapper_common import *

with open('/data/env_vars/sessionId', 'r') as file:
    session_id  = file.read().replace('\n', '')

with open('/data/env_vars/poolId', 'r') as file:
    pool_id  = file.read().replace('\n', '')

with open('/data/env_vars/serviceId', 'r') as file:
    service_id  = file.read().replace('\n', '')

commands_to_trace = ('install', 'update', 'upgrade', 'remove', 'uninstall')

# For testing uncomment next 3 lines
# def main():
#     print("MAIN")
#     return 0

def version():
    return subprocess.check_output(["/opt/conda/bin/conda", "--version"]).decode(sys.stdout.encoding).split()[1]

if __name__ == '__main__':

    prov_binding["var"]["updateLibrariesStartTime"].append(get_current_iso_time_str())

    current_id = unique_id()
    previous_id = current_id

    ## If you update this env variable name, also do it in pip-wrapper.py and notebook/configmap.yaml
    is_create_session = "SPROV_CREATE_SESSION" in os.environ and os.environ["SPROV_CREATE_SESSION"] == "true"

    do_trace_command = len(sys.argv) > 1 and sys.argv[1] in commands_to_trace

    ## If this is a createSession invocation, the provenance template
    ## is expanded by the API.
    libs = []
    if do_trace_command and not is_create_session:
        prov_binding["var"]["updateAgent"].append({"@id": "uuid:conda-%s" % version().replace('.', '_')})
        prov_binding["var"]["Jupyter"].append({"@id": "uuid:%s" % current_id})
        previous_id = update_service_id(current_id)

        prov_binding["var"]["sessionId"].append("%s" % session_id)
        prov_binding["var"]["poolId"].append("%s" % pool_id)
        prov_binding["var"]["notebookId"].append("%s" % service_id)

        argidx = 0
        for argument in sys.argv:
            if re.match("--file$", argument):
                req_file_name = sys.argv[argidx + 1]
                libs.extend(extract_libs(req_file_name))
            argidx += 1

    ## Redirect stderr to a buffer
    with io.StringIO() as err_buf, redirect_stderr(err_buf):

        ## Execute what pip would normally do
        sys.argv[0] = re.sub(r'(-script\.pyw?|\.exe)?$', '', sys.argv[0])
        rc = main()
        if do_trace_command:
            print("Storing environment yaml.")
            store_environment_yaml()

        if do_trace_command and not is_create_session:
            print("Tracing installed libraries.")
            trace_installed_libraries()
            prov_binding["var"]["updateLibrariesEndTime"].append(get_current_iso_time_str())
            prov_binding["var"]["liblistvalue"] = ['conda ' + ' '.join(sys.argv[1:])]
            if libs or libs == []: ## It's possible requirementsfile is empty. This is not an error
                trace_requirements_txt(libs)
            
            make_prov_binding(err_buf.getvalue(), "Exitcode: %s" % rc)
            if err_buf.getvalue():
                print(err_buf.getvalue(), file=sys.stdout)

    ## Exit with exit code from pip's main routine
    sys.exit(rc)

