#!/opt/conda/bin/python

# -*- coding: utf-8 -*-
import re, subprocess, sys, os

import requests, json, datetime, uuid

with open('/data/env_vars/templateExpansionUrl', 'r') as file:
    prov_expansion_svc_host = file.read().strip()

with open('/data/env_vars/notebookUrlOrigin', 'r') as file:
    notebook_origin_url = file.read().strip()

with open('/data/env_vars/swirrlInternalOrigin', 'r') as file:
    swirrl_origin_url = file.read().strip()

with open('/data/env_vars/sessionId', 'r') as file:
    session_id  = file.read().strip()

with open('/data/env_vars/poolId', 'r') as file:
    pool_id  = file.read().strip()

with open('/data/env_vars/serviceId', 'r') as file:
    service_id  = file.read().strip()

with open('/data/env_vars/provenanceUser', 'r') as file:
    provenance_user = file.read().strip()

with open('/data/env_vars/userName', 'r') as file:
    user_name = file.read().strip()

with open('/data/env_vars/authMode', 'r') as file:
    auth_mode = file.read().strip()

with open('/data/env_vars/group', 'r') as file:
    group = file.read().strip()


jupyterUpdate_id_file = "/home/jovyan/work/.jupyter-id-"+service_id
condaexe = "/opt/conda/bin/conda"

prov_binding = {
    "var": {
        "generatedAt": [],
        "libs": [],
        "Jupyter": [],
        "sessionId": [],
        "poolId": [],
        "notebookId": [],
        "accessurl": [],
        "group": [group],
        "updateLibraries": [],
        "updateLibrariesEndTime": [],
        "updateLibrariesStartTime": [],
        "name_api": [],
        "liblistvalue": [],
        "JupyterPrev": [],
        "libname": [],
        "authmode": [auth_mode],
        "libversion": [],
        "libList": [],
        "updateAgent": [],
        "user": [':'.join(("uuid", user_name))],
        "name": [provenance_user],
        ## To be filled in from command line arguments supplied by user or API.
        "lib": [],
        "installationMode": [],
        "message": []
    },
    "vargen": {},
    "context": {
        "swirrl": "http://swirrl.knmi.nl/ns#",
        "uuid": "urn:uuid:"
    }
}


def unique_id():
    return uuid.uuid4()


def make_prov_binding(error_output, output):
    prov_binding["var"]["generatedAt"].append(get_current_iso_time_str())
    prov_binding["var"]["updateLibraries"].append({"@id": "uuid:%s" % unique_id()})
    prov_binding["var"]["libs"].append({"@id": "uuid:%s" % unique_id()})
    prov_binding["var"]["libList"].append({"@id": "uuid:%s" % unique_id()})
    prov_binding["var"]["accessurl"].append("{}/swirrl/jupyter/{}".format(notebook_origin_url, service_id))
    if error_output:
        prov_binding["var"]["message"].append(error_output)
    else:
        prov_binding["var"]["message"].append(output)

    if not os.isatty(sys.stdout.fileno()):
        prov_binding["var"]["method_path"] = ["PUT /notebook/%s" % service_id]
        prov_binding["var"]["name_api"] = ["SWIRRL-API"]
    else:
        prov_binding["var"]["method_path"] = ["Terminal"]
        prov_binding["var"]["name_api"] = [sys.argv[0]]

    data = json.dumps(prov_binding)

    expansion_response = requests.post('/'.join([prov_expansion_svc_host, "templates",
                                                 get_update_template_id(),
                                                 "expand?fmt=provn&writeprov=false&bindver=v3"]),
                                       data=data, verify=False)
    store_prov_response = requests.post(swirrl_origin_url+'/swirrl-api/v1.0/provenance',
                                        data=expansion_response.text.encode(), verify=False)
    provfile = open("/home/jovyan/provdoc.provn", "w")
    provfile.write(expansion_response.text)
    responsefile = open("/home/jovyan/provdoc-response.provn", "w")
    responsefile.write(store_prov_response.text)
    bindingfile = open("/home/jovyan/bindings.json", "w")
    bindingfile.write(data)


def extract_libs(req_file_name):
    req_file = open(req_file_name, "r")
    libs = req_file.readlines()
    return libs


def trace_requirements_txt(req_libs):
    new_requirements_txt = []
    required_libs_dict = {}  ##  could be a set too, see comment below.

    for req_lib in req_libs:
        req_hasoperator = re.match(".+(==|<=|>=|<|>).+", req_lib)
        req_libname = req_lib.strip()
        if not req_libname: continue  ## empty line.
        req_operator = None
        req_libversion = "1"
        if (req_hasoperator):
            req_operator = req_hasoperator.group(1)
            req_libname = req_lib.split(req_operator)[0]
            req_libversion = req_lib.split(req_operator)[1]
        ## operator and version not needed, could be a set with just the name,
        ## but maybe there's a future use for it.
        required_libs_dict[req_libname] = {"operator": req_operator, "version": req_libversion}

        new_requirements_txt.append(req_lib.strip())

    prov_binding["var"]["liblistvalue"][0] += "\n"
    prov_binding["var"]["liblistvalue"][0] += "\n".join(new_requirements_txt)


def update_service_id(current_id):
    try:
        cur_req_file = open(jupyterUpdate_id_file, "r")
        cur_libs = cur_req_file.readlines()
        cur_req_file.close()
    except:
        cur_libs = []

    if len(cur_libs) and "## CurrentJupyterUpdateID:" in cur_libs[0]:
        previous_id = cur_libs[0].strip().split()[2]
        ## Overwrite the current ID place holder
        prov_binding["var"]["JupyterPrev"].append({"@id": "uuid:%s" % previous_id})
        ## TODO: We should strip this line when we add the contents to the provenance 'liblistvalue' below.
        cur_libs[0] = ("## CurrentJupyterUpdateID: %s\n" % current_id)
    elif len(cur_libs) == 0:
        cur_libs.append("## CurrentJupyterUpdateID: %s\n" % current_id)
        previous_id = current_id

    try:
        new_req_file = open(jupyterUpdate_id_file, "w")
        new_req_file.writelines(cur_libs)
        new_req_file.close()
        return previous_id
    except:
        return None


def trace_installed_libraries():
    try:
        syslibnames = set()
        python_version=float("%s.%s" % (sys.version_info.major, sys.version_info.minor))
        if python_version > 3.6:
            condaproc = subprocess.run([condaexe,"list"], capture_output=True, text=True)
        else:
            condaproc = subprocess.run([condaexe,"list"], stdout=subprocess.PIPE, stderr=subprocess.PIPE, encoding="ASCII")
        if condaproc.stderr:
            return None
        for libline in condaproc.stdout.split('\n'):
            lib = libline.split()
            if not lib or lib[0][0] == '#': continue
            if len(lib) < 4:
                installmode = "unknown"
            else: installmode = lib[3]
            prov_binding["var"]["installationMode"].append(installmode)
            libname = lib[0]
            libversion = lib[1]
            libbuildnr = lib[2]
            prov_binding["var"]["lib"].append({"@id" : "uuid:"+'-'.join((libname.replace('.', '_'),
                                                                         libversion.replace('.', '_'),
                                                                         libbuildnr.replace('.', '_')))})
            prov_binding["var"]["libname"].append(libname)
            prov_binding["var"]["libversion"].append('-'.join((libversion, libbuildnr)))

    except:
        raise


def get_current_iso_time_str():
    return datetime.datetime.utcnow().isoformat()[:-3]+'Z'

def get_update_template_id():
    prov_template_list_response = requests.post('/'.join([prov_expansion_svc_host, "getTemplateList"]))
    template_list = json.loads(prov_template_list_response.text)
    for template in template_list:
        if template["title"] == "update_libraries":
            return template["id"]
    return None

def store_environment_yaml():
    result = subprocess.run(['conda', 'env', 'export', '-n', 'base'], stdout=subprocess.PIPE)
    with open("/home/jovyan/work/.environment-"+service_id+".yml", 'wb') as file:
        file.write(result.stdout)
