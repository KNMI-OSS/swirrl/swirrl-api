package org.openapitools.v1_0.model.kubernetes;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Service("serviceDeploymentFactoryImpl")
@Profile({"test"})
public class ServiceDeploymentFactoryImpl extends ServiceDeploymentFactory {

    @Override
    public String getServiceType() {
        return "test-service-type";
    }
}
