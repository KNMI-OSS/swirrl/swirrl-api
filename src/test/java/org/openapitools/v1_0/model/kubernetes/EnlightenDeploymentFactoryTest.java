package org.openapitools.v1_0.model.kubernetes;

import io.kubernetes.client.openapi.models.V1Pod;
import io.kubernetes.client.openapi.models.V1Service;
import io.kubernetes.client.util.Yaml;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.fail;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.openapitools.v1_0.api.KubernetesService;
import org.openapitools.v1_0.model.Viewer;
import org.openapitools.v1_0.provenance.Neo4jProvenanceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;
import java.io.InputStreamReader;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@SpringBootTest
@TestPropertySource(properties = {
        "enlighten.viewer.docker.image.tag=testtagname",
        "service.docker.image.pull.secret=testsecretname"
})

public class EnlightenDeploymentFactoryTest {

    @Mock
    ObjectFactory<KubernetesService> kubernetesServiceObjectFactoryMock;

    @Mock
    private KubernetesService kubernetesServiceMock;

    @MockBean // Prevent connection attempt to the database
    Neo4jProvenanceService neo4jProvenanceServiceMock;

    final private String TEST_SERVICE_ID = "test-service-id";
    final private String TEST_POOL_ID = "test-pool-id";
    final private String TEST_SESSION_ID = "test-session-id";
    private static Logger logger = LoggerFactory.getLogger(EnlightenDeploymentFactoryTest.class);
    final private ResponseEntity notFoundResponse = new ResponseEntity<>(HttpStatus.NOT_FOUND);

//    @InjectMocks
//    @Autowired
//    private EnlightenDeploymentFactory enlightenDeploymentFactory;

    @InjectMocks
    @Autowired // necessary to initialize it's @Value annotated members.
    ViewerDeploymentFactoryProvider factoryProvider;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        when(kubernetesServiceObjectFactoryMock.getObject()).thenReturn(kubernetesServiceMock);
    }

    @Test
    public void testDeploymentYaml() {
        V1Pod expectedDeployment = null;
        try {
            expectedDeployment = Yaml.loadAs(
                    new InputStreamReader(
                            new ClassPathResource("expected-deployment-enlighten.yaml").getInputStream()),
                    V1Pod.class);
        } catch (IOException e) {
            logger.error("Exception occured whilst reading yaml file.",e);
            fail("Not possible to open expected deployment yaml file.");
        }

        when(kubernetesServiceMock.retrievePVCsByLabel(ServiceDeploymentFactory.SESSION_ID_LABEL + "=" + TEST_SESSION_ID))
                .thenReturn(notFoundResponse);
        when(kubernetesServiceMock.retrievePodByLabel(ServiceDeploymentFactory.SERVICE_ID_LABEL + "=" + TEST_SERVICE_ID))
                .thenReturn(notFoundResponse);

        Viewer enlighten = new Viewer();
        enlighten.setId(TEST_SERVICE_ID);
        enlighten.setPoolId(TEST_POOL_ID);
        enlighten.setSessionId(TEST_SESSION_ID);
        enlighten.setServiceType("enlighten");

        ViewerDeploymentFactory factory = factoryProvider.getFactory(enlighten);
        ServiceDeployment enlightenDeployment = factory.makeViewerDeployment(enlighten);
        V1Pod currentDeployment = enlightenDeployment.getDeployment();

        assertThat(currentDeployment).isEqualTo(expectedDeployment);
    }

    @Test
    public void testServiceYaml() {
        V1Service expectedService = null;
        try {
            expectedService = Yaml.loadAs(
                    new InputStreamReader(
                            new ClassPathResource("expected-service-enlighten.yaml").getInputStream()),
                    V1Service.class);
        } catch (IOException e) {
            logger.error("Exception occured whilst reading yaml file.",e);
            fail("Not possible to open expected service yaml file.");
        }

        when(kubernetesServiceMock.retrievePVCsByLabel(ServiceDeploymentFactory.SESSION_ID_LABEL + "=" + TEST_SESSION_ID))
                .thenReturn(notFoundResponse);
        when(kubernetesServiceMock.retrievePodByLabel(ServiceDeploymentFactory.SERVICE_ID_LABEL + "=" + TEST_SERVICE_ID))
                .thenReturn(notFoundResponse);

        Viewer enlighten = new Viewer();
        enlighten.setId(TEST_SERVICE_ID);
        enlighten.setPoolId(TEST_POOL_ID);
        enlighten.setSessionId(TEST_SESSION_ID);
        enlighten.setServiceType("enlighten");

        ViewerDeploymentFactory factory = factoryProvider.getFactory(enlighten);
        ServiceDeployment enlightenDeployment = factory.makeViewerDeployment(enlighten);
        V1Service currentService = enlightenDeployment.getService();

        assertThat(currentService).isEqualTo(expectedService);
    }
}
