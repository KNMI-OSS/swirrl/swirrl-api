package org.openapitools.v1_0.model.kubernetes;

import io.kubernetes.client.openapi.models.*;
import io.kubernetes.client.util.Yaml;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.fail;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.openapitools.v1_0.api.KubernetesService;
import org.openapitools.v1_0.model.Service;
import org.openapitools.v1_0.provenance.Neo4jProvenanceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;
import java.io.InputStreamReader;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@SpringBootTest
@TestPropertySource(properties = {
        "service.docker.image.pull.secret=testsecretname",
        "spring.profiles.active=test,default"
})

public class ServiceDeploymentFactoryTest {

    @Mock
    ObjectFactory<KubernetesService> kubernetesServiceObjectFactoryMock;

    @Mock
    private KubernetesService kubernetesServiceMock;

    @MockBean // Prevent connection attempt to the database
    Neo4jProvenanceService neo4jProvenanceServiceMock;

    private static Service service;

    @InjectMocks
    @Autowired
    private ServiceDeploymentFactoryImpl serviceDeploymentFactory;

    final private static String TEST_SERVICE_ID = "test-service-id";
    final private static String TEST_POOL_ID = "test-pool-id";
    final private static String TEST_SESSION_ID = "test-session-id";
    final private static String TEST_NODE_NAME = "test-node-name";
    final private String TEST_SERVICE_TYPE = "test-service-type";
    private static Logger logger = LoggerFactory.getLogger(ServiceDeploymentFactoryTest.class);
    final private ResponseEntity successResponse = new ResponseEntity<>(HttpStatus.OK);
    final private ResponseEntity notFoundResponse = new ResponseEntity<>(HttpStatus.NOT_FOUND);

    @BeforeAll
    public static void intializeExpectedDeployment() {
        service = new Service();
        service.setId(TEST_SERVICE_ID);
        service.setPoolId(TEST_POOL_ID);
        service.setSessionId(TEST_SESSION_ID);
    }

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        when(kubernetesServiceObjectFactoryMock.getObject()).thenReturn(kubernetesServiceMock);
    }

    @Test
    public void testDeploymentYaml() {
        V1Pod expectedDeployment = null;
        try {
            expectedDeployment = Yaml.loadAs(
                    new InputStreamReader(
                            new ClassPathResource("expected-deployment.yaml").getInputStream()),
                    V1Pod.class);
        } catch (IOException e) {
            logger.error("Exception occured whilst reading yaml file.",e);
            fail("Not possible to open expected deployment yaml file.");
        }

        when(kubernetesServiceMock.retrievePVCsByLabel(ServiceDeploymentFactory.SESSION_ID_LABEL + "=" + TEST_SESSION_ID))
                .thenReturn(notFoundResponse);

        ServiceDeployment serviceDeployment = serviceDeploymentFactory.makeServiceDeployment(service);
        V1Pod deployment = serviceDeployment.getDeployment();

        assertThat(deployment).isEqualTo(expectedDeployment);
    }

    @Test
    public void testDeploymentExistingSessionYaml() {
        V1Pod expectedDeployment = null;
        try {
            expectedDeployment = Yaml.loadAs(
                    new InputStreamReader(
                            new ClassPathResource("expected-deployment-existing-session.yaml").getInputStream()),
                    V1Pod.class);
        } catch (IOException e) {
            logger.error("Exception occured whilst reading yaml file.",e);
            fail("Not possible to open expected deployment yaml file.");
        }

        when(kubernetesServiceMock.retrievePVCsByLabel(ServiceDeploymentFactory.SESSION_ID_LABEL + "=" + TEST_SESSION_ID))
                .thenReturn(successResponse);

        ServiceDeployment serviceDeployment = serviceDeploymentFactory.makeServiceDeployment(service);
        V1Pod deployment = serviceDeployment.getDeployment();

        assertThat(deployment).isEqualTo(expectedDeployment);
    }

    @Test
    public void testServiceYaml() {
        V1Service expectedService = null;
        try {
            expectedService = Yaml.loadAs(
                    new InputStreamReader(
                            new ClassPathResource("expected-service.yaml").getInputStream()),
                    V1Service.class);
        } catch (IOException e) {
            logger.error("Exception occured whilst reading yaml file.",e);
            fail("Not possible to open expected deployment yaml file.");
        }

        when(kubernetesServiceMock.retrievePVCsByLabel(ServiceDeploymentFactory.SESSION_ID_LABEL + "=" + TEST_SESSION_ID))
                .thenReturn(notFoundResponse);

        ServiceDeployment serviceDeployment = serviceDeploymentFactory.makeServiceDeployment(service);
        V1Service service = serviceDeployment.getService();

        assertThat(service).isEqualTo(expectedService);
    }

    @Test
    public void testIngressYaml() {
        V1HTTPIngressPath expectedIngress = null;
        try {
            expectedIngress = Yaml.loadAs(
                    new InputStreamReader(
                            new ClassPathResource("expected-ingress-patch.yaml").getInputStream()),
                            V1HTTPIngressPath.class);
        } catch (IOException e) {
            logger.error("Exception occured whilst reading yaml file.",e);
            fail("Not possible to open expected deployment yaml file.");
        }

        when(kubernetesServiceMock.retrievePVCsByLabel(ServiceDeploymentFactory.SESSION_ID_LABEL + "=" + TEST_SESSION_ID))
                .thenReturn(notFoundResponse);

        ServiceDeployment serviceDeployment = serviceDeploymentFactory.makeServiceDeployment(service);
        V1HTTPIngressPath ingress = serviceDeployment.getIngress().get("default").get(0);


        assertThat(ingress).isEqualTo(expectedIngress);
    }
}
