package org.openapitools.v1_0.model.kubernetes;

import io.kubernetes.client.openapi.models.V1Job;
import io.kubernetes.client.util.Yaml;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.fail;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;
import java.io.InputStreamReader;

import static net.javacrumbs.jsonunit.assertj.JsonAssertions.assertThatJson;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class CreateSnapshotJobFactoryTest {
    @Autowired
    private CreateSnapshotJobFactory factory;

    private static Logger logger = LoggerFactory.getLogger(CreateSnapshotJobFactoryTest.class);

    final private String TEST_SERVICE_ID = "test-service-id";
    final private String TEST_POOL_ID = "test-pool-id";
    final private String TEST_SESSION_ID = "test-session-id";
    final private String TEST_CREATE_SNAPSHOT_JOB_ID = "test-create-snapshot-job-id";

    @Test
    public void testMakeCreateSnapShotJob() {
        V1Job k8sJobExpect = null;
        try {
            k8sJobExpect = Yaml.loadAs(
                    new InputStreamReader(new ClassPathResource("expected-create-snapshot-job.yaml").getInputStream()), V1Job.class);
        } catch (IOException e) {
            logger.error("Exception occured whilst reading yaml file.", e);
            fail("Not possible to open expected pod yaml file.");
        }
        String snapshotMessage = "Create test snapshot";
        CreateSnapshotJob cssJob = factory.makeCreateSnapshotJob(TEST_SESSION_ID, TEST_POOL_ID, TEST_SERVICE_ID, snapshotMessage, "jupyter/minimal-notebook", null);
        assertThat(cssJob.getServiceId().equals(TEST_SERVICE_ID));
        assertThat(cssJob.getSessionId().equals(TEST_SESSION_ID));
        assertThatJson(cssJob.getJob()).isEqualTo(k8sJobExpect);
    }
}
