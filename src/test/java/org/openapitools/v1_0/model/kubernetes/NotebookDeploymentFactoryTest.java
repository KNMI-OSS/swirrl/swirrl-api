package org.openapitools.v1_0.model.kubernetes;

import io.kubernetes.client.openapi.models.V1ConfigMapVolumeSource;
import io.kubernetes.client.openapi.models.V1Pod;
import io.kubernetes.client.util.Yaml;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.fail;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.openapitools.v1_0.api.EposUtils;
import org.openapitools.v1_0.api.KubernetesService;
import org.openapitools.v1_0.model.Notebook;
import org.openapitools.v1_0.model.UserInfo;
import org.openapitools.v1_0.provenance.Neo4jProvenanceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;
import java.io.InputStreamReader;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@SpringBootTest
@TestPropertySource(properties = {
        "notebook.docker.image.tag=testtagname",
        "service.docker.image.pull.secret=testsecretname"
})

public class NotebookDeploymentFactoryTest {

    @Mock
    ObjectFactory<KubernetesService> kubernetesServiceObjectFactoryMock;

    @Mock
    private KubernetesService kubernetesServiceMock;

    @MockBean // Prevent connection attempt to the database
    Neo4jProvenanceService neo4jProvenanceServiceMock;

    final private String TEST_SERVICE_ID = "test-service-id";
    final private String TEST_POOL_ID = "test-pool-id";
    final private String TEST_SESSION_ID = "test-session-id";
    private static Logger logger = LoggerFactory.getLogger(NotebookDeploymentFactoryTest.class);
    final private ResponseEntity notFoundResponse = new ResponseEntity<>(HttpStatus.NOT_FOUND);

    @InjectMocks
    @Autowired
    private NotebookDeploymentFactory notebookDeploymentFactory;

    @Value("${notebook.docker.image.tag}")
    private String dockerImageTag;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        when(kubernetesServiceObjectFactoryMock.getObject()).thenReturn(kubernetesServiceMock);
    }

    @Test
    public void testDeploymentYaml() {
        V1Pod expectedDeployment = null;
        try {
            expectedDeployment = Yaml.loadAs(
                    new InputStreamReader(
                            new ClassPathResource("expected-deployment-notebook.yaml").getInputStream()),
                    V1Pod.class);
        } catch (IOException e) {
            logger.error("Exception occured whilst reading yaml file.",e);
            fail("Not possible to open expected deployment yaml file.");
        }

        when(kubernetesServiceMock.retrievePVCsByLabel(ServiceDeploymentFactory.SESSION_ID_LABEL + "=" + TEST_SESSION_ID))
                .thenReturn(notFoundResponse);

        V1Pod currentDeployment;Notebook notebook = new Notebook();
        notebook.setId(TEST_SERVICE_ID);
        notebook.setPoolId(TEST_POOL_ID);
        notebook.setSessionId(TEST_SESSION_ID);

        UserInfo userInfo = new UserInfo("mock_user_id", "swirrl-api", "mock_authmode_id", "mock_group_id");
        NotebookDeployment notebookDeployment = notebookDeploymentFactory.makeNotebookDeployment(notebook, dockerImageTag, userInfo, "1234abcd");
        currentDeployment = notebookDeployment.getDeployment();

        // At every deployment a new configmapname is generated, therefore comparison to a expected-template will fail.
        // Equalise Configmap-names between Expected and Currently active notebook to make comparison possible.
        V1ConfigMapVolumeSource currentConfigmap = EposUtils.findConfigmapNameForVolume(currentDeployment, NotebookDeploymentFactory.STARTUP_DIRECTORY);
        V1ConfigMapVolumeSource expectedConfigmap = EposUtils.findConfigmapNameForVolume(expectedDeployment, NotebookDeploymentFactory.STARTUP_DIRECTORY);
        expectedConfigmap.setName(currentConfigmap.getName());

        assertThat(currentDeployment).isEqualTo(expectedDeployment);
    }


    @Test
    public void testDeploymentRestoreYaml() {
        V1Pod expectedDeployment = null;
        try {
            expectedDeployment = Yaml.loadAs(
                    new InputStreamReader(
                            new ClassPathResource("expected-deployment-notebook-restore.yaml").getInputStream()),
                    V1Pod.class);
        } catch (IOException e) {
            logger.error("Exception occured whilst reading yaml file.",e);
            fail("Not possible to open expected deployment yaml file.");
        }

        when(kubernetesServiceMock.retrievePVCsByLabel(ServiceDeploymentFactory.SESSION_ID_LABEL + "=" + TEST_SESSION_ID))
                .thenReturn(notFoundResponse);

        V1Pod currentDeployment;Notebook notebook = new Notebook();
        notebook.setId(TEST_SERVICE_ID);
        notebook.setPoolId(TEST_POOL_ID);
        notebook.setSessionId(TEST_SESSION_ID);

        UserInfo userInfo = new UserInfo("mock_user_id", "swirrl-api", "mock_authmode_id", "mock_group_id");
        NotebookDeployment notebookDeployment = notebookDeploymentFactory.makeNotebookDeployment(notebook, dockerImageTag, userInfo, "1234abcd");
        currentDeployment = notebookDeployment.getDeployment();

        // At every deployment a new configmapname is generated, therefore comparison to a expected-template will fail.
        // Equalise Configmap-names between Expected and Currently active notebook to make comparison possible.
        V1ConfigMapVolumeSource currentConfigmap = EposUtils.findConfigmapNameForVolume(currentDeployment, NotebookDeploymentFactory.STARTUP_DIRECTORY);
        V1ConfigMapVolumeSource expectedConfigmap = EposUtils.findConfigmapNameForVolume(expectedDeployment, NotebookDeploymentFactory.STARTUP_DIRECTORY);
        expectedConfigmap.setName(currentConfigmap.getName());

        assertThat(currentDeployment).isEqualTo(expectedDeployment);
    }
}
