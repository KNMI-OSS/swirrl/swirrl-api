package org.openapitools.v1_0.provenance;

import io.kubernetes.client.openapi.models.V1Pod;
import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.fail;
import org.mockito.*;
import org.openapitools.v1_0.api.KubernetesService;
import org.openapitools.v1_0.api.NotebookService;
import org.openapitools.v1_0.model.ExtendedNotebook;
import org.openapitools.v1_0.model.NotebookLibrary;
import org.openapitools.v1_0.model.UserInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;
import java.io.InputStreamReader;
import java.time.OffsetDateTime;
import java.util.Arrays;
import java.util.List;

import static net.javacrumbs.jsonunit.assertj.JsonAssertions.assertThatJson;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import org.springframework.test.util.ReflectionTestUtils;

@SpringBootTest
@TestPropertySource(properties = {
        "notebook.url.origin=" + ProvenanceServiceTest.TEST_NOTEBOOK_URL_ORIGIN,
        "provenance.template.createsession-id=" + ProvenanceServiceTest.TEST_TEMPLATE_ID
})
public class ProvenanceServiceTest {

    @SpyBean
    private ProvenanceService provenanceService;

    @Mock
    ObjectFactory<KubernetesService> kubernetesServiceObjectFactoryMock;

    @Mock
    private KubernetesService kubernetesServiceMock;

    @MockBean // Prevent connection attempt to the database
    Neo4jProvenanceService neo4jProvenanceServiceMock;

    @MockBean
    TemplateCatalog templateCatalog;

    @Mock
    private NotebookService notebookServiceMock;

    @Mock (answer = Answers.RETURNS_DEEP_STUBS)
    private V1Pod successPodMock;

    @Captor
    private ArgumentCaptor<String> bindingsCaptor;

    @Captor
    private ArgumentCaptor<String> templateIdCaptor;

    private static Logger logger = LoggerFactory.getLogger(ProvenanceServiceTest.class);

    final private String TEST_NOTEBOOK_ID = "test-id";
    static final protected String TEST_NOTEBOOK_URL_ORIGIN = "http://test-notebook-host";
    static final protected String TEST_TEMPLATE_ID = "12345";
    static final private String TEST_POOL_ID = "test-pool-id";
    static final private String TEST_SESSION_ID = "test-session-id";


    final private ResponseEntity successResponse = new ResponseEntity<>("Dummy expanded template string", HttpStatus.OK);

    final private List<NotebookLibrary> EXPECTED_LIB_LIST = Arrays.asList(
            new NotebookLibrary().libname("lib1").libversion("libversion1"),
            new NotebookLibrary().libname("lib2").libversion("libversion2"),
            new NotebookLibrary().libname("lib3"),
            new NotebookLibrary().libname("lib4").libversion("libversion4")
    );

    final private List<NotebookLibrary> EXPECTED_INSTALLED_LIB_LIST = Arrays.asList(
            new NotebookLibrary().libname("lib1").libversion("libversion1-build_1").source("unit-test"),
            new NotebookLibrary().libname("lib2").libversion("libversion2-build_2").source("pypi"),
            new NotebookLibrary().libname("lib3").libversion("libversion3-unknown").source("unknown"),
            new NotebookLibrary().libname("lib4").libversion("libversion4-build_4").source("unknown"),
            new NotebookLibrary().libname("lib_a").libversion("libversion_a-build_a").source("source_a"),
            new NotebookLibrary().libname("lib_b").libversion("libversion_b-build_b").source("source_a")
    );

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        when(kubernetesServiceObjectFactoryMock.getObject()).thenReturn(kubernetesServiceMock);
    }

    @Test
    public void traceProvenanceCreateSession() throws IOException {
        // -------- Arrange --------
        ExtendedNotebook notebook = new ExtendedNotebook();
        notebook.setId(TEST_NOTEBOOK_ID);
        notebook.setPoolId(TEST_POOL_ID);
        notebook.setSessionId(TEST_SESSION_ID);
        notebook.setUserRequestedLibraries(EXPECTED_LIB_LIST);
        notebook.setServiceURL(TEST_NOTEBOOK_URL_ORIGIN);
        notebook.setInstalledLibraries(EXPECTED_INSTALLED_LIB_LIST);

        ResponseEntity successResponseNotebook = new ResponseEntity<>(notebook, HttpStatus.OK);

        // Mock away the calls to K8s and templateExpansionService
        mockTemplateExpansion();
        when(templateCatalog.getIdByTitle(any(String.class))).thenReturn(TEST_TEMPLATE_ID);
        mockKubernetes();
        when(notebookServiceMock.getNotebookById(any(String.class)))
                .thenReturn(successResponseNotebook);

        // -------- Act --------
        UserInfo userInfo = new UserInfo("mock_user_id", "swirrl-api", "mock_authmode_id", "mock_group_id");
        ResponseEntity<?> response = provenanceService.traceProvenanceCreateSessionNonAsync(
                notebook,"2019-03-19T14:34:40+01:00", notebookServiceMock, userInfo);

        // -------- Assert --------

        // Capture the bindings and the template id passed to the provenance service mock and check if they are correct.
        verify(provenanceService, times(1)).expandProvTemplate(
                bindingsCaptor.capture(),
                templateIdCaptor.capture());

        String expectedBindings = "";
        try {
            expectedBindings = IOUtils.toString(new InputStreamReader(
                    new ClassPathResource("expected-bindings.json").getInputStream()));
        } catch (IOException e) {
            logger.error("Exception occurred whilst reading expected bindings json file.",e);
            fail("Not possible to open expected bindings json file.");
        }

        assertThatJson(bindingsCaptor.getValue()).isEqualTo(expectedBindings);
        assertThat(templateIdCaptor.getValue()).isEqualTo(TEST_TEMPLATE_ID);

        // Check if the response of the provenance service is passed along correctly.
        assertThat(response.getStatusCode()).isEqualTo(successResponse.getStatusCode());
        assertThat(response.getBody()).isEqualTo(successResponse.getBody());
    }

    private void mockTemplateExpansion() {
        doReturn(successResponse).when(provenanceService).expandProvTemplate(any(String.class), any(String.class));
    }

    private void mockKubernetes() throws IOException {
        // Note that normally we can use @InjectMocks to inject a mock into a bean, but this does not work together with @SpyBean.
        // Therefore we set the dependency here explicitly by ourselves.
        ReflectionTestUtils.setField(provenanceService, "kubernetesServiceFactory", kubernetesServiceObjectFactoryMock);

        OffsetDateTime date = OffsetDateTime.parse("2019-03-19T14:34:11.000Z");
        when(successPodMock.getStatus().getContainerStatuses().get(0).getState().getRunning().getStartedAt())
                .thenReturn(date);
        when(successPodMock.getStatus().getContainerStatuses().get(0).getImageID())
                .thenReturn(
                        "docker-pullable://jupyter/minimal-notebook@sha256:aaece29f14a2c23db6b0cd2c00fc2642ebd15a925c592aa76467d0b041f87053");
        when(successPodMock.getMetadata().getName())
                .thenReturn("jupyter-ce13815c-01c6-4a30-80e4-2ad145785922-5cfb6765df-rtt6w");

        when(kubernetesServiceMock.waitOnContainerReady(any(String.class), any(Integer.class)))
                .thenReturn(successPodMock);
    }
}
