package org.openapitools.v1_0.api;

import io.kubernetes.client.openapi.models.*;
import io.kubernetes.client.util.Yaml;

import org.json.JSONException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.fail;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import org.openapitools.v1_0.model.*;
import org.openapitools.v1_0.model.Error;
import org.openapitools.v1_0.model.kubernetes.ServiceDeploymentFactory;
import org.openapitools.v1_0.provenance.Neo4jProvenanceService;
import org.openapitools.v1_0.provenance.ProvenanceApi;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;

import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest
@TestPropertySource(properties = {
        "notebook.url.origin=" + NotebookServiceTest.TEST_NOTEBOOK_URL_ORIGIN,
})
public class NotebookServiceTest {

    @InjectMocks
    @Autowired
    private NotebookService notebookService;

    private static final String SERVICE_TYPE = "jupyter";

    @Mock
    ObjectFactory<KubernetesService> kubernetesServiceObjectFactoryMock;

    @MockBean // Prevent connection attempt to the database
    Neo4jProvenanceService neo4jProvenanceServiceMock;

    @Mock
    private KubernetesService kubernetesServiceMock;

    @Mock
    ProvenanceApi provenanceApi;

    private static Logger logger = LoggerFactory.getLogger(NotebookServiceTest.class);

    final private String TEST_SERVICE_ID = "test-service-id";
    final private String TEST_POOL_ID = "test-pool-id";
    final private String TEST_SESSION_ID = "test-session-id";
    final private String TEST_CREATE_SESSION_ACTIVITY_ID = "urn:uuid:test-create-session-activity-id";
    final private String TEST_CREATE_SNAPSHOT_JOB_ID = "test-create-snapshot-job-id";
    final private String TEST_DATA_DIRECTORY_PVC_NAME = "data-directory-test-session-id";
    final private String TEST_WORKING_DIRECTORY_PVC_NAME = "working-directory-test-pool-id";
    final private BigDecimal TEST_DATA_DIRECTORY_PVC_SIZE = BigDecimal.valueOf(1024 * 1024 * 1024);
    final private BigDecimal TEST_WORKING_DIRECTORY_PVC_SIZE = BigDecimal.valueOf(1024 * 1024 * 1024);
    static final protected String TEST_NOTEBOOK_URL_ORIGIN = "http://test-notebook-host";
    final private ResponseEntity successResponse = new ResponseEntity<>(HttpStatus.OK);
    final private ResponseEntity notFoundResponse = new ResponseEntity<>(HttpStatus.NOT_FOUND);
    final private List<NotebookLibrary> EXPECTED_LIB_LIST = Arrays.asList(
            new NotebookLibrary().libname("lib1").libversion("libversion1-build_1").source("source_1"),
            new NotebookLibrary().libname("lib2").libversion("libversion2-build_2").source("source_1"),
            new NotebookLibrary().libname("lib3").libversion("unknown").source("unknown"),
            new NotebookLibrary().libname("lib4").libversion("libversion4-build_4").source("unknown")
    );
    final private List<NotebookLibrary> USER_REQUESTED_LIB_LIST = Arrays.asList(
            new NotebookLibrary().libname("lib1").libversion("libversion1"),
            new NotebookLibrary().libname("lib2").libversion("libversion2"),
            new NotebookLibrary().libname("lib3").libversion("unknown"),
            new NotebookLibrary().libname("lib4").libversion("libversion4")
    );
    final private List<NotebookLibrary> EXPECTED_RESTORED_LIB_LIST = Arrays.asList(
            new NotebookLibrary().libname("lib1").libversion("libversion1-build_1").source("unit_test"),
            new NotebookLibrary().libname("lib2").libversion("libversion2-build_2").source("pypi"),
            new NotebookLibrary().libname("lib3").libversion("libversion3.3-unknown").source("unit-test")
    );

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        when(kubernetesServiceObjectFactoryMock.getObject()).thenReturn(kubernetesServiceMock);
    }

    @Test
    public void testBuildNotebookURL() {

        // -------- Act --------
        String notebookURL = notebookService.buildNotebookURL(TEST_SERVICE_ID, "1234abcd");

        // -------- Assert --------
        assertThat(notebookURL).isEqualTo(
                        TEST_NOTEBOOK_URL_ORIGIN
                        + "/swirrl/jupyter/"
                        + TEST_SERVICE_ID + "?token=1234abcd");
    }

    @Test
    public void testCreateNotebookWithSessionId() {

        // -------- Arrange --------
        Notebook notebook = new Notebook();
        notebook.setUserRequestedLibraries(USER_REQUESTED_LIB_LIST);
        notebook.setSessionId(TEST_SESSION_ID);

        prepareKubernetesMockForSuccessfulCreation();

        when(kubernetesServiceMock.retrievePVCsByLabel(ServiceDeploymentFactory.SESSION_ID_LABEL + "=" + TEST_SESSION_ID))
                .thenReturn(successResponse);

        // -------- Act --------
        ResponseEntity<?> response = notebookService.createNotebook(notebook);

        // -------- Assert --------
        verifyKubernetesMockForSuccessfulCreation();
        verify(kubernetesServiceMock, times(1)).retrievePVCsByLabel(ServiceDeploymentFactory.SESSION_ID_LABEL + "=" + TEST_SESSION_ID);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

        Notebook notebookResponse = (Notebook) response.getBody();
        assertThat(notebookResponse.getId()).matches(
                "(?i)[0-9A-F]{8}-[0-9A-F]{4}-[4][0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}"); // Regex for v4 UUID.
        assertThat(notebookResponse.getPoolId()).matches(
                "(?i)[0-9A-F]{8}-[0-9A-F]{4}-[4][0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}"); // Regex for v4 UUID.
        assertThat(notebookResponse.getServiceURL().split("=")[0]).isEqualTo(
                notebookService.buildNotebookURL(notebookResponse.getId(), "1234abcd").split("\\?")[0]);
        assertThat(notebookResponse.getUserRequestedLibraries()).containsExactlyInAnyOrderElementsOf(USER_REQUESTED_LIB_LIST);
        assertThat(notebookResponse.getSessionId()).isEqualTo(TEST_SESSION_ID);
    }

    @Test
    public void testCreateNotebookWithPoolId() {

        // -------- Arrange --------
        Notebook notebook = new Notebook();
        notebook.setUserRequestedLibraries(USER_REQUESTED_LIB_LIST);
        notebook.setPoolId(TEST_POOL_ID);

        prepareKubernetesMockForSuccessfulCreation();

        // -------- Act --------
        ResponseEntity<?> response = notebookService.createNotebook(notebook);

        // -------- Assert --------
        verifyKubernetesMockNotUsedForCreation();

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    }

    @Test
    public void testCreateNotebookWithInvalidPoolId() {

        // -------- Arrange --------
        Notebook notebook = new Notebook();
        notebook.setUserRequestedLibraries(USER_REQUESTED_LIB_LIST);
        notebook.setPoolId(TEST_POOL_ID);
        notebook.setSessionId(TEST_SESSION_ID);

        prepareKubernetesMockForSuccessfulCreation();

        when(kubernetesServiceMock.retrievePVCsByLabel(ServiceDeploymentFactory.SESSION_ID_LABEL + "=" + TEST_SESSION_ID))
                .thenReturn(successResponse);
        when(kubernetesServiceMock.retrievePVCsByLabel(ServiceDeploymentFactory.POOL_ID_LABEL + "=" + TEST_POOL_ID))
                .thenReturn(notFoundResponse);

        // -------- Act --------
        ResponseEntity<?> response = notebookService.createNotebook(notebook);

        // -------- Assert --------
        verifyKubernetesMockNotUsedForCreation();
        verify(kubernetesServiceMock, times(1)).retrievePVCsByLabel(ServiceDeploymentFactory.SESSION_ID_LABEL + "=" + TEST_SESSION_ID);
        verify(kubernetesServiceMock, times(1)).retrievePVCsByLabel(ServiceDeploymentFactory.POOL_ID_LABEL + "=" + TEST_POOL_ID);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    @Test
    public void testCreateNotebookWithNonMatchingPoolAndSessionId() {

        // -------- Arrange --------
        Notebook notebook = new Notebook();
        notebook.setUserRequestedLibraries(USER_REQUESTED_LIB_LIST);
        notebook.setPoolId(TEST_POOL_ID);
        notebook.setSessionId("not-" + TEST_SESSION_ID);

        V1PersistentVolumeClaimList pvcList = new V1PersistentVolumeClaimList();

        try {
            pvcList.addItemsItem(Yaml.loadAs( new InputStreamReader(
                    new ClassPathResource("expected-working-directory-pvc.yaml").getInputStream()), V1PersistentVolumeClaim.class));
        } catch (IOException e) {
            logger.error("Exception occured whilst reading yaml file.",e);
            fail("Not possible to open expected pod yaml file.");
        }

        final V1PersistentVolumeClaimList testpvcList = pvcList;

        prepareKubernetesMockForSuccessfulCreation();

        when(kubernetesServiceMock.retrievePVCsByLabel(ServiceDeploymentFactory.SESSION_ID_LABEL + "=not-" + TEST_SESSION_ID))
                .thenReturn(successResponse);
        when(kubernetesServiceMock.retrievePVCsByLabel(ServiceDeploymentFactory.POOL_ID_LABEL + "=" + TEST_POOL_ID))
                .thenAnswer((String) -> new ResponseEntity<>(testpvcList, HttpStatus.OK));

        // -------- Act --------
        ResponseEntity<?> response = notebookService.createNotebook(notebook);

        // -------- Assert --------
        verifyKubernetesMockNotUsedForCreation();
        verify(kubernetesServiceMock, times(1)).retrievePVCsByLabel(ServiceDeploymentFactory.SESSION_ID_LABEL + "=not-" + TEST_SESSION_ID);
        verify(kubernetesServiceMock, times(1)).retrievePVCsByLabel(ServiceDeploymentFactory.POOL_ID_LABEL + "=" + TEST_POOL_ID);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    }

    @Test
    public void testCreateNotebookWithPoolAndSessionId() {

        // -------- Arrange --------
        Notebook notebook = new Notebook();
        notebook.setUserRequestedLibraries(USER_REQUESTED_LIB_LIST);
        notebook.setPoolId(TEST_POOL_ID);
        notebook.setSessionId(TEST_SESSION_ID);

        V1PersistentVolumeClaimList pvcList = new V1PersistentVolumeClaimList();

        try {
            pvcList.addItemsItem(Yaml.loadAs( new InputStreamReader(
                    new ClassPathResource("expected-working-directory-pvc.yaml").getInputStream()), V1PersistentVolumeClaim.class));
        } catch (IOException e) {
            logger.error("Exception occured whilst reading yaml file.",e);
            fail("Not possible to open expected pod yaml file.");
        }

        final V1PersistentVolumeClaimList testpvcList = pvcList;

        prepareKubernetesMockForSuccessfulCreation();

        when(kubernetesServiceMock.retrievePVCsByLabel(ServiceDeploymentFactory.SESSION_ID_LABEL + "=" + TEST_SESSION_ID))
                .thenReturn(successResponse);
        when(kubernetesServiceMock.retrievePVCsByLabel(ServiceDeploymentFactory.POOL_ID_LABEL + "=" + TEST_POOL_ID))
                .thenAnswer((String) -> new ResponseEntity<>(testpvcList, HttpStatus.OK));

        // -------- Act --------
        ResponseEntity<?> response = notebookService.createNotebook(notebook);

        // -------- Assert --------
        verifyKubernetesMockForSuccessfulCreation();
        verify(kubernetesServiceMock, times(1)).retrievePVCsByLabel(ServiceDeploymentFactory.SESSION_ID_LABEL + "=" + TEST_SESSION_ID);
        verify(kubernetesServiceMock, times(1)).retrievePVCsByLabel(ServiceDeploymentFactory.POOL_ID_LABEL + "=" + TEST_POOL_ID);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

        Notebook notebookResponse = (Notebook) response.getBody();
        assertThat(notebookResponse.getId()).matches(
                "(?i)[0-9A-F]{8}-[0-9A-F]{4}-[4][0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}"); // Regex for v4 UUID.
        assertThat(notebookResponse.getServiceURL().split("=")[0]).isEqualTo(
                notebookService.buildNotebookURL(notebookResponse.getId(), "1234abcd").split("\\?")[0]);
        assertThat(notebookResponse.getUserRequestedLibraries()).containsExactlyInAnyOrderElementsOf(USER_REQUESTED_LIB_LIST);
        assertThat(notebookResponse.getPoolId()).isEqualTo(TEST_POOL_ID);
        assertThat(notebookResponse.getSessionId()).isEqualTo(TEST_SESSION_ID);
    }

    @Test
    public void testCreateNotebookWithInvalidSessionId() {

        // -------- Arrange --------
        Notebook notebook = new Notebook();
        notebook.setUserRequestedLibraries(USER_REQUESTED_LIB_LIST);
        notebook.setSessionId(TEST_SESSION_ID);

        prepareKubernetesMockForSuccessfulCreation();

        when(kubernetesServiceMock.retrievePVCsByLabel(ServiceDeploymentFactory.SESSION_ID_LABEL + "=" + TEST_SESSION_ID))
                .thenReturn(notFoundResponse);

        // -------- Act --------
        ResponseEntity<?> response = notebookService.createNotebook(notebook);

        // -------- Assert --------
        verifyKubernetesMockNotUsedForCreation();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    @Test
    public void testCreateNotebookWithoutSessionId() {

        // -------- Arrange --------
        Notebook notebook = new Notebook();
        notebook.setUserRequestedLibraries(USER_REQUESTED_LIB_LIST);

        prepareKubernetesMockForSuccessfulCreation();

        // -------- Act --------
        ResponseEntity<?> response = notebookService.createNotebook(notebook);

        // -------- Assert --------
        verifyKubernetesMockForSuccessfulCreation();

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

        Notebook notebookResponse = (Notebook) response.getBody();
        assertThat(notebookResponse.getId()).matches(
                "(?i)[0-9A-F]{8}-[0-9A-F]{4}-[4][0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}"); // Regex for v4 UUID.
        assertThat(notebookResponse.getServiceURL().split("=")[0]).isEqualTo(
                notebookService.buildNotebookURL(notebookResponse.getId(), "1234abcd").split("\\?")[0]);
        assertThat(notebookResponse.getUserRequestedLibraries()).containsExactlyInAnyOrderElementsOf(USER_REQUESTED_LIB_LIST);
        assertThat(notebookResponse.getSessionId()).matches(
                "(?i)[0-9A-F]{8}-[0-9A-F]{4}-[4][0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}"); // Regex for v4 UUID.
        assertThat(notebookResponse.getPoolId()).matches(
                "(?i)[0-9A-F]{8}-[0-9A-F]{4}-[4][0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}"); // Regex for v4 UUID.
    }

    private void prepareKubernetesMockForSuccessfulCreation() {
        when(kubernetesServiceMock.createOrReplaceConfigMap(any(V1ConfigMap.class)))
                .thenReturn(successResponse);
        when(kubernetesServiceMock.createPersistentVolumeClaimWhenNotExists(any(V1PersistentVolumeClaim.class)))
                .thenReturn(successResponse);
        when(kubernetesServiceMock.createDeployment(any(V1Pod.class)))
                .thenReturn(successResponse);
        when(kubernetesServiceMock.createService(any(V1Service.class)))
                .thenReturn(successResponse);
        when(kubernetesServiceMock.createIngress(any(Map.class)))
                .thenReturn(successResponse);
    }

    private void verifyKubernetesMockForSuccessfulCreation() {
        verify(kubernetesServiceMock, times(4)).createOrReplaceConfigMap(any(V1ConfigMap.class));
        verify(kubernetesServiceMock, times(2)).createPersistentVolumeClaimWhenNotExists(any(V1PersistentVolumeClaim.class));
        verify(kubernetesServiceMock, times(1)).createDeployment(any(V1Pod.class));
        verify(kubernetesServiceMock, times(1)).createService(any(V1Service.class));
        verify(kubernetesServiceMock, times(1)).createIngress(any(Map.class));
    }

    private void verifyKubernetesMockNotUsedForCreation() {
        verify(kubernetesServiceMock, times(0)).createOrReplaceConfigMap(any(V1ConfigMap.class));
        verify(kubernetesServiceMock, times(0)).createPersistentVolumeClaimWhenNotExists(any(V1PersistentVolumeClaim.class));
        verify(kubernetesServiceMock, times(0)).createDeployment(any(V1Pod.class));
        verify(kubernetesServiceMock, times(0)).createService(any(V1Service.class));
        verify(kubernetesServiceMock, times(0)).createIngress(any(Map.class));
    }

    @Test
    public void testUpdateNotebook() {

        // -------- Arrange --------
        Notebook notebook = new Notebook();
        notebook.setUserRequestedLibraries(USER_REQUESTED_LIB_LIST);

        
        V1Pod podBeforeUpdatePreload = null;

        try {
        	podBeforeUpdatePreload = Yaml.loadAs(
                    new InputStreamReader(
                            new ClassPathResource("expected-pod-test-id.yaml").getInputStream()),
                    V1Pod.class);
        } catch (IOException e) {
            logger.error("Exception occured whilst reading yaml file.",e);
            fail("Not possible to open expected pod yaml file.");
        }
        
        final V1Pod podBeforeUpdate = podBeforeUpdatePreload;

        String[] pipcommand = {"pip", "install", "lib1==libversion1", "lib2==libversion2", "lib3", "lib4==libversion4"};
        String[] condacommand = {"swirrl-conda", "install", "--yes", "lib1==libversion1", "lib2==libversion2", "lib3", "lib4==libversion4"};

        // Testing installing with pip.
        when(kubernetesServiceMock.retrievePodByLabel(ServiceDeploymentFactory.SERVICE_ID_LABEL + "=" + TEST_SERVICE_ID))
                .thenAnswer((V1Pod) -> new ResponseEntity(podBeforeUpdate, HttpStatus.OK));
        when(kubernetesServiceMock.executeShellCommandOnPod(podBeforeUpdate, pipcommand))
             .thenAnswer((String) -> new ResponseEntity("Libraries installed", HttpStatus.OK));

        // -------- Act --------
        ResponseEntity<?> response = notebookService.updateNotebook(TEST_SERVICE_ID, notebook);

        // -------- Assert --------
        verify(kubernetesServiceMock, times(1)).retrievePodByLabel(ServiceDeploymentFactory.SERVICE_ID_LABEL + "=" + TEST_SERVICE_ID);
        verify(kubernetesServiceMock, times(1)).executeShellCommandOnPod(any(V1Pod.class), eq(pipcommand));

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

        Notebook notebookResponse = (Notebook) response.getBody();
        assertThat(notebookResponse.getId()).isEqualTo(TEST_SERVICE_ID);
        assertThat(notebookResponse.getServiceURL()).isEqualTo(notebookService.buildNotebookURL(TEST_SERVICE_ID, ""));
        assertThat(notebookResponse.getUserRequestedLibraries()).containsExactlyInAnyOrderElementsOf(USER_REQUESTED_LIB_LIST);

        // Testing installing with conda after pip fails.
        when(kubernetesServiceMock.executeShellCommandOnPod(podBeforeUpdate, pipcommand))
                .thenAnswer((String) -> new ResponseEntity("Libraries not installed", HttpStatus.INTERNAL_SERVER_ERROR));
        when(kubernetesServiceMock.executeShellCommandOnPod(podBeforeUpdate, condacommand))
                .thenAnswer((String) -> new ResponseEntity("Libraries installed", HttpStatus.OK));
        // -------- Act --------
        response = notebookService.updateNotebook(TEST_SERVICE_ID, notebook);
        verify(kubernetesServiceMock, times(2)).retrievePodByLabel(ServiceDeploymentFactory.SERVICE_ID_LABEL + "=" + TEST_SERVICE_ID);
        verify(kubernetesServiceMock, times(2)).executeShellCommandOnPod(any(V1Pod.class), eq(pipcommand));
        verify(kubernetesServiceMock, times(1)).executeShellCommandOnPod(any(V1Pod.class), eq(condacommand));

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

        notebookResponse = (Notebook) response.getBody();
        assertThat(notebookResponse.getId()).isEqualTo(TEST_SERVICE_ID);
        assertThat(notebookResponse.getServiceURL()).isEqualTo(notebookService.buildNotebookURL(TEST_SERVICE_ID, ""));
        assertThat(notebookResponse.getUserRequestedLibraries()).containsExactlyInAnyOrderElementsOf(USER_REQUESTED_LIB_LIST);

        // Testing error response if both pip and conda fail.
        when(kubernetesServiceMock.executeShellCommandOnPod(podBeforeUpdate, pipcommand))
                .thenAnswer((String) -> new ResponseEntity("Libraries not installed\n", HttpStatus.INTERNAL_SERVER_ERROR));
        when(kubernetesServiceMock.executeShellCommandOnPod(podBeforeUpdate, condacommand))
                .thenAnswer((String) -> new ResponseEntity("Libraries not installed", HttpStatus.INTERNAL_SERVER_ERROR));
        // -------- Act --------
        response = notebookService.updateNotebook(TEST_SERVICE_ID, notebook);

        // -------- Assert --------
        verify(kubernetesServiceMock, times(3)).retrievePodByLabel(ServiceDeploymentFactory.SERVICE_ID_LABEL + "=" + TEST_SERVICE_ID);
        verify(kubernetesServiceMock, times(3)).executeShellCommandOnPod(any(V1Pod.class), eq(pipcommand));
        verify(kubernetesServiceMock, times(2)).executeShellCommandOnPod(any(V1Pod.class), eq(condacommand));

        assertThat(response.getStatusCode()).isNotEqualTo(HttpStatus.OK);

        Error expectedErrorResponse = new Error();
        expectedErrorResponse.setCode(HttpStatus.INTERNAL_SERVER_ERROR.toString());
        expectedErrorResponse.setDescription("Libraries not installed\nLibraries not installed");
        Error errorResponse = (Error) response.getBody();

        assertThat(errorResponse.getDescription()).isEqualTo(expectedErrorResponse.getDescription());
        assertThat(errorResponse.getCode()).isEqualTo(expectedErrorResponse.getCode());
        // Kind of redundant, but lets test Error class' equals method while we're at it.
        assertThat(errorResponse.equals(expectedErrorResponse));
    }

    @Test
    public void testGetNotebookById() {

        // -------- Arrange --------
        final String LIB_LIST = new StringBuilder()
                .append("lib1 libversion1 build_1 source_1\n")
                .append("lib2 libversion2 build_2 source_1\n")
                .append("lib3\n")
                .append("lib4 libversion4 build_4\n")
                .toString();

        final List<Volume> EXPECTED_VOLUMES = Arrays.asList(
                new Volume().name(TEST_DATA_DIRECTORY_PVC_NAME).size(TEST_DATA_DIRECTORY_PVC_SIZE),
                new Volume().name(TEST_WORKING_DIRECTORY_PVC_NAME).size(TEST_WORKING_DIRECTORY_PVC_SIZE)
        );

        V1Pod pod = null;
        V1PersistentVolumeClaimList pvcAllList = new V1PersistentVolumeClaimList();
        V1PersistentVolumeClaimList pvcWorkingList = new V1PersistentVolumeClaimList();

        try {
            pod = Yaml.loadAs( new InputStreamReader(
                    new ClassPathResource("expected-pod-test-id.yaml").getInputStream()), V1Pod.class);
            pvcAllList.addItemsItem(Yaml.loadAs( new InputStreamReader(
                    new ClassPathResource("expected-data-directory-pvc.yaml").getInputStream()), V1PersistentVolumeClaim.class));
            pvcAllList.addItemsItem(Yaml.loadAs( new InputStreamReader(
                    new ClassPathResource("expected-working-directory-pvc.yaml").getInputStream()), V1PersistentVolumeClaim.class));
            pvcAllList.addItemsItem(Yaml.loadAs( new InputStreamReader(
                    new ClassPathResource("expected-working-directory-pvc.yaml").getInputStream()), V1PersistentVolumeClaim.class));
            pvcWorkingList.addItemsItem(Yaml.loadAs( new InputStreamReader(
                    new ClassPathResource("expected-working-directory-pvc.yaml").getInputStream()), V1PersistentVolumeClaim.class));
        } catch (IOException e) {
            logger.error("Exception occured whilst reading yaml file.",e);
            fail("Not possible to open expected yaml file.");
        }

        final V1Pod testpod = pod;
        final V1PersistentVolumeClaimList testpvcAllList = pvcAllList;
        final V1PersistentVolumeClaimList testpvcWorkingList = pvcWorkingList;

        when(kubernetesServiceMock.retrievePodByLabel(ServiceDeploymentFactory.SERVICE_ID_LABEL + "=" + TEST_SERVICE_ID))
                .thenAnswer((V1Pod) -> new ResponseEntity(testpod, HttpStatus.OK));
        when(kubernetesServiceMock.executeShellCommandOnPod(eq(testpod), eq(NotebookService.COMMAND_GET_INSTALLED_LIBS)))
                .thenAnswer((String) -> new ResponseEntity<>(LIB_LIST, HttpStatus.OK));
        when(kubernetesServiceMock.retrievePVCsByLabel(ServiceDeploymentFactory.SESSION_ID_LABEL + "=" + TEST_SESSION_ID))
                .thenAnswer((String) -> new ResponseEntity<>(testpvcAllList, HttpStatus.OK));
        when(kubernetesServiceMock.retrievePVCsByLabel(ServiceDeploymentFactory.POOL_ID_LABEL + "=" + TEST_POOL_ID))
                .thenAnswer((String) -> new ResponseEntity<>(testpvcWorkingList, HttpStatus.OK));

        // -------- Act --------
        ResponseEntity<?> response = notebookService.getNotebookById(TEST_SERVICE_ID);

        // -------- Assert --------
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

        ExtendedNotebook notebookResponse = (ExtendedNotebook) response.getBody();
        assertThat(notebookResponse.getId()).isEqualTo(TEST_SERVICE_ID);
        assertThat(notebookResponse.getSessionId()).isEqualTo(TEST_SESSION_ID);
        assertThat(notebookResponse.getPoolId()).isEqualTo(TEST_POOL_ID);
        assertThat(notebookResponse.getVolumes()).isEqualTo(EXPECTED_VOLUMES);
        assertThat(notebookResponse.getServiceURL()).isEqualTo(notebookService.buildNotebookURL(TEST_SERVICE_ID, ""));
        assertThat(notebookResponse.getInstalledLibraries()).containsExactlyInAnyOrderElementsOf(EXPECTED_LIB_LIST);
    }

    @Test
    public void testRestoreNotebookLibs() throws JSONException {
        // -------- Arrange --------
        V1Pod podBeforeUpdatePreload = null;
        String activityDocBody = null;
        String sessionActivityDocBody = null;
        V1PersistentVolumeClaimList pvcList = new V1PersistentVolumeClaimList();

        try {
            podBeforeUpdatePreload = Yaml.loadAs(
                    new InputStreamReader(new ClassPathResource("expected-pod-test-id.yaml").getInputStream()), V1Pod.class);
            activityDocBody = new String(Files.readAllBytes(Paths.get(new ClassPathResource("expected-activity-by-id-response.json").getURL().getPath())));
            sessionActivityDocBody = new String(Files.readAllBytes(Paths.get(new ClassPathResource("expected-session-activity.json").getURL().getPath())));
            pvcList.addItemsItem(Yaml.loadAs( new InputStreamReader(
                    new ClassPathResource("expected-working-directory-pvc.yaml").getInputStream()), V1PersistentVolumeClaim.class));
        } catch (IOException e) {
            logger.error("Exception occured whilst reading yaml file.",e);
            fail("Not possible to open expected pod yaml file.");
        }

        final V1Pod podBeforeUpdate = podBeforeUpdatePreload;
        final V1PersistentVolumeClaimList testpvcList = pvcList;

        final String EnvironmentYml = new StringBuilder()
                .append("channels: [conda-forge, defaults]\n")
                .append("prefix: /opt/conda")
                .append("name: base")
                .append("dependencies:")
                .append("- lib1=libversion1=build_1\n")
                .append("- lib3=libversion3.3=unknown\n")
                .append("- pip: [")
                .append("lib2==libversion2\n")
                .append("]\n")
                .toString();

        // Testing uninstalling.
        when(kubernetesServiceMock.executeShellCommandOnPod(any(V1Pod.class), eq(NotebookService.COMMAND_CONDA_RESTORE_ENV)))
                .thenAnswer((String) -> new ResponseEntity<>("Environment restored", HttpStatus.OK));
        when(kubernetesServiceMock.retrievePodByLabel(ServiceDeploymentFactory.SERVICE_ID_LABEL + "=" + TEST_SERVICE_ID))
                .thenAnswer((V1Pod) -> new ResponseEntity(podBeforeUpdate, HttpStatus.OK));
        String finalActivityDocBody = activityDocBody;
        String finalSessionActivityDocBody = sessionActivityDocBody;
        when(provenanceApi.getActivityById(TEST_CREATE_SESSION_ACTIVITY_ID)).thenAnswer((String) -> new ResponseEntity(finalActivityDocBody, HttpStatus.OK));
        when(provenanceApi.getSessionActivity(TEST_SESSION_ID, null, null, null, null, Boolean.TRUE, Boolean.FALSE)).thenAnswer((String) -> new ResponseEntity(finalSessionActivityDocBody, HttpStatus.OK));
        when(kubernetesServiceMock.writeStringToFileOnPod(any(V1Pod.class), eq(EnvironmentYml), eq("/home/jovyan/work/.environment-"+TEST_SERVICE_ID+".yml")))
                .thenAnswer((String) -> new ResponseEntity<>("Buffer written to /home/jovyan/work/.environment.yml on pod.", HttpStatus.OK));
        when(kubernetesServiceMock.retrieveActiveJobsByLabel(ServiceDeploymentFactory.SERVICE_ID_LABEL + "=" + TEST_SERVICE_ID)).
                thenAnswer((String) -> new ResponseEntity<>(new ArrayList<String>(), HttpStatus.OK));
        when(kubernetesServiceMock.deleteJobsByLabel(ServiceDeploymentFactory.SERVICE_ID_LABEL + "=" + TEST_SERVICE_ID)).
                thenAnswer((String) -> new ResponseEntity<>(HttpStatus.OK));
        when(kubernetesServiceMock.deleteConfigMapsByLabel(ServiceDeploymentFactory.SERVICE_ID_LABEL + "=" + TEST_SERVICE_ID)).
                thenAnswer((String) -> new ResponseEntity<>(HttpStatus.OK));
        when(kubernetesServiceMock.deleteServiceIngress(SERVICE_TYPE, TEST_SERVICE_ID)).
                thenAnswer((String) -> new ResponseEntity<>(HttpStatus.OK));
        when(kubernetesServiceMock.deleteServiceByName("jupyter-" + TEST_SERVICE_ID)).
                thenAnswer((String) -> new ResponseEntity<>(HttpStatus.OK));
        when(kubernetesServiceMock.deletePodsByLabel(ServiceDeploymentFactory.SERVICE_ID_LABEL + "=" + TEST_SERVICE_ID)).
                thenAnswer((String) -> new ResponseEntity<>(HttpStatus.OK));
        when(kubernetesServiceMock.retrievePVCsByLabel(ServiceDeploymentFactory.SESSION_ID_LABEL + "=" + TEST_SESSION_ID))
                .thenReturn(successResponse);
        when(kubernetesServiceMock.retrievePVCsByLabel(ServiceDeploymentFactory.POOL_ID_LABEL + "=" + TEST_POOL_ID))
                .thenAnswer((String) -> new ResponseEntity<>(testpvcList, HttpStatus.OK));
        when(kubernetesServiceMock.isPodSessionIdEqualTo(podBeforeUpdate, TEST_SESSION_ID)).thenReturn(true);
        when(kubernetesServiceMock.getNotebookToken(any(V1Pod.class))).thenReturn("1234abcd");
        prepareKubernetesMockForSuccessfulCreation();
        when(kubernetesServiceMock.waitOnContainerTerminated(ServiceDeploymentFactory.SERVICE_ID_LABEL + "=" + TEST_SERVICE_ID, 100)).
                thenReturn(true);

        // -------- Act --------
        UserInfo userInfo = new UserInfo("mock_user_id", "swirrl-api", "mock_authmode_id", "mock_group_id");
        ResponseEntity<?> response = notebookService.restoreNotebookLibs(TEST_SERVICE_ID, TEST_CREATE_SESSION_ACTIVITY_ID, userInfo);

        // -------- Assert --------
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        Notebook notebookResponse = (Notebook) response.getBody();
        assertThat(notebookResponse.getId()).isEqualTo(TEST_SERVICE_ID);
        assertThat(notebookResponse.getServiceURL().split("=")[0]).isEqualTo(
                notebookService.buildNotebookURL(notebookResponse.getId(), ""));
        assertThat(notebookResponse.getUserRequestedLibraries()).containsExactlyInAnyOrderElementsOf(EXPECTED_RESTORED_LIB_LIST);
        assertThat(notebookResponse.getSessionId()).isEqualTo(TEST_SESSION_ID);
        assertThat(notebookResponse.getPoolId()).isEqualTo(TEST_POOL_ID);
        verify(kubernetesServiceMock, times(0)).executeShellCommandOnPod(any(V1Pod.class), eq(NotebookService.COMMAND_CONDA_RESTORE_ENV));
    }

    private V1Pod makeMockedPod() {
        Map<String, String> map = new HashMap<>();
        map.put("service-id", TEST_SERVICE_ID);
        map.put("pool-id", TEST_POOL_ID);
        map.put("session-id", TEST_SESSION_ID);
        map.put("session-id-workflow", TEST_SESSION_ID);

        V1Pod pod = null;
        try {
            pod = Yaml.loadAs(
                    new InputStreamReader(new ClassPathResource("expected-pod-test-id.yaml").getInputStream()), V1Pod.class);
            pod.getMetadata().setLabels(map);
        } catch (IOException e) {
            logger.error("Exception occured whilst reading yaml file.", e);
            fail("Not possible to open expected pod yaml file.");
        }
        return pod;
    }

    @Test
    public void testStoreSnapshotWithoutMessage() {

        // -------- Arrange --------
        Snapshot snapshot = new Snapshot();
        final ArrayList<String> ACTIVE_JOBS = new ArrayList<String>();
        V1Pod pod = makeMockedPod();
        prepareKubernetesMockForSuccessfulCreation();
        when(kubernetesServiceMock.retrieveActiveJobsByLabel(ServiceDeploymentFactory.SERVICE_ID_LABEL + "=" + TEST_SERVICE_ID))
                .thenAnswer((String) -> new ResponseEntity(ACTIVE_JOBS, HttpStatus.OK));
        when(kubernetesServiceMock.retrieveActiveJobsByLabel("session-id-workflow" + "=" + TEST_SESSION_ID))
                .thenAnswer((String) -> new ResponseEntity(ACTIVE_JOBS, HttpStatus.OK));

        V1Pod finalPodWithSessionId = pod;
        when(kubernetesServiceMock.retrievePodByLabel(ServiceDeploymentFactory.SERVICE_ID_LABEL + "=" + TEST_SERVICE_ID))
                .thenAnswer((String) -> new ResponseEntity(finalPodWithSessionId, HttpStatus.OK));

        // Snapshot still without a message:
        // -------- Act --------
        ResponseEntity<?> response = notebookService.storeSnapshot(TEST_SERVICE_ID, snapshot, null, null);

        // -------- Assert --------
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    }

    @Test
    public void testStoreSnapshotWithMessage() {
        /**********************
         * TODO: The problem with this test is that it doesn't actually test anything at all.
         * The reason is that we can only test what we've set up.
         * Kept for reference and perhaps some improvements in the future. Testing of some
         * underlying code is done in CreateSnapshotJobFactoryTest class.
         */
        // -------- Arrange --------
        Snapshot snapshot = new Snapshot();
        String snapshotMessage = "sometext";
        final ArrayList<String> ACTIVE_JOBS = new ArrayList<String>();
        V1Pod pod = makeMockedPod();
        prepareKubernetesMockForSuccessfulCreation();

        Job csJobExpect = new Job();
        csJobExpect.setId(TEST_CREATE_SNAPSHOT_JOB_ID);
        byte [] logs = ("https://github.com/swirrl-api-test-user/"+snapshotMessage+"-"+UUID.randomUUID().toString().split("-")[0]).getBytes();
        csJobExpect.setLogs(logs);
        csJobExpect.setStatus("Succeeded");
        when(kubernetesServiceMock.retrieveActiveJobsByLabel(ServiceDeploymentFactory.SERVICE_ID_LABEL + "=" + TEST_SERVICE_ID))
                .thenAnswer((String) -> new ResponseEntity(ACTIVE_JOBS, HttpStatus.OK));
        when(kubernetesServiceMock.retrieveActiveJobsByLabel("session-id-workflow" + "=" + TEST_SESSION_ID))
                .thenAnswer((String) -> new ResponseEntity(ACTIVE_JOBS, HttpStatus.OK));
        V1Pod finalPodWithSessionId = pod;
        when(kubernetesServiceMock.retrievePodByLabel(ServiceDeploymentFactory.SERVICE_ID_LABEL + "=" + TEST_SERVICE_ID))
                .thenAnswer((String) -> new ResponseEntity(finalPodWithSessionId, HttpStatus.OK));
        when(kubernetesServiceMock.createJob(any(V1Job.class)))
                .thenAnswer((String) -> new ResponseEntity(csJobExpect, HttpStatus.OK));
        when(kubernetesServiceMock.getJobStatus("controller-uid", TEST_CREATE_SNAPSHOT_JOB_ID))
                .thenAnswer((String) -> new ResponseEntity(csJobExpect, HttpStatus.OK));

        // Set a user message and test:
        // -------- Act --------
        snapshot.setUserMessage(snapshotMessage);
        ResponseEntity<?> validResponse = notebookService.storeSnapshot(TEST_SERVICE_ID, snapshot, null, null);

        // -------- Assert --------
        assertThat(validResponse.getStatusCode()).isEqualTo(HttpStatus.OK);
        SnapshotURL url = (SnapshotURL) validResponse.getBody();
        logger.debug(url.getSnapshotURL());
        assertThat(snapshotMessage).isNotNull();
    }

    @Test
    public void checkActiveJobsWithLabel() {

        // -------- Arrange --------
        Map<String,String> map=new HashMap<>();
        map.put("service-id", TEST_SERVICE_ID);

        final ArrayList<String> ACTIVE_JOBS = new ArrayList<String>();
        ACTIVE_JOBS.add("FAKEJOB");

        V1Pod pod = null;
        try {
            pod = Yaml.loadAs(
                    new InputStreamReader(new ClassPathResource("expected-pod-test-id.yaml").getInputStream()), V1Pod.class);
            pod.getMetadata().setLabels(map);
        } catch (IOException e) {
            logger.error("Exception occured whilst reading yaml file.",e);
            fail("Not possible to open expected pod yaml file.");
        }

        prepareKubernetesMockForSuccessfulCreation();

        // Simulate an active job and make sure that method reports failure:
        when(kubernetesServiceMock.retrieveActiveJobsByLabel(TEST_SERVICE_ID))
                .thenAnswer((String) -> new ResponseEntity(ACTIVE_JOBS, HttpStatus.OK));

        // -------- Act --------
        ResponseEntity<?> response = notebookService.checkActiveJobsWithLabel(kubernetesServiceMock, TEST_SERVICE_ID);

        // -------- Assert --------
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    }

}