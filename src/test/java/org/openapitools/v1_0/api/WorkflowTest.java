package org.openapitools.v1_0.api;

import org.junit.jupiter.api.Test;
import org.openapitools.v1_0.model.WorkflowItem;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class WorkflowTest {

    @Test
    void testConfigmapToCorrectWorkflowList() {
        Map<String, String> workflowConfigMap = new LinkedHashMap<>();
        workflowConfigMap.put("swirrl-workflow.properties", """
                "workflow.wow.docker.image.tag=workflow-wow:77c0526
                		workflow.wowcron.docker.image.tag=workflow-wowcron:77c0526
                		session.deletestage.image.tag=deletestage:a647235
                		workflow.download.docker.image.tag=workflow-download:a647235
                		workflow.feather.docker.image.tag=registry.gitlab.com/knmi-oss/swirrl/swirrl-workflow/workflow-feather:a647235
                		workflow.base.docker.image.tag=registry.gitlab.com/knmi-oss/swirrl/swirrl-workflow/workflow-base:f7d4b38
                		workflow.post.docker.image.tag=registry.gitlab.com/knmi-oss/swirrl/swirrl-workflow/workflow-post:f7d4b38
                		workflow.pre.docker.image.tag=registry.gitlab.com/knmi-oss/swirrl/swirrl-workflow/workflow-pre:f7d4b38
                		workflow.dockercmds.docker.image.tag=registry.gitlab.com/knmi-oss/swirrl/swirrl-workflow/workflow-dockercmds:f7d4b38-dirty
                		workflow.dinddaemon.docker.image.tag=registry.gitlab.com/knmi-oss/swirrl/swirrl-workflow/workflow-dinddaemon:f7d4b38
                		""");

        var result = new ArrayList<WorkflowItem>();
        result.add(new WorkflowItem().name("wow").version("latest").imageTag("workflow-wow:77c0526"));
        result.add(new WorkflowItem().name("wowcron").version("latest").imageTag("workflow-wowcron:77c0526"));
        result.add(new WorkflowItem().name("download").version("latest").imageTag("workflow-download:a647235"));
        result.add(new WorkflowItem().name("feather").version("latest").imageTag("workflow-feather:a647235"));

        List<WorkflowItem> list = ParseUtils.parseWorkflowMap(workflowConfigMap);
        assertThat(list).containsExactlyInAnyOrderElementsOf(result);
    }


    @Test
    void testConfigmapToCorrectCustomWorkflowList() {
        Map<String, String> workflowConfigMap = new LinkedHashMap<>();
        workflowConfigMap.put("custom-mytestworkflow.v1", "nginx");
        workflowConfigMap.put("custom-mytestworkflow.v2", "nginx");
        workflowConfigMap.put("custom-mytestworkflow2.v1", "nginx");

        var result = new ArrayList<WorkflowItem>();
        result.add(new WorkflowItem().name("custom-mytestworkflow.v1").version("v1").imageTag("nginx"));
        result.add(new WorkflowItem().name("custom-mytestworkflow.v2").version("v2").imageTag("nginx"));
        result.add(new WorkflowItem().name("custom-mytestworkflow2.v1").version("v1").imageTag("nginx"));

        List<WorkflowItem> list = ParseUtils.parseCustomWorkflowMap(workflowConfigMap);
        assertThat(list).containsExactlyInAnyOrderElementsOf(result);
    }
}
