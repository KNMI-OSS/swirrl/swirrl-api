package org.openapitools.v1_0.api;

import io.kubernetes.client.openapi.models.V1PersistentVolumeClaim;
import io.kubernetes.client.util.Yaml;
import org.openapitools.v1_0.model.kubernetes.SessionFactory;
import org.openapitools.v1_0.provenance.Neo4jProvenanceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.test.context.TestPropertySource;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.IOException;
import java.io.InputStreamReader;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@TestPropertySource(properties = {
        "notebook.docker.image.tag=testtagname",
        "service.docker.image.pull.secret=testsecretname"
})

public class SessionFactoryTest {

    @Autowired
    SessionFactory sessionFactory;

    final private String TEST_SESSION_ID = "test-session-id";
    final private String TEST_POOL_ID = "test-pool-id";
    private static Logger logger = LoggerFactory.getLogger(SessionFactoryTest.class);

    @MockBean // Prevent connection attempt to the database
    Neo4jProvenanceService neo4jProvenanceServiceMock;

    @Test
    public void testWorkingDirectoryYaml() {
        V1PersistentVolumeClaim expectedPvc = null;

        try {
            expectedPvc = Yaml.loadAs(
                    new InputStreamReader(
                            new ClassPathResource("expected-working-directory-pvc.yaml").getInputStream()),
                    V1PersistentVolumeClaim.class);
        } catch (IOException e) {
            logger.error("Exception occurred whilst reading yaml file.",e);
            fail("Not possible to open expected pvc yaml file.");
        }

        V1PersistentVolumeClaim pvc = sessionFactory.makeWorkingdirPersistentVolumeClaim(TEST_SESSION_ID, TEST_POOL_ID, "1Gi");
        assertThat(pvc).isEqualTo(expectedPvc);
    }

    @Test
    public void testDataDirectoryYaml() {
        V1PersistentVolumeClaim expectedPvc = null;

        try {
            expectedPvc = Yaml.loadAs(
                    new InputStreamReader(
                            new ClassPathResource("expected-data-directory-pvc.yaml").getInputStream()),
                    V1PersistentVolumeClaim.class);
        } catch (IOException e) {
            logger.error("Exception occurred whilst reading yaml file.",e);
            fail("Not possible to open expected pvc yaml file.");
        }

        V1PersistentVolumeClaim pvc = sessionFactory.makeDatadirPersistentVolumeClaim(TEST_SESSION_ID, "1Gi");
        assertThat(pvc).isEqualTo(expectedPvc);
    }
}
