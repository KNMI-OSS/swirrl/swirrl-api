import express from "express";
import path from "path";

// Create a new express app instance
const app = express();
app.get("/healthz", (req, res) => {
  res.sendStatus(200);
});
app.use((req, res, next) => {
  console.log(
    `NEW REQUEST\npath: ${req.path}\nheaders: ${JSON.stringify(req.headers)}`
  );
  if (req.path.startsWith("/swirrl/jupyter/")) {
    if (req.path.endsWith("/config.js")) {
      res.status(200).sendFile(path.join(__dirname, "dist/config.js"));
    } else if (req.path.endsWith("/custom-error.js")) {
      res.status(200).sendFile(path.join(__dirname, "dist/custom-error.js"));
    } else if (req.path.endsWith("/custom-error.js.map")) {
      res
        .status(200)
        .sendFile(path.join(__dirname, "dist/custom-error.js.map"));
    } else {
      next();
    }
  } else {
    next();
  }
});
app.get("/swirrl/*", (req, res) => {
  if (req.headers["x-code"] === "503") {
    if (req.path.startsWith("/swirrl/jupyter/")) {
      res.status(501).sendFile(path.join(__dirname, "dist/index.html"));
    } else {
      res.status(501).sendFile(path.join(__dirname, "error-page.html"));
    }
  } else {
    res.status(501).sendFile(path.join(__dirname, "error-page.html"));
  }
});
app.listen(8080, () => {
  console.log("App is listening on port 8080!");
});
