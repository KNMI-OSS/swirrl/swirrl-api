# Custom error pages

The dockerfile in this directory builds a simple expressjs server that
serves `server/error-page.html`.

The ingress for the services is configured to send a request to this
server for every error status 404, 502 or 503. It will put this code
in the `x-code` header. This server then serves
`server/error-page.html` with the same status code.

# error-page.html

The page that is served is made to look similar to the juptyerlab
extension and notifies the user that the page they requested is not
available. Furthermore this page will keep checking on a schedule if
the requested resource is available. Once the resource is available
this page will automatically refresh.
