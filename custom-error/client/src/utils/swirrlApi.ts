import { SWIRRL_URL } from "../components/App/ApplicationConfig";

export type Notebook = {
  sessionId: string;
  poolId: string;
  id: string;
  serviceURL?: string;
};

export type InstalledLibraries = {
  libname: string;
  libversion: string;
  source: string;
}[];

export type ExtendedNotebook = Notebook & {
  notebookStatus: string;
  volumes: {
    name: string;
    size: number;
  }[];
  installedLibraries: {
    libname: string;
    libversion: string;
    source: string;
  }[];
};

export const createNotebook = async (
  notebook: Notebook
): Promise<ExtendedNotebook> => {
  const url = `${SWIRRL_URL}/notebook`;
  const payload = JSON.stringify(notebook);

  const response = (await fetch(url, {
    method: "POST",
    mode: "cors",
    credentials: "include",
    headers: {
      "Content-Type": "application/json",
    },
    referrerPolicy: "no-referrer-when-downgrade",
    body: payload,
  }).then((res) =>
    res.status === 200 ? res.json() : null
  )) as ExtendedNotebook;

  return response;
};

export const getNotebook = async (
  serviceId: string
): Promise<ExtendedNotebook> => {
  const url = `${SWIRRL_URL}/notebook/${serviceId}`;
  const response = (await fetch(url, {
    method: "GET",
    mode: "cors",
    credentials: "include",
    headers: {
      Accept: "application/json",
    },
    referrerPolicy: "no-referrer-when-downgrade",
  }).then((res) =>
    res.status === 200 || res.status === 404 ? res.json() : null
  )) as ExtendedNotebook;

  return response;
};

export const getSessionIds = async (serviceId: string): Promise<Notebook> => {
  const url = `${SWIRRL_URL}/provenance/service/${serviceId}/sessionIds`;
  const response = await fetch(url, {
    method: "GET",
    mode: "cors",
    credentials: "include",
    headers: {
      Accept: "application/json",
    },
    referrerPolicy: "no-referrer-when-downgrade",
  }).then((res) => (res.status === 200 ? res.json() : null));
  const result: Notebook = {
    id: response["swirrl:serviceId"],
    poolId: response["swirrl:poolId"],
    sessionId: response["swirrl:sessionId"],
  };

  return result;
};

export const getCurrentUrl = async (): Promise<string> => {
  const url = window.location.href;
  const response = await fetch(url, {
    method: "GET",
    mode: "cors",
    referrerPolicy: "no-referrer-when-downgrade",
  }).then((res) => (res.status === 200 ? res.text() : null));

  return response;
};
