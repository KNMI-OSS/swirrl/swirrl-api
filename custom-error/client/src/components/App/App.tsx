import React from "react";
import { UserProvider } from "./ApplicationContext";
import { LOGIN_ENABLED } from "./ApplicationConfig";
import { Login } from "../Login";
import { Restore } from "../Restore";

const App: React.FC = () => {
  return (
    <UserProvider>
      <>
        {LOGIN_ENABLED && <Login />}
        <Restore />
      </>
    </UserProvider>
  );
};
export default App;
