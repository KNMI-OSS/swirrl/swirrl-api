import React from "react";

type Props = {
  children: React.ReactElement;
};

export type OpenidUser = {
  email: string;
  email_verified: boolean;
  family_name: string;
  given_name: string;
  groups: string[];
  name: string;
  preferred_username: string;
  sub: string;
  registered: boolean;
  notebookId?: string;
  notebookURL?: string;
  sessionId?: string;
};

export type UserCtxType = {
  user: OpenidUser;
  setUser: (user: OpenidUser) => void;
};

export const UserCtx = React.createContext({} as UserCtxType);

export const UserProvider = ({ children }: Props): JSX.Element => {
  const [user, setUser] = React.useState<OpenidUser>(null);
  const userObject = React.useMemo(() => ({ user, setUser }), [user, setUser]);
  return <UserCtx.Provider value={userObject}>{children}</UserCtx.Provider>;
};
