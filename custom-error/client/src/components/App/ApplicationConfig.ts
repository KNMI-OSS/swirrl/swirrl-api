// Global config settings
import { getConfig } from "../../utils/configReader";

type Config = {
  openidUrl: string;
  loginEnabled: string;
  swirrlUrl: string;
};
const config: Config = getConfig();

/** OPENID configuration. */
export const OPENID = {
  authorization_endpoint: `${config.openidUrl}/protocol/openid-connect/auth`,
  jwks_uri: `${config.openidUrl}/protocol/openid-connect/certs`,
  token_endpoint: `${config.openidUrl}/protocol/openid-connect/token`,
  introspection_endpoint: `${config.openidUrl}/protocol/openid-connect/token/introspect`,
  userinfo_endpoint: `${config.openidUrl}/protocol/openid-connect/userinfo`,
  end_session_endpoint: `${config.openidUrl}/protocol/openid-connect/logout`,
};

export const LOGIN_ENABLED = config.loginEnabled === "yes";

export const SWIRRL_URL = `${config.swirrlUrl}${
  config.loginEnabled === "yes" ? "/relay" : ""
}/swirrl-api/v1.0`;
