import React from "react";
import { UserCtx } from "./App/ApplicationContext";
import { Typography, Button, Grid } from "@mui/material";
import {
  createNotebook,
  getCurrentUrl,
  getNotebook,
  getSessionIds,
} from "../utils/swirrlApi";

const restoreButtonHandler = async (
  user,
  setText,
  setShowRestore,
  setClickedRestore
) => {
  setText("Working...");
  setClickedRestore(true);
  setShowRestore(false);
  const splittedUrl = window.location.href.split("/");
  const serviceId =
    splittedUrl[splittedUrl.indexOf("jupyter") + 1].split("?")[0];
  const notebookPayload = await getSessionIds(serviceId);
  if (!notebookPayload) {
    setText("");
    setShowRestore(true);
    setClickedRestore(false);
    alert("Something went wrong restoring your notebook, please try again.");
  }
  if (user && user.notebookId && user.sessionId && user.notebookURL) {
    if (
      user.notebookId !== serviceId ||
      user.sessionId !== notebookPayload.sessionId
    ) {
      setText("");
      setShowRestore(true);
      setClickedRestore(false);
      alert(
        `This is not your (latest) notebook, please visit your latest notebook here: ${user.notebookURL}`
      );
      return;
    }
    notebookPayload.serviceURL = user.notebookURL;
  }
  const notebook = await getNotebook(serviceId);
  if (notebook.notebookStatus) {
    setText("");
    setShowRestore(true);
    setClickedRestore(false);
    alert("This notebook is already restored, please reload this page.");
    return;
  }
  const newNotebook = await createNotebook(notebookPayload);
  if (!newNotebook) {
    setText("");
    setShowRestore(true);
    setClickedRestore(false);
    alert("Something went wrong restoring your notebook, please try again.");
    return;
  }
  setText("Notebook restored!");
  var url = new URL(newNotebook.serviceURL);
  url.searchParams.append("clickedRestore", "true");
  window.location.replace(url);
};

export const Restore: React.FC = () => {
  const { user } = React.useContext(UserCtx);
  const [text, setText] = React.useState<string>("");
  const splittedUrl = window.location.href.split("/");
  const serviceId =
    splittedUrl[splittedUrl.indexOf("jupyter") + 1].split("?")[0];
  const [showRestore, setShowRestore] = React.useState<boolean>(false);
  const params = new URL(window.location.href).searchParams;
  const [clickedRestore, setClickedRestore] = React.useState<boolean>(
    params.get("clickedRestore") === "true"
  );

  React.useEffect(() => {
    const interval = setInterval(() => {
      if (!clickedRestore) {
        getNotebook(serviceId).then((notebook) => {
          if (notebook.notebookStatus) {
            setShowRestore(false);
          } else {
            setShowRestore(true);
          }
        });
      }
      getCurrentUrl().then((response) => {
        if (response) {
          window.location.reload();
        }
      });
    }, 5000);
    return () => {
      clearInterval(interval);
    };
  }, [clickedRestore]);
  return (
    <>
      <Grid container justifyContent="center">
        <Typography sx={{ border: 1, maxWidth: 500, marginTop: 10 }}>
          {showRestore && (
            <div style={{ padding: 20 }}>
              <h2>Restoring deleted notebooks</h2>
              <div style={{ marginBottom: 20 }}>
                If you are trying to reach an older notebook that was created
                some time ago, it could be that this notebook has been cleaned
                up due to inactivity or because of infrastructure upgrades. If
                you suspect this might be the case you can try to recreate your
                notebook by clicking the restore button below. If the
                associated data has not yet been deleted from our
                infrastructure, you should be able to continue where you left
                of.
              </div>
              <Button
                onClick={() =>
                  restoreButtonHandler(
                    user,
                    setText,
                    setShowRestore,
                    setClickedRestore
                  )
                }
                color="primary"
                variant="contained"
              >
                Restore
              </Button>
            </div>
          )}
          <div>{text}</div>
        </Typography>
      </Grid>
    </>
  );
};
