import React from "react";
import { v4 as uuidv4 } from "uuid";
import { OPENID } from "./App/ApplicationConfig";
import { UserCtx, OpenidUser } from "./App/ApplicationContext";
import { LoginModal } from "./LoginModal";

const loginClickHandler = (): void => {
  // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access,@typescript-eslint/no-unsafe-call
  const state = uuidv4();
  sessionStorage.setItem("openidState", state);
  if (window.location.href.indexOf("/lab") > 0) {
    sessionStorage.setItem(
      "openidRedirectURI",
      window.location.href.slice(0, window.location.href.indexOf("/lab") + 4)
    );
  } else {
    sessionStorage.setItem("openidRedirectURI", window.location.href);
  }
  window.location.href = `${
    OPENID.authorization_endpoint
  }?state=${state}&redirect_uri=${sessionStorage.getItem("openidRedirectURI")}`;
};

const loginRedirect = (setUser: (user: OpenidUser) => void): void => {
  fetch(OPENID.userinfo_endpoint, {
    method: "GET",
    credentials: "include",
  })
    .then((res) => (res.status === 200 ? res.json() : null))
    .then((res: OpenidUser) => {
      if (res.registered) {
        setUser(res);
      }
    })
    .catch((err) => console.error(err));
};

export const Login: React.FC = () => {
  const { user, setUser } = React.useContext(UserCtx);
  const urlParams = new URLSearchParams(window.location.search);
  const code = urlParams.get("code");
  const state = urlParams.get("state");
  // eslint-disable-next-line @typescript-eslint/naming-convention
  const session_state = urlParams.get("session_state");

  React.useEffect(() => {
    loginRedirect(setUser);
  }, []);
  if (
    session_state &&
    code &&
    state === sessionStorage.getItem("openidState")
  ) {
    fetch(OPENID.token_endpoint, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        code,
        state,
        session_state,
        redirect_uri: sessionStorage.getItem("openidRedirectURI"),
      }),
      credentials: "include",
    })
      .then((res) => (res.status === 200 ? res.json() : null))
      .then((res: OpenidUser) => {
        console.log(res);
        urlParams.delete("code");
        urlParams.delete("state");
        urlParams.delete("session_state");
        window.location.search = urlParams.toString();
      })
      .catch((err) => {
        console.error(err);
      });
  }
  if (user) {
    return <div />;
  }
  return <LoginModal loginClickHandler={() => loginClickHandler()} />;
};
