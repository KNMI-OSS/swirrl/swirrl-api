import React from 'react';
import {
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Button,
  Dialog,
} from '@mui/material';
import WarningIcon from '@mui/icons-material/Warning';

type LoginModalProps = {
  loginClickHandler: () => void;
};

const classes = {
  dialogTitle: {
    color: '#fff',
    backgroundColor: '#FF0000',
    fontWeight: 400,
  },
  dialogContent: {
    color: 'var(--jp-ui-font-color1)',
    backgroundColor: 'var(--jp-layout-color1)',
  },
  dialogText: {
    color: 'var(--jp-ui-font-color1)',
  },
  dialogFooter: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: '1px',
    backgroundColor: 'var(--jp-layout-color2)',
  },
  link: {
    padding: '10px 0',
    display: 'block',
  },
  highlight: {
    fontWeight: 800,
  },
  icon: {
    color: 'fff',
    verticalAlign: 'text-bottom',
    paddingRight: '15px',
  },
};

export const LoginModal = ({
  loginClickHandler,
}: LoginModalProps): React.ReactElement => {
  return (
    <Dialog
      open
      aria-labelledby='alert-dialog-title'
      aria-describedby='alert-dialog-description'
      // eslint-disable-next-line @typescript-eslint/no-empty-function
      onClose={() => {}}
    >
      <DialogTitle id='alert-dialog-title' sx={classes.dialogTitle}>
        <WarningIcon sx={classes.icon} />
        You need to log in
      </DialogTitle>
      <DialogContent sx={classes.dialogContent}>
        <DialogContentText
          sx={classes.dialogText}
          id='alert-dialog-description'
        >
          To use all swirrl-api functionalities you need to (re-)login to the
          identity provider.
        </DialogContentText>
      </DialogContent>
      <DialogActions sx={classes.dialogFooter}>
        <Button onClick={loginClickHandler} color='primary'>
          Login
        </Button>
      </DialogActions>
    </Dialog>
  );
};
