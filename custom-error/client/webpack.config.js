/* eslint-disable @typescript-eslint/no-var-requires */
const path = require("path");
const webpack = require("webpack");
const HtmlWebPackPlugin = require("html-webpack-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");

module.exports = {
  entry: "./src/application/index.js",
  cache: false,
  mode: "development",
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: "custom-error.js",
  },
  devtool: "source-map",
  module: {
    rules: [
      {
        test: /\.(ts|tsx)$/,
        use: ["ts-loader"],
      },
      {
        test: /\.(js|jsx)$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: "babel-loader",
        },
      },
    ],
  },
  resolve: {
    extensions: [".ts", ".tsx", ".js", ".jsx", "scss"],
  },
  plugins: [
    new HtmlWebPackPlugin({
      template: "./src/application/index.html",
      filename: "./index.html",
    }),
    new CopyWebpackPlugin({ patterns: [{ from: "src/static" }] }),
    new webpack.HotModuleReplacementPlugin(),
  ],
  devServer: {
    hot: true,
  },
  watchOptions: {
    // ignored: /node_modules/
  },
};
module.loaders = [
  { test: /\.js$/, exclude: /node_modules/, use: "babel-loader" },
];
