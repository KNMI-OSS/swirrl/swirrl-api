#!/bin/bash

currentimage=`grep notebook.docker.image.tag src/main/resources/application.properties | cut -d '=' -f2`
docker pull ${currentimage}
currentdigest=`docker inspect --format='{{.RepoDigests}}' ${currentimage}`

latestimage=`echo ${currentimage} | cut -d ':' -f1`
docker pull ${latestimage}
latestdigest=`docker inspect --format='{{.RepoDigests}}' ${latestimage}`

echo "Current image ${currentimage} has digest: ${currentdigest}"
echo "Latest image ${latestimage} has digest: ${latestdigest}"

if [[ "${currentdigest}" != "${latestdigest}" ]] ; then
  echo "Jupyter image tag needs to be updated."
  exit 1
fi
echo "Jupyter image is still current."

exit 0