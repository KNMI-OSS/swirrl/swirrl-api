#!/bin/bash

## This script doesn't actually work it seems. Restoring a dump
## doesn't actually restore the data, even if no errors are reported.
## Apparently this functionality is only supported on Enterprise
## editions of Neo4J.

database=$1
dumpfile=$2

if [[ -z ${database} || -z ${dumpfile} ]] ; then
  echo "Usage: neo4j-restore.bash database dumpfile"
  echo "  database - Name of the database to restore"
  echo "  dumpfile - Path and filename of the backup file to restore"
  exit 1
fi

echo "Saving neo4j deployment manifest."
kubectl -n swirrl get -o yaml sts neo4j > neo4j-sts.yaml
echo "Removing neo4j StatefulSet"
kubectl -n swirrl delete sts neo4j
kubectl -n swirrl wait --for=delete pod/neo4j-0 --timeout=150s

echo "Restoring ${database} database from ${dumpfile}"
base64 ${dumpfile} > ${dumpfile}.b64
kubectl -n swirrl create configmap --from-file=neo4j.dump.b64=${dumpfile}.b64 neo4j-dumpfile

kubectl -n swirrl run --attach --restart=Never --image=neo4j:4.3.10 \
  --pod-running-timeout=30m \
  --overrides="
    {
      \"kind\":\"Pod\",\"apiVersion\":\"v1\",\"metadata\":{\"name\":\"neo4j-restore\"},
      \"spec\":{
        \"volumes\":[
            {\"name\":\"neo4j-dumpfile\",\"configMap\":{\"name\":\"neo4j-dumpfile\"}},
            {\"name\":\"neo4j-data\",\"persistentVolumeClaim\":{\"claimName\":\"neo4j-data-neo4j-0\"}}],
          \"containers\":[{
            \"name\":\"neo4j-backup\",
            \"image\":\"neo4j:4.3.10\",
            \"command\": [ \"sh\", \"-c\",
              \"base64 -d /backupdata/neo4j.dump.b64 > /tmp/neo4j.dump && /var/lib/neo4j/bin/neo4j-admin load --force --info --database=${database} --from=/tmp/neo4j.dump\" ],
            \"env\": [{
              \"name\": \"NEO4J_AUTH\",
              \"valueFrom\": {
                \"secretKeyRef\":
                  {\"name\": \"neo4j-secret\", \"key\": \"neo4j_driver_string\"}
                }
            }],
            \"volumeMounts\":[
              {\"name\":\"neo4j-dumpfile\",\"mountPath\":\"/backupdata\"},
              {\"name\":\"neo4j-data\",\"mountPath\":\"/var/lib/neo4j/data\"}
            ]
          }]
        }}
  " \
  neo4j-restore
  echo "Restored from ${dumpfile}"

kubectl -n swirrl apply -f neo4j-sts.yaml && rm neo4j-sts.yaml
kubectl -n swirrl rollout restart deployment swirrl-api

kubectl -n swirrl delete pod neo4j-restore
kubectl -n swirrl delete cm neo4j-dumpfile
