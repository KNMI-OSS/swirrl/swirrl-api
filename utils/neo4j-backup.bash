#!/bin/bash

## This script doesn't actually work it seems. A dump file is created
## and it looks alright. However, restoring it using the restore
## command doesn't actually restore the data. Apparently this
## functionality is only supported on Enterprise editions of Neo4J.

timestamp=$(date '+%Y%m%d%H%M%S')
databases="system neo4j"

echo "Saving neo4j deployment manifest."
kubectl -n swirrl get -o yaml sts neo4j > neo4j-sts.yaml
echo "Removing neo4j StatefulSet"
kubectl -n swirrl delete sts neo4j
kubectl -n swirrl wait --for=delete pod/neo4j-0 --timeout=150s

## The stdout of neo4j-admin is redirected to stderr,
## so the database content can be streamed to a local dump file
## using cat from the container and we can store neo4j-admin
## stdout to a local log file.
for database in ${databases} ; do
  dumpfile=neo4j-${database}-${timestamp}.dump
  echo "Dumping ${database} database to ${dumpfile}"
  kubectl -n swirrl run --attach --restart=Never --image=neo4j:4.3.10 \
    --pod-running-timeout=30m \
    --overrides="
      {
        \"kind\":\"Pod\",\"apiVersion\":\"v1\",\"metadata\":{\"name\":\"neo4j-${database}-backup\"},
        \"spec\":{
          \"volumes\":[
              {\"name\":\"neo4j-data\",\"persistentVolumeClaim\":{\"claimName\":\"neo4j-data-neo4j-0\"}}],
            \"containers\":[{
              \"name\":\"neo4j-backup\",
              \"image\":\"neo4j:4.3.10\",
              \"command\": [ \"sh\", \"-c\",
                \"/var/lib/neo4j/bin/neo4j-admin dump --database=${database} --to=/tmp/neo4j-dump 1>&2 && cat /tmp/neo4j-dump\" ],
              \"env\": [{
                \"name\": \"NEO4J_AUTH\",
                \"valueFrom\": {
                  \"secretKeyRef\":
                    {\"name\": \"neo4j-secret\", \"key\": \"neo4j_driver_string\"}
                  }
              }],
              \"volumeMounts\":[
                {\"name\":\"neo4j-data\",\"mountPath\":\"/var/lib/neo4j/data\"}
              ]
            }]
          }}
    " \
    neo4j-${database}-backup > ${dumpfile} 2> ${dumpfile}.log ## stderr is neo4j-admin's stdout
    echo "Dumped database ${database} to ${dumpfile}"
    echo "Logs in ${dumpfile}.log"
    kubectl -n swirrl delete pod neo4j-${database}-backup
done

echo "Restoring neo4j StatefulSet"
kubectl -n swirrl apply -f neo4j-sts.yaml && rm neo4j-sts.yaml

kubectl -n swirrl rollout restart deployment swirrl-api