#!/bin/bash -x
for SPEC in $(ls OpenApiSpecs)
do
  VERSION=$(echo $SPEC | sed -r 's/openapi-(.*).yaml/\1/' | sed -r 's/\./_/')

  docker run \
  -it --rm \
  -v ${PWD}:/local \
  -u $(id -u) \
  openapitools/openapi-generator-cli:v3.1.2 \
  generate -i \
  /local/OpenApiSpecs/$SPEC \
  -g spring \
  -o /local \
  -c /local/openapi-generator/openapi-generator-options.json \
  --model-package org.openapitools.$VERSION.model \
  --api-package org.openapitools.$VERSION.api
done

if git status --short --untracked-files=all | grep -qe '^?? src/main/java/org/openapitools/v1_0/api/ProvenanceApi.java$' ; then
    mv src/main/java/org/openapitools/v1_0/api/ProvenanceApi.java src/main/java/org/openapitools/v1_0/provenance/ProvenanceApi.java
fi

