This folder contains PROVN templates as well as example binding files which can be used to expand the template.

# Templates

## Notebooks, Enlighten, etc...

The templates in this directory describe the provenance of creating
and manipulating notebooks and other activities supported by swirrl-api
except for running workflows.

The [PROV-Template expansion
service](https://gitlab.com/KNMI-OSS/swirrl/provtemplatecatalog/)
imports these during deployment in CI/CD.

## Run workflow

See [swirrl-workflow/prov-templates](https://gitlab.com/KNMI-OSS/swirrl/swirrl-workflow/-/blob/master/prov-templates/README.md).

# Notes:

1. It seems currently impossible to expand vargen variables with user
   specified values when using the v3 json bindings format. This also does
   not work when expanding templates with the ProvToolBox.
1. It seems currently impossible to expand the bundle id with a variable
   via the ProvTemplateCatalog. This does work in when expanding the template
   with the ProvToolBox. As a workaround a vargen variable can be used as
   bundle id, although this again cannot be substituted with a user specified
   value, only with a generated value.
