# EPOS DEMO

## Create a notebook pod
SwaggerUI: POST /notebook
```json
{
  "workspaceId": "epos-demo",
  "libraries": []
}
```
## Connect to empty notebook
connect to http://epos-ics-c-beta.brgm.fr/epos/jupyter-epos-demo

Backup: http://epos-ics-c-beta.brgm.fr/epos/jupyter-epos-demo-create

## Create Jupyter notebook 
***Creating a Jupyter notebook could take ages (>20m)! 
For demo might be better using a second pod with running, empty, Jupyter notebook. Creating a second notebook is almost instantainious. 
Or use SwaggerUI to do a put notebook without libaries. This will restart the pod whereafter starting a Jupyter notebook is quick.***

upload GeoPandas-demo.ipynb
click on this file to start a Jupyter notebook

Backup: http://epos-ics-c-beta.brgm.fr/epos/jupyter-epos-demo-notebook

## Attempt to run Jupyter notebook
run first code-cell with imports. --> Raises ModuleNotFoundError

## Import Libraries
### SwaggerUI:
1. PUT /notebook
2. workspaceId: epos-demo
3. RequestBody:

```json
{
  "libraries": [
    {
      "libname": "geopandas",
      "libversion": ""
    },
    {
      "libname": "matplotlib",
      "libversion": ""
    },
    {
      "libname": "pandas",
      "libversion": ""
    },
    {
      "libname": "pyproj",
      "libversion": ""
    },
    {
      "libname": "shapely",
      "libversion": ""
    },
    {
      "libname": "descartes",
      "libversion": "1.0.2"
    }
  ]
}
```

Backup: http://epos-ics-c-beta.brgm.fr/epos/jupyter-epos-demo-libs

## Second attempt to run Jupyter notebook
Again run first code-cell with imports. --> No output, but no error either!
Run second code-cell (! head ....) --> cannot open 'data/current/uk_earthquakes.csv'

## Upload files
### SwaggerUI:
1. POST Workflow
2. workflowName: download
3. RequestBody:
```
{
  "workspaceId": "epos-demo",
  "inputs": "bmFtZTogJ0RlbW8gaW5wdXQnCm1lc3NhZ2U6ICdFUE9TIGRlbW8gMjAxOCwgT2N0JwoKbGlua3M6
CiAgLSB1cmw6ICdodHRwczovL3Jhdy5naXRodWJ1c2VyY29udGVudC5jb20vQnJpdGlzaEdlb2xv
Z2ljYWxTdXJ2ZXkvZ2VvcGFuZGFzLWRlbW8vbWFzdGVyL3VrX2VhcnRocXVha2VzLmNzdicKICAg
IGZpbGVuYW1lOiB1a19lYXJ0aHF1YWtlcy5jc3YKICAtIHVybDogJ2h0dHBzOi8vczMtZXUtd2Vz
dC0xLmFtYXpvbmF3cy5jb20vZXVuYWRpY3MtczN0ZXN0L25lXzEwbV91ay5ncGtnJwogICAgZmls
ZW5hbWU6IG5lXzEwbV91ay5ncGtnCgpmaWxlb3V0OiAnZGVtbycK"
}
```
> name: 'Demo input'
>message: 'EPOS demo 2018, Oct'
>
>links:
>  - url: 'https://raw.githubusercontent.com/BritishGeologicalSurvey/geopandas-demo/master/uk_earthquakes.csv'
>    filename: uk_earthquakes.csv
>  - url: 'https://s3-eu-west-1.amazonaws.com/eunadics-s3test/ne_10m_uk.gpkg'
>    filename: ne_10m_uk.gpkg
>
>fileout: 'demo'

Show files in /data/backups and /data/current


Backup: http://epos-ics-c-beta.brgm.fr/epos/jupyter-epos-demo-upload

## Third attempt to run Jupyter notebook
Run second code-cell again and admire the output.
Run the entire notebook: Cell --> Run All

***Note:
We still encounter an error: ModuleNotFoundError. This because the Ubuntu libspatioalindex-c4v5 package isn't present.
So still todo: implement an option to install OS packages.***


