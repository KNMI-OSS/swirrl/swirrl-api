{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Which UK town feels the most earthquakes?\n",
    "\n",
    "> The short answer is Manchester tied with Fort William.  To see the top 20, skip on to [the results section](#Top-20-UK-towns-for-earthquake-activity).  Otherwise, read on to learn about Pandas, GeoPandas and how you can do GIS with Python without needing a desktop GIS software (e.g. QGIS, ESRI ArcMap).\n",
    "\n",
    "This [Jupyter notebook](http://jupyter-notebook-beginner-guide.readthedocs.io/en/latest/what_is_jupyter.html#notebook-document) demonstrates how the GeoPandas library can be used to perform GIS analysis using just Python code.  In this case, we calculate the UK town that feels the most earthquakes.  It is a simple example using real-world data from the British Geological Survey's [earthquake catalogue](http://earthquakes.bgs.ac.uk/earthquakes/dataSearch.html).  To run this for yourself and experiment with changing variables, follow with instructions in the [README.md](README.md) file.\n",
    "\n",
    "The notebook has three parts:\n",
    "\n",
    "1. Introduction to [Pandas](https://pandas.pydata.org/).\n",
    "2. Introduction to [GeoPandas](http://geopandas.org/).\n",
    "3. Example analysis."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import geopandas as gpd\n",
    "import matplotlib.pyplot as plt\n",
    "import pandas as pd\n",
    "import pyproj\n",
    "import os\n",
    "from shapely.geometry import Point, Polygon\n",
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 1. Introduction to Pandas\n",
    "\n",
    "Pandas adds _dataframe_ structures to Python and has been central to the rise of Python in the field of data science.  This section shows some the features of Pandas for handling tables of data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# View the structure of the input data\n",
    "! head data/latest/uk_earthquakes.csv"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Load data"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Load into Pandas dataframe, specifying that two columns represent date and time\n",
    "earthquakes = pd.read_csv('data/latest/uk_earthquakes.csv',\n",
    "    skipinitialspace=True,\n",
    "    usecols=['yyyy-mm-dd', 'hh:mm:ss.ss', 'lat', 'lon', 'depth', 'ML', 'locality'],\n",
    "    parse_dates=[['yyyy-mm-dd', 'hh:mm:ss.ss']], infer_datetime_format=True)\n",
    "\n",
    "# Fix datetime column name and typo in locality name\n",
    "earthquakes.rename(columns={'yyyy-mm-dd_hh:mm:ss.ss': 'datetime'}, inplace=True)\n",
    "earthquakes.set_index('datetime', inplace=True)\n",
    "\n",
    "# Check data within data frame\n",
    "earthquakes.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Explore the data\n",
    "\n",
    "The following analysis uses plain Pandas, without real geospatial analysis.\n",
    "\n",
    "#### Range of magnitudes\n",
    "\n",
    "`describe` function gives summary statistics on columns"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(earthquakes[['ML', 'depth', 'lat']].describe())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`hist` function plots histograms"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ax = earthquakes['depth'].hist(bins=25, edgecolor='white')\n",
    "txt = ax.set_title('Histogram of earthquake depth')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "All of the earthquakes are less than 30 km deep, which is about the thickness of the Earth's crust.  Shallower rocks are more brittle and more likely to produce earthquakes."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Ten largest quakes since 1700\n",
    "\n",
    "`sort_values` sorts values.  The clue is in the name."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "earthquakes.sort_values('ML', ascending=False).head(10)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The UK is not a very seismically active area.  These earthquakes are certainly large enough to be felt and to cause damage, but across the whole planet there are at least [1,000 earthquakes of this size each year](https://earthquake.usgs.gov/earthquakes/browse/stats.php).  The British Geological Survey has a page on [Earthquake magnitude calculations](http://www.bgs.ac.uk/discoveringGeology/hazards/earthquakes/magnitudeScaleCalculations.html)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Variation in rate of earthquakes with time\n",
    "\n",
    "Pandas has excellent time-series processing capabilities.  The `resample` function groups data into annual bins.\n",
    "\n",
    "The number of earthquakes measured each year doesn't just depend on how often they occur; it also depends on how well they can be detected."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "quakes_per_year = earthquakes.resample('1Y').size()\n",
    "big_quakes_per_year = earthquakes[earthquakes['ML'] >= 3].resample('10Y').size()\n",
    "\n",
    "fig, (ax1, ax2) = plt.subplots(2, 1, sharex=True)\n",
    "quakes_per_year.plot(ax=ax1, label='Earthquakes per year', marker='x')\n",
    "ax1.legend()\n",
    "ax1.set_ylabel('count')\n",
    "big_quakes_per_year.plot(ax=ax2, label='Earthquakes >= M3 per year', marker='x')\n",
    "ax2.legend()\n",
    "txt = ax2.set_ylabel('count')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The big rise in recorded earthquakes around 1970 reflects the establishment a modern seismic monitoring network.  Older earthquake data come from isolated seismometers or have been inferred from reports of shaking and damage."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Plot x, y data\n",
    "\n",
    "The `plot.scatter` function is good for x,y data.  In this case, there is no awareness that the data are spatial.  Depth vs Magnitude can be plotted in the same way as Longitude vs Latitude."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ax = earthquakes.plot.scatter('depth', 'ML', alpha=0.5)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ax = earthquakes.plot.scatter('lon', 'lat', c='ML', alpha=0.5)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2. Introduction to Geopandas\n",
    "\n",
    "GeoPandas extends Pandas to allow spatial operations on geometry objects.  It uses the same spatial libraries as other open source GIS tools e.g. QGIS and PostGIS, so the results are the same as the analysis had been done in desktop GIS.  Common GIS tasks are shown here."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Load UK regions from GIS file\n",
    "\n",
    "Geopandas uses [Fiona](http://toblerity.github.io/fiona), which in turn uses [GDAL/OGR](http://gdal.org), which means that it can read and write\n",
    "most GIS formats.  Here, data are read from a [GeoPackage](http://geopackage.org) file.  These are the new standard in open-source GIS and contain multiple layers.\n",
    "\n",
    "First load UK administrative regions and populated places based on [NaturalEarth](http://www.naturalearthdata.com) data. This is a public domain dataset of physical and political GIS data.  It is useful for adding context to maps.  The attribute tables are accessible as Pandas-style dataframes."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Load data form GIS file\n",
    "regions = gpd.read_file('./data/latest/ne_10m_uk.gpkg', layer='admin_1_states_provinces')\n",
    "towns = gpd.read_file('./data/latest/ne_10m_uk.gpkg', layer='populated_places')\n",
    "\n",
    "# View attribute data for first 5 rows\n",
    "regions.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Spatial data are stored in the `geometry` column."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "regions.geometry.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `plot` function plots the map.  Features can be coloured based on column data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ax = regions.plot(figsize=(8, 8), column='name')\n",
    "towns.plot(ax=ax, color='black')\n",
    "txt = ax.set_title('UK regions and town')\n",
    "\n",
    "# Draw arrow to highlight Rockall, which is too small to\n",
    "# have been drawn on the map (see next section).\n",
    "plt.annotate('Rockall', xy=(-13.687, 57.596), xytext=(-13, 58),\n",
    "             arrowprops=dict(width=0.5, headwidth=4, headlength=5))\n",
    "\n",
    "## Note that you can easily save plots to file in a variety of formats e.g.\n",
    "# plt.savefig('map.pdf')\n",
    "# plt.savefig('map.png', dpi=300)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Perform geometric operations e.g. intersections\n",
    "\n",
    "The map above is shifted to the east because of the tiny island of [Rockall](https://en.wikipedia.org/wiki/Rockall).  We can trim this off using the `intersection` method."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "area_of_interest = Polygon([(-9, 49), (-9, 62), (2, 62), (2, 49)])\n",
    "regions['geometry'] = regions.intersection(area_of_interest)\n",
    "ax = regions.plot(figsize=(8, 8), column='name')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Perform spatial operations based on attribute data\n",
    "\n",
    "We can select individual polygons based on their attributes, then manipulate their geometries.  Vector data can be `dissolved`, which is the spatial equivalent of `group_by`.  The _geonunit_ attribute column contains country name.  Here we combine all region polygons belonging to the same country into a single polygon."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "countries = regions[['geonunit', 'geometry']].dissolve(by='geonunit')\n",
    "countries['country'] = countries.index.values\n",
    "ax = countries.plot(column='country')\n",
    "\n",
    "# GeoDataframes have extra attributes based on their geometry\n",
    "# e.g. centroids\n",
    "print(countries.centroid)\n",
    "countries.centroid.plot(ax=ax, marker='*', color='yellow')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Re-project data to Ordnance Survey coordinate system\n",
    "\n",
    "Coordinate reference systems can be specified using [EPSG codes](http://spatialreference.org/ref/epsg/).  `epsg:4326` is longitude and latitude as returned by GPS systems (using WGS84 datum).  `epsg:27700` is the Ordnance Survey for Great Britain.  GeoPandas provides the `to_crs` method to set this."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "countries_osgb = countries.to_crs({'init': 'epsg:27700'})\n",
    "towns_osgb = towns.to_crs({'init': 'epsg:27700'})"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Working in projected coordinates allows spatial calculations e.g. compare areas of countries of the UK in square kilometres..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "1e-6 * countries_osgb.area"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "... or in the UK media's favourite measure of area comparison, the _sizeofwales_."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "countries_osgb.area / countries_osgb.loc['Wales', 'geometry'].area"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Export to file\n",
    "\n",
    "`to_file` writes data to any vector file format supported by OGR.  Which is most of them.  Here we write our new `countries` dataframe as a Shapefile."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "output_file_name = os.path.join('.', 'countries.shp')\n",
    "countries.to_file(output_file_name)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 3. Example analysis\n",
    "\n",
    "Here we find the most earthquake-prone town in the UK (or, to be precise, the regionally-important UK town with most earthquakes with magnitude > 3 within a 40 km radius since 1970)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Convert existing DataFrame into GeoDataFrame\n",
    "\n",
    "To do spatial operations, it is necessary to convert the `lon` and `lat` columns of our earthquakes dataframe into geometry objects.  GeoPandas uses the [Shapely](https://github.com/Toblerity/Shapely) library for this.  Our coordinates will become a `Point`.  We also have to set the coordinate reference system."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Add geometry and convert to spatial dataframe in source CRS\n",
    "earthquakes['coords'] = list(zip(earthquakes['lon'], earthquakes['lat']))\n",
    "earthquakes['coords'] = earthquakes['coords'].apply(Point)\n",
    "earthquakes = gpd.GeoDataFrame(earthquakes, geometry='coords', crs={'init': 'epsg:4326'})\n",
    "\n",
    "# Reproject data in to Ordnance Survey GB coordinates\n",
    "earthquakes_osgb = earthquakes.to_crs({'init': 'epsg:27700'})"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Earlier we saw that the earthquake catalogue was only complete from 1970 onwards so we only want records from then onwards.  We also take only earthquakes above magnitude 3 as these are the most likely to be felt."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Extract larger quakes from when monitoring network was established\n",
    "big_quakes_osgb = earthquakes_osgb[(earthquakes_osgb['ML'] >= 3)]['1970-01-01':]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Plot earthquakes against UK outline\n",
    "\n",
    "Data can now be plotted against the reference maps loaded from the Geopackage file. The largest earthquakes since 1970 have been highlighted on this map."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create the axis first\n",
    "fig, ax = plt.subplots(figsize=(8, 8))\n",
    "\n",
    "# Plot all earthquakes\n",
    "earthquakes_osgb.sort_values('ML', ascending=True).plot(\n",
    "    ax=ax, column='ML', legend=True, alpha=0.5)\n",
    "# Add country outlines\n",
    "countries_osgb.plot(ax=ax, edgecolor='black', facecolor='none')\n",
    "\n",
    "# Highlight the biggest earthquakes\n",
    "big_quakes_osgb.plot(\n",
    "    ax=ax, color='red', marker='.', markersize=10)\n",
    "\n",
    "# Make figure aspect ratio ensure that 10x10 km square appears square\n",
    "ax.set_aspect('equal', 'box')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The earthquakes are not evenly spread.  They are mainly associated with recent movement on ancient faults.  Many are in the North Sea.  The crust there was stretched during the Jurassic-Triassic period and produced the basins where oil-bearing rocks are found today.  Earthquakes in the NW Highlands of Scotland are near the Great Glen Fault and Moine Thrust fault lines.  These were formed during even older continental collisions."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Find earthquakes within 40 km of towns"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It is reasonable that a magnitude 3 earthquake will be felt 40 km away.  We can define 40 km circles around each town using the `buffer` function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "towns_buffer = towns_osgb[['NAME', 'geometry']].copy()\n",
    "towns_buffer['geometry'] = towns_osgb.buffer(40000)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To find which earthquakes could be felt in each town, we do a [spatial join](http://geopandas.org/mergingdata.html#spatial-joins)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Note that spatial joins requires rtree python package and libspatialindex-c4v5 Debian/Ubuntu package\n",
    "quakes_by_town = gpd.sjoin(big_quakes_osgb, towns_buffer, op='within', how='left')\n",
    "quakes_by_town.dropna(subset=['NAME'], inplace=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Plotting the earthquakes coloured by the `NAME` of the towns that they are close to shows which ones fall within the buffers around the towns."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots(figsize=(8, 8))\n",
    "countries_osgb.plot(ax=ax, edgecolor='brown', facecolor='none', alpha=0.2)\n",
    "towns_buffer.plot(ax=ax, color='green', alpha=0.2)\n",
    "quakes_by_town.plot(ax=ax, column='NAME', figsize=(12, 12), alpha=0.8, label='Near towns')\n",
    "big_quakes_osgb.plot(ax=ax, color='black', edgecolor='none', alpha=0.8, marker='.',\n",
    "                     label='Big quakes')\n",
    "ax.set_aspect('equal', 'box')\n",
    "leg = ax.legend()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Top 20 UK towns for earthquake activity"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The map above shows which earthquakes are close to towns.  Applying the Pandas `value_counts` function to the quakes by town shows the Top 20.\n",
    "\n",
    "And the winner is: a draw!  Fort William ties with Manchester with 9 earthquakes each within a 40 km radius with magnitude greater than 3."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "quakes_by_town['NAME'].value_counts().head(20)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Conclusion\n",
    "\n",
    "This is a very simplistic analysis, but it demonstrates how easily such workflows can be carried out using Python.  Reading, analysis and plotting take very few lines of code, and the code is self-documenting, which is important for transparency and reproducibility.\n",
    "\n",
    "It is also very easy to repeat and adjust the analysis.  For example, 1970 to the present is a very short time period.  It is probably not representative of longer term patterns.  As an exercise, try running the notebook again using all earthquakes since 1700.  The results may surprise you!  See [here](https://onlinelibrary.wiley.com/doi/epdf/10.1111/j.1365-3121.1993.tb00288.x) for explanation."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 4. Notes\n",
    "\n",
    "#### Geospatial analysis as a web service\n",
    "\n",
    "Automated analyses can be incorporated into a functions.  The example below\n",
    "lists earthquakes within a certain radius of a given point.  This could be configured to return output in JSON format output and wrapped in a Python web framework such as [Falcon](http://falconframework.org).  The result would be a geospatial web service that performs automatic GIS-type analysis in response to HTTP requests."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def get_nearby_earthquakes(lon, lat, radius=20000):\n",
    "    \"\"\"\n",
    "    Return records from the earthquake catalogue within a given \n",
    "    distance of a point location.\n",
    "    \n",
    "    :param lon: Longitude in decimal degrees\n",
    "    :param lat: Latitude in decimal degress\n",
    "    :param buffer: Search radius in metres\n",
    "    :return quakes: Pipe-separated representation of earthquake data\n",
    "    \"\"\"\n",
    "    # Create dataframe with buffered point\n",
    "    osgb = pyproj.Proj('+init=epsg:27700')\n",
    "    x, y = osgb(lon, lat)\n",
    "    search_zone = gpd.GeoDataFrame(geometry=[Point(x, y).buffer(radius)],\n",
    "                                   crs={'init': 'epsg:27700'})\n",
    "    \n",
    "    # Do spatial join\n",
    "    quakes = gpd.sjoin(big_quakes_osgb, search_zone, how='inner')\n",
    "    \n",
    "    # Tidy output\n",
    "    quakes.index.name = 'timestamp'\n",
    "    quakes.reset_index(inplace=True)\n",
    "    quakes = quakes[['timestamp', 'lat', 'lon', 'depth', 'ML', 'locality']]\n",
    "    \n",
    "    return quakes.to_csv(index=False, sep='|', float_format='%.4f')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# The coordinates are for BGS HQ in Keyworth\n",
    "result = get_nearby_earthquakes(-1.089, 52.871)\n",
    "print(result)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Data cleaning\n",
    "\n",
    "The CSV file downloaded from the website needed the following tweaks before it was ready for use:\n",
    "\n",
    "+ Add quotes around _locality_ field `cat raw.csv | sed 's/,/,\"/9;s/$/\"/' > uk_earthquakes.csv`\n",
    "+ Manually remove space from _locality_ field name\n",
    "+ Manually remove data before 1700 because Pandas can't parse those timestamps automatically\n",
    "+ Manually fix typo in timestamp (1971-11-21, 07:68:36.2)\n",
    "\n",
    "#### Acknowledgements\n",
    "\n",
    "This notebook was inspired by Joris Van den Bossche's [geopandas-tutorial](https://github.com/jorisvandenbossche/geopandas-tutorial), which he gave at the [GeoPython 2018](http://2018.geopython.net/) conference."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
