import requests, os
from kubernetes import client, config
from datetime import datetime, timedelta
from dateutil.tz import tzutc

namespace = os.environ['SWIRRL_API_NAMESPACE']
api_host = os.environ['SWIRRL_INTERNAL_ORIGIN']

try:
    config.load_incluster_config()
except:
    config.load_kube_config()
core = client.CoreV1Api()
apps = client.AppsV1Api()
batch = client.BatchV1Api()

service_type_map = {
    "jupyter" : "notebook",
    "enlighten" : "viewer/enlighten",
    "adaguc" : "viewer/adaguc"
}

def cleanup_sessions():
    '''
    Delete all sessions that don't have a pod or a public snapshot repo associated with them.
    The snapshot repo needs to be public since a private repo gives a 404 just like a non-existing repo.
    When a deployment is being restored it will temporarily not exist, but it will spawn a placeholder
    configmap during that time, so we check for that also.
    '''
    pvclist = core.list_namespaced_persistent_volume_claim(namespace, watch=False)
    to_delete = []
    for pvc in pvclist.items:
        if 'session-id' in pvc.metadata.labels and 'data-directory' in pvc.metadata.name:
            session_id = pvc.metadata.labels['session-id']
            pods = core.list_namespaced_pod(namespace, label_selector=f'session-id={session_id}', watch=False).items
            configmaps = core.list_namespaced_config_map(namespace, label_selector=f'session-id={session_id}', watch=False).items
            restore_placeholder = next((x for x in configmaps if 'restore-placeholder' in x.metadata.name or 'cleanup-placeholder' in x.metadata.name), None)
            try:
                activities = requests.get(os.path.join(api_host, 'swirrl-api/v1.0/provenance/session', session_id, 'activities')).json()['@graph']
            except Exception as e:
                print(f'Could not parse list of activities or no activities returned from API for session {session_id}. {e}')
                continue

            has_snapshot = False
            for activity in activities:
                if 'swirrl:CreateSnapshot' in activity['@type']:
                    try:
                        full_activity = requests.get(os.path.join(api_host, 'swirrl-api/v1.0/provenance/activity', activity['@id'])).json()
                        repo_status = requests.get(full_activity['prov:generated']['prov:atLocation']).status_code
                        if repo_status != 404:
                            has_snapshot = True
                            break
                    except Exception:
                        pass
            if not pods and not has_snapshot and not restore_placeholder:
                to_delete.append(session_id)

    deleted = []
    for session_id in to_delete:
        delete_status = requests.delete(os.path.join(api_host, 'swirrl-api/v1.0/session', session_id)).status_code
        if delete_status == 200:
            deleted.append(session_id)
        else:
            print(f'Something went wrong deleting session {session_id}, with status code {delete_status}')

    print('Deleted sessions:')
    print(deleted)

def cleanup_jobs():
    '''
    Since the ttlSecondsAfterFinished directive doesn't work in anything but the newest alpha version of k8s,
    we need to manually delete jobs after they are done. Unfortunately the ttlSecondsAfterFinished directive
    doesn't even seem to be added to the job spec, so we can't even use the configured values there in this job.
    Therefore we simply delete every job after it has been done for a day.
    '''
    joblist = batch.list_namespaced_job(namespace, watch=False).items
    to_delete = []
    for job in joblist:
        if not job.status.active:
            last_transition_time = datetime(2000, 1, 1, 0, 0, 0, 000000, tzinfo=tzutc())
            for condition in job.status.conditions:
                if condition.last_transition_time > last_transition_time:
                    last_transition_time = condition.last_transition_time
            if datetime.now(tz=tzutc()) > last_transition_time + timedelta(days=1):
                to_delete.append(job.metadata.name)

    deleted = []
    for job_name in to_delete:
        delete_status = batch.delete_namespaced_job_with_http_info(job_name, namespace)[1]
        if delete_status == 200:
            deleted.append(job_name)
            # In some development environments deleting the job did not delete associated pods, so we manually delete any pods that may remain
            job_podlist = core.list_namespaced_pod(namespace, label_selector=f'job-name={job_name}', watch=False).items
            for pod in job_podlist:
                pod_delete_status = core.delete_namespaced_pod_with_http_info(pod.metadata.name, namespace)[1]
                if pod_delete_status != 200:
                    print(f'Something went wrong deleting pod {pod.metadata.name}, with status code {pod_delete_status}')
        else:
            print(f'Something went wrong deleting job {job_name}, with status code {delete_status}')

    print('Deleted jobs:')
    print(deleted)

def cleanup_completed_pods():
    # Gather lists of pods services and configmaps. We only gather these lists once, so even though
    # we delete stuff from k8s that is contained in these lists the lists themselves remain static for
    # the rest of this method.
    pods = core.list_namespaced_pod(namespace, label_selector="type=jupyter", watch=False)
    services = core.list_namespaced_service(namespace, label_selector="type=jupyter", watch=False)
    configmaps = core.list_namespaced_config_map(namespace, label_selector="type=cleanup-placeholder", watch=False)

    # If the essential process in the notebook exited, then the pod will be in Failed/Succeeded status.
    # We delete these pods and create a cleanup placeholder for these serives so that they can still be restored for up to 8 weeks.
    for pod in pods.items:
        status = pod.status.phase
        if status == "Failed" or status == "Succeeded":
            print(f"Notebook {pod.metadata.name} has exited. Deleting.")
            core.delete_namespaced_pod_with_http_info(pod.metadata.name, namespace)
            core.create_namespaced_config_map_with_http_info(namespace, {'metadata': {'name': f'cleanup-placeholder-{pod.metadata.labels["service-id"]}', 'labels':
                {'type': 'cleanup-placeholder', 'service-id': pod.metadata.labels['service-id'], 'pool-id': pod.metadata.labels['pool-id'], 'session-id': pod.metadata.labels['session-id']}}})

    # Sometimes the notebook pod was deleted already (for example during an infra update), we then still need to create the placeholder configmap.
    for service in services.items:
        if not next((pod for pod in pods.items if pod.metadata.labels['service-id'] == service.metadata.labels['service-id']), None) and \
           not next((configmap for configmap in configmaps.items if configmap.metadata.labels['service-id'] == service.metadata.labels['service-id']), None):
            print(f"Service {service.metadata.name}, does not have associated pod any more. Marking for deletion.")
            core.create_namespaced_config_map_with_http_info(namespace, {'metadata': {'name': f'cleanup-placeholder-{service.metadata.labels["service-id"]}', 'labels':
                {'type': 'cleanup-placeholder', 'service-id': service.metadata.labels['service-id'], 'pool-id': service.metadata.labels['pool-id'], 'session-id': service.metadata.labels['session-id']}}})

    # If there is a placeholder configmap but the pod is restored we delete the placeholder configmap.
    # If there is a placeholder configmap, no pod and 8 weeks have passed, we delete the rest of the service through swirrl api.
    # This deletes all associated services/ingress paths and configmaps (including the placeholder configmap).
    for configmap in configmaps.items:
        found_pods = core.list_namespaced_pod(namespace, label_selector=f"service-id={configmap.metadata.labels['service-id']}", watch=False)
        if found_pods.items:
            print(f"Notebook was restored so removing mark for deletion for service {configmap.metadata.labels['service-id']}")
            core.delete_namespaced_config_map_with_http_info(configmap.metadata.name, namespace)
            continue
        if datetime.now(tz=tzutc()) - configmap.metadata.creation_timestamp > timedelta(weeks=8):
            print(f"Notebook {configmap.metadata.labels['service-id']} older than 8 weeks: Calling delete notebook endpoint for {configmap.metadata.labels['service-id']}")
            requests.delete(os.path.join(api_host, 'swirrl-api/v1.0/notebook',
                                            configmap.metadata.labels['service-id']))


if __name__ == '__main__':
    cleanup_jobs()
    cleanup_completed_pods()
    cleanup_sessions()
