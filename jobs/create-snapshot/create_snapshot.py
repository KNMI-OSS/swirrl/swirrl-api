import github
import sys, argparse, os, requests, json, shutil, os.path
from ruamel.yaml import YAML
from sh import git
import time

def main(args):

    try:
        ## Before we create a repository, make sure the serviceid is a valid
        ## jupyter notebook id.
        nb_url = '/'.join((args.api_url, args.serviceid))
        nb_response = requests.get(nb_url)
        if nb_response.status_code != 200:
            raise Exception("%s: %s: %s" % (nb_url, nb_response.status_code, nb_response.reason))

        gh = github.Github(login_or_token=args.token)
        repo = create_github_repo(gh, args.reponame, args.description)

        repodir = create_local_git_repo(repo, args.token)
        copy_notebook_to_git_repo(repodir, maxfilesize=50e6) ## Sets file size limit to 50MByte.
        shutil.copy("/usr/src/swirrl-api/postBuild", "%s/postBuild" % repodir)
        create_download_script(repodir, nb_response, args.api_url)
        create_environment_yaml(nb_response, repodir)
        create_dockerfile(args.notebook_image, repodir)
        push_snapshot(repodir, args.description)

        print(repo.html_url)
    except github.GithubException as ghe:
        print("github.com returned %d: %s" % (ghe.status, ghe.data["errors"][0]["message"]), file=sys.stderr)
        return 2
    except Exception as e:
        print(e, type(e), file=sys.stderr)
        print("Could not create repository for snapshot or could not obtain notebook data. "
              "Check that a notebook service with id %s exists." % args.serviceid, file=sys.stderr)
        return 1
    return 0


def create_download_script(repodir, notebook_info, api_url):
    sessionid = json.loads(notebook_info.text)["sessionId"]
    serviceid = json.loads(notebook_info.text)["id"]
    notebook_url = json.loads(notebook_info.text)["serviceURL"]

    shutil.copy("/usr/src/swirrl-api/download_data.sh", "%s/download_data.sh" % repodir)
    with open("%s/download_data.sh" % repodir, "a") as script:
        data_topdir = "data/staginghistory"
        
        fileinfo = None
        script.write("NOTEBOOK_URL=%s\n" % notebook_url)
        script.write("SERVICE_ID=%s\n" % serviceid)
        script.write("get_notebook %s %s %s\n" % (api_url, sessionid, serviceid))

        if os.path.exists("/data-dir/staginghistory"):
            os.chdir("/data-dir/staginghistory")
            dirs = os.listdir("./")
            dirs.sort(reverse=True)
            latest = dirs[0]

            # 1. Start at latest stage and create download urls for every file in it.
            # 2. Go to the previous stage.
            #   a. If file is "unchanged" in later stages' swirrl_fileinfo.json: create hard link.
            #   b. If file is "updated" in later stages' swirrl_fileinfo.json: create download url.
            for stage in dirs:
                script.write("mkdir -p ~/%s/%s\n" % (data_topdir, stage))
                with open("%s/swirrl_fileinfo.json" % stage, "r") as fileinfo_fp:
                    for datafile in os.listdir(stage):
                        dlcmd = ("download ${NOTEBOOK_URL}/api/contents/%s/%s/%s ~/%s/%s/%s\n" %
                                 (data_topdir, stage, datafile, data_topdir, stage, datafile))
                        if stage == latest:
                            script.write(dlcmd)
                        elif datafile == "swirrl_fileinfo.json":
                            script.write(dlcmd)
                        else:
                            # Links file to copy in newer (higher number) stage.
                            linkcmd = ("ln %s/%s ~/%s/%s/%s\n" %
                                       (fileinfo["stagePath"], datafile, data_topdir, stage, datafile))
                            for fi_entry in fileinfo["files"]:
                                if fi_entry["filename"] == datafile:
                                    if fi_entry["state"] == "unchanged":
                                        script.write(linkcmd)
                                    if fi_entry["state"] == "updated":
                                        script.write(dlcmd)
                    ## fileinfo alwasys points to the previous stage. So set
                    ## it here to the current stage, so the next execution of
                    ## the loop it will contain the previous stage's info.
                    fileinfo = json.load(fileinfo_fp)
            script.write("ln -s ~/%s/%s ~/data/latest\n" % (data_topdir, latest))
        else:
            script.write("echo 'Nothing to download from data staging directory.'\n")
        script.write("download_working_dir_data ${NOTEBOOK_URL}/api/contents\n")
        script.write("delete_notebook %s\n" % (api_url))

def copy_notebook_to_git_repo(repodir, maxfilesize):
    os.makedirs(os.path.join(repodir, "data"), exist_ok=True)
    shutil.copytree("/work-dir", repodir, symlinks=True, dirs_exist_ok=True, ignore=do_not_copy(repodir, maxfilesize))
    # shutil.copytree("/data-dir", os.path.join(repodir, "data"), symlinks=True, dirs_exist_ok=True, ignore=do_not_copy(repodir, maxfilesize))


def do_not_copy(repodir, maxfilesize):
    def _do_not_copy(path, names):
        do_not_copy_list = []
        download_list = []
        for filename in names:
            filepath = os.path.join(path, filename)
            if os.path.isfile(filepath) and "private/" in filepath:
                do_not_copy_list.append(filename)
                continue
            if os.path.isfile(filepath) and os.path.getsize(filepath) > maxfilesize:
                do_not_copy_list.append(filename)
                download_list.append("%s\n" % os.path.split(filepath)[1:])
            if ".cache" in filepath or "jupyterswirrlui" in filepath:
                do_not_copy_list.append(filename)
        if len(download_list) > 0:
            with open(os.path.join(repodir, "download_filelist.dat"), "a") as dfl:
                dfl.writelines(download_list)
        return do_not_copy_list
    return _do_not_copy


def create_dockerfile(imagetag, repodir):
    swirrldir = "swirrl-docker"
    os.makedirs(os.path.join(repodir, swirrldir), exist_ok=True)
    os.chdir(os.path.join(repodir, swirrldir))
    with open("Dockerfile", "w") as df:
        df.write("FROM %s\n" % imagetag)
        df.write("USER root\n")
        df.write("ARG NB_USER=jovyan\n"
                 "ARG NB_UID=1000\n"
                 "ENV USER ${NB_USER}\n"
                 "ENV NB_UID ${NB_UID}\n"
                 "ENV HOME /home/${NB_USER}\n")
        df.write("RUN id ${NB_USER} || adduser --disabled-password --gecos \"Default user\" --uid ${NB_UID} ${NB_USER}\n")
        df.write("COPY . ${HOME}\n"
                 "RUN chown -R ${NB_UID} ${HOME}\n")
        df.write("USER ${NB_USER}\n")
        df.write("WORKDIR ${HOME}\n")


def create_github_repo(github, reponame, description):
    user = github.get_user()
    repo = user.create_repo(reponame, description)
    return repo


def create_local_git_repo(repo, token):
    count = 0
    max_tries = 3
    while True:
        try:
            git.clone(repo.clone_url.replace("//github.com/", "//%s@github.com/" % token))
            break
        except Exception as e:
            count += 1
            if count >= max_tries:
                raise e
            time.sleep(5)
    repodirname = os.path.splitext(os.path.split(repo.clone_url)[-1])[0]
    curdir = os.getcwd()
    return os.path.join(curdir, repodirname)


def push_snapshot(repodir, description):
    os.chdir(repodir)
    git.config("user.email", "swirrl-api@knmi.nl")
    git.config("user.name", "SWIRRL-API")
    git.add(".")
    git.commit("-m", description)
    git.push()


def create_environment_yaml(notebook_info, repodir):
    nb_json = json.loads(notebook_info.text)
    libs_json = nb_json["installedLibraries"]

    yaml = YAML()
    env_dict = {"name":"base",
                "channels":["conda-forge", "defaults"],
                "prefix":"/opt/conda",
                "dependencies":[]
                }
    pip_list = []
    for lib_obj in libs_json:
        if lib_obj["libname"] != "swirrlui":
            ## swirrlui extension shouldn't be snapshotted
            if lib_obj["source"] == "pypi":
                ## mybinder.org doesn't tolerate the "-pypi_0" in the version.
                pip_list.append("==".join((lib_obj["libname"], lib_obj["libversion"].split("-")[0])))
            else:
                env_dict["dependencies"].append("=".join((lib_obj["libname"], lib_obj["libversion"].replace('-', '='))))
    env_dict["dependencies"].append({"pip":pip_list})
    with open(os.path.join(repodir, "environment.yml"), "w") as envfile:
        yaml.indent(offset=2)
        yaml.dump(env_dict, envfile)


def parse_arguments():
    token = None
    if "GITHUB_API_KEY" in os.environ:
        token = os.environ["GITHUB_API_KEY"]
    if "GITHUB_USERID" in os.environ:
        github_userid = os.environ["GITHUB_USERID"]
    description = ""
    serviceid = ""
    api_url = ""
    notebook_image = "jupyter/minimal-notebook"
    parser = argparse.ArgumentParser(description="Creates a snapshot in GitHub.",
                                     formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('-t', '--token', dest='token', default=token, type=str, nargs='?', required=False,
                        help="Authentication token/API key.")
    parser.add_argument('-r', "--repo", dest='reponame', nargs='?', type=str, required=True,
                        help="Name of the repository to create.")
    parser.add_argument('-d', "--description", dest='description', default=description, type=str, nargs='?',
                        required=False, help="Description of the repository.")
    parser.add_argument('-i', '--serviceid', dest='serviceid', default=serviceid, type=str, nargs='?',
                        required=True, help="ID of the service that we're creating a snapshot of.")
    parser.add_argument('-a', '--api-url', dest='api_url', default=api_url, type=str, nargs='?',
                         required=True, help="Base URL of the notebook.")
    parser.add_argument('-n', '--notebook-image', dest='notebook_image', default=notebook_image, type=str, nargs='?',
                        required=False, help="Tag of the docker image of the original notebook.")

    return parser.parse_args()


if __name__ == "__main__":
    rc = main(parse_arguments())
    sys.exit(rc)
