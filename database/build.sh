#!/bin/bash

## Build and optionally push the neo4j docker image. We need to build neo4j image
## because we require the neosemantics plugin. The mongo image on hub.docker.com
## can be used "as-is" and so we don't need to keep our own copy of it.

if [[ ! -z $1 ]] && [[ $1 = "push" ]] ; then
    push="yes"
fi

if [ ! -z "${CI_REGISTRY_IMAGE}" ] ; then
  neo4jtag=${CI_REGISTRY_IMAGE}/neo4j-swirrl:latest
else
  neo4jtag=neo4j-swirrl:latest
fi

cd neo4j
docker build -t ${neo4jtag} .
if [ "${push}" = "yes" ] ; then
    docker push ${neo4jtag}
fi
cd -
