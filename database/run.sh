#!/bin/bash

## Start the neo4j and/or the mongodb container locally for development
## purposes.

container=''
up="no"
stop="no"
down="no"
restart="no"
cmd=""
databases=()

function usage {
    [ -z "$1" ] || echo $1
    echo "Usage: run.sh [-h] [-u] [-s] [-d] [-r] -c database_1 -c database_2 ..."
    echo ""
    echo "  containername : Start the databases specified in the file(s)"
    echo "                  docker-compose<database>.yml. The files need to"
    echo "                  be in the current directory."
    echo "Commands:"
    echo "  -u            : Bring the databases up."
    echo "  -s            : Stop the databases while retaining the data."
    echo "  -d            : Take the databases down and clean the data"
    echo "  -r            : (re)start the databases with the saved data from stop."
    echo "  -h            : This help message."
    exit 1
}

while getopts husdrc: option ; do
    case ${option} in
        (c) databases+=(${OPTARG})
            ;;
        (u) [ -z "${cmd}" ] || usage "Only one command can be given at a time."
            cmd="up -d --build"
            ;;
        (s) [ -z "${cmd}" ] || usage "Only one command can be given at a time."
            cmd="stop"
            ;;
        (d) [ -z "${cmd}" ] || usage "Only one command can be given at a time."
            cmd="down --remove-orphans"
            ;;
        (r) [ -z "${cmd}" ] || usage "Only one command can be given at a time."
            cmd="restart"
            ;;
        (h) usage
            ;;
        (*) usage
            ;;
    esac
done

[ ! -z "${cmd}" ] || usage

for database in ${databases[@]} ; do
    if [[ ${database} = "Neo" ]] && [[ ${cmd} = "up -d" ]]; then
        [ -d ~/Rdf ] || mkdir ~/Rdf
    fi
    docker-compose -f docker-compose${database}.yml ${cmd}
done
