# Neo4j

### Kubernetes deployment

See the manifest in the deploy directory below the parent directory of this directory.

### Running as a standalong docker container

There is not much point to this as swirrl-api can no longer be run from outside kubernetes. Anyway,
some of the things below might explain a few things in the kubernetes manifest, most notably why
we need to share a volume mount with the swirrl-api pod.

- Access on localhost:7474
- Driver on localhost:7687
- Authorization needs to be set with environment variables, see next section.
- Stores a temporary file on a local path. The file wil be removed after closing the api.

The Neo4j database needs the Neosemantics plugin, so we need to maintain a custom image with this plugin enabled.
Neosemantics enables the RDF import/export from Neo4j. This is used to send the prov-n document to Neo4j.

### Set the environment for authentication

```shell script
export NEO4J_USER=neo4j
export NEO4J_PASSWORD=notneo4j ## Cannot be 'neo4j, or you will get an error from neo4j.
```
Make sure you start the swirrl-api server with the same environment settings. The way
this works is if the `NEO4J_AUTH` environment variable is set to a `username/password`
value, the docker container will be initialized with this username and password configured.
 
### Build and run for local development:

Advised is to pull the docker container from the gitlab registry, but if you need to
build it locally there is a legacy script that hasn't been tested in a while.

The staging directory for neo4j imports is $HOME/Rdf. The commands below will create it automatically if needed:
```
cd database
./build.sh 
```

If needed, you can push the image to the gitlab registry with `build.sh push`. After you have done this,
you do not need to build the image any more unless there is an update or something. If you aren't yet
authenticated to the gitlab registry then do this with `docker login registry.gitlab.com` before pusing.

Start the neo4j database:
```
cd database
./run.sh -u -c Neo
```

The database should now be reachable at [http://localhost:7474/browser/](http://localhost:7474/browser/)

#### Further commands:
Type `./run.sh -h` for more instructions on how to use this script.

To **stop** the database without losing the contents: `./run.sh -s -c Neo`

If you want to **restart** it do: `./run.sh -r -c Neo`

To bring the database down and **remove** the content: `./run.sh -d -c Neo`

### Alternative: Running the docker by hand
You need to start the container on the host network so the swirrl-api can find the
database on localhost:

```shell script
docker run --rm --network="host" -p 7474:7474 -p 7687:7687 -e NEO4J_AUTH=none \
-v $HOME/Rdf:/Rdf:ro --name neo4j neo4jknmi 
```

## MongoDB (outdated)
- Mongo-express on localhost:8081
- Driver on localhost:27017
- Authorization:
    - User: root
    - Password: example

### For local development:

Start up 
```
cd database
./run.sh -u -c Mongo
```
See the instructions for Neo4j to `stop`, `restart` and `shutdown` the database.
