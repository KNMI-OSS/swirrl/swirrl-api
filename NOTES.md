# Spring-Boot with Java 9
https://github.com/spring-projects/spring-boot/wiki/Spring-Boot-with-Java-9-and-above

<dependency>
    <groupId>javax.xml.bind</groupId>
    <artifactId>jaxb-api</artifactId>
</dependency>

# Spring-Boot devtools
https://www.quora.com/How-can-I-reload-my-changes-on-Spring-Boot-without-having-to-restart-my-server

<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-devtools</artifactId>
    <optional>true</optional>
</dependency>